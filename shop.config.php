<?php

	// Emu Shop Configuration
	///////////////////////////////////////////////////////////////////////////////////////////
	$emuShop->productImageSizes['one'] = array( 85, 77, true );
	$emuShop->productImageSizes['three'] = array( 193, 123, true );
	$emuShop->productImageSizes['six'] = array( 474, 315, true );
	$emuShop->productImageSizes['seven'] = array( 250, 250, true );
	///////////////////////////////////////////////////////////////////////////////////////////
	
	$emuShop->setupWPMenu();

?>
