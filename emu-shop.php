<?php
/*
Plugin Name: Emu Shop
Plugin URI: http://www.emuwebware.com
Description: Emu wants to shop
Version: 1.0
Author: Emu
Author URI: http://www.emuwebware.com
test: this is the doug branch
*/

add_action( 'emu_framework_loaded', 'loadEmuShop', 1 );

function loadEmuShop()
{
	include_once( 'class/_main.class.php' );

    global $emuShop;

    $emuShop = new emuShop( __FILE__ );

    include_once( 'shop.config.php' );

}


?>
