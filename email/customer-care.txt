<?php

/*
Emu Email: Customer Care

Email From: [WP admin email]
Email To: [WP admin email]
Email CC:
Email BCC:
Email Subject: Customer Care Request
Email Type: html
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>
<body>

<p>Customer Care Request:</p>

<table cellspacing="2" cellpadding="2">
<tr>
	<th valign="top" align="left">Date Received</th>
	<td>[date]</td>
</tr>
<tr>
	<th valign="top" align="left">Question/Comment Regarding</th>
	<td>[questions]</td>
</tr>
<tr>
	<th valign="top" align="left">Question/Comment</th>
	<td>[comment]</td>
</tr>
<tr>
	<th valign="top" align="left">First Name</th>
	<td>[first name]</td>
</tr>
<tr>
	<th valign="top" align="left">Last Name</th>
	<td>[last name]</td>
</tr>
<tr>
	<th valign="top" align="left">Email</th>
	<td>[email]</td>
</tr>
<tr>
	<th valign="top" align="left">Address</th>
	<td>[address aggregate comma]</td>
</tr>
<tr>
	<th valign="top" align="left">Phone</th>
	<td>[phone number]</td>
</tr>
</table>

<p>[WP site title]<br />[WP tagline]</p>

</body>
</html>

