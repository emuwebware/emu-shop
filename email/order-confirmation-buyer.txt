<?php

/*
Emu Email: Order Confirmation to Buyer

Email From: [WP admin email]
Email To: [customer email]
Email CC: 
Email BCC:
Email Subject: Order for [customer first name] [customer last name] received [order date]
Email Type: html
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>
<body>

<p>Dear [customer first name] [customer last name],</p>

<p>Your order placed on [order date] has been received - a copy of your order is below:</p>

[order summary]

<p>[WP site title]<br />[WP tagline]</p>

</body>
</html>

