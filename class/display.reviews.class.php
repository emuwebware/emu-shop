<?php

class emuReviewsDisplay extends emuDisplay
{
	public $terms = array();

	public function __construct()
	{
        $this->templateFile = 'customer-reviews.htm';
        parent::__construct();

        if( $username = request_val('customer') )
			$this->setUsername( $username );
	}

	public function setUsername( $username )
	{
		$this->terms['username'] = $username;
	}

	public function build()
	{
		global $emuShop;

		$customer_reviews = $emuShop->customerManager->getCustomerReviews( $this->terms );

		if( count( $customer_reviews ) == 0 )
		{
			$this->content = 'No reviews found';
			return;
		}

		$basket = $emuShop->getBasket();

		$tM = $this->templateManager;

		// Use the product display to generate the product markup
		$product_display = $emuShop->getInstance( 'emuProductDisplay' );

		$template = $this->template;

		// Reviews item template
		$review_item_template = $tM->getTemplateRepeat( 'review', $template );

		$reviews = '';

		if( !$review_votes = $basket->getSessionData('review-votes') ) $review_votes = array();

		foreach( $customer_reviews as $review )
		{
			if( $review->post->post_status != 'publish' ) continue;

			$review->getComments();

			$comments = '';

			// build the comments list
			if( count( $review->comments ) > 0 )
			{
				$comments .= '<ul class="review-comments">';
				for( $n = 0; $n < count( $review->comments); $n++ )
				{
					$comment = $review->comments[$n];
					$comments .= '<li><em>&quot;'.$comment->comment_content.'&quot;</em><span class="comment-details"><strong> - '.$comment->comment_author.'</strong>, '.apply_date_format('standard-with-time-alternative', $comment->comment_date).'</span></li>';
				}
				$comments .= '</ul>';
			}

			$product = $emuShop->getInstance( 'emuProduct', array( null, $review->productPostID ) );

			$show_voting_form = in_array( $review->dbID, $review_votes ) ? false : true;

			$review_item = $tM->fillTemplate( $review_item_template, $review->getTemplateTags(), $review, $review->ID );
			$review_item = $tM->fillTemplate( $review_item, array( 'comments' => $comments ) );
			$review_item = $tM->fillTemplate( $review_item, $product->getTemplateTags(), $product, $product->postID );
			$review_item = $tM->setTemplateConditionals( $review_item, array( 'review voting form' => $show_voting_form ) );

			$review_item = $tM->setTemplateConditionals( $review_item, array( 'quantity dropdown' => count( $product->variants ) == 0 ) );

			$product_display->template = $review_item;
			$product_display->post = $product->getPost();
			$product_display->build();

			$review_item = $product_display->content;

			$reviews .= $review_item;
		}

		$review_header = '';

		if( count( $this->terms ) > 0 )
		{
			// We're showing reviews for a specific customer
			$customer = $customer_reviews[0]->customer;

			$review_header = "Reviews written by <span>{$customer->username}</span>";
		}

		$template = $tM->fillTemplateRepeats( $template, array( 'review' => $reviews ) );
		$template = $tM->fillTemplate( $template, array( 'review header' => $review_header, 'product messages' => $emuShop->getMessages( 'product' ) ) );

		$this->content = apply_filters( 'emu_reviews_content', $template );
	}
}

?>
