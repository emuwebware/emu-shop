<?php

class emuCouponProcessor extends emuProcessor
{
	public $requiredFields = array('coupon_code');

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();

		switch( $this->button )
		{
			case 'Apply Code':

				if( !$this->hasRequiredFields )
				{
					$this->messages[] = 'Not all required fields were provided - check those marked with *';
					$this->error = true;
					$emuShop->addMessage( 'coupon', $this->messages, 'error' );
					return;
				}

				extract( $_POST );

				$coupon_code = post_val( 'coupon_code' );

				$coupon = $emuShop->getInstance( 'emuCoupon', array(null, $coupon_code) );

				if( $coupon->isValid() )
				{
					$basket->setSessionData( 'coupon_code', $coupon_code );
					$this->messages[] = 'Coupon code applied';
				}
				else
				{
					$this->messages = $coupon->messages;
					$this->error = true;
				}

				$emuShop->addMessage( 'coupon', $this->messages, $this->error ? 'error' : 'notice' );

				break;

			case 'Remove Code':

				$basket->setSessionData( 'coupon_code', null );


		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		// $location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $_SERVER[ 'HTTP_REFERER' ] );

		// header( 'Location: '.$location );
		// exit();

	}
}

?>
