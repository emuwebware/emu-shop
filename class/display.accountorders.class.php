<?php



class emuAccountOrdersDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'account-orders.htm';

        parent::__construct();

	}

	public function build()
	{
		global $wpdb;
		global $emuShop;

		if( !is_user_logged_in() )
		{
			$this->content = 'You must be logged in to view this page';
			return;
		}

		$customer = $emuShop->getCustomer();

		$template = $this->template;

		$order_repeat = $this->templateManager->getTemplateRepeat( 'order row', $template );

		if( !$customer_orders = $customer->hasOrders() )
		{
			$order_items = 'You do not currently have any orders.';
		}
		else
		{
			$order_items = '';

			foreach( $customer_orders as $order )
				$order_items .= $this->templateManager->fillTemplate( $order_repeat, $order->getTemplateTags(), $order );
		}

		$template = $this->templateManager->fillTemplateRepeats( $template, array( 'order row' => $order_items ) );
		$template = $this->templateManager->fillTemplate( $template, $customer->getTemplateTags(), $customer );

		$this->content = apply_filters( 'emu_account_orders', $template );
	}
}

?>
