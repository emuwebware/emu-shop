<?php

class emuM_Orders extends emuManager
{
	public $pages;

    public function init()
    {
		// add the various meta boxes (products page mainly)
		add_action( 'admin_menu', array( $this, 'registerMetaBoxes' ) );

		add_action( 'save_post', array( $this, 'wpSaveOrderDetails' ) );
		add_action( 'delete_post', array( $this, 'wpRemoveOrder' ) , 10, 2);

		add_filter( 'manage_edit-customer-order_columns', array( $this, 'addOrderColumns' ) );
		add_action( 'manage_posts_custom_column', array( $this, 'manageCustomColumns' ), 10, 2 );
    }

	public function registerClasses()
	{
		global $emuShop;

		$emuShop->registerClass( 'emuOrder', 'model.order.class.php' );
		$emuShop->registerClass( 'emuOrderItem', 'model.orderitem.class.php' );
	}

	function addOrderColumns( $columns )
	{
		$columns = array();

		$columns['cb'] = '<input type="checkbox" />';
		$columns['title'] = _x('Order', 'column name');
		// $columns['e-o-print'] = 'Print';
		$columns['date'] = _x('Date', 'column name');
		$columns['e-o-status'] = 'Order Status';
		$columns['e-o-customer'] = 'Customer';
		$columns['e-o-shipping'] = 'Shipping Address';
		$columns['e-o-total'] = 'Order Total';

		return $columns;
	}

	function manageCustomColumns( $column_name, $id )
	{

		global $wpdb, $emuShop;

		if( in_array( $column_name, array( 'e-o-status', 'e-o-customer', 'e-o-shipping', 'e-o-total' ) ) )
		{
			if( !isset( $GLOBALS["order-$id"] ) )
			{
				$order = $emuShop->getInstance( 'emuOrder', array( null, $id ) );
				$GLOBALS["order-$id"] = $order;
			}
			else
				$order = $GLOBALS["order-$id"];
		}

		switch ( $column_name )
		{
			case 'e-o-status':

				echo $order->getOrderStatusDescription();

				break;

			case 'e-o-customer':

                echo $order->customerFirstName.' '.$order->customerLastName. ' ('.$order->customerEmail.')';

                if( !$order->customerID ) echo ' <em>via guest checkout</em>';

				break;

			case 'e-o-shipping':

                if( $shipping_address = $order->getShippingAddress() )
                    echo $shipping_address->getAddressAggregate(', ');

				break;

			case 'e-o-total':

				echo $order->getTotal();
				break;

		}


	}

	function getOrderStatusOptions()
	{
		$order_status = array( ORDER_STATUS_RECEIVED => 'Received', ORDER_STATUS_PROCESSING => 'Processing', ORDER_STATUS_SHIPPED => 'Shipped' );

        return apply_filters( 'emu_shop_order_status_options', $order_status);
	}

	function wpRemoveOrder( $post_id )
	{
        global $emuShop;

        $post = get_post( $post_id );

        if( $post->post_type !== 'customer-order' ) return $post_id;

        $order = $emuShop->getInstance( 'emuOrder', array(null, $post_id) );
        $order->deleteRecord();
	}

	function wpSaveOrderDetails( $post_id )
	{
		if( ! post_val( 'e-action' ) == 'order-admin' ) return $post_id;

		global $wpdb, $emuShop;

		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;

		if( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) return $post_id;

		// Make sure we're doing a proper save (this function seems to be called when the page loads as well)
		if(! post_val( 'save-form' ) ) return $post_id;

		if( $order_status = post_val( 'order_status' ) )
		{
			$order_status = $order_status == 'null' ? 'null' : "'$order_status'";

			$sql = "update {$emuShop->dbPrefix}orders set orderStatus = $order_status where postID = $post_id";
			$wpdb->query( $sql );
		}
	}


	function registerMetaBoxes()
	{
		add_meta_box( 'emu-order-details', 'Order Status', array( $this, 'wpGetOrderDetails' ), 'customer-order', 'normal', 'high' );
	}

	function wpGetOrderDetails ( $post )
	{
		global $wpdb, $emuShop;

		$order = $wpdb->get_row( "select orderDetails, orderStatus from {$emuShop->dbPrefix}orders where postID = {$post->ID}" );

		/*
		$order =  unserialize( base64_decode( $order ) );

		print_r($order);
		*/

		?>
		<div class="emu-e">

			<div class="e-status-options">

				<table>
				<tr>
					<th>Order Status</th>
					<td><?php echo drop_down( '', 'order_status', '', $order->orderStatus, $this->getOrderStatusOptions() )?></td>
				</tr>
				</table>
				<input type="hidden" name="save-form" value="true" />
				<input type="hidden" name="e-action" value="order-admin" />
			</div>

		</div>
		<!-- / emu-e -->
		<?php
	}

	public function registerCustomPostTypes()
	{
		global $emuShop;

		$labels = array(
			'name' => 'Orders',
			'singular_name' => 'Order',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Order',
			'edit_item' => 'Edit Order',
			'new_item' => 'New Order',
			'view_item' => 'View Order',
			'search_items' => 'Search Orders',
			'not_found' => 'No orders found',
			'not_found_in_trash' => 'No orders found in Trash',
			'parent_item_colon' => ''
		);

		$args = array(
			'labels' => $labels,
			'public' => false,
			'publicly_queryable' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array('slug' => 'order'),
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 4,
			'supports' => array( 'title', 'excerpt', 'editor', 'author', 'custom-fields', 'revisions' ),
		);

		register_post_type( 'customer-order', $args );
		register_taxonomy( 'ocat', 'customer-order' , array( 'label' => 'Order Categories', 'show_ui' => true, 'hierarchical' => true, 'rewrite' => array( 'slug' => 'order-category' ), 'query_var' => true ) );
	}

    public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $sql = "CREATE TABLE {$emuShop->dbPrefix}orders (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        customerID int(10) NULL,
        customerEmail varchar(300) default NULL,
        customerFirstName varchar(300) default NULL,
        customerLastName varchar(300) default NULL,
        postID int(10) NULL,
        userID int(10) NULL,
        chargesTotal float(2) default NULL,
        paymentsTotal float(2) default NULL,
        discountTotal float(2) default NULL,
        itemsTotal float(2) default NULL,
        grandTotal float(2) default NULL,
        subTotal float(2) default NULL,
        deliveryOption varchar(300) default NULL,
        shippingAddress text default NULL,
        billingAddress text default NULL,
        charges text default NULL,
        payments text default NULL,
        discounts text default NULL,
        dateCreated DATETIME default NULL,
        dateUpdated DATETIME default NULL,
        orderDetails text default NULL,
        comments text default NULL,
        couponID int(10) NULL,
        orderStatus varchar(30) default NULL,
        orderRefNumber varchar(30) default NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

        // orders
        $sql = "CREATE TABLE {$emuShop->dbPrefix}order_items (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        orderID int(10) NULL,
        productID int(10) NULL,
        description varchar(300) default NULL,
        qty int(10) NULL,
        price float(2) default NULL,
        total float(2) default NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);
    }

}


?>
