<?php

class emuM_Taxes extends emuManager
{
	public function registerAdminPages()
	{
		global $emuShop;

		$emuShop->registerAdminPage( array( 'name' => 'Taxes',
									'filename' => 'taxes.php',
									'position' => 4,
									'styles' => array( 'emu-common' ),
									'scripts' => array() ) );
	}

	public function registerModels()
	{
		global $emuShop;

		$emuShop->registerModel( 'emuTaxRate', 'model.taxrate.class.php' );
	}

	public function getTaxRates()
	{
		global $wpdb, $emuShop;

		// loop through all the countries
		$sql = "select dbID from {$emuShop->dbPrefix}tax_rates";

		$result_set = $wpdb->get_results($sql);

		$tax_rates = array();

		foreach( $result_set as $tax_set )
			$tax_rates[] = $emuShop->getModel('emuTaxRate', $tax_set->dbID);

		return $tax_rates;
	}

	function clearTaxRates()
	{
		global $wpdb, $emuShop;
		return $wpdb->query( "delete from {$emuShop->dbPrefix}tax_rates" );
	}

	public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$sql = "CREATE TABLE {$emuShop->dbPrefix}tax_rates (
		dbID int(10) NOT NULL AUTO_INCREMENT,
		countryID int(10) NULL,
		stateID int(10) NULL,
		rate float(2) default NULL,
		UNIQUE KEY id (dbID)
		);";

		dbDelta($sql);
    }

	// public function getTaxes( $order, $current_user )
	// {
	// 	global $emuShop;

	// 	// tax is based on the delivery location
	// 	$country = esc_attr( get_the_author_meta( 's_country', $current_user->ID ) );

	// 	if( count( $emuShop->getManager('region')->getStates( array( 'country' => $country ) ) ) > 0 )
	// 		$state = esc_attr( get_the_author_meta( 's_selected_state', $current_user->ID ) );
	// 	else
	// 		$state = esc_attr( get_the_author_meta( 's_entered_state', $current_user->ID ) );

	// 	$tax_rates = $this->getTaxRates();

	// 	$applicable_taxes = array();

	// 	foreach( $tax_rates as $rate )
	// 	{
	// 		if( $rate->country == $country || !$rate->country )
	// 		{
	// 			if( $rate->state == $state || !$rate->state )
	// 				$applicable_taxes[] = $rate;
	// 		}
	// 	}

	// 	return (object) $applicable_taxes;

	// }

	// public function setTaxRate($terms = array())
	// {
	// 	global $wpdb, $emuShop;

	// 	// only set a rate if we have one!
	// 	if( !isset( $terms['rate'] ) ) return false;

	// 	if( isset( $terms['country'] ) )
	// 	{
	// 		// set a country tax
	// 		return $wpdb->query( "update {$emuShop->dbPrefixShared}countries set tax = {$terms['rate']} where ID = {$terms['country']}" );
	// 	}

	// 	if( isset( $terms['state'] ) )
	// 	{
	// 		// set a state tax
	// 		return $wpdb->query( "update {$emuShop->dbPrefixShared}states set tax = {$terms['rate']} where ID = {$terms['state']}" );
	// 	}

	// 	return false;

	// }

	// public function getTaxRate($terms = array())
	// {
	// 	global $emuShop;

	// 	if(isset($terms['state']))
	// 	{
	// 		$state = $emuShop->getManager('region')->getStates( array( 'ID' => $terms['state'] ) );

	// 		if( $state[0]->tax )
	// 			return (object) array ( 'rate' => $state[0]->tax, 'type' => 'state', 'name' => $state[0]->name, 'resultSet' => $state[0] );
	// 		else
	// 			// the country may have the tax rate (i.e. it's an 'all states' tax rate)
	// 			$terms['country'] = $state[0]->country_id;
	// 	}

	// 	if(isset($terms['country']))
	// 	{
	// 		$country = $emuShop->getCountries( array( 'ID' => $terms['country'] ) );

	// 		if( $country->tax ) return (object) array ( 'rate' => $country[0]->tax, 'type' => 'country', 'name' => $country[0]->name, 'resultSet' => $country[0] );
	// 	}
	// }

	// public function getTaxRates()
	// {
	// 	global $wpdb, $emuShop;

	// 	// loop through all the countries
	// 	$sql = "select
	// 			  state.ID as 'stateID',
	// 			  state.name as 'stateName',
	// 			  state.active as 'stateActive',
	// 			  state.tax as 'stateTax',
	// 			  country.ID as 'countryID',
	// 			  country.name as 'countryName',
	// 			  country.active as 'countryActive',
	// 			  country.tax as 'countryTax'
	// 			from
	// 			  {$emuShop->dbPrefixShared}countries country
	// 			left outer join {$emuShop->dbPrefixShared}states state on ( state.country_id = country.ID and state.tax is not null )
	// 			where
	// 			  country.tax is not null or
	// 			  state.tax is not null";

	// 	$result_set = $wpdb->get_results($sql);

	// 	$tax_exceptions = array();

	// 	foreach( $result_set as $tax_set )
	// 	{
	// 		$rate = $tax_set->stateTax ? $tax_set->stateTax : $tax_set->countryTax;
	// 		$tax_exceptions[] = (object) array( "country" => $tax_set->countryID, "countryName" => $tax_set->countryName, "state" => $tax_set->stateID, "stateName" => $tax_set->stateName, "rate" => $rate );
	// 	}

	// 	return $tax_exceptions;

	// }


	// function clearTaxRates()
	// {
	// 	global $wpdb, $emuShop;

	// 	if( $wpdb->query( "update {$emuShop->dbPrefixShared}countries set tax = null" ) &&	$wpdb->query( "update {$emuShop->dbPrefixShared}states set tax = null" ) )
	// 		return true;
	// 	else
	// 		return false;

	// }




}


?>
