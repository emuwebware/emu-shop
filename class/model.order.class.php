<?php


class emuOrder extends emuTemplatedDbPostEntity
{
    public $items;
    public $array_charges = array();
    public $array_discounts = array();
    public $array_payments = array();

    public $shippingAddressObject;
    public $billingAddressObject;

    public $customer;

    public function __construct( $dbID = null, $post = null )
    {
        global $emuShop;

        $this->postType = 'customer-order';
        $this->taxonomy = 'ocat';

        $emuShop->loadModel('emuAddress');

        parent::__construct( $dbID, $post, $emuShop->dbPrefix, 'orders' );
    }


    public function registerTemplateTags()
    {
        $this->templateTags = array( 'order id' => 'dbID',
                                     'order comments' => array( 'this', 'getComments', null ),
                                     'customer first name' => 'customerFirstName',
                                     'customer last name' => 'customerLastName',
                                     'customer email' => 'customerEmail',
                                     'order reference number' => array( 'this', 'getRef', null ),
                                     'order date' => array( 'this', 'getDate', ORDER_DATE_RECEIVED_FORMATTED ),
                                     'order sub total' => array( 'this', 'getTotal', ORDER_SUB_TOTAL_FORMATTED ),
                                     'order items total' => array( 'this', 'getTotal', ORDER_ITEMS_TOTAL_FORMATTED ),
                                     'order discount total' => array( 'this', 'getTotal', ORDER_DISCOUNT_TOTAL_FORMATTED ),
                                     'order charges total' => array( 'this', 'getTotal', ORDER_CHARGES_TOTAL_FORMATTED ),
                                     'order delivery option' => 'deliveryOption',
                                     'order status' => array('this', 'getOrderStatusDescription', null),
                                     'order shipping address' => 'shippingAddressFormatted',
                                     'order billing address' => 'billingAddressFormatted',
                                     'order payments total' => array( 'this', 'getTotal', ORDER_PAYMENTS_TOTAL_FORMATTED ),
                                     'order grand total' => array( 'this', 'getTotal', ORDER_GRAND_TOTAL_FORMATTED ),
                                     'order amount due' => array( 'this', 'getTotal', ORDER_AMOUNT_DUE_FORMATTED ) );
    }

    public function __get( $member )
    {
        global $emuShop;

        if( $member_value = parent::__get( $member ) ) return $member_value;

        switch( $member )
        {
            case 'shippingAddressFormatted':

                if( $shipping_address = $this->getShippingAddress() )
                    return $shipping_address->getAddressAggregate(', ');

                return '';

            case 'billingAddressFormatted':

                if( $billing_address = $this->getBillingAddress() )
                    return $billing_address->getAddressAggregate(', ');

                return '';

            default:

                if( !isset( $this->data[ $member ] ) ) return null;
                return $this->data[ $member ];
        }

    }

    public function setBillingAddress( $address )
    {
        $this->billingAddressObject = $address;
    }

    public function setShippingAddress( $address )
    {
        $this->shippingAddressObject = $address;
    }

    public function getShippingAddress()
    {
        return $this->shippingAddressObject;
    }

    public function getBillingAddress()
    {
        return $this->billingAddressObject;
    }

    public function prepareFromBasket( $basket )
    {
        global $emuShop;

        // Set the shipping address
        if( $shipping_address = $basket->getShippingAddress() )
            $this->setShippingAddress( $shipping_address );

        // Set the billing address
        if( $billing_address = $basket->getBillingAddress() )
            $this->setBillingAddress( $billing_address );

        // Set the customer details
        $this->setCustomerDetailsFromAddress( $billing_address );

        if( $customer = $basket->getCustomer() )
        {
            $this->customer = $customer;
            $this->customerID = $customer->dbID;
        }

        foreach( $basket->getItems() as $basket_item )
        {
            $order_item = $emuShop->getModel('emuOrderItem');
            $order_item->prepareFromBasketItem( $basket_item );

            $this->addItem( $order_item );
        }

        // Coupon
        if( $coupon = $basket->hasCouponDiscount() )
            $this->addDiscount( 'Coupon '.$coupon->code, $basket->getTotal(BASKET_DISCOUNT_TOTAL) );

        // Special Discounts
        if( $special_discounts = $basket->getSpecialDiscounts() )
        {
            foreach( $special_discounts as $discount )
                $this->addDiscount( $discount->description, $discount->getDiscountValue() );
        }

        // Shipping costs
        if( $shipping_charge = $basket->hasShippingCost() )
        {
            $this->addCharge( $shipping_charge->description, $basket->getTotal(BASKET_SHIPPING_TOTAL) );
            $this->setDeliveryOption( $shipping_charge->description );
        }

        // Handling costs
        if( $handling_charge = $basket->hasHandlingCost() )
            $this->addCharge( 'Handling', $basket->getTotal(BASKET_HANDLING_TOTAL) );

        $this->comments = $basket->getSessionData('order_comments');

        // Taxes
        if( $taxes = $basket->hasTaxes() )
        {
            foreach( $taxes as $tax )
                $this->addCharge( $tax->description, $tax->getCalculatedValue() );
        }

        // Special charges
        if( $special_charges = $basket->getSpecialCharges() )
        {
            foreach( $special_charges as $charge )
                $this->addCharge( $charge->description, $charge->getChargeValue() );
        }

    }

    public function getComments()
    {
        if( !$this->comments )
            return '- No Comments -';

        return $this->comments;
    }

    public function getItems()
    {
        global $emuShop, $wpdb;

        if( !$this->items && !$this->isNewOrder() )
        {
            $this->items = array();

            $sql = "select dbID from {$emuShop->dbPrefix}order_items where orderID = ".$this->getID();

            foreach( $wpdb->get_results($sql) as $result )
                $this->items[] = $emuShop->getModel('emuOrderItem', $result->dbID);
        }

        return $this->items;

    }

    public function setDeliveryOption($delivery_option)
    {
        $this->deliveryOption = $delivery_option;
    }

    public function getDeliveryOption()
    {
        return $this->deliveryOption;
    }

    public function getDiscounts()
    {
        return $this->array_discounts;
    }

    public function getPayments()
    {
        return $this->array_payments;
    }

    public function getCharges()
    {
        return $this->array_charges;
    }

    public function addCharge( $description, $value )
    {
        $this->array_charges[$description] = $value;
    }

    public function addDiscount( $description, $value )
    {
        $this->array_discounts[$description] = $value;
    }

    public function addPayment( $description, $value )
    {
        $this->array_payments[$description] = $value;
    }

    public function addItem( $order_item )
    {
        if( !$this->items ) $this->items = array();

        $this->items[] = $order_item;
    }

    public function getTotal($format = null)
    {
        global $emuShop;

        switch( $format )
        {
            case ORDER_SUB_TOTAL:
                return $this->getSubTotal();

            case ORDER_SUB_TOTAL_FORMATTED:
                return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_SUB_TOTAL) ));

            case ORDER_ITEMS_TOTAL:
                return $this->getItemsTotal();

            case ORDER_ITEMS_TOTAL_FORMATTED:
                return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_ITEMS_TOTAL) ));

            case ORDER_DISCOUNT_TOTAL:
                return $this->getDiscountTotal();

            case ORDER_DISCOUNT_TOTAL_FORMATTED:
                return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_DISCOUNT_TOTAL) ));

            case ORDER_CHARGES_TOTAL:
                return $this->getChargesTotal();

            case ORDER_CHARGES_TOTAL_FORMATTED:
                return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_CHARGES_TOTAL) ));

            case ORDER_PAYMENTS_TOTAL:
                return $this->getPaymentsTotal();

            case ORDER_PAYMENTS_TOTAL_FORMATTED:
                return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_PAYMENTS_TOTAL) ));

            case ORDER_AMOUNT_DUE:
                return $this->getAmountDue();

            case ORDER_AMOUNT_DUE_FORMATTED:
                return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_AMOUNT_DUE) ));

            case ORDER_GRAND_TOTAL:
                return $this->getGrandTotal();

            case ORDER_GRAND_TOTAL_FORMATTED:
            default:
                return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_GRAND_TOTAL) ));
        }
    }

    public function getDate($format)
    {
        global $emuShop;

        switch( $format )
        {
            case ORDER_DATE_RECEIVED:
                return $this->dateCreated;

            case ORDER_DATE_RECEIVED_FORMATTED:
            default:
                return apply_date_format( 'standard-with-time', $this->getDate(ORDER_DATE_RECEIVED) );
        }
    }

    public function getGrandTotalValue()
    {
        return $this->getTotal(ORDER_GRAND_TOTAL);
    }

    public function formatEmpty($number)
    {
        global $emuShop;

        if( trim( $number ) == $emuShop->currency ) return '-';

        return $number;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;
        $this->customerID = $customer->dbID;
        $this->customerEmail = $customer->email;
        $this->customerFirstName = $customer->firstName;
        $this->customerLastName = $customer->lastName;
    }

    public function setCustomerDetailsFromAddress($address)
    {
        $this->customerEmail = $address->email;
        $this->customerFirstName = $address->firstName;
        $this->customerLastName = $address->lastName;
    }

    public function getAmountDue()
    {
        return $this->getTotal(ORDER_GRAND_TOTAL) - $this->getTotal(ORDER_PAYMENTS_TOTAL);
    }

    public function getPaymentsTotal()
    {
        return array_sum( $this->array_payments );
    }

    public function getItemsTotal()
    {
        $item_total = 0;

        foreach( $this->getItems() as $item )
            $item_total += $item->getTotalValue();

        return $item_total;
    }

    public function getGrandTotal()
    {
        return $this->getTotal(ORDER_SUB_TOTAL) + $this->getTotal(ORDER_CHARGES_TOTAL);
    }

    public function getSubTotal()
    {
        return $this->getTotal(ORDER_ITEMS_TOTAL) - $this->getTotal(ORDER_DISCOUNT_TOTAL);
    }

    public function getDiscountTotal()
    {
        return array_sum( $this->array_discounts );
    }

    public function getChargesTotal()
    {
        return array_sum( $this->array_charges );
    }

    public function getOrderTitle()
    {
        return $this->getRef();
    }

    public function generate( $send_confirmation_emails = true )
    {
        global $emuShop;

        // Create the order reference
        $this->orderRefNumber = $this->generateReferenceNumber();

        // Set the order post title
        $this->postTitle = $this->getOrderTitle();

        $this->setPostStatusPublished();

        if( $send_confirmation_emails )
            $this->sendEmailNotifications();

        $this->setOrderStatus( ORDER_STATUS_RECEIVED );

        // Save the record and the post
        $this->save();

        do_action( 'emu_shop_order_generated', $this);

        $emuShop->setSessionData('customer_order', $this->postID);
    }

    public function sendEmailNotifications()
    {
        global $emuShop;

        // Get the managers
        $templateManager = $emuShop->getManager('email-template');

        // Email to buyer
        // -----------------------------------------------------------------------------------------------------------------
        // Get the email template
        $template = $templateManager->getTemplate( 'order-confirmation-buyer.txt' );

        // Fill the email with common tags
        $anon_tags = array( 'customer email' => $this->customerEmail,
                            'customer first name' => $this->customerFirstName,
                            'customer last name' => $this->customerLastName );

        $template = $templateManager->fillTemplate( $template, $anon_tags );
        $template = $templateManager->fillTemplate( $template, $this->customer->getTemplateTags('customer '), $this->customer );
        $template = $templateManager->fillTemplate( $template, $this->getTemplateTags(), $this );

        // Fill with address tags
        if( $shipping_address = $this->getShippingAddress() )
            $template = $templateManager->fillTemplate( $template, $shipping_address->getTemplateTags('shipping '), $shipping_address );

        if( $billing_address = $this->getBillingAddress() )
            $template = $templateManager->fillTemplate( $template, $billing_address->getTemplateTags('billing '), $billing_address );

        // Fill email with specific tags
        $tags = array(  'order summary' => $this->getOrderSummary() );
        $template = $templateManager->fillTemplate( $template, $tags );

        // Extract the email information and remove the header
        $email = $templateManager->extractEmailData( $template );

        // Send the mail
        $emuShop->mail( $email->to, $email->from, $email->subject, $email->content, $email->type, $email->cc, $email->bcc );
        // -----------------------------------------------------------------------------------------------------------------

        // Email to seller
        // -----------------------------------------------------------------------------------------------------------------
        // Get the email template
        $template = $templateManager->getTemplate( 'order-confirmation-seller.txt' );

        // Fill the email with common tags
        $template = $templateManager->fillTemplate( $template, $anon_tags );
        $template = $templateManager->fillTemplate( $template, $this->customer->getTemplateTags('customer '), $this->customer );
        $template = $templateManager->fillTemplate( $template, $this->getTemplateTags(), $this );

        // Fill with address tags
        if( $shipping_address = $this->getShippingAddress() )
            $template = $templateManager->fillTemplate( $template, $shipping_address->getTemplateTags('shipping '), $shipping_address );

        if( $billing_address = $this->getBillingAddress() )
            $template = $templateManager->fillTemplate( $template, $billing_address->getTemplateTags('billing '), $billing_address );

        // Fill email with specific tags
        $tags = array(  'order summary' => $this->getOrderSummary() );
        $template = $templateManager->fillTemplate( $template, $tags );

        // Extract the email information and remove the header
        $email = $templateManager->extractEmailData( $template );

        // Send the mail
        $emuShop->mail( $email->to, $email->from, $email->subject, $email->content, $email->type, $email->cc, $email->bcc );
        // -----------------------------------------------------------------------------------------------------------------

    }

    public function setOrderStatus( $status )
    {
        $this->orderStatus = $status;
    }

    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    public function getOrderStatusDescription()
    {
        global $emuShop;

        $status_options = $emuShop->orderManager->getOrderStatusOptions();

        if( isset( $status_options[$this->orderStatus] ) )
            return $status_options[$this->orderStatus];

        return '';
    }

    public function getOrderSummary()
    {
        return $this->createOrderSummary();
    }

    public function createOrderSummary()
    {
        global $emuShop;

        $templateManager = $emuShop->getManager( 'theme' );

        // get the summary template
        $template = $templateManager->getTemplate( 'summary.htm' );

        $order_item_template = $templateManager->getTemplateSection( 'order items', $template );
        $discounts_template = $templateManager->getTemplateSection( 'discounts', $template );
        $charges_template = $templateManager->getTemplateSection( 'charges', $template );
        $payments_template = $templateManager->getTemplateSection( 'payments', $template );

        // Create the sections
        $order_items = '';

        foreach( $this->getItems() as $order_item )
            $order_items .= $templateManager->fillTemplate( $order_item_template, $order_item->getTemplateTags(), $order_item );

        $discounts = '';

        foreach( $this->getDiscounts() as $description => $total )
            $discounts .= $templateManager->fillTemplate( $discounts_template, array( 'description' => $description, 'total' => $this->format($total) ) );

        $charges = '';

        foreach( $this->getCharges() as $description => $total )
            $charges .= $templateManager->fillTemplate( $charges_template, array( 'description' => $description, 'total' => $this->format($total) ) );

        $payments = '';

        foreach( $this->getPayments() as $description => $total )
            $payments .= $templateManager->fillTemplate( $payments_template, array( 'description' => $description, 'total' => $this->format($total) ) );

        // Fill the sections
        $template = $templateManager->fillTemplateSections( $template, array( 'order items' => $order_items, 'discounts' => $discounts, 'charges' => $charges, 'payments' => $payments ) );

        // Fill with address tags
        if( $shipping_address = $this->getShippingAddress() )
            $template = $templateManager->fillTemplate( $template, $shipping_address->getTemplateTags('shipping '), $shipping_address );

        if( $billing_address = $this->getBillingAddress() )
            $template = $templateManager->fillTemplate( $template, $billing_address->getTemplateTags('billing '), $billing_address );

        $template = $templateManager->setTemplateConditionals( $template, array( 'shipping' => $this->getShippingAddress(), 'billing' => $this->getBillingAddress(), 'offline payment message' => $emuShop->offlinePayments ) );

        // Fill with order tags
        $template = $templateManager->fillTemplate( $template, $this->getTemplateTags(), $this );

        return $template;
    }

    public function format($number)
    {
        global $emuShop;
        return $emuShop->currency.apply_number_format('currency', $number);
    }

    public function getRef()
    {
        return $this->orderRefNumber;
    }

    public function isNewOrder()
    {
        return $this->dbID ? false : true;
    }

    public function __toString()
    {
        $summary = array();
        $line_repeat = 1;

        if( $this->isNewOrder() )
            $summary[] = 'New Order';
        else
            $summary[] = 'Order #'.$this->getRef;

        $summary[] = str_repeat('-', $line_repeat);
        $summary[] = 'Customer ID: '.$this->customerID;

        if( $billing_address = $this->getBillingAddress() )
            $summary[] = 'Billing Address: '.$billing_address->getAddressAggregate(', ');

        if( $shipping_address = $this->getShippingAddress() )
            $summary[] = 'Shipping Address: '.$shipping_address->getAddressAggregate(', ');

        $summary[] = str_repeat('-', $line_repeat);

        foreach( $this->items as $item )
            $summary[] = 'Item: '.$item->qty.' x '.$item->description.' = '.$item->getTotal();

        foreach( $this->getDiscounts() as $description => $value )
            $summary[] = 'Discount: '.$description.' -'.$this->format($value);

        $summary[] = str_repeat('-', $line_repeat);
        $summary[] = 'Items total: '.$this->getTotal(ORDER_ITEMS_TOTAL_FORMATTED);
        $summary[] = 'Discount total: '.$this->getTotal(ORDER_DISCOUNT_TOTAL_FORMATTED);
        $summary[] = 'Sub total: '.$this->getTotal(ORDER_SUB_TOTAL_FORMATTED);
        $summary[] = str_repeat('-', $line_repeat);

        foreach( $this->getCharges() as $description => $value )
            $summary[] = 'Charge: '.$description.' '.$this->format($value);

        $summary[] = str_repeat('-', $line_repeat);
        $summary[] = 'Charges total: '.$this->getTotal(ORDER_CHARGES_TOTAL_FORMATTED);
        $summary[] = str_repeat('-', $line_repeat);
        $summary[] = 'Grand total: '.$this->getTotal(ORDER_GRAND_TOTAL_FORMATTED);
        $summary[] = str_repeat('-', $line_repeat);

        foreach( $this->getPayments() as $description => $value )
            $summary[] = 'Payment: '.$description.' -'.$this->format($value);

        $summary[] = str_repeat('-', $line_repeat);
        $summary[] = 'Payments Total: '.$this->getTotal(ORDER_PAYMENTS_TOTAL_FORMATTED);
        $summary[] = str_repeat('=', $line_repeat);
        $summary[] = 'Amount Due: '.$this->getTotal(ORDER_AMOUNT_DUE_FORMATTED);
        $summary[] = str_repeat('=', $line_repeat);


        return implode("\n", $summary)."\n";

    }

    public function save()
    {
        global $wpdb, $emuShop;

        // Create the order summary
        $this->postContent = $this->createOrderSummary();

        if( $shipping_address = $this->getShippingAddress() )
            $this->shippingAddress = serialize($shipping_address);
        else
            $this->shippingAddress = null;

        if( $billing_address = $this->getBillingAddress() )
            $this->billingAddress = serialize($billing_address);
        else
            $this->billingAddress = null;

        $this->charges = serialize($this->array_charges);
        $this->payments = serialize($this->array_payments);
        $this->discounts = serialize($this->array_discounts);

        $this->chargesTotal = $this->getTotal(ORDER_CHARGES_TOTAL);
        $this->paymentsTotal = $this->getTotal(ORDER_PAYMENTS_TOTAL);
        $this->discountTotal = $this->getTotal(ORDER_DISCOUNT_TOTAL);
        $this->itemsTotal = $this->getTotal(ORDER_ITEMS_TOTAL);
        $this->subTotal = $this->getTotal(ORDER_SUB_TOTAL);
        $this->grandTotal = $this->getTotal(ORDER_GRAND_TOTAL);

        if( $this->isNewOrder() )
            $this->dateCreated = apply_date_format('db');
        else
            $this->dateUpdated = apply_date_format('db');

        $this->userID = $this->customer->currentUser->ID;

        parent::save();

        $this->saveOrderItems();

    }

     function generateReferenceNumber()
     {
         global $emuShop;

         $current_order_number = $emuShop->getMeta('current-order-number');

         if( !$current_order_number ) $current_order_number = $emuShop->firstOrderNumber;

         $ref = $emuShop->orderRefPrefix.date('m').chr(rand(48,57)).date('d').$current_order_number;

         $current_order_number++;

         $emuShop->updateMeta( 'current-order-number', $current_order_number );

         return $ref;
     }

    public function saveOrderItems()
    {
        foreach( $this->getItems() as $item )
        {
            $item->orderID = $this->getID();
            $item->save();
        }
    }

    public function getData()
    {
        global $emuShop;

        parent::getData();

        if( $this->shippingAddress )
            $this->shippingAddressObject = unserialize( $this->shippingAddress );

        if( $this->billingAddress )
            $this->billingAddressObject = unserialize( $this->billingAddress );

        if( $this->charges )
            $this->array_charges = unserialize($this->charges);

        if( $this->discounts )
            $this->array_discounts = unserialize($this->discounts);

        if( $this->payments )
            $this->array_payments = unserialize($this->payments);

        if( $this->customerID )
        {
            $customer = $emuShop->getInstance('emuCustomer', array(null, $this->customerID));
            $this->setCustomer( $customer );
        }

    }

    public function deleteRecord()
    {
        parent::deleteRecord();

        if( !$items = $this->getItems() ) return;

        foreach( $items as $item )
            $item->delete();
    }

}

?>
