<?php

class emuH_Links extends emuHelper
{
    function getAddToBasketLink($product_id = null, $variant_id = null, $link_text = 'Add To Basket')
    {
        if( empty( $product_id ) && empty( $variant_id ) ) return '';

        if( empty( $product_id ) &! empty( $variant_id ) )
        {
            // Find the product from the variant
            $oVariant = $this->emuApp->getInstance('emuVariant', $variant_id);

            if( !$oVariant ) return '';

            $product_id = $oVariant->parentID;
        }

        global $emuShop;

        $query_string = $this->emuApp->buildActionRefs($e_action = 'products', $e_button = 'Add to Basket', $emuShop, true, true );

        $query_string .= "&p_id=$product_id";

        if( $variant_id )
        {
            $query_string .= '&'.urlencode('variants[]')."=$variant_id";
            $query_string .= '&'.urlencode('variant_qty[]')."=1";
        }
        else
        {
            $query_string .= '&qty=1';
        }

        return "<a href='{$this->emuApp->pages->basket->url}?{$query_string}'>$link_text</a>";
    }

    function getCheckoutLink( $link_text = 'Checkout' )
    {
        return "<a href='{$this->emuApp->pages->checkout->url}'>$link_text</a>";
    }

}