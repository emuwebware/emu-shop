<?php

class emuWishlistProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		if( !$product_id = post_val('product_id') ) return;

		$customer = $emuShop->getCustomer();

		switch( $this->button )
		{
			case 'Move to Cart':

				$customer->removeFromWishlist( $product_id );

				$basket = $emuShop->getBasket();

				$basket->addItem( $product_id );

				$this->messages[] = 'Item moved to <a href="'.$emuShop->pageManager->pages->basket->url.'">your cart</a>.';
				$emuShop->addMessage( 'wishlist', $this->messages, 'notice' );

				break;

			case 'Remove from Wishlist':

				$customer->removeFromWishlist( $product_id );

				$this->messages[] = 'Item removed.';
				$emuShop->addMessage( 'wishlist', $this->messages, 'notice' );

				break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $_SERVER[ 'HTTP_REFERER' ] );

		header( 'Location: '.$location );
		exit();
	}

}

?>
