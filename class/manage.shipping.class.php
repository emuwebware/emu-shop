<?php

class emuM_Shipping extends emuManager
{
	public $pages;

	// Shipping options
	const OPTION_CHARGES = 1; // use the defined shipping charges (rules)
	const OPTION_SPECIFIC_CHARGE = 2; // set an individual shipping charge for this item
	const OPTION_NO_CHARGE = 3; // no charge

	public function registerAdminPages()
	{
		global $emuShop;

		$emuShop->registerAdminPage( array( 'name' => 'Shipping',
									'filename' => 'shipping.php',
									'position' => 2,
									'styles' => array( 'emu-common' ),
									'scripts' => array() ) );
	}

	public function registerClasses()
	{
		global $emuShop;

		$emuShop->registerClass( 'emuShippingCharge', 'model.shippingcharge.class.php', 'emuShopCommon' );
	}


	function getWeightUnits()
	{
		$units = array();

		if( $unit = $this->emuApp->getMeta('large_weight_unit' ) )
			$units[] = $unit;

		if( $unit = $this->emuApp->getMeta('small_weight_unit' ) )
			$units[] = $unit;

		return implode( ', ', $units );
	}

	function getShippingCharges($terms = array())
	{
		global $wpdb, $emuShop;

		$sql_add = array();

		// get the ID for america
		$sql = "select dbID from {$emuShop->dbPrefix}shipping_charges %s order by charge desc";

		if(isset($terms['description'])) $sql_add[] = "description = '{$terms['description']}'";
		if(isset($terms['dbID'])) $sql_add[] = "dbID = '{$terms['dbID']}'";
		if(isset($terms['countryID'])) $sql_add[] = "countryID = '{$terms['countryID']}'";
		if(isset($terms['stateID'])) $sql_add[] = "stateID = '{$terms['stateID']}'";
		if(isset($terms['basedOn'])) $sql_add[] = "basedOn = '{$terms['basedOn']}'";
		$sql_add[] = 'isActive = '.( isset( $terms['isActive'] ) ? $terms['isActive'] : '1' );

		$sql_add = count($sql_add) > 0 ? 'where ('.implode(') and (', $sql_add).')' : '';

		$sql = sprintf($sql, $sql_add);

		$charges = array();

		if( $charges_rs = $wpdb->get_results( $sql ) )
		{
			foreach( $charges_rs as $charge )
				$charges[] = $emuShop->getInstance( 'emuShippingCharge', array( $charge->dbID ), $refresh = true );
		}

		return $charges;

	}

	function clearShippingCharges()
	{
		global $wpdb, $emuShop;

		return $wpdb->query( "delete from {$emuShop->dbPrefix}shipping_charges" );
	}

	function getShippingOptions()
	{
		return array( self::OPTION_CHARGES => 'Apply Shipping Charges', self::OPTION_SPECIFIC_CHARGE => 'Set Individual Charge', self::OPTION_NO_CHARGE => 'No Shipping Charge' );
	}

	public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		// shipping
		$sql = "CREATE TABLE {$emuShop->dbPrefix}shipping_charges (
		dbID int(10) NOT NULL AUTO_INCREMENT,
		countryID int(10) NULL,
		stateID int(10) NULL,
		description varchar(300) default NULL,
		basedOn varchar(10) default NULL,
		rangeFrom varchar(10) default NULL,
		rangeTo varchar(10) default NULL,
		charge float(2) default NULL,
		freeShipping BOOLEAN NULL DEFAULT 0,
		isActive BOOLEAN NOT NULL DEFAULT 1,
		UNIQUE KEY id (dbID)
		);";

		dbDelta($sql);


    }

}


?>
