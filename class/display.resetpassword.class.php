<?php


class emuResetPasswordDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'reset-password.htm';
        parent::__construct();

	}

	public function build()
	{
		global $emuShop;

		$template = $this->template;

		$template = $this->templateManager->fillTemplate( $template, array( 'login email' => post_val( 'login_email' ), 'reset messages' => $emuShop->getMessages( 'reset' ) ) );

		$this->content = apply_filters( 'emu_reset_password_content', $template );

	}
}

?>
