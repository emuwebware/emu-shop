<?php

class emuReview extends emuShopCommon
{
	public $data = array();
	public $post = array();
	public $comments = array();

	public $ID;
	public $title;
	public $text;
	public $textWithQuotes;
	public $excerpt;
	public $categories;

	public $productPostID;

	public static $staticTemplateTags = array( 	'helpful summary' => 'helpfulSummary',
												'comment summary' => 'commentsSummary',
												'review title' => 'title',
												'rating stars small' => array( 'this', 'getStars', 'small', array( 'this', 'rating' ) ),
												'rating stars large' => array( 'this', 'getStars', 'large', array( 'this', 'rating' ) ),
												'review link' => 'permalink',
												'customer review link' => 'customerReviewLink',
												'comment link' => 'commentLink',
												'review id' => 'dbID',
												'review post id' => 'ID',
												'rating' => 'displayRating',
												'reviewer' => 'reviewer',
												'reviewer location' => 'reviewerLocation',
												'review date' => 'displayDate',
												'review text' => 'text',
												'review text with quotes' => 'textWithQuotes' );
	public $templateTags;

	public function __construct( $post = null )
	{
		$this->templateTags = self::$staticTemplateTags;

		if( is_numeric( $post ) )
		{
			// then a post ID is being sent, so get the post object first
			//$post = $emuShop->getPost( $post ); // setup_postdata( $post );
			$post = get_post( $post );
		}

		if( $post )
		{
			$this->getPostData( $post );

			// get the rest of the review data
			$this->getReviewData();
		}

	}


	public function getStars( $size, $no_stars, $total_stars = 5 )
	{
		global $emuShop;

		return $emuShop->getManager('product')->getStars( $size, $no_stars, $total_stars );
	}

	public function getRatingStars( $size )
	{
		if( !$this->ratingCount ) return ''; // no ratings yet

		return $emuShop->getManager('product')->getStars( $size, $this->ratingAverage );
	}


	public function getPostData( $post )
	{
		global $emuShop;

		$this->post = $post;

		$this->ID = $post->ID;
		$this->title = apply_filters( 'the_title', $post->post_title );
		$this->text = wpautop( $post->post_content );
		$this->textWithQuotes = wpautop( '"'.$post->post_content.'"' );
		$this->excerpt = apply_filters( 'the_excerpt', $post->post_excerpt );
		$this->categories = get_the_term_list( $post->ID, 'pcat', '', ', ', '' );
		$this->permalink = get_permalink( $post->ID );

		if( empty( $this->excerpt ) )
			$this->excerpt = preg_replace( '/^(.{'.$emuShop->autoExcerptLength.'})(.{1,})/', '$1...', $this->text );

	}

    public function __get( $member )
	{
		global $emuShop;

		switch( $member )
		{
			case 'commentLink':

				return $this->permalink.'#respond';

			case 'helpfulSummary':

				$helpful_summary = '';

				if( $this->helpfulNoCount + $this->helpfulYesCount > 0 )
					$helpful_summary = $this->helpfulYesCount.' out of '.( $this->helpfulNoCount + $this->helpfulYesCount ).' people found the following review helpful:';

				return $helpful_summary;

			case 'commentsSummary':

				$this->getComments();

				$comment_summary = 'No comments';

				if( count( $this->comments ) > 0 )
				{
					$c_count = count( $this->comments );
					$comment_summary = '<a href="'.$this->permalink.'#comments">'.$c_count.' comment'.( $c_count > 1 ? 's' : '' ).'</a>';
				}

				return $comment_summary;

			case 'customerReviewLink':

				return $emuShop->pageManager->pages->customerReviews->url.'?customer='.urlencode( $this->reviewer );

			case 'reviewer':

				return $this->customer->username;

				//return ucfirst( $this->customer->firstName ).' '.ucfirst( $this->customer->lastName );

			case 'reviewerLocation':

				$address = '';

				if( $this->customer->addressBook->defaultBillingAddress )
				{
					$address = $this->customer->addressBook->defaultBillingAddress;
					$address = $address->city.', '.$address->stateName;
				}

				return $address;

			case 'displayRating':

				return $this->rating.' star'.( $this->rating > 1 ? 's' : '' );

			case 'displayDate':

				return apply_date_format( 'review', $this->reviewDate );

			default:

				if( !isset( $this->data[ $member ] ) ) return null;

				return $this->data[ $member ];
		}
    }

	public function __set( $member, $value )
	{
        $this->data[ $member ] = $value;
    }

	public function getReviewData()
	{
		global $wpdb, $emuShop;

		$sql = "select * from {$emuShop->dbPrefix}reviews where postID = {$this->ID}";

		if( $row = $wpdb->get_row( $sql, ARRAY_A ) )
		{
			$this->data = array_merge( $this->data, $row );
			$this->customer = $emuShop->getInstance( 'emuCustomer', array( $this->wpUserID ) );

			$sql = "select postID from {$emuShop->dbPrefix}products where dbID = {$this->productID}";
			$this->productPostID = $wpdb->get_var( $sql ); // so that we have easy way to find the product if the review class is created on its own

		}
		else return;

	}

	public function getComments()
	{
		$this->comments = get_comments( array( 'post_id' => $this->ID, 'status' => 'approve' ) );
		return $this->comments;
	}

	public function update()
	{
		global $wpdb, $emuShop;

		$wp_data = array();

		$wp_data['post_title'] = $this->title;
		$wp_data['post_content'] = $this->text;
		$wp_data['post_type'] = 'review';

		$this->data = $this->sanitize( $this->data );

		// Update / create the database record
		if( $this->dbID )
		{
			$this->updateRecord( "{$emuShop->dbPrefix}reviews", $this->data, null, array( 'dbID' => $this->dbID ) );

			$wp_data['ID'] = $this->postID;

			wp_update_post( $wp_data );

		}
		else
		{
			// Add the review date field
			if( !isset( $this->reviewDate ) ) $this->reviewDate = 'date';

			if( !$this->wpUserID )
			{
				global $current_user; get_currentuserinfo();
				$this->wpUserID = $current_user->ID;
			}

			$wp_data['post_author'] = $this->wpUserID;
			$wp_data['post_status'] = 'pending';

			if( $this->postID = wp_insert_post( $wp_data ) )
			{
				$this->dbID = $this->insertRecord( "{$emuShop->dbPrefix}reviews", $this->data, array( 'reviewDate' => '%datefill' ) );
			}
		}
	}

	public function increaseHelpfulCounter( $type, $review_id )
	{
		global $wpdb, $emuShop;

		$type = strtolower( trim( $type ) );

		$field = $type == 'no' ? 'helpfulNoCount' : 'helpfulYesCount';

		$sql = "update {$emuShop->dbPrefix}reviews set $field = $field + 1 where dbID = $review_id";

		$wpdb->query( $sql );

	}

	public function setReviewInappropriate( $review_id )
	{
		global $wpdb, $emuShop;

		$sql = "select postID from {$emuShop->dbPrefix}reviews where dbID = $review_id";

		$post_id = $wpdb->get_var( $sql );

		$wp_data = array();
		$wp_data['ID'] = $post_id;
		$wp_data['post_status'] = 'pending';

		wp_update_post( $wp_data );

	}


}


?>
