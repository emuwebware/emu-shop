<?php

class emuM_Customers extends emuManager
{
    public $pages;

    public $customer;

    public function init()
    {
        add_filter( 'manage_edit-review_columns', array( $this, 'addReviewColumns' ) );
        add_action( 'manage_posts_custom_column', array( $this, 'manageCustomColumns' ), 10, 2 );

        // Show the customer information on the profile page
        add_action( 'show_user_profile', array( $this, 'showCustomerInformation' ) );
        add_action( 'edit_user_profile', array( $this, 'showCustomerInformation' )  );
        add_action( 'personal_options_update', array( $this, 'saveCustomerInformation' ) );
        add_action( 'edit_user_profile_update', array( $this, 'saveCustomerInformation' ) );
        add_action( 'delete_user', array( $this, 'deleteCustomer'));
        add_action( 'user_register', array( $this, 'registerUser' ));
    }

    public function loadScripts()
    {
        global $emuShop, $pagenow;

        if( $pagenow == 'user-edit.php' || $pagenow == 'profile.php' )
            $emuShop->loadScript( 'emu-customer-admin', plugins_url( "/{$emuShop->pluginName}/js/customer-admin.js" ), array('jquery') );
    }

    function registerUser($wp_user_id)
    {
        global $emuShop;

        $new_user = get_userdata( $wp_user_id );

        $new_customer = $emuShop->getInstance( 'emuCustomer', $new_user );
    }

    function manageCustomColumns( $column_name, $id )
    {
        global $wpdb, $emuShop;

        if( in_array( $column_name, array( 'e-r-rating', 'e-r-reviewer', 'e-r-product' ) ) )
        {
            if( !isset( $GLOBALS["review-$id"] ) )
            {
                $review = $emuShop->getInstance( 'emuReview', array( $id ) );
                $GLOBALS["review-$id"] = $review;
            }
            else
            {
                $review = $GLOBALS["review-$id"];
            }
        }

        switch ( $column_name )
        {
            case 'e-r-rating':

                echo $emuShop->getManager('product')->getStars( 'large', $review->rating );

                break;
            case 'e-r-reviewer':

                echo ucfirst( $review->customer->firstName ).' '.ucfirst( $review->customer->lastName ).' ('.$review->reviewer.')';

                break;

            case 'e-r-product':

                $product = $emuShop->getInstance( 'emuProduct', array( null, $review->productPostID ) );

                echo '<a href="'.$product->permalink.'" target="_blank">'.$product->name.'</a>';

                break;
        }

    }


    function addReviewColumns( $columns )
    {
        $columns = array();

        $columns['cb'] = '<input type="checkbox" />';
        $columns['title'] = _x('Title', 'column name');
        $columns['e-r-product'] = 'Product';
        $columns['e-r-rating'] = 'Rating';
        $columns['e-r-reviewer'] = 'Reviewer';
        $columns['date'] = _x('Date', 'column name');

        return $columns;
    }

    function deleteCustomer( $user_id )
    {
        global $wpdb;

        $sql = "update {$this->emuApp->dbPrefixShared}customers set deleted = 1 where wpUserID = $user_id";
        $wpdb->query($sql);
    }

    function getCustomer( $new_instance = false )
    {
        global $emuShop;

        if( !$this->customer || $new_instance )
        {
            global $current_user; get_currentuserinfo();

            $this->customer = $emuShop->getInstance( 'emuCustomer', $current_user );
        }

        do_action( 'emu_get_customer', $this->customer );

        return $this->customer;
    }

    function showCustomerInformation( $user )
    {
        global $emuShop;

        $customer = $emuShop->getInstance( 'emuCustomer', $user->ID );

        $arr_activation_options = array( '1' => 'Account Activated', '0' => 'Account Inactive' );

        ?>
        <h3>Emu Shop Additional Customer Information</h3>

        <table class="form-table">
            <tr>
                <th>Prefix</th>
                <td><input type="text" name="prefix" value="<?php echo $customer->prefix?>" /></td>
            </tr>
            <tr>
                <th>Suffix</th>
                <td><input type="text" name="suffix" value="<?php echo $customer->suffix?>" /></td>
            </tr>
            <tr>
                <th>Company Name</th>
                <td><input type="text" name="company_name" value="<?php echo $customer->companyName?>" /></td>
            </tr>
            <tr>
                <th>Department</th>
                <td><input type="text" name="department" value="<?php echo $customer->department?>" /></td>
            </tr>
            <tr>
                <th>Job Title</th>
                <td><input type="text" name="job_title" value="<?php echo $customer->jobTitle?>" /></td>
            </tr>
            <tr>
                <th>Account Status</th>
                <td><?php echo drop_down( '', 'account_status', '', $customer->isActive, $arr_activation_options )?></td>
            </tr>
        </table>

        <table class="form-table custom-var">
        <tbody>
            <tr>
                <th><strong>Custom Field Name</strong> <input type="button" class="button add-custom-var" value="Add" /></th>
                <td><em>Custom Field Value</em></th>
            </tr>
            <tr class="custom-var-template hidden">
                <th><input type="text" name="varName[]" value="" /></th>
                <td><input type="text" name="varValue[]" value="" /> <a href="javascript:{}" class="delete-custom-var"><img src="<?php echo EMU_FRAMEWORK_URL?>/image/del.gif" height="14" width="14" alt="delete" /></a></td>
            </tr>
            <?php

            if( $customer->arrCustomVars )
            {
                foreach( $customer->arrCustomVars as $var_name => $var_value )
                {
                    ?>
                    <tr>
                        <th><input type="text" name="varName[]" value="<?php echo $var_name?>" /></th>
                        <td><input type="text" name="varValue[]" value="<?php echo $var_value?>" /> <a href="javascript:{}" class="delete-custom-var"><img src="<?php echo EMU_FRAMEWORK_URL?>/image/del.gif" height="14" width="14" alt="delete" /></a></td>
                    </tr>
                    <?php
                }
            }

            ?>
        </tbody>
        </table>

    <?php

    }

    function saveCustomerInformation( $user_id )
    {
        if ( !current_user_can( 'edit_user', $user_id ) ) return false;

        global $emuShop;

        extract( $_POST );

        $customer = $emuShop->getInstance( 'emuCustomer', $user_id );

        $customer->firstName = $first_name;
        $customer->lastName = $last_name;
        $customer->prefix = $prefix;
        $customer->suffix = $suffix;
        $customer->companyName = $company_name;
        $customer->department = $department;
        $customer->jobTitle = $job_title;
        $customer->isActive = $account_status == '1' ? 1 : 0;

        $customer->clearCustomVars();

        // Are there any custom vars?
        if( count( $varName ) > 1 )
        {
            // add the custom vars (ignore first set which is the dom template)
            for( $i = 1; $i < count( $varName ); $i++ )
                $customer->addCustomVar( $varName[$i], $varValue[$i] );
        }

        $customer->update();
    }

    function sendActivationCode( $code, $customer )
    {
        global $emuShop;

        $email = $emuShop->getManager('email-template')->getEmailTemplate( 'account-activation.txt' );

        $message = $email->content;

        $activation_url = home_url();

        $templateManager = $emuShop->getManager('theme');

        $message = $templateManager->fillTemplate( $message, $customer->getTemplateTags('customer '), $customer );
        $message = $templateManager->fillTemplate( $message, Array( 'activation url' => $emuShop->pageManager->pages->login->url.'?e-action=login&e-plugin='.$emuShop->emuAppID.'&activate='.urlencode( $code ).'&return='.urlencode('/') ) );

        $email->to = $templateManager->fillTemplate( $email->to, $customer->getTemplateTags('customer '), $customer );
        $email->from = $templateManager->fillTemplate( $email->from, $customer->getTemplateTags('customer '), $customer );
        $email->cc = $templateManager->fillTemplate( $email->cc, $customer->getTemplateTags('customer '), $customer );
        $email->bcc = $templateManager->fillTemplate( $email->bcc, $customer->getTemplateTags('customer '), $customer );

        $emuShop->mail( $email->to, $email->from, $email->subject, $message, $email->type, $email->cc, $email->bcc );
    }

    function syncWithExistingUsers()
    {
        global $wpdb, $emuShop;

        if( function_exists( 'get_users' ) )
            $blogusers = get_users();
        else
            $blogusers = get_users_of_blog();

        foreach( $blogusers as $user )
        {
            if( $wpdb->get_var( "select count(*) from {$emuShop->dbPrefixShared}customers where wpUserID = {$user->ID}" ) == 0 )
            {
                $user = get_userdata( $user->ID );

                $customer = $emuShop->getInstance( 'emuCustomer' );

                $customer->firstName = $user->first_name;
                $customer->lastName = $user->last_name;
                $customer->email = $user->user_email;
                $customer->wpUserID = $user->ID;

                $customer->syncUpdate();

                unset( $customer );
                unset( $user );
            }
        }
    }

    function getCustomers( $terms = array() )
    {
        global $wpdb, $emuShop;

        $sql_add = array();

        $sql = "select * from {$emuShop->dbPrefixShared}customers %s";

        foreach( $terms as $term => $value )
            $sql_add[] = "$term = '$value'";

        $sql_add = count( $sql_add ) > 0 ? 'where ('.implode(') and (', $sql_add).')' : '';

        $sql = sprintf( $sql, $sql_add );

        return $wpdb->get_results( $sql );
    }

    function getCustomerReviews( $terms = array() )
    {
        global $wpdb, $emuShop;

        $sql_add = array();

        $sql = "select r.* from {$emuShop->dbPrefixShared}customers c inner join {$emuShop->dbPrefix}reviews r on r.wpUserID = c.wpUserID %s";

        foreach( $terms as $term => $value )
            $sql_add[] = "$term = '$value'";

        $sql_add = count( $sql_add ) > 0 ? 'where (c.'.implode(') and (c.', $sql_add).')' : '';

        $sql = sprintf( $sql, $sql_add );

        $reviews_rs = $wpdb->get_results( $sql );

        $reviews = array();

        foreach( $reviews_rs as $review_record )
        {
            $reviews[] = $emuShop->getInstance( 'emuReview', array( $review_record->postID ) );
        }

        return $reviews;
    }

    function registerModels()
    {
        $this->emuApp->registerModel( 'emuAddress', 'model.address.class.php' );
        $this->emuApp->registerModel( 'emuAddressBook', 'model.addressbook.class.php');
        $this->emuApp->registerModel( 'emuReview', 'model.review.class.php' );
        $this->emuApp->registerModel( 'emuPasswordReminder', 'model.reminder.class.php' );
        $this->emuApp->registerModel( 'emuAccountActivator', 'model.activator.class.php' );
        $this->emuApp->registerModel( 'emuCustomer', 'model.customer.class.php' );
    }

    function registerCustomPostTypes()
    {
        $labels = array(
            'name' => 'Reviews',
            'singular_name' => 'Review',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Review',
            'edit_item' => 'Edit Review',
            'new_item' => 'New Review',
            'view_item' => 'View Review',
            'search_items' => 'Search Reviews',
            'not_found' => 'No reviews found',
            'not_found_in_trash' => 'No reviews found in Trash',
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'review'),
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 4,
            'supports' => array( 'title', 'excerpt', 'editor', 'author', 'custom-fields', 'revisions', 'thumbnail', 'comments' ),
        );

        register_post_type( 'review', $args );

        register_taxonomy( 'rcat', 'review' , array( 'label' => 'Review Categories', 'show_ui' => true, 'hierarchical' => true, 'rewrite' => array( 'slug' => 'review-category' ), 'query_var' => true ) );

    }

    public function install( $emuShop = null )
    {
        if( !$emuShop ) return;

        global $wpdb;

        // Generate customer records for any existing users
        add_action( 'after_setup_theme', array( $this, 'syncWithExistingUsers' ) );

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        // customers
        $sql = "CREATE TABLE {$emuShop->dbPrefixShared}customers (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        wpUserID int(10) NULL,
        email varchar(300) default NULL,
        username varchar(300) default NULL,
        prefix varchar(300) default NULL,
        suffix varchar(300) default NULL,
        companyName varchar(300) default NULL,
        department varchar(300) default NULL,
        jobTitle varchar(300) default NULL,
        firstName varchar(300) default NULL,
        lastName varchar(300) default NULL,
        shippingChargeID int(10) NULL,
        allowEmailContact BOOLEAN NULL DEFAULT 0,
        customVars text default NULL,
        isActive BOOLEAN NULL DEFAULT 0,
        deleted BOOLEAN NULL DEFAULT 0,
        activationCode varchar(10) default NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

        // customer wishlist
        $sql = "CREATE TABLE {$emuShop->dbPrefix}wishlist (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        productID int(10) NULL,
        customerID int(10) NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

        // addresses
        $sql = "CREATE TABLE {$emuShop->dbPrefixShared}address_book (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        customerID int(10) NULL,
        firstName varchar(300) default NULL,
        lastName varchar(300) default NULL,
        email varchar(300) default NULL,
        addressLineOne varchar(300) default NULL,
        addressLineTwo varchar(300) default NULL,
        countryID int(5) NULL,
        stateID int(5) NULL,
        state varchar(300) default NULL,
        city varchar(300) default NULL,
        postalCode varchar(40) default NULL,
        phoneNumber varchar(300) default NULL,
        isDefaultBilling boolean not null default 0,
        isDefaultShipping boolean not null default 0,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);


        // payment methods
        $sql = "CREATE TABLE {$emuShop->dbPrefixShared}payment_methods (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        customerID int(10) NULL,
        billingAddressID int(10) NULL,
        paymentType varchar(20) NULL,
        cardNumber varchar(30) NULL,
        expiryMonth int(2) NULL,
        expiryYear int(2) NULL,
        validFromMonth int(2) NULL,
        validFromYear int(2) NULL,
        isDefaultPaymentMethod boolean not null default 0,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

    }

}


?>
