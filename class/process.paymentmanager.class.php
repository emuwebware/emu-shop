<?php

class emuPaymentManagerProcessor extends emuProcessor
{
	public $requiredFields = array( 'payment_type', 'cc_exp_month', 'cc_exp_year' );

	public function process()
	{
		extract( $_POST );

		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$customer = $emuShop->getManager('customer')->getCustomer();

		$redirect = false;

		switch( post_val('e-button') )
		{
			case 'Add':

				$this->requiredFields[] = 'card_number';

				// fall-through

			case 'Update':

				$this->checkRequiredFields();

				if( !$this->hasRequiredFields )
				{
					$this->messages[] = '<strong>Not all required fields were provided</strong> - check those marked with *';
					$this->error = true;

					$emuShop->addMessage('payment-manager', $this->messages, 'error');

					return;
				}

				if( $dbID = post_val( 'payment_method_id' ) )
				{
					$paymentMethod = $emuShop->getInstance( 'emuPaymentMethod', array( $dbID ) );

					if( strlen( trim( $card_number ) ) > 0 )
						$paymentMethod->cardNumber = $card_number;
				}
				else
				{
					// creating a new payment method
					$paymentMethod = $emuShop->getInstance( 'emuPaymentMethod' );
					$paymentMethod->customerID = $customer->dbID;
					$paymentMethod->cardNumber = $card_number;
				}

				$paymentMethod->paymentType = $payment_type;
				$paymentMethod->isDefaultPaymentMethod = post_val('default_payment_method') ? 1 : 0;
				$paymentMethod->expiryMonth = $cc_exp_month;
				$paymentMethod->expiryYear = $cc_exp_year;
				$paymentMethod->validFromMonth = $cc_v_from_month;
				$paymentMethod->validFromYear = $cc_v_from_year;

				$paymentMethod->update();

				// see if we're editing an address from the order process
				$basket = $emuShop->getBasket();

				if( $emuShop->getSessionData( 'e-action' ) == 'payment-method' )
				{
					$return = $emuShop->getSessionData('return');

					$emuShop->setSessionData( array( 'e-action' => '', 'e-method' => '', 'return' => '', 'payment_method_id' => '' ) );

					$location = $return;
				}
				else
				{

					$_POST = null;

					$this->messages[] = $dbID ? 'Payment Method updated.' : 'Payment method created.';
					$this->error = false;
					$emuShop->addMessage('method-list', $this->messages, 'notice' );

					$location = $_SERVER[ 'HTTP_REFERER' ];
				}

				$redirect = true;

			break;
			case 'Delete':

				$customer->paymentManager->removePaymentMethod( post_val( 'payment_method_id' ) );
				$customer->paymentManager->update();

				$this->messages[] = 'Payment method removed.';
				$this->error = false;

				$emuShop->addMessage('method-list', $this->messages, 'notice' );

				$location = $_SERVER[ 'HTTP_REFERER' ];

				$redirect = true;

			break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		if( $redirect )
		{
			$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );

			header( 'Location: '.$location );
			exit();
		}
	}

}

?>
