<?php



class emuDeliveryDisplay extends emuDisplay
{

	public function __construct()
	{
        $this->templateFile = 'delivery.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$basket = $emuShop->getBasket();

		$customer = $basket->customer;

		// see if we have any shipping charges
		$shipping_charges = $basket->getApplicableShippingCharges();

		if( count( $shipping_charges ) > 0 )
		{
			if( count( $shipping_charges ) == 1 )
			{
				$shippingCharge = $shipping_charges[0];

				$charge_description = $shippingCharge->description;

				// we don't need a drop down, we can just display the charge
				$shipping_charge_options = $shippingCharge->displayCharge;
			}
			else if( count( $shipping_charges ) > 1 )
			{
				$charge_description = 'Please select your choice of shipping';

				$selected_charge = $basket->shippingCharge ? $basket->shippingCharge->description : '';

				// the user needs to make a selection from the charges
				$shipping_charge_options = '<select name="shipping_charge" class="emu-select">';

				foreach( $shipping_charges as $shippingCharge )
				{
					$selected = $selected_charge == $shippingCharge->description ? ' selected="selected"' : '';
					$shipping_charge_options .= sprintf( '<option value="%s"%s>%s</option>', $shippingCharge->description, $selected, $shippingCharge->description.' - '.$shippingCharge->displayCharge );
				}

				$shipping_charge_options .= '</select>';
			}
		}
		else
		{
			$charge_description = 'Shipping Charge';
			$shipping_charge_options = 'No shipping charges';
		}

		$template = $this->template;

		$orderSummaryDisplay = $emuShop->getInstance( 'emuOrderSummaryDisplay' );
		$orderSummaryDisplay->build();

		$regionOptions = $emuShop->regionManager->getRegionOptions();

		$tags = array( 	'messages' => $emuShop->getMessages('delivery'),
						'charge options' => $shipping_charge_options,
						'charge description' => $charge_description,
						'order summary' => $orderSummaryDisplay->content );

		$template = $this->templateManager->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_delivery_content', $template );

	}
}

?>
