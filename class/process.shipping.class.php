<?php

class emuShippingProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();
		$customer = $basket->customer;

		switch( $this->button )
		{
			case 'Edit Address':

				$emuShop->setSessionData( array( 	'e-action' => 'address',
													'e-method' => 'edit',
													'address_id' => post_val('existing_address'),
													'return' => $emuShop->pageManager->pages->shipping->url ) );

				header( 'Location: '.$emuShop->pageManager->pages->addressBook->url );

				exit();

			case 'Continue':

				$address_book = $customer->getAddressBook();

				if( !post_val('existing_address') && $address_book->hasAddresses() )
				{
					$this->messages[] = 'Please enter a new address or chooose an address from your address book';
					$emuShop->addMessage( 'shipping-address', $this->messages, 'error' );
					$this->error = true;
					return;
				}

				if( post_val('existing_address') == 'new' || !$address_book->hasAddresses() )
					$address = $this->processNewAddress();
				else
					$address = $this->processExistingAddress();

				// regardless of whether all required fields have been filled...
				$basket->setShippingAddress($address);

				if( !$this->error )
				{
					$location = $emuShop->pageManager->pages->delivery->url;

					do_action( 'emu_shop_'.__CLASS__.'_post_process' );

					$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );

					header( 'Location: '.$location );
					exit();
				}

			break;
		}

	}

	public function processExistingAddress()
	{
		global $emuShop;

		$address_id = post_val('existing_address');
		$address = $emuShop->getModel('emuAddress', $address_id);

		if( post_val('shipping_default') )
		{
			$address->isDefaultShipping = 1;
			$address->update();
		}

		return $address;
	}

	public function processNewAddress()
	{
		global $emuShop;

		extract( $_POST );

		$basket = $emuShop->getBasket();

		$address = $emuShop->getInstance( 'emuAddress' );

		$address->customerID = $basket->customer->dbID;
		$address->firstName = $first_name;
		$address->lastName = $last_name;
		$address->addressLineOne = $address_line_one;
		$address->addressLineTwo = $address_line_two;
		$address->countryID = $country;
		$address->stateID = post_val('selected_state');
		$address->state = post_val('entered_state');
		$address->city = $city;
		$address->postalCode = $postal_code;
		$address->phoneNumber = $phone_number;
		$address->isDefaultShipping = post_val('shipping_default') ? 1 : 0;

		$address->populateRegionNames();

		$this->requiredFields = array( 'first_name', 'last_name', 'address_line_one', 'city', 'postal_code', 'phone_number', 'country', array( 'entered_state', 'selected_state' ) );

		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$emuShop->addMessage( 'shipping-address', $this->messages, 'error' );
			$this->error = true;
		}
		else
		{
			if( post_val( 'save_address' ) ) $address->update();
		}

		return $address;

	}


}

?>
