<?php


class emuBasketDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'basket.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$basket = $emuShop->getBasket( true );

		if( $basket->isEmpty )
		{
			$this->content = '<p>You do not currently have any items in your basket.</p>';
			return;
		}

		// Load the product templates
		$basket_template = $this->template;
		$basket_item_template = $this->templateManager->getTemplateRepeat( 'basket row', $basket_template );

		$basket_items = '';

		foreach( $basket->getItems() as $item )
		{
			$tmp_template = $basket_item_template;
			$tmp_template = $this->templateManager->fillTemplate( $basket_item_template, $item->getTemplateTags(), $item, $item->ID );

			$basket_items .= $tmp_template;
		}

		$orderSummaryDisplay = $emuShop->getInstance( 'emuOrderSummaryDisplay' );
		$orderSummaryDisplay->build();

		$basket_template = $this->templateManager->fillTemplateRepeats( $basket_template, array( 'basket row' => $basket_items ) );

		$basket_template = $this->templateManager->fillTemplate( $basket_template, $basket->getTemplateTags(), $basket );

		$tags = array( 	'basket messages' => $emuShop->getMessages( 'basket' ),
						'order summary' => $orderSummaryDisplay->content );

		$tmp_template = $this->templateManager->fillTemplate( $basket_template, $tags );

		$this->content = apply_filters( 'emu_basket_content', $tmp_template );

	}
}

?>
