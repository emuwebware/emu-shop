<?php


class emuDisplay extends emuUI
{
	public $template;
    public $templateFile;

	public function __construct()
	{
        global $emuShop;

		$this->templateManager = $emuShop->getManager( 'theme' );

        if( $this->templateFile )
            $this->getTemplate();
	}

    public function getTemplate()
    {
        $this->template = $this->templateManager->getTemplate( $this->templateFile );
    }

}

?>
