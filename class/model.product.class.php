<?php


class emuProduct extends emuTemplatedDbPostEntity
{
	public $variantGroups;
	public $parent;

	public $arrCustomVars = array();
	public $productOptions;
	public $reviews;

	// Non db/post fields
	public $postTitle_unfiltered;
	public $postContent_unfiltered;
	public $postExcerpt_unfiltered;
	public $slug;

	// Rating information
	public $ratingCount;
	public $ratingAverage;

	public $customerPurchaseGroup = array();
	public $recommendedProductGroup = array();

	public function __construct( $dbID = null, $post = null, $isVariant = false )
	{
		global $emuShop;

		$this->postType = 'product';
		$this->taxonomy = 'pcat';
		// $this->isVariant = $isVariant;

		if( is_object( $dbID ) )
		{
			$back_trace = debug_backtrace();

			foreach( $back_trace as $trace )
			{
				echo @$trace['file'].' line '.@$trace['line']."<br />\n";
			}
			exit('Incorrect parameter');
		}


		parent::__construct( $dbID, $post, $emuShop->dbPrefix, 'products', $special_field_types = array( 'hasShippingCost' => '%b', 'hasHandlingCost' => '%b', 'isActive' => '%b', 'isVariant' => '%b' ) );

		//////////////////////////////////// Review - shouldn't be loading this in constructor but as required
		$this->getRating();
		////////////////////////////////////

		do_action( 'emu_shop_product_loaded', $this );
	}

	public function registerTemplateTags()
	{
		$this->templateTags = array( 	'product id' => 'dbID',
										'product name' => 'name',
										'product url' => 'permalink',

										'product categories' => 'categories',

										'price' => array( 'this', 'getPrice', PRODUCT_PRICE_FORMATTED ),
										'original price' => array( 'this', 'getPrice', PRODUCT_ORIGINAL_PRICE_FORMATTED ),
										'sale saving' => array( 'this', 'getPrice', PRODUCT_SALE_SAVING_FORMATTED ),
										'sale saving percentage' => array( 'this', 'getPrice', PRODUCT_SALE_SAVING_PERCENTAGE_FORMATTED ),
										'sale price' => array( 'this', 'getPrice', PRODUCT_SALE_PRICE_FORMATTED ),

										'product slug' => 'slug',
										'product description' => 'description',
										'unfiltered product description' => 'unfilteredDescription',
										'unfiltered product excerpt' => 'unfilteredExcerpt',
										'product excerpt' => 'excerpt',
										'rating summary' => 'ratingSummary',
										'concise rating summary' => 'conciseRatingSummary',
										'stock summary' => 'stockSummary',
										'variant description' => 'variantDescription',
										'weight' => 'weight',
										'converted weight' => 'convertedWeight',

										'facebook' => 'facebookLink',
										'product options' => 'productOptionsMarkup',

										'rating stars large' => array( 'this', 'getRatingStars', 'large' ),
										'rating stars small' => array( 'this', 'getRatingStars', 'small' ),
										'variants' => 'variantsDropDown',
										'product image size one' => array( 'this', 'getImage', 'one' ),
										'product image size two' => array( 'this', 'getImage', 'two' ),
										'product image size three' => array( 'this', 'getImage', 'three' ),
										'product image size four' => array( 'this', 'getImage', 'four' ),
										'product image size five' => array( 'this', 'getImage', 'five' ),
										'product image size six' => array( 'this', 'getImage', 'six' ),
										'product image size seven' => array( 'this', 'getImage', 'seven' ),
										'product image url' => array( 'this', 'getImage', 'url' ),

										'edit product link' => 'editProductLink',
										'edit product icon' => 'editProductIcon'  );
	}

	public function formatEmpty($number)
	{
		global $emuShop;

		if( trim( $number ) == $emuShop->currency ) return '-';

		return $number;
	}

	public function getPrice($format = null)
	{
		global $emuShop;

		switch( $format )
		{
			case PRODUCT_ORIGINAL_PRICE:
				return $this->price;

			case PRODUCT_ORIGINAL_PRICE_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getPrice(PRODUCT_ORIGINAL_PRICE) ));

			case PRODUCT_SALE_PRICE:
				return $this->salePrice;

			case PRODUCT_SALE_PRICE_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getPrice(PRODUCT_SALE_PRICE) ));

			case PRODUCT_PRICE:
				$price = $this->getPrice(PRODUCT_ORIGINAL_PRICE);
				$sale_price = $this->getPrice(PRODUCT_SALE_PRICE);

				return strlen( trim( $sale_price ) ) > 0 ? $sale_price : $price;

			case PRODUCT_VARIANT_PRICE_RANGE:
				return $emuShop->productHelper->getPriceRange($this);

			case PRODUCT_SALE_SAVING:
				return $this->price - $this->salePrice;

			case PRODUCT_SALE_SAVING_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getPrice(PRODUCT_SALE_SAVING) ));

			case PRODUCT_SALE_SAVING_PERCENTAGE:
 				$saving = $this->Price(PRODUCT_SALE_SAVING_PERCENTAGE);
 				$perc = ( $saving / $this->price ) * 100;
 				return $perc;

			case PRODUCT_SALE_SAVING_PERCENTAGE_FORMATTED:
				$perc = $this->getPrice(PRODUCT_SALE_SAVING_PERCENTAGE);
 				if( intval( $perc ) <= 0 ) return '0%';
 				return $this->formatEmpty(apply_number_format( 'percentage', $perc )).'%';

 			case PRODUCT_SHIPPING_COST:
 				return $this->shippingCost;

 			case PRODUCT_SHIPPING_COST_FORMATTED:
 				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getPrice(PRODUCT_SHIPPING_COST) ));

 			case PRODUCT_HANDLING_COST:
 				return $this->handlingCost;

 			case PRODUCT_HANDLING_COST_FORMATTED:
 				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getPrice(PRODUCT_HANDLING_COST) ));

			case PRODUCT_PRICE_FORMATTED:
			default:

				if( $this->hasVariants() )
				{
					// then the price is shown as a range
					$price = $this->getPrice(PRODUCT_VARIANT_PRICE_RANGE);
				}
				else
				{
					if( $this->hasSalePrice() )
						$price = $emuShop->productHelper->getSalePriceMarkup($this);
					else
						$price = $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getPrice(PRODUCT_PRICE) ));
				}

				return $price;
		}
	}

	public function getPriceValue()
	{
		return $this->getPrice(PRODUCT_PRICE);
	}

	public function hasSalePrice()
	{
		return strlen( trim( $this->salePrice ) ) > 0;
	}

	public function hasPrice()
	{
		return strlen( trim( $this->price ) ) > 0;
	}

    public function __get( $member )
 	{
 		global $emuShop;

 		if( $member_value = parent::__get( $member ) ) return $member_value;

 		switch( $member )
 		{
 			case 'name':
 				return $this->postTitle;

 			case 'description':
 				return $this->postContent;

 			case 'excerpt':
 				return $this->postExcerpt;

 			case 'unfilteredDescription':
 				return $this->postContent_unfiltered;

 			case 'unfilteredExcerpt':
 				return $this->postExcerpt_unfiltered;

			case 'ratingSummary':
				return $emuShop->productHelper->getRatingSummary($this);

			case 'conciseRatingSummary':
				return $emuShop->productHelper->getConciseRatingSummary($this);

			case 'stockSummary':
				return $emuShop->productHelper->getStockSummary($this);

			case 'convertedWeight':
				return $emuShop->applyUnitConversion($this->weight);

			case 'facebookLink':
				return $emuShop->productHelper->getFacebookLink($this);

			case 'productOptionsMarkup':
				return $emuShop->productHelper->getProductOptionsMarkup($this);

			case 'variantsDropDown':
				return $emuShop->productHelper->getVariantsDropDown( $this->getVariantGroups() );

			case 'editProductLink':
				return $emuShop->productHelper->getEditProductLink($this);

			case 'editProductIcon':
				return $emuShop->productHelper->getEditProductIcon($this);

			case 'isStockTracked':
				return $this->isStockTracked();

			case 'image':
				return $this->getImage();

			case 'hasVariants':
				return $this->isVariant ? false : ($this->getVariantCount() > 0);

			case 'isActiveFormatted':
				return $this->isActive ? 'Active' : 'Inactive';

			default:

				if( !isset( $this->data[ $member ] ) ) return null;
				return $this->data[ $member ];

 		}

 	}

 	public function hasTax()
 	{
 		return (bool) $this->hasTax;
 	}

 	public function hasVariants()
 	{
 		return $this->hasVariants;
 	}

 	public function getVariantCount()
 	{
		//////////////////////////////////////////////////////////////// REVIEW - replace with SQL count
 		if( $this->isVariant )
 			return 0;
 		else
 			return count( $this->getVariants() );
		////////////////////////////////////////////////////////////////
 	}

 	public function isStockTracked()
 	{
 		global $emuShop;

		return ( $this->trackStock || ( $emuShop->trackStock &! $this->trackStock ) );
 	}

	public function getRatingStars( $size )
	{
		global $emuShop;
		return $emuShop->productHelper->getStars( $size, $this );
	}

	public function getRating()
	{
		if( $this->isNewRecord() ) return;

		global $wpdb, $emuShop;

		$sql = "select
				count(er.rating) as ratingCount,
				avg( er.rating ) as ratingAverage
				from
				{$emuShop->dbPrefix}reviews er
				inner join {$wpdb->prefix}posts p on p.ID = er.postID
				where
				er.productID = {$this->dbID} and p.post_status = 'publish'";

		$result = $wpdb->get_row( $sql );

		$this->ratingCount = $result->ratingCount;
		$this->ratingAverage = $result->ratingAverage;

		return $result;
	}

	public function getImage( $size = 'one' )
	{
		global $emuShop;
		return $emuShop->productHelper->getProductImage($size, $this);
	}

	public function getParent()
	{
		global $emuShop;

		if( !$this->isVariant )
		{
			trigger_error( "Can only get the parent of a variant" );
			return null;
		}

		if( !$this->parent )
			$this->parent = $emuShop->getInstance('emuProduct', array( $this->postID, $this->parentID ) );

		return $this->parent;
	}

	public function getVariants( $include_inactive_variants = false )
	{
		if( !$variant_groups = $this->getVariantGroups( $include_inactive_variants ) ) return array();

		$variants = array();

		foreach( $variant_groups as $variant_group )
			$variants = array_merge( $variants, $variant_group->getVariants( $include_inactive_variants ) );

		return $variants;
	}

	public function getVariantGroups( $include_inactive_variants = false )
	{
		global $emuShop, $wpdb;

		if( $this->isVariant )
		{
			trigger_error( "Variants can't have variants..." );
			return null;
		}

		if( $this->isNewRecord() ) {
			return array();
		}

		if( !$this->variantGroups )
		{
			$this->variantGroups = array();

			$sql = "select dbID from {$emuShop->dbPrefix}variant_groups where productID = {$this->dbID}";

			if( $result = $wpdb->get_results( $sql ) )
			{
				foreach( $result as $variant_group )
					$this->variantGroups[] = $emuShop->getInstance('emuVariantGroup', $variant_group->dbID);
			}
		}

		return $this->variantGroups;
	}

	public function setVariantGroup($group)
	{
		if( is_object( $group ) )
			$groupID = $group->getID();
		else if (is_numeric( $group ) )
			$groupID = $group;
		else
		{
			trigger_error( "Variant group must be numeric (dbID) or an instance of emuVariantGroup class" );
			return null;
		}
		$this->variantGroupID = $groupID;
	}

	public function setParent( $product )
	{
		if( is_object( $product ) )
			$dbID = $product->getID();
		else if (is_numeric( $product ) )
			$dbID = $product;
		else
		{
			trigger_error( "Parent must be numeric (dbID) or an instance of emuProduct class" );
			return null;
		}

		$this->parentID = $dbID;
	}

	public function getVariantPriceRange()
	{
		$prices = array();

		$variants = $this->getVariants();

		foreach( $variants as $variant )
		{
			if( $variant->hasPrice() )
				$prices[] = $variant->getPrice(PRODUCT_PRICE);
		}

		$min = count($prices) > 0 ? min($prices) : 0;
		$max = count($prices) > 0 ? max($prices) : 0;

		return (object) array ( 'min' => $min, 'max' => $max );
	}

	public function hasShippingCost()
	{
		return ( $this->shippingOption == emuM_Shipping::OPTION_SPECIFIC_CHARGE || $this->shippingOption == emuM_Shipping::OPTION_CHARGES );
	}

	public function hasHandlingCost()
	{
		return ( $this->handlingOption == emuM_Shipping::OPTION_SPECIFIC_CHARGE || $this->handlingOption == emuM_Shipping::OPTION_CHARGES );
	}

	public function basicDetails()
	{
		$data = array();

		$data['id'] = $this->getID();
		$data['hasVariants'] = $this->hasVariants;
		$data['displayPrice'] = $this->getPrice();
		$data['displayOriginalPrice'] = $this->getPrice(PRODUCT_ORIGINAL_PRICE_FORMATTED);
		$data['displaySalePrice'] = $this->getPrice(PRODUCT_SALE_PRICE_FORMATTED);
		$data['hasTax'] = $this->hasTax();

		switch( $this->shippingOption )
		{
			case emuM_Shipping::OPTION_SPECIFIC_CHARGE:
				$data['displayShippingCost'] = $this->getPrice(PRODUCT_SHIPPING_COST_FORMATTED);
				break;
			case emuM_Shipping::OPTION_CHARGES:
				$data['displayShippingCost'] = 'Yes';
				break;
			case emuM_Shipping::OPTION_NO_CHARGE:
				$data['displayShippingCost'] = 'No';
				break;
		}

		switch( $this->handlingOption )
		{
			case emuM_Handling::OPTION_SPECIFIC_CHARGE:
				$data['displayHandlingCost'] = $this->getPrice(PRODUCT_HANDLING_COST_FORMATTED);
				break;
			case emuM_Handling::OPTION_CHARGES:
				$data['displayHandlingCost'] = 'Yes';
				break;
			case emuM_Handling::OPTION_NO_CHARGE:
				$data['displayHandlingCost'] = 'No';
				break;
		}

		$data['displayActive'] = $this->isActiveFormatted;
		$data['customVars'] = $this->arrCustomVars;

		return array_merge( $this->data, $data );
	}


	public function addCustomVar( $var_name, $var_value )
	{
		$this->arrCustomVars[ $var_name ] = $var_value;
	}

	public function clearCustomVars()
	{
		$this->arrCustomVars = array();
	}

	public function getCustomVar( $var_name )
	{
		if( isset( $this->arrCustomVars[ $var_name ] ) )
			return $this->arrCustomVars[ $var_name ];
		else
			return null;
	}

	// Find the other products that were purchased when this product was purchased
	public function getCustomerPurchaseGroup()
	{
		$this->customerPurchaseGroup = array();
		$purchase_group = array();

		// find purchase group for this product...
		$this->addPurchaseGroup( $this->dbID, $purchase_group );

		// ... and all it's variants
		$variants = $this->getVariants();

		foreach( $variants as $variant )
			$this->addPurchaseGroup( $variant->dbID, $purchase_group );
	}

	public function addPurchaseGroup( $id, &$purchase_group )
	{
		global $wpdb, $emuShop;

		$sql = "select
				  eoi.productID,
				  ep.postID,
				  ep.parentID
				from
				  {$emuShop->dbPrefix}order_items eoi
				inner join {$emuShop->dbPrefix}products ep on ep.dbID = eoi.productID
				inner join {$emuShop->dbPrefix}order_items eoi_child on ( eoi_child.orderID = eoi.orderID and eoi_child.productID = $id )
				where not eoi.productID = $id";

		if( ! $product_rs = $wpdb->get_results( $sql ) ) return;

		foreach( $product_rs as $product )
		{
			// Only want the product parent .. for the moment anyway!
			if( $product->parentID )
				$product_id = $product->parentID;
			else
				$product_id = $product->productID;

			if( !in_array( $product_id, $purchase_group ) && $product_id != $this->dbID )
			{
				$this->customerPurchaseGroup[] = $emuShop->getInstance( 'emuProduct', $product_id );
				$purchase_group[] = $product_id;
			}
		}
	}


	// Find the other products in this category
	public function getRecommendedProductGroup( $number = 5 )
	{
		global $emuShop;

		$this->recommendedProductGroup = array(); // reset

		// See if there are specific items in the custom field 'Recommended Items'
		if( $recommended_items = $this->getCustomField( 'Recommended Items' ) )
		{
			$recommended_items = split( ',', $recommended_items );

			if( !is_array( $recommended_items ) ) $recommended_items = array( $recommended_items );

			// get each of the items
			foreach( $recommended_items as $item )
			{
				if( $rProduct = $emuShop->getInstance('emuProduct', $item ) )
				{
					$this->recommendedProductGroup[] = $rProduct;
				}
			}
			return;
		}

		// if there aren't any recommended menu items then just get other items from the
		// same category

		$cats = array();

		foreach( wp_get_object_terms($this->post->ID, 'pcat') as $term )
			$cats[] = $term->slug;

		$products = $emuShop->productManager->getProducts( array( 'showposts' => $number, 'pcat' => @$cats[0], 'exclude' => $this->postID ) );

		$this->recommendedProductGroup = $products;
	}

	public function getCustomField( $meta_name )
	{
		return get_post_meta( $this->postID, $meta_name, true );
	}

	public function getReviews( $options = array(), $force_refresh = false )
	{
		global $wpdb, $emuShop;

		if( !$this->reviews || $force_refresh )
		{
			// Reset any previous queries
			$this->reviews = array();

			$defaults = array( 'mostHelpfulFavorable' => false, 'mostHelpfulCritical' => false, 'defaultList' => true );

			$option = wp_parse_args( $options, $defaults );
			$option = (object) $option;

			$sql = '';

			if( $option->defaultList )
				$sql = "select
							r.postID,
							( r.helpfulYesCount / ( r.helpfulYesCount + r.helpfulNoCount ) ) as helpfulRatio
						from
							{$emuShop->dbPrefix}reviews r
						inner join {$wpdb->prefix}posts p on p.ID = r.postID
						where
							p.post_status = 'publish' and p.post_type = 'review' and
							r.productID = {$this->dbID}
						order by helpfulRatio desc";

			if( $option->mostHelpfulFavorable )
				$sql = "select
							r.postID,
							( r.helpfulYesCount / ( r.helpfulYesCount + r.helpfulNoCount ) ) as helpfulRatio
						from
							{$emuShop->dbPrefix}reviews r
						inner join {$wpdb->prefix}posts p on p.ID = r.postID
						where
							p.post_status = 'publish' and p.post_type = 'review' and
							r.productID = {$this->dbID} and r.rating > 3
						order by helpfulRatio desc limit 1";

			if( $option->mostHelpfulCritical )
				$sql = "select
							r.postID,
							( r.helpfulYesCount / ( r.helpfulYesCount + r.helpfulNoCount ) ) as helpfulRatio
						from
							{$emuShop->dbPrefix}reviews r
						inner join {$wpdb->prefix}posts p on p.ID = r.postID
						where
							p.post_status = 'publish' and p.post_type = 'review' and
							r.productID = {$this->dbID} and r.rating < 4
						order by helpfulRatio desc limit 1";

			if( !$sql ) return;

			if( $result = $wpdb->get_results( $sql ) )
			{
				foreach( $result as $row )
					$this->reviews[] = $emuShop->getInstance( 'emuReview', $row->postID );
			}
		}

		return $this->reviews;
	}

	public function hasProductOptions()
	{
		return $this->getProductOptions() ? true : false;
	}

	public function getProductOptions()
	{
		if( !$this->productOptions )
		{
			$options = get_post_meta( $this->postID, 'Product Options', true );

			if($options == '') return false;

			$eachopt = explode("\n",$options);

			$arr_options = array();

			foreach($eachopt as $e)
			{
				$name = $position = $type = $label = '';
				$options = array();

				if( preg_match('/(.*?)name=("|\')(?P<name>.*?)("|\')(.*?)/',$e,$matches) )
					$name = $matches['name'];

				if( preg_match('/(.*?)position=("|\')(?P<position>.*?)("|\')(.*?)/',$e,$matches) )
					$position = $matches['position'];

				if( preg_match('/(.*?)options=("|\')(?P<options>.*?)("|\')(.*?)/',$e,$matches) )
				{
					$options = $matches['options'];
					unset($trimmed);

					foreach (explode(',',$options) as $o)
					{
						$o = trim($o,', ');//trim commas and spaces
						if(trim($o) !='') $trimmed[] = trim($o);//make sure we dont add crap to the options list
					}

					$options = $trimmed;
				}

				if( preg_match('/(.*?)type=("|\')(?P<type>.*?)("|\')(.*?)/',$e,$matches) )
					$type = $matches['type'];

				if( preg_match('/(.*?)label=("|\')(?P<label>.*?)("|\')(.*?)/',$e,$matches) )
					$label = $matches['label'];

				if( $label == '' ) $label = $name;

				$arr_options[$position] = (object) array( 'name' => $name, 'options' => $options, 'type' => $type, 'label' => $label );
			}

			ksort( $arr_options );
			$this->productOptions = $arr_options;
		}

		return $this->productOptions;
	}//function getProductOptions

	// Overrides
	///////////////////////////////////////////////////////////////////////////////////////////
	public function getPostData( $post )
	{
		parent::getPostData($post);

		$this->postTitle_unfiltered = $this->post->post_title;
		$this->postContent_unfiltered = $this->post->post_content;
		$this->postExcerpt_unfiltered = $this->post->post_excerpt;
		$this->slug = $post->post_name;
	}

	public function save()
	{
		global $wpdb, $emuShop;

		if( count( $this->arrCustomVars ) > 0 )
			$this->customVars = serialize( $this->arrCustomVars );
		else
			$this->customVars = '';

		parent::save();
	}

	public function getData()
	{
		parent::getData();

		if( $this->customVars )
			$this->arrCustomVars = unserialize( $this->customVars );
	}

	public function getUpdateConditions()
	{
		$conditions = parent::getUpdateConditions();

		// If we're only being provided a post object/post id and not a db id then
		// we need to make sure that only the product is updated, not any variants.
		// So we add the variant field to the update conditions

		if( !$this->isVariant && !$this->dbID )
			$conditions['isVariant'] = false;

		return $conditions;
	}

	public function deletePost()
	{
		if( $this->isVariant ) return;
		parent::deletePost();
	}

	public function deleteRecord()
	{
		parent::deleteRecord();

		if( $this->isVariant ) return;

		// Otherwise we need to delete the product variants and variant groups
		foreach( $this->getVariantGroups() as $variant_group )
			$variant_group->delete();
	}
	///////////////////////////////////////////////////////////////////////////////////////////

}


?>
