<?php



class emuShopCommon extends emuDB
{
	public function getTemplateTags( $prefix = '' )
	{
		if( !isset( $this->templateTags ) ) return array();
		
		if( $prefix )
		{
			// rebuild the template tags
			$new_tags = array();
			
			foreach( $this->templateTags as $tag => $value )
				$new_tags[$prefix.$tag] = $value;
			
			return $new_tags;
		}
		
		return $this->templateTags;
		
	}
	
	public function sanitize( $string )
	{
		if( is_array( $string ) )
		{
			foreach( $string as $key => $value )
				$string[$key] = $this->sanitize( $value );
			
			return $string;
		}
		
		return htmlentities($string);
	}
	
}


?>
