<?php

class emuM_Regions extends emuManager
{
	public function init()
	{
		$this->registerAjaxAction( 'get_states', array( $this, 'AJAX_getStates' ) );
	}

	public function AJAX_getStates()
	{
		$country_id = request_val('country_id');

		header('Content-type: application/json');
		echo json_encode( $this->getStates( array('country' => $country_id) ) );
	}

	public function populateCountryData()
	{
		global $emuShop;

		$country_csv_list = $emuShop->getData( 'countries' );

		$countries = explode( ',', $this->cleanCSV( $country_csv_list ) );

		if( $this->insertCountries( $countries, true ) )
		{
			if( $country = $this->getCountries( array( 'name' => 'United States' ) ) )
			{
				$states_csv_list = $emuShop->getData( 'us_states' );

				$states = explode( ',', $this->cleanCSV( $states_csv_list ) );

				$this->insertStates( $states, $country[0]->ID, true );
			}
		}
	}

	function getCountries($terms = array())
	{
		global $wpdb, $emuShop;

		$sql_add = array();

		// get the ID for america
		$sql = "select * from {$emuShop->dbPrefixShared}countries %s";

		if(isset($terms['name'])) $sql_add[] = "name = '{$terms['name']}'";
		if(isset($terms['ID'])) $sql_add[] = "id = '{$terms['ID']}'";
		$sql_add[] = 'active = '.( isset( $terms['active'] ) ? $terms['active'] : '1' );

		$sql_add = count($sql_add) > 0 ? 'where ('.implode(') and (', $sql_add).')' : '';

		$sql = sprintf($sql, $sql_add);

		$countries_rs = $wpdb->get_results( $sql );

		return $countries_rs;

	}

	function getStates($terms = array())
	{
		global $wpdb, $emuShop;

		$sql_add = array();

		// get the ID for america
		$sql = "select * from {$emuShop->dbPrefixShared}states %s";

		if(isset($terms['name'])) $sql_add[] = "name = '{$terms['name']}'";
		if(isset($terms['ID'])) $sql_add[] = "id = '{$terms['ID']}'";
		if(isset($terms['country'])) $sql_add[] = "country_id = '{$terms['country']}'";
		$sql_add[] = 'active = '.( isset( $terms['active'] ) ? $terms['active'] : '1' );

		$sql_add = count($sql_add) > 0 ? 'where ('.implode(') and (', $sql_add).')' : '';

		$sql = sprintf($sql, $sql_add);

		$states_rs = $wpdb->get_results( $sql );

		return $states_rs;
	}

	function cleanCSV($csv_list)
	{
		$csv_list = preg_replace( "/[\r\n\t]/", '', $csv_list );
		$csv_list = preg_replace( "/( )+?,( )+?/", ',', $csv_list );

		return $csv_list;

	}

	public function insertCountries($countries, $replace_existing = false)
	{
		global $wpdb, $emuShop;

		if( $replace_existing ) $wpdb->query( "delete from {$emuShop->dbPrefixShared}countries" );

		if( !is_array( $countries ) ) $countries = array( $countries );

		$sql_insert_values = '("'.implode('"),("', $countries).'")';

		$sql_insert = "insert into {$emuShop->dbPrefixShared}countries (name) values $sql_insert_values";

		return $wpdb->query( $sql_insert );
	}

	public function insertStates($states, $country_id, $replace_existing = false)
	{
		global $wpdb, $emuShop;

		if( $replace_existing ) $wpdb->query( "delete from {$emuShop->dbPrefixShared}states" );

		$sql_insert_values = array();

		if( !is_array( $states ) ) $states = array( $states );

		for( $n = 0; $n < count( $states ); $n++ )
			$sql_insert_values[] = "('{$states[$n]}', $country_id)";

		$sql_insert = "insert into {$emuShop->dbPrefixShared}states (name, country_id) values ".implode( ',', $sql_insert_values );

		return $wpdb->query( $sql_insert );
	}

	function getRegionOptions( $country_id = null, $state_id = null, $state = null )
	{
		return $this->emuApp->regionHelper->getRegionOptions( $country_id, $state_id, $state );
	}

	function buildStateOptions( $states, $state_id )
	{
		return $this->emuApp->regionHelper->buildStateOptions( $states, $state_id );
	}

	function getCountryOptions( $country_id = null )
	{
		$countries = $this->getCountries();

		$country_dd = '';

		foreach( $countries as $country )
		{
			$selected = $country->ID == $country_id ? ' selected="selected"' : '';

			$country_dd .= '<option value="'.$country->ID.'"'.$selected.'>'.$country->name.'</option>';
		}

		return $country_dd;

	}

	public function loadCountryData()
	{
		if( count( $this->getCountries() ) == 0 ) $this->populateCountryData();
	}

    public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		// countries
		$sql = "CREATE TABLE {$emuShop->dbPrefixShared}countries (
		ID int(10) NOT NULL AUTO_INCREMENT,
		name varchar(80) default NULL,
		tax float(2) default NULL,
		active BOOLEAN NOT NULL DEFAULT 1,
		UNIQUE KEY id (ID)
		);";

		dbDelta($sql);

		// countries
		$sql = "CREATE TABLE {$emuShop->dbPrefixShared}states (
		ID int(10) NOT NULL AUTO_INCREMENT,
		country_id int(10) NULL,
		name varchar(80) default NULL,
		tax float(2) default NULL,
		active BOOLEAN NOT NULL DEFAULT 1,
		UNIQUE KEY id (ID)
		);";

		dbDelta($sql);

		add_action( $emuShop->emuAppID.'_installed_loaded', array( $this, 'loadCountryData' ) );
    }

}


?>
