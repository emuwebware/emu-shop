<?php



class emuBillingDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'billing.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$basket = $emuShop->getBasket();

		$customer = $emuShop->getCustomer();

		$address_book = $customer->getAddressBook();

		$tM = $this->templateManager;

		$template = $this->template;
		$template = $tM->setTemplateConditionals( $template, array( 'has address book entries' => $address_book->hasAddresses() ));

		$billing_address = $basket->hasBillingAddress();

		if( $address_book->hasAddresses() )
		{
			$existing_address_template = $tM->getTemplateRepeat( 'existing address', $template );

			$existing_addresses = '';

			foreach( $address_book->getAddresses() as $address )
			{
				if( $billing_address )
					$address_checked = !$billing_address->isNewRecord() && $billing_address->getID() == $address->getID();
				else
					$address_checked = $address->isDefaultBilling;

				$tags = array( 'existing address checked' => $address_checked ? 'checked="checked"' : '' );

				$existing_address = $tM->fillTemplate( $existing_address_template, $address->getTemplateTags(), $address );
				$existing_address = $tM->fillTemplate( $existing_address, $tags );

				$existing_addresses .= $existing_address;
			}

			$template = $tM->fillTemplateRepeats( $template, array( 'existing address' => $existing_addresses ) );
		}

		$orderSummaryDisplay = $emuShop->getInstance( 'emuOrderSummaryDisplay' );
		$orderSummaryDisplay->build();

		// If we do have a billing address, and it's a new billing address then populate the form with that billing address object
		if( $billing_address && $billing_address->isNewRecord() )
			$form_address = $billing_address;
		else // Populate with empty address
		{
			$form_address = $emuShop->getModel('emuAddress');

			// Prepopulate with customer details (if there are any)
			$form_address->firstName = $customer->firstName;
			$form_address->lastName = $customer->lastName;
			$form_address->email = $customer->email;
		}

		$template = $tM->fillTemplate( $template, $form_address->getTemplateTags(), $form_address );

		$region_options = $emuShop->regionHelper->getRegionOptions( $form_address->countryID, $form_address->stateID, $form_address->state );

		$tags = array(	'new address checked' => $billing_address && $billing_address->isNewRecord() ? ' checked="checked"' : '',
						'countries' => $region_options->countryDropDown,
						'selected state class' => $region_options->stateDropDownClass,
						'states' => $region_options->stateDropDown,
						'entered state' => $region_options->stateTextInput,
						'entered state class' => $region_options->stateTextInputClass,
						'shipping same checked' => post_val('shipping_same') ? ' checked="checked"' : '',
						'order summary' => $orderSummaryDisplay->content,
						'billing address messages' => $emuShop->getMessages('billing-address')
						);

		$template = $tM->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_billing_content', $template );
	}

}

?>
