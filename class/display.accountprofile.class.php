<?php



class emuAccountProfileDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'account-profile.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$customer = $emuShop->getManager('customer')->getCustomer();

		$_POST['email'] = $customer->email;

		if( post_val('e-action') ) // in the case where there was a problem with the submitted form we want to populate the fields with the new submitted values
		{
			$customer->firstName = post_val('first_name');
			$customer->lastName = post_val('last_name');
			$customer->allowEmailContact = post_val( 'newsletter' ) ? 1 : 0;
		}

		$tags = array( 	'first name' => $customer->firstName,
						'last name' => $customer->lastName,
						'username' => $customer->username,
						'prefix' => $customer->prefix,
						'suffix' => $customer->suffix,
						'company name' => $customer->companyName,
						'department' => $customer->department,
						'job title' => $customer->jobTitle,
						'newsletter checkbox' => $customer->allowEmailContact ? ' checked="checked"' : '',
						'email' => post_val('email'),
						'new email' => post_val('new_email'),
						'new email confirm' => post_val('new_email_confirm'),
						'new password' => post_val('new_password'),
						'new password confirm' => post_val('new_password_confirm'),
						'messages' => $emuShop->getMessages('account-profile') );

		$this->content = $this->templateManager->fillTemplate( $this->template, $tags );
		$this->content = apply_filters( 'emu_account_profile_content', $this->content );
	}
}

?>
