<?php

class emuBillingProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();
		$customer = $basket->customer;

		switch( $this->button )
		{
			case 'Edit Address':

				$emuShop->setSessionData( array( 	'e-action' => 'address',
													'e-method' => 'edit',
													'address_id' => post_val('existing_address'),
													'return' => $emuShop->pageManager->pages->billing->url ) );

				header( 'Location: '.$emuShop->pageManager->pages->addressBook->url );

				exit();

			case 'Continue':

				$address_book = $customer->getAddressBook();

				if( !post_val('existing_address') && $address_book->hasAddresses() )
				{
					$this->messages[] = 'Please enter a new address or chooose an address from your address book';
					$emuShop->addMessage( 'billing-address', $this->messages, 'error' );
					$this->error = true;
					return;
				}

				if( post_val('existing_address') == 'new' || !$address_book->hasAddresses() )
					$address = $this->processNewAddress();
				else
					$address = $this->processExistingAddress();

				// regardless of whether all required fields have been filled...
				$basket->setBillingAddress($address);

				if( !$this->error )
				{
					if( $shipping_address_same = post_val('shipping_same') )
					{
						$basket->setShippingAddress($address);
					}

					if( $emuShop->shippingChargesEnabled && !$shipping_address_same )
                        $location = $emuShop->pageManager->pages->shipping->url;
                    else
                    	$location = $emuShop->pageManager->pages->delivery->url;

					do_action( 'emu_shop_'.__CLASS__.'_post_process' );

					$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );

					header( 'Location: '.$location );
					exit();
				}

			break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

	}

	public function processExistingAddress()
	{
		global $emuShop;

		$address_id = post_val('existing_address');
		$address = $emuShop->getModel('emuAddress', $address_id);

		if( post_val('billing_default') )
		{
			$address->isDefaultBilling = 1;
			$address->update();
		}

		return $address;
	}

	public function processNewAddress()
	{
		global $emuShop;

		extract( $_POST );

		$basket = $emuShop->getBasket();

		$address = $emuShop->getInstance( 'emuAddress' );

		$address->customerID = $basket->customer->dbID;
		$address->firstName = $first_name;
		$address->lastName = $last_name;
		$address->addressLineOne = $address_line_one;
		$address->addressLineTwo = $address_line_two;
		$address->countryID = $country;
		$address->stateID = post_val('selected_state');
		$address->state = post_val('entered_state');
		$address->email = post_val('email_address');
		$address->city = $city;
		$address->postalCode = $postal_code;
		$address->phoneNumber = $phone_number;
		$address->isDefaultBilling = post_val('billing_default') ? 1 : 0;

		$address->populateRegionNames();

		$this->requiredFields = array( 'first_name', 'last_name', 'address_line_one', 'city', 'postal_code', 'phone_number', 'country', array( 'entered_state', 'selected_state' ) );

		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$emuShop->addMessage( 'billing-address', $this->messages, 'error' );
			$this->error = true;
		}
		else
		{
			if( post_val( 'save_address' ) ) $address->update();
		}

		return $address;

	}

}



// class emuBillingProcessor extends emuProcessor
// {
// 	public $requiredFields = array();

// 	public function __construct()
// 	{
// 		parent::__construct();
// 	}

// 	public function process()
// 	{
// 		global $emuShop;

// 		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

// 		$basket = $emuShop->getBasket();

// 		switch( $this->section )
// 		{
// 			case 'existing-address':

// 				$this->processExistingAddress( $basket );

// 			break;
// 			case 'new-address':

// 				$this->processNewAddress( $basket );

// 			break;
// 		}

// 		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

// 		if( !$this->error )
// 		{

// 			$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $emuShop->pageManager->pages->delivery->url );
// 			header( 'Location: '.$location );
// 			exit();
// 		}
// 	}

// 	public function processExistingAddress( $basket )
// 	{
// 		global $emuShop;

// 		$this->requiredFields = array( 'billing_address' );

// 		$this->checkRequiredFields();

// 		if( !$this->hasRequiredFields )
// 		{
// 			$this->messages[] = 'Not all required fields were provided - check those marked with *';
// 			$emuShop->addMessage( 'existing-billing', $this->messages, 'error' );
// 			$this->error = true;
// 			return;
// 		}

// 		switch( $this->button )
// 		{
// 			case 'Edit':

// 				$basket->setSessionData( array( 'e-action' => 'address', 'e-method' => 'edit', 'address_id' => post_val('billing_address'), 'return' => $emuShop->pageManager->pages->billing->url ) );

// 				header( 'Location: '.$emuShop->pageManager->pages->addressBook->url );
// 				exit();

// 			break;
// 			case 'Continue':

// 				$address = $emuShop->getInstance( 'emuAddress', array( post_val('billing_address') ) );

// 				$basket->setSessionData( 'billing_address', serialize( $address ) );
// 			break;
// 		}
// 	}

// 	public function processNewAddress( $basket )
// 	{
// 		global $emuShop;

// 		$this->requiredFields = array( 'first_name', 'last_name', 'address_line_one', 'city', 'postal_code', 'phone_number', 'country', array( 'entered_state', 'selected_state' ) );

// 		$this->checkRequiredFields();

// 		if( !$this->hasRequiredFields )
// 		{
// 			$this->messages[] = 'Not all required fields were provided - check those marked with *';
// 			$emuShop->addMessage( 'new-billing', $this->messages, 'error' );
// 			$this->error = true;
// 			return;
// 		}

// 		extract( $_POST );

// 		$customer = $basket->customer;

// 		// creating a new address
// 		$address = $emuShop->getInstance( 'emuAddress' );
// 		$address->customerID = $customer->dbID;

// 		$address->firstName = $first_name;
// 		$address->lastName = $last_name;
// 		$address->addressLineOne = $address_line_one;
// 		$address->addressLineTwo = $address_line_two;
// 		$address->countryID = $country;
// 		$address->stateID = post_val('selected_state');
// 		$address->state = post_val('entered_state');
// 		$address->city = $city;
// 		$address->postalCode = $postal_code;
// 		$address->phoneNumber = $phone_number;

// 		if( post_val( 'save_address' ) ) $address->update();

// 		$address->populateRegionNames();

// 		$basket->setSessionData( 'billing_address', serialize( $address ) );
// 	}


// }

?>
