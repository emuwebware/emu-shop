<?php

class emuM_Gateways extends emuManager
{
	public $paymentGateways = array();
	public $paymentGatewayDir;

	public function init()
	{
		$this->paymentGatewayDir = $this->emuApp->pluginPath.'/gateway';

		add_action( 'shop_settings_loaded', array( $this, 'initPaymentGateways' ) );
	}

	// gateway manager
	function initPaymentGateways( $emuShop )
	{
		$this->paymentGateways = $this->getPaymentGateways( $emuShop );

		// Update the payment gateway setting with the list of gateways

		$coreSettings = $emuShop->settings->getSettingsGroup( $emuShop->emuAppID.'_core' );

		$arr_gateways = array( '' => 'Select a gateway' );

		foreach( $this->paymentGateways as $gateway_name => $gateway )
			$arr_gateways[ $gateway_name ] = $gateway_name;

		$setting = $coreSettings->getSetting( 'paymentGateway' );

		$setting->type = 'option';
		$setting->options = $arr_gateways;

		$emuShop->settings->saveSettings();

		add_action( 'init', array( $this, 'loadPaymentGateway' ) );
	}

	// gateway manager
	function loadPaymentGateway()
	{
		global $emuShop;

		if( !( $emuShop->paymentGateway ) ) return;

		if( !isset( $this->paymentGateways[ $emuShop->paymentGateway ] ) ) return;

		$gateway = $this->paymentGateways[ $emuShop->paymentGateway ];

		include_once( $gateway['path'] );
	}

	// gateway manager
	function getPaymentGateways( $emuShop )
	{
		$gateway_files = $emuShop->getFiles( $this->paymentGatewayDir );

		$emu_gateways = array();

		if ( is_array( $gateway_files ) ) {

			foreach ( $gateway_files as $gateway_file ) {

				$basename = str_replace( $this->paymentGatewayDir.'/', '', $gateway_file );

				if( file_exists( $gateway_file ) )
				{
					$file_data = implode( '', file( $gateway_file ));

					$name = '';
					$description = '';

					if ( preg_match( '|Emu Payment Gateway:(.*)$|mi', $file_data, $name ) )
						$name = _cleanup_header_comment($name[1]);

					if ( preg_match( '|Emu Payment Gateway Description:(.*)$|mi', $file_data, $description ) )
						$description = _cleanup_header_comment($description[1]);

					if ( !empty( $name ) )
					{
						$emu_gateways[$name] = array( 'name' => $name, 'description' => $description, 'path' => $gateway_file, 'basename' => $basename );
					}
				}
			}
		}

		return $emu_gateways;

	}


    public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

    }

}


?>
