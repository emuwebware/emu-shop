<?php


class emuPaymentManager extends emuDB
{
	public $paymentMethods = array();
	public $messages = array();
	public $error = false;
	public $data = array();

	public $paymentMethodIDs = array();
	public $defaultPM;

	public function __construct( $customer )
	{
		if( is_numeric( $customer ) )
		{
			$this->customerID = $customer;
		}
		else if( is_object( $customer ) )
		{
			$this->customerID = $customer->dbID;
		}

		// if( $this->customerID ) $this->getPaymentMethods();
	}

	public function __get( $member )
	{
		global $wpdb, $emuShop;

		switch( $member )
		{
			case 'defaultPaymentMethod':

				if( $this->defaultPM ) return $this->defaultPM;

				foreach( $this->paymentMethods as $paymentMethod )
				{
					if( $paymentMethod->isDefaultPaymentMethod )
					{
						$this->defaultPM = $paymentMethod;
						return $paymentMethod;
					}
				}

				return false;

			default:

				if( !isset( $this->data[ $member ] ) ) return null;

				return $this->data[ $member ];
		}
    }

    public function __set( $member, $value )
	{
        $this->data[ $member ] = $value;
    }

    public function hasPaymentMethods()
    {
		return count( $this->getPaymentMethods() ) > 0;
    }

	public function getPaymentMethodListforDropDown()
	{
		$payment_methods = array();

		foreach( $this->paymentMethods as $paymentMethod )
		{
			$payment_methods[$paymentMethod->dbID] = $paymentMethod->getDisplayPaymentMethod( ', ' );
		}

		return $payment_methods;
	}

	public function getPaymentMethods()
	{
		if( !$this->customerID ) return;

		global $wpdb, $emuShop;

		// Reset
		$this->paymentMethods = array();
		$this->paymentMethodIDs = array();

		$rs = $wpdb->get_results( "select dbID from {$emuShop->dbPrefixShared}payment_methods where customerID = {$this->customerID}" );

		if( $rs )
		{
			for( $n = 0; $n < count( $rs ); $n++ )
			{
				$this->paymentMethods[] = $emuShop->getInstance( 'emuPaymentMethod', array( $rs[$n]->dbID ) );
				$this->paymentMethodIDs[] = $rs[$n]->dbID;
			}
		}

		return $this->paymentMethods;
	}

	public function addPaymentMethod( $payment_method )
	{
		global $emuShop;

		if( is_array( $payment_method ) )
		{
			$payment_methods = $payment_method;
			foreach( $payment_methods as $payment_method ) $this->addPaymentMethod( $payment_method );
			return;
		}

		$newPaymentMethod = $emuShop->getInstance( 'emuPaymentMethod' );
		$newPaymentMethod->data = (array) $payment_method;

		$this->paymentMethods[] = $newPaymentMethod;
		$this->messages[] = 'Payment method added.';
	}

	public function removePaymentMethod( $paymentMethod )
	{
		if( is_object( $paymentMethod ) )
			$dbID = $paymentMethod->dbID;
		else if ( is_numeric( $paymentMethod ) )
			$dbID = $paymentMethod;
		else return;

		$payment_methods = array();

		foreach( $this->paymentMethods as $paymentMethod )
		{
			if( $paymentMethod->dbID != $dbID ) $payment_methods[] = $paymentMethod;
		}

		$this->paymentMethods = $payment_methods;

	}

	public function removePaymentMethodFromDB( $paymentMethod )
	{
		if( is_object( $paymentMethod ) )
			$dbID = $paymentMethod->dbID;
		else if ( is_numeric( $paymentMethod ) )
			$dbID = $paymentMethod;
		else return;

		global $wpdb, $emuShop;

		$wpdb->query( "delete from {$emuShop->dbPrefixShared}payment_methods where dbID = {$dbID}" );
	}

	public function update()
	{
		$arr_method_ids = array();

		foreach( $this->paymentMethods as $paymentMethod ) $arr_method_ids[] = $paymentMethod->dbID;

		// do we need to remove any payment methods?
		foreach( $this->paymentMethodIDs as $paymentMethodID )
		{
			if( !in_array( $paymentMethodID, $arr_method_ids ) ) $this->removePaymentMethodFromDB( $paymentMethodID );
		}

		// update remaining payment methods
		foreach( $this->paymentMethods as $paymentMethod )
		{
			$paymentMethod->customerID = $paymentMethod->customerID;
			$paymentMethod->update();
		}

		return true;
	}

}

?>
