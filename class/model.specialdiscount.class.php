<?php

class emuSpecialDiscount
{
    public $discount;
    public $description;

    public function getDiscount($format = null)
    {
        global $emuShop;

        switch( $format )
        {
            case SPECIAL_DISCOUNT:
                return $this->discount;

            case SPECIAL_DISCOUNT_FORMATTED:
            default:
                return $emuShop->currency.apply_number_format( 'currency', $this->getDiscount(SPECIAL_DISCOUNT) );
        }
    }

    public function getDiscountValue()
    {
        return $this->getDiscount(SPECIAL_DISCOUNT);
    }

}

?>
