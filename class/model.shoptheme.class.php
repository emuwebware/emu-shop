<?php

class emuShopTheme
{
    public $name;
    public $description;
    public $author;
    public $version;
    public $url;
    public $path;
    public $scriptPath;
    public $libDir;
    public $parent;
    public $stylesheetPath;
    public $stylesheetURL;
    public $scriptURL;
    public $libURL;
    public $imageURL;
    public $basename;

    public $parentTheme;

	public function getTemplate($filename)
	{
        $template_path = $this->getTemplatePath($filename);

        if( !file_exists( $template_path ) )
        {
            // See if the theme has a parent theme
            if( !$parent_theme = $this->hasParent() )
            {
                trigger_error( "{$template_path} does not exist", E_USER_WARNING );
                return '';
            }

            return $parent_theme->getTemplate($filename);
        }

        $theme_template = file_get_contents( $template_path );
        $theme_template = apply_filters_ref_array( 'emu_shop_get_theme_template', array( $theme_template, $filename ) );

        return $theme_template;
	}

	public function getStyleSheetURL()
	{
		if( !file_exists( $this->stylesheetPath ) )
		{
			// Get the stylesheet from the parent
			if( !$parent_theme = $this->hasParent() )
            	return '';

            return $parent_theme->getStyleSheetURL();
		}
		return $this->stylesheetURL;
	}

	public function getParentTheme()
	{
		if( !$this->parent ) return false;

        if( !$this->parentTheme )
        {
    		global $emuShop;
    		$this->parentTheme = $emuShop->themeManager->getTheme($this->parent);
        }

        return $this->parentTheme;
	}

	public function getTemplatePath($filename)
	{
		return $this->path.'/'.$filename;
	}

	public function hasParent()
	{
		if( !$parent_theme = $this->getParentTheme() ) return false;

		return $parent_theme;
	}


    function getScriptURLs()
    {
        global $emuShop;

        $script_urls = array();

        foreach( $emuShop->getFiles( $this->libDir ) as $lib_file )
        {
            $filename = basename($lib_file);
            $script_urls[$filename] = $this->libURL.'/'.$filename;
        }

    	// Lastly add in the main script
    	if( file_exists( $this->scriptPath ) )
            $script_urls['emu-shop-theme'] = $this->scriptURL;

        if( count($script_urls) == 0 )
        {
        	if( $parent_theme = $this->hasParent() )
        		return $parent_theme->getScriptURLs();
        }

        return $script_urls;
    }


}

?>
