<?php

class emuM_Promotions extends emuManager
{
	public function registerClasses()
	{
        $this->emuApp->registerClass( 'emuCoupon', 'model.coupon.class.php', array('emuShopCommon') );
	}

    public function registerAdminPages()
    {
        $this->emuApp->registerAdminPage( array( 'name' => 'Coupons',
                                    'filename' => 'coupons.php',
                                    'position' => 5,
                                    'styles' => array( 'emu-common' ),
                                    'scripts' => array() ) );
    }

    function getDiscountTypes()
    {
        return array( 'currency' => $this->emuApp->currency, 'percentage' => '%' );
    }


    function getCoupons()
    {
        global $wpdb, $emuShop;

        $coupons = array();

        $result = $wpdb->get_results( "select dbID from {$emuShop->dbPrefix}coupons" );

        foreach( $result as $row )
        {
            $coupons[] = $emuShop->getInstance( 'emuCoupon', array( $row->dbID ) );
        }
        return $coupons;
    }



    public function install( $emuShop = null )
    {
        if( !$emuShop ) return;

        global $wpdb;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        // coupons
        $sql = "CREATE TABLE {$emuShop->dbPrefix}coupons (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        code varchar(300) default NULL,
        discount float(2) default NULL,
        discountType varchar(30) default NULL,
        startDate DATETIME default NULL,
        endDate DATETIME default NULL,
        applyFrom float(2) default NULL,
        applyTo float(2) default NULL,
        isActive BOOLEAN NULL DEFAULT 1,
        freeShipping BOOLEAN NULL DEFAULT 0,
        maxUses int(10) NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

    }

}


?>
