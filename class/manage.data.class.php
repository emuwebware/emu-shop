<?php

class emuM_Data extends emuManager
{
	public $pages;

	public function init()
	{
		// Pick up any export requests
		add_action( 'init', array( $this, 'processExport' ) );
	}

	public function registerAdminPages()
	{
		global $emuShop;

		$emuShop->registerAdminPage( array( 'name' => 'Import / Export',
									'filename' => 'import-export.php',
									'position' => 10,
									'styles' => array( 'emu-common', 'emu-ie-admin' ),
									'scripts' => array( 'emu-ie-admin' ) ) );
	}
	function processExport()
	{
		if( post_val( 'emu-shop-export' ) )
		{
			$options = array( 'templates' => post_val('templates'), 'etemplates' => post_val('etemplates') );

			$export_data = $this->getExportFile( $options, post_val( 'emu-shop-export' ) == 'Export All' );

			// To get around issues with quotes, commas and a few other characters
			$export_data = base64_encode( $export_data );

			$filename = preg_replace( '/[^a-zA-Z0-9 ]/', '', get_bloginfo( 'name' ) );
			$filename = str_replace( ' ', '-', $filename ).date('-Y-m-d-hi').'.emu';

			header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false);
			header("Content-Type: application/txt" );
			header("Content-Disposition: attachment; filename=\"".$filename."\";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".$this->strBytes( $export_data ));

			exit ( $export_data );
		}
	}

	function getExportFile( $options, $export_all = false )
	{
		global $emuShop;

		$defaults = array( 'templates' => false, 'etemplates' => false );
		$options = wp_parse_args( $options, $defaults );
		$options = (object) $options;

		$templates = array();
		$etemplates = array();

		$templateManager = $emuShop->getManager( 'theme' );
        $emailTemplateManager = $emuShop->getManager('email-template');

		if( $export_all )
		{
			$templates = $templateManager->getTemplates();
			$etemplates = $emailTemplateManager->getTemplates();
		}
		else
		{
			if( $options->templates )
				foreach( $options->templates as $template )
					if( $template = $templateManager->getTemplateRecord( $template ) ) $templates[] = $template;

			if( $options->etemplates )
				foreach( $options->etemplates as $template )
					if( $template = $emailTemplateManager->getTemplateRecord( $template ) ) $etemplates[] = $template;
		}

		$output = (object) array( 	'exportDateTime' => time(),
									'templates' => count($templates) > 0 ? $templates : false,
									'etemplates' => count($etemplates) > 0 ? $etemplates : false );


		return serialize( $output );
	}

	function import( $data )
	{
		$info = array();

		if( !$this->isImportDataValid( $data ) ) return (object) array( 'ok' => false, 'message' => 'The import file does not appear to be valid' );

		if( $data->templates )
		{
			$templateManager = $this->getManager( 'theme' );

			foreach( $data->templates as $template )
			{
				$info[] = "Updating content template {$template->filename}";
				$templateManager->updateTemplate( $template->template, array( 'filename' => $template->filename ) );
			}
		}

		if( $data->etemplates )
		{
            $templateManager = $this->getManager( 'email-template' );

			foreach( $data->etemplates as $template )
			{
				$info[] = "Updating email template {$template->filename}";
				$templateManager->updateTemplate( $template->template, array( 'filename' => $template->filename ) );
			}
		}


		return (object) array ( 'ok' => true, 'message' => '' );
	}

	function isImportDataValid( $data )
	{
		if ( !is_object( $data ) ) return false;

		if( !isset($data->exportDateTime) ) return false;
		if( !isset($data->templates) ) return false;
		if( !isset($data->etemplates) ) return false;

		return true;
	}


    public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

    }

}


?>
