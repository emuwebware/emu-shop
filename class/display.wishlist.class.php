<?php

class emuWishlistDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'wishlist.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$customer = $emuShop->getCustomer();

		$product_wishlist = $customer->getWishList();

		if( count( $product_wishlist ) == 0 )
		{
			$this->content = '<p>You do not currently have any items in your wishlist.</p>';
			return;
		}

		$product_list = '';

		// Load the product template
		$template = $this->template;

		$product_template = $this->templateManager->getTemplateRepeat( 'wishlist item', $template );

		$variant_description = '';

		foreach( $product_wishlist as $product )
		{
			$tmp_template = $this->templateManager->fillTemplate( $product_template, $product->getTemplateTags(), $product, $product->postID );
			$product_list .= $tmp_template;
		}

		$template = $this->templateManager->fillTemplateRepeats( $template, array( 'wishlist item' => $product_list ) );
		$template = $this->templateManager->fillTemplate( $template, array( 'wishlist messages' => $emuShop->getMessages( 'wishlist' ) ) );

		$this->content = apply_filters( 'emu_wishlist_content', $template );

	}
}

?>
