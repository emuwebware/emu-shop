<?php

class emuLoginProcessor extends emuProcessor
{
	public $requiredFields = array( 'login_email', 'login_password' );

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		if( $customer_email = request_val('send_activation') )
		{
			global $wpdb;

			$row = $wpdb->get_row( $wpdb->prepare( "select wpUserID, activationCode from {$emuShop->dbPrefixShared}customers where email = %s", $customer_email ) );

			if( !$row )
			{
				$this->messages[] = 'Sorry but this email does not appear to be registered';
				$emuShop->addMessage( 'login', $this->messages, 'error' );
				$this->error = true;
				return;
			}

			$customer = $emuShop->getInstance( 'emuCustomer', array( $row->wpUserID ) );
			$emuShop->getManager('customer')->sendActivationCode( $row->activationCode, $customer );
			$this->messages[] = 'Another activation email has been sent to '.$customer->email;

			$emuShop->addMessage( 'login', $this->messages, 'notice' );
			return;
		}

		if( $activation_code = request_val( 'activate' ) )
		{
			$activation_code = urldecode( $activation_code );

			$activator = $emuShop->getInstance( 'emuAccountActivator' );

			$activator->activationCode = $activation_code;
			$activator->activate();

			$this->error = $activator->error;
			$this->messages = $activator->messages;

			$emuShop->addMessage( 'login', $this->messages, $this->error ? 'error' : 'notice' );
			return;
		}

		switch( $this->button )
		{
			case 'Login':

				$this->processLogin();

				if( $this->error )
				{
					$emuShop->addMessage( 'login', $this->messages, 'error' );
					header( 'Location: '.$_SERVER[ 'HTTP_REFERER' ] );
					exit();
				}

				if( !$location = $emuShop->getSessionData('login_return_url', $delete_after_read = true) )
					$location = '/';

				break;

			case 'Register':

				$location = $emuShop->pageManager->pages->registration->url.'?email='.request_val( 'email' );
				break;

		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );

		header( 'Location: '.$location );
		exit();

	}


	public function processLogin()
	{
		global $wpdb, $emuShop;

		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$this->error = true;
			return;
		}

		extract( $_POST );

		$row = $wpdb->get_row( $wpdb->prepare( "select username, isActive from {$emuShop->dbPrefixShared}customers where email = %s and deleted = 0", $login_email ) );

		if( !$row )
		{
			$this->messages[] = '<strong>Couldn\'t find that user.</strong> Check the spelling and try again.';
			$this->messages[] = 'Alternatively create a new account with this email by entering your email under <strong>New Customers</strong> and clicking \'Register\'.';
			$this->error = true;
			return;
		}

		$username = $row->username;

		if( !$row->isActive )
		{
			$this->messages[] = 'Your account has not yet been activated. Details of how to activate your account were sent to your email during registration. If you no longer have this email we can <a href="'.$emuShop->pageManager->pages->login->url.'?e-action=login&e-plugin='.$emuShop->emuAppID.'&send_activation='.urlencode($login_email).'">send you another</a>.';
			$this->error = true;
			return;
		}

		$creds = array( 'user_login' => $username, 'user_password' => $login_password, 'remember' => false );

		$user = wp_signon( $creds, false );

		if ( is_wp_error( $user ) )
		{
			$login_error = $user->get_error_message();

			if( in_array( 'invalid_username', array_keys( $user->errors ) ) )
			{
				$this->messages[] = 'Couldn\'t find that user - check the spelling and try again.';
			}
			else if( in_array( 'incorrect_password', array_keys( $user->errors ) ) )
			{
				$this->messages[] = 'The password you have provided is incorrect. If you have forgotten your password then we can <a href="'.$emuShop->pageManager->pages->resetPassword->url.'">send a new password</a>.';
			}
			else
			{
				$this->messages[] = 'A problem occurred with your login but it looks like it\'s on our side. Please check back later.';
			}
			$this->error = true;
		}
		else
		{
			wp_set_current_user($user->ID);
			$this->error = false;
		}
	}


}

?>
