<?php

class emuShop extends emuApp
{
	// Settings
	///////////////////////////////////////////////////////////////////////////////////

    public $isDisabled 				= false;
	public $autoLoadModules 		= true;

	public $currency;
	public $autoExcerptLength;
	public $trackStock;
	public $loadStylesheets;
	public $loadScripts;
	public $orderRefPrefix;
	// public $useSourceTemplateFiles;
	public $paymentsDisabled; // still go through the payment system but ignore errors and don't check credit card authentication
	public $offlinePayments; // bypass payments completely go straight from confirmation to thankyou (order generated in emuConfirmationProcessor class)
	public $firstOrderNumber;
	public $paymentGateway;
	public $shippingChargesEnabled;
	public $handlingChargesEnabled;
	public $allowGuestCheckout;
	public $bypassAccountActivation;
	public $storeOrderObject;
	public $loadThickBox;
	public $SSLEnabled;
	public $pickupEnabled;
	public $pickupFreeHandling;
	public $pickupText;
	public $pickupCost;
    public $stayOnProductPage;
    public $cv2Required;

	public $templateDirectory;		// set in constructor
	public $publicStyles; 			// set in constructor
	public $publicScript;			// set in constructor
	public $emailDirectory;			// set in constructor
	public $productImageSizes = array( 	'one' => array( 60, 60, true ),
										'two' => array( 100, 100, true ),
										'three' => array( 140, 140, true ),
										'four' => array( 180, 180, true ),
										'five' => array( 220, 220, true ),
										'six' => array( 260, 260, true ),
										'seven' => array( 300, 300, true ) );

	///////////////////////////////////////////////////////////////////////////////////

	public $basket;

	protected $formProcessor;
	protected $userInterface;

	public $pages;

	public function setAppConfig()
	{
		// App settings
        ////////////////////////////////////
        $this->emuAppID = 'emuS';
		$this->menuName = 'Emu Shop';
		$this->dbPrefix = 'emu_shop_';
		$this->poolObjects = false;
		$this->hasFeaturedImages = true;
		//$this->forceInstall = true;

        // Templating settings
        ////////////////////////////////////
        $this->templatingEnabled = false;
        $this->emailTemplatesEnabled = true;
	}

	public function init()
	{
		// Constants
		////////////////////////////////////////////////////////////////////////////////////////////////
		$this->loadConstants( 'product', 'emuProductConstants', 'constants.product.class.php' );
		$this->loadConstants( 'basket', 'emuBasketConstants', 'constants.basket.class.php' );
		$this->loadConstants( 'order', 'emuOrderConstants', 'constants.order.class.php' );

        // Helpers
        ////////////////////////////////////////////////////////////////////////////////////////////////
        $this->loadHelper( 'links', 'emuH_Links', 'help.links.class.php' );
        $this->loadHelper( 'product-admin', 'emuH_ProductAdmin', 'help.product.admin.class.php' );
        $this->loadHelper( 'product', 'emuH_Product', 'help.product.class.php' );
        $this->loadHelper( 'region', 'emuH_Region', 'help.region.class.php' );

        // Managers
        ////////////////////////////////////////////////////////////////////////////////////////////////
        $this->loadManager( 'region', 'emuM_Regions', 'manage.regions.class.php' );
        $this->loadManager( 'promotion', 'emuM_Promotions', 'manage.promotions.class.php' );
        $this->loadManager( 'payment', 'emuM_Payments', 'manage.payments.class.php' );
        $this->loadManager( 'basket', 'emuM_Baskets', 'manage.baskets.class.php' );
		$this->loadManager( 'product', 'emuM_Products', 'manage.products.class.php' );
		$this->loadManager( 'page', 'emuM_Pages', 'manage.pages.class.php' );
		$this->loadManager( 'customer', 'emuM_Customers', 'manage.customers.class.php' );
		$this->loadManager( 'order', 'emuM_Orders', 'manage.orders.class.php' );
		$this->loadManager( 'shipping', 'emuM_Shipping', 'manage.shipping.class.php' );
		$this->loadManager( 'handling', 'emuM_Handling', 'manage.handling.class.php' );
		$this->loadManager( 'data', 'emuM_Data', 'manage.data.class.php' );
		$this->loadManager( 'gateway', 'emuM_Gateways', 'manage.gateways.class.php' );
		$this->loadManager( 'tax', 'emuM_Taxes', 'manage.tax.class.php' );
		$this->loadManager( 'theme', 'emuM_ShopThemes', 'manage.themes.class.php' );
		$this->loadManager( 'helper', 'emuM_Helpers', 'manage.helpers.class.php' );
		$this->loadManager( 'route', 'emuM_Routes', 'manage.routes.class.php' );

		$this->checkInstall();

		// Load classes;
		$this->loadClasses();

		// Register settings
		$this->registerSettings();

		// Load settings
		$this->loadSettings();

		do_action( 'shop_settings_loaded', $this );

		$this->registerAdminPages();

        add_action( 'init', array( $this, 'registerScripts'), 2 );
        add_action( 'init', array( $this, 'registerStyles'), 2 );
		add_action( 'init', array( $this, 'getPages' ), 1);
		add_action( 'init', array( $this, 'checkLogout' ), 1 );
		add_action( 'init', array( $this, 'addCustomImageSizes' ) );
        add_action( 'init', array( $this, 'loadAdminScriptsStyles' ) );

		do_action( 'emu_shop_loaded', $this );
	}

    public function loadAdminScriptsStyles()
    {
        // load the admin scripts and styles
		if( is_admin() )
		{
			add_action( 'admin_print_scripts', array( $this, 'defineEnvVariables' ) );
			wp_enqueue_script( 'emu-shop-admin' );
			wp_enqueue_style( 'emu-shop-admin' );
		}
		else
		{
			add_action( 'template_redirect', array( $this, 'loadPublicScripts' ) );
		}
    }

	public function getPages()
	{
		$this->pages = &$this->pageManager->pages;
	}

	public function registerSettings()
	{
		$coreSettings = $this->settings->createSettingsGroup( $this->emuAppID.'_core', $this->pluginName.' Core Settings' );

		$coreSettings->addSetting( 'currency', '&#36;', 'Currency', 3, array( '&#36;' => '&#36;', '&pound;' => '&pound;' ) );
		$coreSettings->addSetting( 'autoExcerptLength', 100, 'Product Excerpt Length', 4, 'number' );
		$coreSettings->addSetting( 'trackStock', false, 'Track Stock', 5, 'boolean' );
		$coreSettings->addSetting( 'loadStylesheets', true, 'Load Stylesheets', 16, 'boolean' );
		$coreSettings->addSetting( 'loadScripts', true, 'Load Scripts', 17, 'boolean' );
		$coreSettings->addSetting( 'orderRefPrefix', 'EMU', 'Order Reference Prefix', 6, 'text' );
		$coreSettings->addSetting( 'paymentsDisabled', false, 'Payment Authorization Disabled', 8, 'boolean' );
		$coreSettings->addSetting( 'offlinePayments', false, 'Online Payments Disabled (bypass payments)', 9, 'boolean' );
		$coreSettings->addSetting( 'firstOrderNumber', 1, 'Starting Order Number', 10, 'number' );
		$coreSettings->addSetting( 'paymentGateway', 'Test', 'Payment Gateway', 11 );
		$coreSettings->addSetting( 'shopTheme', 'Standard', 'Shop Theme', 11 );
		$coreSettings->addSetting( 'shippingChargesEnabled', true, 'Shipping Charges Enabled', 12, 'boolean' );
		$coreSettings->addSetting( 'handlingChargesEnabled', true, 'Handling Charges Enabled', 12, 'boolean' );
		$coreSettings->addSetting( 'bypassAccountActivation', false, 'Bypass Account Activation Process', 13, 'boolean' );
		$coreSettings->addSetting( 'storeOrderObject', true, 'Store Order Processing Details (for debug)', 19, 'boolean' );
		$coreSettings->addSetting( 'loadThickBox', true, 'Thickbox Enabled', 15, 'boolean' );
		$coreSettings->addSetting( 'SSLEnabled', false, 'SSL Enabled', 6, 'boolean' );
		$coreSettings->addSetting( 'pickupEnabled', false, 'Pickup Order Enabled', 6, 'boolean' );
		$coreSettings->addSetting( 'pickupFreeHandling', false, 'Free Handling with Pickup', 6, 'boolean' );
		$coreSettings->addSetting( 'pickupText', 'In store pickup', 'Pickup Text', 6, 'text' );
		$coreSettings->addSetting( 'pickupCost', 0, 'Pickup Cost', 6, 'number' );
		$coreSettings->addSetting( 'stayOnProductPage', false, 'Remain on page after adding product to basket', 6, 'boolean' );
		$coreSettings->addSetting( 'cv2Required', true, 'CV2 Required', 6, 'boolean' );
		$coreSettings->addSetting( 'allowGuestCheckout', false, 'Allow Guest Checkout', 12, 'boolean' );

		$this->settings->addSettingsGroup( $coreSettings );
		$this->settings->saveSettings();
	}


	public function loadSettings()
	{
		$settings = $this->settings->getSettingsGroup( $this->emuAppID.'_core' )->getSettings();

		foreach( $settings as $settingName => $setting )
		{
			$this->$settingName = $setting->value;
		}
	}

	// core
	public function loadClasses()
	{
		// Core classes
		$this->loadModel( 'emuShopCommon', 'shopcommon.class.php' );
		$this->loadModel( 'emuTemplatedPostDbEntity', 'model.templateddbpostentity.class.php' );
		$this->loadModel( 'emuTemplatedDbEntity', 'model.templateddbentity.class.php' );
		$this->registerModel( 'emuDisplay', 'model.display.class.php', 'emuUI' );
	}

	// core / page manager
	function isSecure()
	{
		return is_ssl();
	}

	// core / page manager
	function getSecureURL( $page )
	{
		return preg_replace( '/http:/', 'https:', $page->url );
	}

	public function setFormProcessor( emuFormProcessor $processor )
	{
		$this->formProcessor = $processor;
	}

	public function processForm()
	{
		$this->formProcessor->process();
	}

	public function setUserInterface( $interface )
	{
		$this->userInterface = $interface;
	}

	public function getUserInterface()
	{
		$this->userInterface->build();
		return $this->userInterface->content;
	}

	public function showUserInterface()
	{
		$this->userInterface->build();
		echo $this->userInterface->content;
	}

	function registerScripts()
	{
		wp_register_script( 'emu-shop-ie-admin', plugins_url( "/{$this->pluginName}/js/import-export-admin.js" ), array( 'jquery' ) );
		wp_register_script( 'emu-shop-admin', plugins_url( "/{$this->pluginName}/js/shop-admin.js" ), array( 'jquery' ) );
	}

	function registerStyles()
	{
		wp_register_style( 'emu-shop-ie-admin', plugins_url( "/{$this->pluginName}/style/import-export-admin.css" ), false);
		wp_register_style( 'emu-common', plugins_url( "/{$this->pluginName}/style/common.css" ), false);
		wp_register_style( 'emu-shop-admin', plugins_url( "/{$this->pluginName}/style/shop-admin.css" ), false);
	}

	function registerAdminPages()
	{
		// $this->registerAdminPage( array( 'name' => 'Tag Reference',
		// 							'filename' => 'tag-reference.php',
		// 							'position' => 8,
		// 							'styles' => array( 'emu-common' ),
		// 							'scripts' => array() ) );
	}

	function getYears( $start = null, $count = 8 )
	{
		if( !$start ) $start = date('Y');

		$years = array();

		for( $i = $start; $i < ( $start + $count ); $i++ ) $years[] = $i;

		return $years;
	}

	function getMonths()
	{
		$months = array();

		for( $i = 1; $i < 13; $i++ ) $months[$i] = date( 'M', strtotime( $i.'/27/1981' ) );

		return $months;
	}

	function getStatusTypes()
	{
		return array( '1' => 'Active', '0' => 'Inactive', '' => 'Deactivated' );
	}

	function getCouponAppliesTo()
	{
		return array( 'all' => 'All Orders', 'over' => 'Orders over', 'under' => 'Orders under' );
	}

	function getRatingOptions()
	{
		return array( '1' => '1 - Terrible!', '2' => '2 - Bad', '3' => '3 - OK', '4' => '4 - Good', '5' => '5 - Great!' );
	}

	function generateRandomString( $number_chars = 5 )
	{
		return preg_replace('/([ ])/e', 'chr(rand(97,122))', str_repeat( ' ', $number_chars ) );
	}

	function loadPublicScripts()
	{
		global $post;

		// actually we need to show the script and styles on all pages
		// just in case a shortcode has been used somewhere.
		if( $this->loadStylesheets )
		{
			wp_enqueue_style ( 'emu-shop-public', $this->publicStyles );
		}

		if( $this->loadScripts )
		{
			wp_enqueue_script ( 'emu-shop-public', $this->publicScript, array('jquery') );

			if( !is_ssl() && $this->loadThickBox )
			{
				wp_enqueue_style ( 'thickbox' );
				wp_enqueue_script ( 'thickbox' );
				add_action( 'wp_footer', array( $this, 'thickBoxPathFix' ) );
			}
		}
	}

	// product manager
	public function thickBoxPathFix()
	{
		echo 	'<script type="text/javascript">'."\n".
				'/* <![CDATA[ */ '."\n".
				'tb_closeImage = "/wp-includes/js/thickbox/tb-close.png";'."\n".
				'/* ]]> */'."\n".
				'</script>';
	}

	function getBasket( $refresh = false )
	{
		return $this->basketManager->getBasket( $refresh );
	}

	function getCustomer()
	{
		return $this->customerManager->getCustomer();
	}

	function getWeightUnit()
	{
		return $this->getMeta('large_weight_unit' );
	}

	public function applyUnitConversion( $value = null )
	{
		if( strlen( trim( $value ) ) == 0 ) return '';

		$value = preg_replace( '/[^0-9,\.]/', '', $value );

		$value = explode( ',', $value );

		$total = $value[0];

		$fraction = count( $value ) > 1 ? $value[1] : 0;

		if( !$conversion_factor = $this->getMeta('unit_conversion' ) )
			$conversion_factor = 1;

		// add fraction total to total
		$total = $total + ( $fraction / $conversion_factor );

		return $total;
	}

	function isValidPassword( $password )
	{
		return eregi( "^[a-zA-Z0-9]{6,20}$", $password );
	}

	function checkLogout()
	{
		if( preg_match( '/emuLogout/', $_SERVER["QUERY_STRING"] ) <> 0 )
		{
			$basket = $this->getBasket();
			$basket->emptyBasket();
			wp_logout();

			$location = preg_replace( '/\?emuLogout/', '', $_SERVER["REQUEST_URI"] );
			$location = apply_filters( 'emu_shop_checkLogout_redirect_location', $location );

			header( 'Location: '.$location );
			exit;
		}
	}

	function addCustomImageSizes()
	{
		foreach( $this->productImageSizes as $size => $dimensions )
		{
			add_image_size( $this->emuAppID."-$size", $dimensions[0], $dimensions[1], $dimensions[2] );
		}
	}

	function defineEnvVariables()
	{
		$emu = (object) array( 'path' => $this->pluginURLPath, 'name' => $this->pluginName, 'url' => $this->pluginURL );

		?>
		<script type="text/javascript">

			var emu = <?php echo json_encode( $emu );?>

		</script>
		<?php
	}
}

?>
