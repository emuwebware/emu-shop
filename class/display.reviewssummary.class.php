<?php


class emuReviewsSummaryDisplay extends emuDisplay
{
	public $post;
	public $product;

	public function __construct()
	{
        $this->templateFile = 'reviews-summary.htm';
        parent::__construct();


        if( $post_name = request_val('p') )
		{
			$product = $emuShop->getManager('product')->getProducts( array('name' => $post_name ) );

			if( count( $product ) > 0 )
			{
				$this->product = $product[0];
			}
		}
	}

	public function build()
	{
		if( !$this->product )
		{
			if( !$this->post )
			{
				$this->content = 'No product!';
				return;
			}
			if( !$this->product = $emuShop->getManager( 'product', array( $this->post ) ) )
			{
				$this->content = 'No product!';
				return;
			}
		}

		global $emuShop;

		$product = $this->product;

		$template = $this->template;

		// Reviews item template
		$review_item_template = $this->templateMananger->getTemplateSection( 'most helpful favourable review', $template );

		// Get the most helpful reviews for comparison
		$product->getReviews( array( 'mostHelpfulFavorable' => true ) );

		if( count( $product->reviews ) > 0 )
		{
			$review = $product->reviews[0];

			$most_helpful_favorable = $this->templateMananger->fillTemplate( $review_item_template, $review->getTemplateTags(), $review, $review->ID );
		}
		else
		{
			$most_helpful_favorable = 'There aren\'t yet any favorable reviews.';
		}

		// Get the most helpful critical review
		$product->getReviews( array( 'mostHelpfulCritical' => true ) );

		if( count( $product->reviews ) > 0 )
		{
			$review = $product->reviews[0];

			$most_helpful_critical = $this->templateMananger->fillTemplate( $review_item_template, $review->getTemplateTags(), $review, $review->ID );
		}
		else
		{
			$most_helpful_critical = 'There are no critical reviews!';
		}

		$template = $this->templateMananger->fillTemplateSections( $template, array( 'most helpful favourable review' => $most_helpful_favorable, 'most helpful critical review' => $most_helpful_critical ) );

		// Build the 'All Reviews' section
		$review_item_template = $this->templateMananger->getTemplateRepeat( 'review', $template );

		$reviews = '';

		$product->getReviews(); // get all reviews

		$basket = $emuShop->getBasket();

		if( !$review_votes = $basket->getSessionData('review-votes') ) $review_votes = array();

		foreach( $product->reviews as $review )
		{
			if( $review->post->post_status != 'publish' ) continue;

			$review->getComments();

			$comments = '';

			// print_r( $review->comments );

			// build the comments list
			if( count( $review->comments ) > 0 )
			{
				$comments .= '<ul class="review-comments">';
				for( $n = 0; $n < count( $review->comments); $n++ )
				{
					$comment = $review->comments[$n];
					$comments .= '<li><em>&quot;'.$comment->comment_content.'&quot;</em><span class="comment-details"><strong> - '.$comment->comment_author.'</strong>, '.apply_date_format('standard-with-time-alternative', $comment->comment_date).'</span></li>';
				}
				$comments .= '</ul>';
			}

			$show_voting_form = in_array( $review->dbID, $review_votes ) ? false : true;

			$review_item = $this->templateMananger->fillTemplate( $review_item_template, $review->getTemplateTags(), $review, $review->ID );
			$review_item = $this->templateMananger->fillTemplate( $review_item, array( 'comments' => $comments ) );
			$reviews .= $this->templateMananger->setTemplateConditionals( $review_item, array( 'review voting form' => $show_voting_form ) );

		}

		$template = $this->templateMananger->fillTemplateRepeats( $template, array( 'review' => $reviews ) );

		$template = $this->templateMananger->fillTemplate( $template, $product->getTemplateTags(), $product, $product->postID );

		$tags = array( 	'review summary table' => $emuShop->productHelper->buildReviewsSummaryTable( $product ),
						'average rating stars large' => $product->getRatingStars( 'large' ),
						'product messages' => $emuShop->getMessages('product'),
						'rating summary' => $product->ratingSummary );

		$template = $this->templateMananger->setTemplateConditionals( $template, array( 'quantity dropdown' => count( $product->variants ) == 0 ) );

		$template = $this->templateMananger->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_reviews_summary_content', $template );
	}
}

?>
