<?php


class emuAccountPaymentProcessor extends emuProcessor  
{
	public $requiredFields = array( 'payment_type', 'cc_month', 'cc_year' );
	
	public function __construct()
	{
		$this->checkRequiredFields();
	}
	
	public function process()
	{
		global $emuShop;
		
		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );
		
		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$this->error = true;
			return;
		}
			
		extract( $_POST );
		
		$customer = $emuShop->getManager('customer')->getCustomer();
		
		$customer->paymentType = post_val('payment_type');
		
		if( post_val( 'card_number' ) ) 
			$customer->cardNumber = post_val('card_number');
		
		$customer->expiryMonth = post_val('cc_month');
		$customer->expiryYear = post_val('cc_year');
		
		if( !$customer->update() )
		{
			$this->error = true;
			$this->messages = $customer->messages;
		}
		else
		{
			$this->messages[] = 'Payment Information updated.';
			$this->error = false;
		}			

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );
	
	}

}

?>
