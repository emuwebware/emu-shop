<?php

class emuProductConstants extends emuConstants
{
    public function addConstants()
    {
        $prices = $this->createConstantGroup( 'PRODUCT_', 'Price formats used by emuProduct class getPrice() method' );

        $prices->addConstant( 'ORIGINAL_PRICE', 'Product price' );
        $prices->addConstant( 'ORIGINAL_PRICE_FORMATTED', 'product price formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $prices->addConstant( 'SALE_PRICE', 'Sale price');
        $prices->addConstant( 'SALE_PRICE_FORMATTED', 'Sale price formatted with currency symbol and rounded to 2 decimal places e.g. $10.99');

        $prices->addConstant( 'PRICE', 'Price if the product doesn\'t have a sale price. Sale price if it does.');
        $prices->addConstant( 'PRICE_FORMATTED', 'Price (sale price or price) formatted with currency symbol and rounded to 2 decimal places e.g. $12.99. If the product has a sale price this will return a html wrapper: <span class="sale-strike">[original price]</span> <span class="product-sale-price">[sale price] Sale Price</span>');

        $prices->addConstant( 'VARIANT_PRICE_RANGE', 'Displays lowest variant price in the format \'From [lowest price]\'');

        $prices->addConstant( 'SALE_SAVING', 'Difference between price and sale price');
        $prices->addConstant( 'SALE_SAVING_FORMATTED', 'Difference between price and sale price formatted with currency symbol and rounded to 2 decimal places e.g. $12.99.');

        $prices->addConstant( 'SALE_SAVING_PERCENTAGE', 'Difference between price and sale price as percentage');
        $prices->addConstant( 'SALE_SAVING_PERCENTAGE_FORMATTED', 'Difference between price and sale price as percentage with %');

        $prices->addConstant( 'SHIPPING_COST', 'Shipping cost');
        $prices->addConstant( 'SHIPPING_COST_FORMATTED', 'Shipping cost formatted with currency symbol and rounded to 2 decimal places e.g. $12.99.');

        $prices->addConstant( 'HANDLING_COST', 'Handling cost');
        $prices->addConstant( 'HANDLING_COST_FORMATTED', 'Handling cost formatted with currency symbol and rounded to 2 decimal places e.g. $12.99.');

    }

}


