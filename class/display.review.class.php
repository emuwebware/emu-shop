<?php

class emuReviewDisplay extends emuDisplay
{
	public $post;

	public $review;

	public function __construct()
	{
        $this->templateFile = 'review.htm';
        parent::__construct();

	}

	public function getPost()
	{
		if( !$this->post )
		{
			global $post;
			$this->post = $post;
		}
	}

	public function build()
	{
		if( !$this->post )
		{
			$this->content = 'No product!';
			return;
		}

		global $emuShop;

		$basket = $emuShop->getBasket();

		$review = $emuShop->getInstance('emuReview', $this->post );
		$product = $emuShop->getInstance('emuProduct', array(null, $review->productPostID) );

		$tM = $this->templateManager;

		// $template = $tM->getTemplate( $this->template );

		// Use the product display to generate the product markup
		$product_display = $emuShop->getInstance( 'emuProductDisplay' );
		$product_display->template = $this->template;
		$product_display->post = $product->getPost();
		$product_display->build();

		$template = $product_display->content;

		// Fill review tags
		$template = $tM->fillTemplate( $template, $review->getTemplateTags(), $review );

		if( !$review_votes = $basket->getSessionData('review-votes') ) $review_votes = array();

		$show_voting_form = in_array( $review->dbID, $review_votes ) ? false : true;

		$template = $tM->setTemplateConditionals( $template, array( 'review voting form' => $show_voting_form ) );

		$review_comments = '';

		if( $comments = $review->getComments() )
		{
			$review_comments .= '<ul class="review-comments">';

			foreach( $comments as $comment )
				$review_comments .= '<li><em>&quot;'.$comment->comment_content.'&quot;</em><span class="comment-details"><strong> - '.$comment->comment_author.'</strong>, '.apply_date_format('standard-with-time-alternative', $comment->comment_date).'</span></li>';
			$review_comments .= '</ul>';
		}

		$tags = array( 'comments' => $review_comments );

		$template = $tM->fillTemplate( $template, $tags );


		if( is_user_logged_in() )
		{

			if( !$comment_submissions = $basket->getSessionData('comment-submissions') ) $comment_submissions = array();

			if( in_array( $review->ID, $comment_submissions ) )
			{
				$comment_form = '<p>Your comment has been submitted and is awaiting moderation</p>';
			}
			else
			{
				$comment_form = $tM->getTemplateSection( 'comment form', $template );
				$comment_form = $tM->fillTemplate( $comment_form, array( 'comment messages' => $emuShop->getMessages('comment') ) );
			}
		}
		else
		{
			$comment_form = '<p><a href="'.$emuShop->pageManager->pages->login->url.'?return='.$review->permalink.'">Login</a> to post a comment</p>';
		}

		$template = $tM->fillTemplateSections( $template, array( 'comment form' => $comment_form ) );

		$this->content = apply_filters( 'emu_review_content', $template );
	}
}

?>
