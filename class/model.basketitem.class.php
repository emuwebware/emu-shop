<?php


class emuBasketItem extends emuProduct
{
	public $qty = 0;
	public $options = array();

	public function __construct( $dbID = null, $post = null )
	{
		parent::__construct( $dbID, $post );
	}

	public function setOptions($options)
	{
		if( !is_array( $options ) ) return;
		$this->options = $options;
	}

	public function getOptions()
	{
		return $this->options;
	}

	public function addQty($qty = 1)
	{
		$this->qty += $qty;
	}

	public function subtractQty($qty = 1)
	{
		$this->qty = $this->qty - $qty;
	}

	public function getQty()
	{
		return $this->qty;
	}

	public function getUniqueRef()
	{
		// Unique features of the basket item is the combination of product id, qty and product options
		// returned as a hash
		$ref = $this->getID().json_encode( $this->getOptions() );
		return md5($ref);
	}

	public function setQty($qty = null)
	{
		if( !$qty )
		{
			trigger_error( "Can't set item quantity to null: integer required." );
			return null;
		}

		if( !is_numeric( $qty ) )
		{
			trigger_error( "Item quantity must be an integer" );
			return null;
		}

		$this->qty = $qty;
	}

	public function registerTemplateTags()
	{
		parent::registerTemplateTags();

		$basket_item_tags = array(  'selected product options' => 'selectedProductOptions',
									'product options data json' => 'productOptionsDataJson',
									'total' =>  array( 'this', 'getTotal', BASKET_ITEM_TOTAL_FORMATTED ),
									'weight' => array( 'this', 'getTotal', BASKET_ITEM_WEIGHT_TOTAL ),
									'quantity' => 'qty',
									'converted weight' => 'convertedWeight',
									'item ref' => array( 'this', 'getUniqueRef', null ) );

		$this->templateTags = array_merge($this->templateTags, $basket_item_tags);
	}

    public function __get( $member )
 	{
 		global $emuShop;

 		if( $member_value = parent::__get( $member ) ) return $member_value;

 		switch( $member )
 		{
 			case 'selectedProductOptions':
 				// return 'test';
 				return $emuShop->productHelper->listProductOptions($this);
 				// return $this->postTitle;

 			case 'productOptionsDataJson':

 				return urlencode(json_encode($this->getOptions()));

			default:

				if( !isset( $this->data[ $member ] ) ) return null;
				return $this->data[ $member ];

 		}

 	}

	public function getTotal($format = null)
	{
		global $emuShop;

		switch ($format)
		{
			case BASKET_ITEM_WEIGHT_TOTAL:
				return $this->qty * $this->convertedWeight;

			case BASKET_ITEM_WEIGHT_TOTAL_FORMATTED:
				return apply_number_format( 'weight', $this->getTotal(BASKET_ITEM_WEIGHT_TOTAL) );

			case BASKET_ITEM_TAX_TOTAL:
				return $this->hasTax() ? $this->getTotal(BASKET_ITEM_TOTAL) : 0;

			case BASKET_ITEM_TAX_TOTAL_FORMATTED:
				return apply_number_format( 'weight', $this->getTotal(BASKET_ITEM_TAX_TOTAL) );

			case BASKET_ITEM_SHIPPING_COST_TOTAL:
				return $this->qty * $this->shippingCost;

			case BASKET_ITEM_SHIPPING_COST_TOTAL_FORMATTED:
				return $emuShop->currency.apply_number_format( 'currency', $this->getTotal(SHIPPING_COST_TOTAL) );

			case BASKET_ITEM_HANDLING_COST_TOTAL:
				return $this->qty * $this->shippingCost;

			case BASKET_ITEM_HANDLING_COST_TOTAL_FORMATTED:
				return $emuShop->currency.apply_number_format( 'currency', $this->getTotal(HANDLING_COST_TOTAL) );

			case BASKET_ITEM_TOTAL:
				return $this->qty * $this->getPrice(PRODUCT_PRICE);;

			case BASKET_ITEM_TOTAL_FORMATTED:
			default:

				return $emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_ITEM_TOTAL) );

				break;
		}
		return;
	}

	public function getTotalValue()
	{
		return $this->getTotal(BASKET_ITEM_TOTAL);
	}

	// Overrides
	///////////////////////////////////////////////////////////////////////////////////////////
	public function save()
	{
		// don't do anything
	}

	public function update()
	{
		// don't do anything
	}

	public function delete()
	{
		// don't do anything
	}

	public function deleteRecord()
	{
		// don't do anything
	}

	public function deletePost()
	{
		// don't do anything
	}
	///////////////////////////////////////////////////////////////////////////////////////////
}


?>
