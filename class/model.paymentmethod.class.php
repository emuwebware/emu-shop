<?php



class emuPaymentMethod extends emuTemplatedDbEntity
{
    public $error = false;
    public $messages = array();

	public $cv2Number = ''; // not saved to the db

	public $billingAddress;

    public function __construct( $dbID = null )
    {
        global $emuShop;
        parent::__construct( $dbID, null, $emuShop->dbPrefixShared, 'payment_methods', array( 'isDefaultPaymentMethod' => '%b' ) );
    }

    public function registerTemplateTags()
    {
        $this->templateTags = array(    'payment type' => 'paymentType',
										'payment method id' => 'dbID',
										'credit card number' => 'safeCreditCardNumber',
										'credit card expiry month name' => 'expiryMonthName',
										'credit card expiry month' => 'expiryMonth',
										'credit card expiry year' => 'expiryYear',
										'credit card valid from month name' => 'validFromMonthName',
										'credit card valid from month' => 'validFromMonth',
										'credit card valid from year' => 'validFromYear' );
    }

    public function __get( $member )
    {
        global $emuShop;

        if( $member_value = parent::__get( $member ) ) return $member_value;

        switch( $member )
        {
			case 'safeCreditCardNumber':

				return $this->cardNumber ? str_repeat( '*', strlen( $this->cardNumber ) - 4 ).substr( $this->cardNumber, -4, 4 ) : '';

			case 'expiryMonthName':

				return $this->expiryMonth ? date( 'M', strtotime( "{$this->expiryMonth}/1/1971" ) ) : '';


			case 'validFromMonthName':

				return $this->validFromMonth ? date( 'M', strtotime( "{$this->validFromMonth}/1/1971" ) ) : '';

            default:

                if( !isset( $this->data[ $member ] ) ) return null;
                return $this->data[ $member ];

        }
    }


	public function getDisplayPaymentMethod( $separater = '<br />' )
	{
		$method = array();

		$method[] = $this->paymentType;
		$method[] = "Card ending {$this->safeCreditCardNumber}";
		$method[] = "Exp. {$this->expiryMonth}/{$this->expiryYear}";

		return implode( $separater, $method );
	}

    public function getData()
    {
    	global $emuShop;

    	parent::getData();

    	if( $this->billingAddressID ) $this->billingAddress = $emuShop->getInstance( 'emuAddress', array( $this->billingAddressID ) );
    }

	public function clearDefaults()
	{
		global $wpdb, $emuShop;

		$wpdb->query( "update {$emuShop->dbPrefixShared}payment_methods set isDefaultPaymentMethod = false where customerID = {$this->customerID}" );
	}

	public function update()
	{

		if( $this->isDefaultPaymentMethod ) $this->clearDefaults();

		parent::update();
	}

}

?>
