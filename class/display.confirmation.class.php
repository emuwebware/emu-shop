<?php


class emuConfirmationDisplay extends emuDisplay
{

	public function __construct()
	{
        $this->templateFile = 'confirmation.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$basket = $emuShop->getBasket();

		do_action( 'emu_shop_confirmation_pre_display', $basket );

		$confirm_template = $this->template;

		$basketDisplay = $emuShop->getInstance( 'emuBasketDisplay' );
		$basketDisplay->templateFile = 'confirmation-basket.htm';
		$basketDisplay->getTemplate();
		$basketDisplay->build();

		// Billing
		$billing_address = $basket->getBillingAddress();

		$confirm_template = $this->templateManager->fillTemplate( $confirm_template, $billing_address->getTemplateTags('billing '), $billing_address);

		// Basket
		$confirm_template = $this->templateManager->fillTemplate( $confirm_template, array( 'basket' => $basketDisplay->content ) );

		// Comments
		$confirm_template = $this->templateManager->fillTemplate( $confirm_template, array( 'order comments' => $basket->getSessionData('order_comments') ) );

		// Shipping
		if( $shipping_address = $basket->getShippingAddress() )
		{
            $confirm_template = $this->templateManager->fillTemplate( $confirm_template, $shipping_address->getTemplateTags('shipping '), $shipping_address );
		}

		// Shipping Charges
		if( $shipping_charge = $basket->hasShippingCharge() )
		{
			$shipping_charge = $shipping_charge->description.' - '.$shipping_charge->chargeFormatted;
		}
		else
			$shipping_charge = 'No shipping charges';

		$show_delivery_change_link = count( $basket->shippingCharges ) > 1;

		$confirm_template = $this->templateManager->setTemplateConditionals( $confirm_template, array( 'delivery change link' => $show_delivery_change_link ) );
		$confirm_template = $this->templateManager->fillTemplate( $confirm_template, array( 'shipping method' => $shipping_charge ) );

		$orderSummaryDisplay = $emuShop->getInstance( 'emuOrderSummaryDisplay' );
		$orderSummaryDisplay->build();

		$confirm_template = $this->templateManager->fillTemplate( $confirm_template, array( 'order summary' => $orderSummaryDisplay->content, 'order comments' => $basket->getSessionData('order_comments') ) );

		$this->content = apply_filters( 'emu_confirmation_content', $confirm_template );
	}
}

?>
