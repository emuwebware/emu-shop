<?php


class emuCustomer extends emuShopCommon
{
	public $currentUser; // containing the wordpress user object

	public $messages = array();
	public $error = false;
	public $wishList;

	// Data stored by wordpress
	public $password;

	// These are kept out of $data because they are not updated in the db
	public $addressBook;
	public $paymentManager;

	public $arrCustomVars = array();

	public $syncWithWP = true;

	// Data stored in the emu table
	public $data = array();

	public $orders;

	public static $staticTemplateTags = array( 	'first name' => 'firstName',
												'last name' => 'lastName',
												'company name' => 'companyName',
												'job title' => 'jobTitle',
												'prefix' => 'prefix',
												'suffix' => 'suffix',
												'department' => 'department',
												'email' => 'email',
												'username' => 'username',
												'activation code' => 'activationCode',
												'newsletter' => 'newsletter'
											);
	public $templateTags;

	public $creds;

	public function __construct( $current_user = null, $db_id = null )
	{
		$this->templateTags = self::$staticTemplateTags;

		if( $current_user )
		{
			if( is_numeric( $current_user ) )
			{
				$current_user = get_userdata( $current_user );
			}
		}
		else
		{
			if( $db_id )
			{
				$wp_user_id = $this->getWpUserIDByCustomerID( $db_id );
				$current_user = get_userdata( $wp_user_id );
			}
		}


		$this->currentUser = $current_user;

		if( $this->currentUser )
		{
			if( $this->currentUser->ID ) $this->populateCustomerData();
		}
	}

	public function isAnonymous()
	{
		return $this->dbID ? false : true;
	}

	public function isAnon()
	{
		return $this->isAnonymous();
	}

	public function __get( $member )
	{
		global $wpdb, $emuShop;

		switch( $member )
		{
			case 'newsletter':

				return $this->allowEmailContact ? 'Yes' : 'No';

			case 'wishListNoItems':

				return (float) $wpdb->get_var( "select count(*) from {$emuShop->dbPrefix}wishlist where customerID = {$this->dbID}" );

			default:

				if( !isset( $this->data[ $member ] ) ) return null;

				return $this->data[ $member ];
		}

    }

    public function delete()
    {
    	$this->deleted = true;
    	$this->update();
    }

    public function __set( $member, $value )
	{
        $this->data[ $member ] = $value;
    }

	public function getWpUserIDByCustomerID( $customer_db_id )
	{
		global $wpdb, $emuShop;

		$sql = $wpdb->prepare( "select wpUserID from {$emuShop->dbPrefixShared}customers where dbID = %d", $customer_db_id );

		if( $wpUserID = $wpdb->get_var( $sql ) )
			return $wpUserID;

		return false;
	}

	public function populateCustomerData()
	{
		global $wpdb, $emuShop;

		if( !$this->currentUser ) return;

		if( $customer_data = $wpdb->get_row( "select * from {$emuShop->dbPrefixShared}customers where wpUserID = {$this->currentUser->ID}", ARRAY_A ) )
		{
			$this->data = $customer_data;

			if( $this->customVars )
				$this->arrCustomVars = unserialize( $this->customVars );
		}
		else
		{
			// then we don't have create an associated emu customer record
			// get their wordpress account details

			$this->email = $this->currentUser->user_email;
			$this->firstName = $this->currentUser->user_nicename;
			$this->lastName = $this->currentUser->last_name;
			$this->allowEmailContact = 0;
			$this->username = $this->currentUser->user_login;
			$this->isActive = true;

			$this->wpUserID = $this->currentUser->ID;

			do_action('emu_db_before_insert_customers', $this);
			$this->dbID = $this->insertRecord( "{$emuShop->dbPrefixShared}customers", $this->data );

			$this->populateCustomerData();
			do_action('emu_db_after_insert_customers', $this);
			do_action( 'emu_shop_user_created', $this );
			return;
		}

		$this->getAddressBook();
		$this->getPaymentManager();
	}

	public function getAddressBook()
	{
		if( !$this->addressBook )
		{
			global $emuShop;

			$this->addressBook = $emuShop->getInstance( 'emuAddressBook', array( $this ) );
		}

		return $this->addressBook;
	}

	public function getPaymentManager()
	{
		if( !$this->paymentManager )
		{
			global $emuShop;

			$this->paymentManager = $emuShop->getInstance( 'emuPaymentManager', array( $this ) );
		}

		return $this->paymentManager;

	}

	public function getOrders()
	{
		if( !$this->orders )
		{
			global $wpdb, $emuShop;

			$orders_rs = $wpdb->get_results( "select dbID from {$emuShop->dbPrefix}orders where customerID = {$this->dbID} order by dateCreated DESC" );

			for( $n = 0; $n < count( $orders_rs ); $n++ )
				$this->orders[] = $emuShop->getInstance( 'emuOrder', array( $orders_rs[$n]->dbID ) );
		}

		return $this->orders;
	}

	public function hasOrders()
	{
		return $this->getOrders();
	}

	public function syncUpdate()
	{
		global $wpdb, $emuShop;

		do_action('emu_db_before_insert_customers', $this);
		$this->insertRecord( "{$emuShop->dbPrefixShared}customers", $this->data );
		do_action('emu_db_after_insert_customers', $this);
	}

	public function update()
	{
		global $wpdb, $emuShop;

		global $wp_version;

		if( $wp_version < '3.1' ) require_once(ABSPATH . WPINC . '/registration.php');

		$this->data = $this->sanitize( $this->data );

		if( count( $this->arrCustomVars ) > 0 )
			$this->customVars = serialize( $this->arrCustomVars );
		else
			$this->customVars = '';

		if( $this->currentUser )
		{
			// Update the user in the emu table

			if( !$this->email )
			{
				$this->messages[] = "An email address is required to update the user";
				$this->error = true;
				return $this->error;
			}

			do_action('emu_db_before_update_customers', $this);
			$this->updateRecord( "{$emuShop->dbPrefixShared}customers", $this->data, array( 'isActive' => '%b' ), array( 'wpUserID' => $this->currentUser->ID ) );

			$wp_user_id = $this->currentUser->ID;

			if( $this->password )
			{
				// Then we're doing a password update as well
				wp_update_user( array( "ID" => $wp_user_id, "user_pass" => $this->password ) );
			}
			do_action('emu_db_after_update_customers', $this);

		}
		else
		{
			// Create the new user
			if( ! ( $this->email && $this->password && $this->username ) )
			{
				$this->messages[] = "At least an email address, username and password are required to create a new user";
				$this->error = true;
			}

			// Check that the email address (which is used as the username) does not already exist
			if( username_exists( $this->username ) )
			{
				$this->messages[] = "Username already exists";
				$this->error = true;
			}

			if( email_exists( $this->email ) )
			{
				$this->messages[] = "Email address already registered to another account";
				$this->error = true;
			}

			if( $this->error ) return $this->error;

			// Create a new wordpress user
			$wp_user_id = wp_create_user( $this->username, $this->password, $this->email );

			$this->wpUserID = $wp_user_id;

			$this->dbID = $this->insertRecord( "{$emuShop->dbPrefixShared}customers", $this->data );

			// Log the current user into the wordpress system
			$this->creds = array( "user_login" => $this->username, "user_password" => $this->password, "remember" => false );

			do_action( 'emu_shop_user_created', $this );

		}

		// Update the user details in wordpress
		wp_update_user( array( "ID" => $wp_user_id, "first_name" => $this->firstName, "last_name" => $this->lastName, "user_email" => $this->email ) );

		return true;
	}

	public function signIn()
	{
		if( !$this->creds ) return;
		if( !$this->currentUser ) $user = wp_signon( $this->creds, false );
	}

	public function addToWishlist( $product_id )
	{
		if( is_array( $product_id ) )
		{
			foreach( $product_id as $p_id )
				$this->addToWishlist( $p_id );

			return;
		}

		global $wpdb, $emuShop;

		// See if they already have the item in their wishlist
		$sql = "select count(*) from {$emuShop->dbPrefix}wishlist where productID = $product_id and customerID = {$this->dbID}";

		$count = $wpdb->get_var( $sql );

		if( $count == 0 )
			$this->insertRecord( "{$emuShop->dbPrefix}wishlist", array( 'productID' => $product_id, 'customerID' => $this->dbID ) );

		return true;
	}

	public function removeFromWishlist( $product_id )
	{
		if( is_array( $product_id ) )
		{
			foreach( $product_id as $p_id )
				$this->removeFromWishlist( $p_id );

			return;
		}

		global $wpdb, $emuShop;

		$sql = "delete from {$emuShop->dbPrefix}wishlist where productID = $product_id and customerID = {$this->dbID}";
		$wpdb->query( $sql );

	}

	public function getWishList()
	{
		if( !$this->wishlist )
		{
			global $wpdb, $emuShop;

			$sql = "select productID from {$emuShop->dbPrefix}wishlist where customerID = {$this->dbID}";

	        $this->wishList = array();

			$list = $wpdb->get_results( $sql );

			foreach( $list as $product )
			{
				$item = $emuShop->getInstance( 'emuProduct', $product->productID );
				$this->wishList[] = $item;
			}
		}

		return $this->wishList;
	}

	public function addCustomVar( $var_name, $var_value )
	{
		$this->arrCustomVars[ $var_name ] = $var_value;
	}

	public function updateCustomVar($var_name, $var_value)
	{
		$this->addCustomVar( $var_name, $var_value );
	}

	public function clearCustomVars()
	{
		$this->arrCustomVars = array();
	}

	public function getCustomVar( $var_name )
	{
		if( isset( $this->arrCustomVars[ $var_name ] ) )
			return $this->arrCustomVars[ $var_name ];
		else
			return null;
	}

    public function getID()
    {
        return $this->dbID;
    }

}

?>
