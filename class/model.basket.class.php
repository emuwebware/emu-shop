<?php


class emuBasket
{
	public $error = false;
	public $messages = array();

	// Array to store product items
	public $items = array();
	public $sessionData;

	// To store various objects
	public $customer;
	public $shippingAddress;
	public $billingAddress;
	public $paymentMethod;
	public $shippingCharge;
	public $handlingCharge;
	public $couponDiscount;

	// Or array of objects
	public $shippingCharges = array();
	public $handlingCharges = array();

	public $specialCharges = array();
	public $specialDiscounts = array();

	public $taxes = array();

	public static $staticTemplateTags = array( 	'order basket items sub total' => array( 'this', 'getTotal', BASKET_ITEM_SUB_TOTAL_FORMATTED ),
												'order sub total' => array( 'this', 'getTotal', BASKET_SUB_TOTAL_FORMATTED ),
												'order grand total' => array( 'this', 'getTotal', BASKET_GRAND_TOTAL_FORMATTED ),
												'order shipping total' => array( 'this', 'getTotal', BASKET_SHIPPING_TOTAL_FORMATTED ),
												'order handling total' => array( 'this', 'getTotal', BASKET_HANDLING_TOTAL_FORMATTED ),
												'order discount total' => array( 'this', 'getTotal', BASKET_DISCOUNT_TOTAL_FORMATTED ),
												'order tax total' => array( 'this', 'getTotal', BASKET_TAX_TOTAL_FORMATTED ),
												'number items' => 'numberItemsFormatted'
											);

	public function __construct($refresh = false)
	{
		global $emuShop;

		$emuShop->loadModel('emuBasketItem');

		$this->templateTags = self::$staticTemplateTags;

		$this->loadBasketFromSession();
		$this->getCustomer();

		do_action( 'emu_shop_basket_preload', $this );
	}

	public function createOrder()
	{
		global $emuShop;

		$order = $emuShop->getModel('emuOrder');

		$order->prepareFromBasket( $this );

		return $order;
	}

	public function getCustomer()
	{
		global $emuShop;

		$this->customer = $emuShop->customerManager->getCustomer();

		return $this->customer;
	}

	public function getItems()
	{
		return $this->items;
	}

	public function emptyBasket()
	{
		$this->resetBasket();
		$this->updateBasketDetailsInSession();
	}

	// Both addItem and removeItem can receive the product to add/remove in a number of ways
	// this function creates the corresponding product object for the params given
	protected function createProductByParams($product_params)
	{
		global $emuShop;

		switch( true )
		{
			case is_array( $product_params ):

				if( count( $product_params ) == 2 )
					$product = $emuShop->getInstance('emuProduct', array( $variant_id = $product_params[1], $post_id = $product_params[0] ) );
				else // because they're passing in the product id in a single array?
				{
					trigger_error( "Could not add product to basket - invalid parameters." );
					return null;
				}

				break;

			case is_numeric( $product_params ):

				$product = $emuShop->getInstance('emuProduct', $product_id = $product_params );

				break;

			case is_object( $product_params ):

				$product = $product_params;
				break;
		}

		return $product;

	}

	public function addItem( $new_basket_item, $qty = 1, $product_options = array() )
	{
		global $emuShop;

		if( !is_a($new_basket_item, 'emuBasketItem') )
		{
			$product = $this->createProductByParams($new_basket_item);

			$new_basket_item = $emuShop->getInstance('emuBasketItem', $product->getID() );
			$new_basket_item->setOptions($product_options);

		}

		if( !$basket_item = $this->findBasketItem($new_basket_item) )
		{
			$basket_item = $new_basket_item;
			$this->items[] = $basket_item;
		}

		if( $qty == 0 )
		{
			$this->removeItem( $basket_item );
			return;
		}

		if( $basket_item->isStockTracked && $basket_item->qtyInStock <= 0 )
		{
			if( $basket_item->noStockLeftOption == 'deny' )
			{
				$this->error = true;
				$this->messages[] = "This item is currently out of stock and can not be added to your basket.";
				return;
			}
		}

		$basket_item->addQty($qty);

		$this->updateBasketDetailsInSession();
	}

	public function removeItem( $the_basket_item )
	{
		$the_basket_item = $this->createProductByParams($the_basket_item);

		// Reset;
		$basket_items = array();

		for( $i = 0, $iCount = count($this->items); $i < $iCount; $i++ )
		{
			$basket_item = $this->items[$i];

			if( $basket_item->getUniqueRef() == $the_basket_item->getUniqueRef() )
				continue; // i.e. don't include in the updated basket item array

			$basket_items[] = $basket_item;
		}

		$this->items = $basket_items;

		$this->updateBasketDetailsInSession();
	}

	public function updateItemQty($the_basket_item, $qty )
	{
		if( !$basket_item = $this->findBasketItem( $the_basket_item ) )
		{
			trigger_error( "Could not update product quantity: couldn't find product in basket." );
			return null;
		}

		if( $qty == 0 )
			$this->removeItem( $basket_item );
		else
			$basket_item->setQty($qty);

		$this->updateBasketDetailsInSession();
	}

	public function findBasketItem($basket_item_found)
	{
		foreach( $this->getItems() as $basket_item )
		{
			if( ($basket_item->getUniqueRef() == $basket_item_found->getUniqueRef()) ) return $basket_item;
		}
		return false;
	}

	public function findItemByRef($ref)
	{
		foreach( $this->getItems() as $basket_item )
		{
			if( $basket_item->getUniqueRef() == $ref ) return $basket_item;
		}
		return false;
	}

	public function getTotal($format = null)
	{
		global $emuShop;

		switch( $format )
		{
			case BASKET_ITEM_SUB_TOTAL:
				return $this->getBasketItemsTotal();

			case BASKET_ITEM_SUB_TOTAL_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_ITEM_SUB_TOTAL) ));

			case BASKET_SUB_TOTAL:
				return $this->getSubTotal();

			case BASKET_SUB_TOTAL_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_SUB_TOTAL) ));

			case BASKET_SHIPPING_TOTAL:
				return $this->getShippingTotal();

			case BASKET_SHIPPING_TOTAL_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_SHIPPING_TOTAL) ));

			case BASKET_HANDLING_TOTAL:
				return $this->getHandlingTotal();

			case BASKET_HANDLING_TOTAL_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_HANDLING_TOTAL) ));

			case BASKET_DISCOUNT_TOTAL:
				return $this->getCouponDiscountTotal() + $this->getSpecialDiscountsTotal();

			case BASKET_DISCOUNT_TOTAL_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_DISCOUNT_TOTAL) ));

			case BASKET_TAX_TOTAL:
				return $this->getTaxTotal();

			case BASKET_TAX_TOTAL_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_TAX_TOTAL) ));

			case BASKET_GRAND_TOTAL:
				return $this->getGrandTotal();

			case BASKET_GRAND_TOTAL_FORMATTED:
			default:

				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(BASKET_GRAND_TOTAL) ));
		}
	}

	public function getGrandTotal()
	{
		$grand_total = $this->getSubTotal() + $this->getShippingTotal() + $this->getHandlingTotal() + $this->getTaxTotal() + $this->getSpecialChargesTotal();

		if( $grand_total <= 0 ) $grand_total = 0;

		return $grand_total;
	}

	public function hasShippingCharge()
	{
		return $this->hasShippingCost();
	}

	public function hasSpecialCharges()
	{
		$special_charges = $this->getSpecialCharges();

		if( count( $special_charges ) > 0 ) return $special_charges;

		return false;
	}

	public function hasSpecialDiscounts()
	{
		$special_discounts = $this->getSpecialDiscounts();

		if( count( $special_discounts ) > 0 ) return $special_discounts;

		return false;
	}

	public function getSpecialCharges()
	{
		return apply_filters( 'emu_shop_special_charges', $this->specialCharges, $this );
	}

	public function getSpecialDiscounts()
	{
		return apply_filters( 'emu_shop_special_discounts', $this->specialDiscounts, $this );
	}

	public function addSpecialDiscount($special_discount)
	{
		$this->specialDiscounts[] = $special_discount;
	}

	public function addSpecialCharge($special_charge)
	{
		$this->specialCharges[] = $special_charge;
	}

	public function hasShippingCost()
	{
		global $emuShop;

		if( !$emuShop->shippingChargesEnabled ) return false;
		if( !$shipping_charge = $this->getShippingCharge() ) return false;

		return $shipping_charge;
	}

	public function getShippingTotal()
	{
		if( !$shipping_charge = $this->hasShippingCost() ) return 0;
		return $shipping_charge->charge;
	}

	public function hasHandlingCost()
	{
		return $this->getHandlingTotal() == 0 ? false : true;
	}

	public function getHandlingTotal()
	{
		global $emuShop;

		if( !$emuShop->handlingChargesEnabled ) return 0;

		if( $this->getSessionData('shipping_charge') == $emuShop->pickupText && $emuShop->pickupFreeHandling ) return 0; // no handling

		$applicable_handling_charges = $this->getApplicableHandlingCharges();

		$handling_total = 0;

		// All handling charges apply (unlike shipping charges which they can choose)
		foreach( $applicable_handling_charges as $handling_charge )
		{
			$handling_total += $handling_charge->charge;
		}

		// Add items that have specific handling costs (and aren't handled by the global handling rules)
		$handling_total += $this->getIndividualItemsHandlingCost();

		return $handling_total;
	}

	public function getSpecialDiscountsTotal()
	{
		if( !$special_discounts = $this->getSpecialDiscounts() ) return 0;

		$total = 0;

		foreach( $special_discounts as $special_discount )
			$total += $special_discount->getDiscountValue();

		return $total;
	}


	public function getSpecialChargesTotal()
	{
		if( !$special_charges = $this->getSpecialCharges() ) return 0;

		$total = 0;

		foreach( $special_charges as $special_charge )
			$total += $special_charge->getChargeValue();

		return $total;
	}


	public function hasCouponDiscount()
	{
		if( !$coupon = $this->getCoupon() ) return false;
		return $coupon;
	}

	public function getCouponDiscountTotal()
	{
		if( !$coupon = $this->hasCouponDiscount() ) return 0;

		$discount_total = 0;

		$basket_items_total = $this->getBasketItemsTotal();

		switch( $coupon->discountType )
		{
			case 'currency':

				$discount_total = $coupon->discount;
				break;

			case 'percentage':

				// want the percentage as a decimal
				$percentage = $coupon->discount > 1 ? ( $coupon->discount / 100 ) : $coupon->discount;

				// Discounts are taken off the basket sub total
				$discount_total = $basket_items_total * $percentage;
				break;
		}

		// Limit the discount to the basket item total
		if( $basket_items_total < $discount_total ) $discount_total = $basket_items_total;

		return $discount_total;

	}

	public function hasTaxes()
	{
		$taxes = $this->getTaxes();

		if( count( $taxes ) == 0 ) return false;

		return $taxes;
	}

	public function getTaxTotal()
	{
		if( !$taxes = $this->hasTaxes() ) return 0;

		$tax_total = 0;

		foreach( $taxes as $tax )
			$tax_total += $tax->getCalculatedValue();

		return $tax_total;
	}

	public function getSubTotal()
	{
		$sub_total = $this->getBasketItemsTotal() - $this->getCouponDiscountTotal() - $this->getSpecialDiscountsTotal();
		return $sub_total;
	}

	public function getSubTotalForTaxes()
	{
		$sub_total = $this->getBasketItemsTotalForTaxes() - $this->getCouponDiscountTotal() - $this->getSpecialDiscountsTotal();
		if( $sub_total < 0 ) $sub_total = 0;
		return $sub_total;
	}

	public function getShippingCharge()
	{
		// Get the list of applicable charges
		$applicable_shipping_charges = $this->getApplicableShippingCharges();

		// If there's only one charge available then that's the charge that's applied
		if( count( $applicable_shipping_charges ) == 1 )
		{
			// Only one mandatory option
			return $applicable_shipping_charges[0];
		}

		// Otherwise return the charge selected
		foreach( $applicable_shipping_charges as $shipping_charge )
		{
			if( $shipping_charge->description === $this->getSessionData('shipping_charge') )
				return $shipping_charge;
		}

		// If no charge has been selected then...
		return false;
	}


	public function getIndividualItemsHandlingCost()
	{
		$basket_items = $this->getItems();

		$handling_cost = 0;

		foreach( $basket_items as $basket_item )
		{
			if( $basket_item->handlingOption == emuM_Handling::OPTION_SPECIFIC_CHARGE )
				$handling_cost = $basket_item->handlingCost === '' ? (float) $emuShop->getMeta('default_handling_charge') : $basket_item->getPrice(PRODUCT_HANDLING_COST);
		}
		return $handling_cost;
	}

	public function getBasketItemsTotal()
	{
		$basket_items_total = 0;

		$basket_items = $this->getItems();

		foreach( $basket_items as $basket_item )
			$basket_items_total += $basket_item->getTotal(BASKET_ITEM_TOTAL);

		return $basket_items_total;
	}

	public function getBasketItemsTotalForTaxes()
	{
		$basket_items_total = 0;

		$basket_items = $this->getItems();

		foreach( $basket_items as $basket_item )
			$basket_items_total += $basket_item->getTotal(BASKET_ITEM_TAX_TOTAL);

		return $basket_items_total;
	}

    public function __get( $member )
 	{
 		global $emuShop;

 		switch( $member )
 		{
 			case 'numberItemsFormatted':

 				$number_items = $this->numberItems;

 				return "{$number_items} item".($number_items > 1 ? 's' : '');

 			case 'numberItems':

 				return $this->getNumberItems();

			case 'isEmpty':

				return $this->numberItems == 0;

			case 'basketSubTotal':

				return $this->getBasketItemsSubTotal();

			case 'subTotal':

				return $this->basketSubTotal - $this->discountTotal;

			default:

				if( !isset( $this->data[ $member ] ) ) return null;
				return $this->data[ $member ];

 		}

 	}

 	public function getNumberItems()
 	{
 		$number_items = 0;
 		foreach( $this->items as $basket_item )
 			$number_items += $basket_item->qty;

 		return $number_items;
 	}

	public function formatEmpty($number)
	{
		global $emuShop;
		if( trim( $number ) == $emuShop->currency ) return '-';
		return $number;
	}

    public function getTemplateTags( $prefix = '' )
    {
        if( !isset( $this->templateTags ) ) return array();

        if( $prefix )
        {
            // rebuild the template tags
            $new_tags = array();

            foreach( $this->templateTags as $tag => $value )
                $new_tags[$prefix.$tag] = $value;

            return $new_tags;
        }

        return $this->templateTags;

    }

	public function loadBasketFromSession()
	{
		$this->resetBasket();

		if( $basket_details = $this->getBasketDetailsInSession() )
	 	{
		 	$this->sessionData = $basket_details->data;
		 	$this->loadSessionItems( $basket_details->items );
	 	}
	}

	public function getBasketDetailsInSession()
	{
		if( isset( $_SESSION['basket'] ) )
			return unserialize( $_SESSION['basket'] );

		return false;
	}

	public function loadSessionItems($basket_session_items)
	{
		global $emuShop;

		foreach( $basket_session_items as $session_item )
		{
			$basket_item = $emuShop->getInstance('emuBasketItem', $session_item->productID );
			$basket_item->setQty($session_item->qty);
			$basket_item->setOptions($session_item->options);
			$this->items[] = $basket_item;
		}
	}

	public function setSessionData( $field, $value = null, $update_basket = true )
	{
		if( is_array( $field ) ){
			$arr_fields = $field;

			foreach( $arr_fields as $field => $value ) $this->setSessionData( $field, $value, false );

			if( $update_basket ) $this->updateBasketDetailsInSession();

			return;
		}

		$this->sessionData[$field] = $value;

		if( $update_basket ) $this->updateBasketDetailsInSession();
	}

	public function getSessionData( $field )
	{
		if( ! isset( $this->sessionData[ $field ] ) )
			return '';

		return $this->sessionData[$field];
	}

	public function resetBasket()
	{
		$this->items = array();
		$this->sessionData = null;
	}

	public function updateBasketDetailsInSession()
	{
		$basket_details = (object) array( 'data' => $this->sessionData, 'items' => array() );

		foreach( $this->items as $basket_item )
			$basket_details->items[] = (object) array( 'productID' => $basket_item->getID(), 'qty' => $basket_item->getQty(), 'options' => $basket_item->getOptions() );

		$_SESSION['basket'] = serialize($basket_details);
	}

	function setShippingAddress($address)
	{
		$this->setSessionData( 'shipping_address', serialize($address) );
	}

	function setBillingAddress($address)
	{
		$this->setSessionData( 'billing_address', serialize($address));
	}

	function setPaymentMethod($payment_method)
	{
		$this->setSessionData( 'payment_method', serialize($payment_method) );
	}

	function getShippingAddress()
	{
		global $emuShop;

		if( !$this->shippingAddress )
		{
			$emuShop->loadClass( 'emuAddress' );

			if( $shipping_address = $this->getSessionData('shipping_address') )
			{
				$this->shippingAddress = unserialize( $shipping_address );
			}
			else
			{
				if( !$this->customer->isAnon() && $this->customer->addressBook->defaultShippingAddress )
					$this->shippingAddress = $this->customer->addressBook->defaultShippingAddress;
			}
		}

		return $this->shippingAddress;
	}

	function getBillingAddress()
	{
		global $emuShop;

		if( !$this->billingAddress )
		{
			$emuShop->loadClass( 'emuAddress' );

			if( $billing_address = $this->getSessionData('billing_address') )
				$this->billingAddress = unserialize( $billing_address );
			else
			{
				if( !$this->customer->isAnon() && $this->customer->addressBook->defaultBillingAddress )
					$this->billingAddress = $this->customer->addressBook->defaultBillingAddress;
			}
		}
		return $this->billingAddress;
	}

	function getPaymentMethod()
	{
		if( !$this->paymentMethod )
		{
			global $emuShop;

			$emuShop->loadClass( 'emuPaymentMethod' );

			if( $payment_method = $this->getSessionData('payment_method') )
				$this->paymentMethod = unserialize( $payment_method );
			else
			{
				if( !$this->customer->isAnon() && $this->customer->paymentManager->defaultPaymentMethod )
					$this->paymentMethod = $this->customer->paymentManager->defaultPaymentMethod;
			}
		}

		return $this->paymentMethod;
	}

	public function getApplicableShippingCharges()
	{
		global $emuShop;

		if( ! $emuShop->shippingChargesEnabled ) return array();

		// get charges
		$applicable_charges = array();

		// loop through all the active shipping options and see which ones apply
		$charges = $emuShop->shippingManager->getShippingCharges( array( 'isActive' => '1' ) );

		// Shipping charge rules that apply to total weight or total cost, only apply to those items that
		// come under those shipping rules
		$items_shipping_price_total = $this->getItemsShippingPriceTotal();
		$items_shipping_weight_total = $this->getItemsShippingWeightTotal();

		$shipping_country_id = null;
		$shipping_state_id = null;

		if( $shipping_address = $this->getShippingAddress() )
		{
			$shipping_country_id = $shipping_address->countryID;
			$shipping_state_id = $shipping_address->stateID;
		}

		$this->shippingCharges = array(); // reset

		foreach( $charges as $shippingCharge )
		{
			if( $shippingCharge->appliesTo( $items_shipping_price_total, $items_shipping_weight_total, $shipping_country_id, $shipping_state_id ) )
				$applicable_charges[] = $shippingCharge;
		}

		$have_specific_charge = false;

		// Loop through all the charges and see if we have a specific charge i.e. a specific country as opposed to 'All Countries'
		foreach( $applicable_charges as $charge )
		{
			if( $charge->countryID ) $have_specific_charge = true;
		}

		// rebuild the shipping charges
		$charges = array();

		foreach( $applicable_charges as $shippingCharges )
		{
			if( !$shippingCharges->countryID && $have_specific_charge ) continue;

			if( $shippingCharge->freeShipping )
			{
				$shippingCharge->description .= ' - Free shipping';
				$shippingCharge->charge = 0;
			}

			$charges[] = $shippingCharges;
		}

		$coupon_free_shipping = false;

		// See if the coupon (if one has been applied) has free shipping
		if( $coupon = $this->getCoupon() )
		{
			if( $coupon->freeShipping )
			{
				$shippingCharge = $emuShop->getInstance( 'emuShippingCharge' );
				$shippingCharge->description = 'Free shipping';
				$shippingCharge->charge = 0;

				// Overwrite all other shipping charges with a single free charge
				$charges = array( $shippingCharge );

				$coupon_free_shipping = true;
			}
		}

		if( !$coupon_free_shipping )
		{
			// Add in any individual shipping cost
			if( $total_individual_item_shipping_cost = $this->getItemsIndividualShippingCost() )
			{
				if( count( $charges ) > 0 )
				{
					// then add the individual shipping cost to all the options
					foreach( $charges as $shippingCharge )
					{
						if( !$shippingCharge->freeShipping )
						{
							$shippingCharge->charge += $total_individual_item_shipping_cost;
						}
					}
				}
				else
				{
					// Add it as a new individual shipping charge
					$shippingCharge = $emuShop->getInstance( 'emuShippingCharge' );
					$shippingCharge->description = 'Shipping';
					$shippingCharge->charge = $total_individual_item_shipping_cost;
					$charges[] = $shippingCharge;
				}
			}
		}

		if( $emuShop->pickupEnabled )
		{
			$shippingCharge = $emuShop->getInstance( 'emuShippingCharge' );
			$shippingCharge->description = $emuShop->pickupText;
			$shippingCharge->charge = $emuShop->pickupCost;
			$charges[] = $shippingCharge;
		}

		if( has_filter( 'emu_shipping_charges' ) )
			$charges = apply_filters( 'emu_shipping_charges', array( $charges, $this));

		return $charges;
	}


	public function getItemsIndividualShippingCost()
	{
		$basket_items = $this->getItems();

		$items_individual_shipping_cost = 0;

		foreach( $basket_items as $basket_item )
		{
			if( $basket_item->shippingOption == emuM_Shipping::OPTION_SPECIFIC_CHARGE )
				$items_individual_shipping_cost += $basket_item->getTotal(BASKET_ITEM_SHIPPING_COST_TOTAL);
		}

		return $items_individual_shipping_cost;
	}

	public function getCoupon($return_false_if_invalid = true)
	{
		global $emuShop;

		if( !$coupon_code = $this->getSessionData('coupon_code') ) return false;

		$coupon = $emuShop->getInstance( 'emuCoupon', array(null, $coupon_code) );

		if( $return_false_if_invalid )
		{
			if( $coupon->isValid() )
				return $coupon;
			else
				return false;
		}
		return $coupon;
	}

	public function getItemsHandlingPriceTotal()
	{
		$basket_items = $this->getItems();

		$items_handling_price_total = 0;

		foreach( $basket_items as $basket_item )
		{
			if( $basket_item->handlingOption == emuM_Handling::OPTION_CHARGES )
				$items_handling_price_total += $basket_item->getTotal(BASKET_ITEM_TOTAL);
		}

		return $items_handling_price_total;
	}

	public function getItemsHandlingWeightTotal()
	{
		$basket_items = $this->getItems();

		$items_handling_weight_total = 0;

		foreach( $basket_items as $basket_item )
		{
			if( $basket_item->handlingOption == emuM_Handling::OPTION_CHARGES )
				$items_handling_weight_total += $basket_item->getTotal(BASKET_ITEM_WEIGHT_TOTAL);
		}

		return $items_handling_weight_total;
	}

	public function getItemsShippingPriceTotal()
	{
		$basket_items = $this->getItems();

		$items_shipping_price_total = 0;

		foreach( $basket_items as $basket_item )
		{
			if( $basket_item->shippingOption == emuM_Shipping::OPTION_CHARGES )
				$items_shipping_price_total += $basket_item->getTotal(BASKET_ITEM_TOTAL);
		}

		return $items_shipping_price_total;
	}

	public function getItemsShippingWeightTotal()
	{
		$basket_items = $this->getItems();

		$items_shipping_weight_total = 0;

		foreach( $basket_items as $basket_item )
		{
			if( $basket_item->shippingOption == emuM_Shipping::OPTION_CHARGES )
				$items_shipping_weight_total += $basket_item->getTotal(BASKET_ITEM_WEIGHT_TOTAL);
		}

		return $items_shipping_weight_total;
	}


	public function getApplicableHandlingCharges()
	{
		global $emuShop;

		if( ! $emuShop->handlingChargesEnabled ) return array();

		// get charges
		$applicable_charges = array();

		// loop through all the active handling options and see which ones apply
		$charges = $emuShop->handlingManager->getHandlingCharges( array( 'isActive' => '1' ) );

		// Handling charge rules that apply to total weight or total cost, only apply to those items that
		// come under those handling rules
		$items_handling_price_total = $this->getItemsHandlingPriceTotal();
		$items_handling_weight_total = $this->getItemsHandlingWeightTotal();

		$shipping_country_id = null;
		$shipping_state_id = null;

		if( $shipping_address = $this->getShippingAddress() )
		{
			$shipping_country_id = $shipping_address->countryID;
			$shipping_state_id = $shipping_address->stateID;
		}

		foreach( $charges as $handlingCharge )
		{
			if( $handlingCharge->appliesTo( $items_handling_price_total, $items_handling_weight_total, $shipping_country_id, $shipping_state_id  ) )
				$applicable_charges[] = $handlingCharge;
		}

		$have_specific_charge = false;

		// Loop through all the charges and see if we have a specific charge i.e. a specific country as opposed to 'All Countries'
		foreach( $applicable_charges as $charge )
		{
			if( $charge->countryID ) $have_specific_charge = true;
		}

		// rebuild the handling charges
		$charges = array();

		// Specific charges get presidence over non specific charges (so that you can set default handling rules)
		foreach( $applicable_charges as $handlingCharge )
		{
			if( !$handlingCharge->countryID && $have_specific_charge ) continue;

			if( $handlingCharge->freeHandling )
			{
				$handlingCharge->description .= ' - Free handling';
				$handlingCharge->charge = 0;
			}

			$charges[] = $handlingCharge;
		}

		if( has_filter( 'emu_handling_charges' ) )
			$charges = apply_filters( 'emu_handling_charges', array( $charges, $this ) );

		return $charges;
	}


	public function hasShippingAddress()
	{
		return $this->getShippingAddress();
	}

	public function hasBillingAddress()
	{
		return $this->getBillingAddress();
	}

	public function hasPaymentMethod()
	{
		return $this->getPaymentMethod();
	}

	public function getTaxes()
	{
		global $emuShop;

		// Can only apply taxes if we have a shipping address...
		if( !$shipping_address = $this->getShippingAddress() ) return array();

		if( !$sub_total = $this->getSubTotalForTaxes() ) return array();

		$taxes = array();

		foreach( $emuShop->taxManager->getTaxRates() as $rate )
		{
			if( $rate->appliesTo( $shipping_address->countryName, $shipping_address->stateName ) )
				$taxes[] = $rate;
		}

		if( count( $taxes ) == 0 )
		{
			// Apply the default tax rate (if one exists)
			if( $default_tax_rate = $emuShop->getMeta('default_tax_rate') )
			{
				$tax_rate = $emuShop->getModel('emuTaxRate');
				$tax_rate->updateRate($default_tax_rate);
				$taxes[] = $tax_rate;
			}
		}

		// $sub_total = $this->getSubTotal();

		// Calculate taxes (and see if any of the taxes need updating for washington state)
		foreach( $taxes as $tax_rate )
		{
			if( $tax_rate->stateName == 'Washington' )
			{
				if( $wa_tax_rate = $this->retrieveWATaxRate() )
					$tax_rate->updateRate($wa_tax_rate);

				$tax_rate->calculateUsingValue( $sub_total + $this->getShippingTotal() );
			}
			else
			{
				$tax_rate->calculateUsingValue( $sub_total );
			}

		}

		return $taxes;
	}

	public function retrieveWATaxRate()
	{
		if( !$shipping_address = $this->getShippingAddress() ) return false;

		$fields = array( 'output' => 'xml', 'zip' => $shipping_address->postalCode, 'addr' => $shipping_address->addressLineOne, 'city' => $shipping_address->city );

		$query_string = '';

		foreach( $fields as $key => $value )
			$query_string .= "$key=" . urlencode( $value ) . "&";

		$request = curl_init( 'http://dor.wa.gov/AddressRates.aspx?'.$query_string );

		curl_setopt( $request, CURLOPT_HEADER, 0 ); // set to 0 to eliminate header info from response
		curl_setopt( $request, CURLOPT_RETURNTRANSFER, 1 ); // Returns response data instead of TRUE(1)

		$post_response = curl_exec( $request );
		$header  = curl_getinfo( $request );

		curl_close( $request ); // close curl object

		if( $header['http_code'] == 200 )
		{
			$xml_response = $post_response;

			$root = new \SimpleXMLElement( $xml_response );

			$rate_data = $root->attributes();

			if( (int) $rate_data['code'] === 0 )
				return (float) $rate_data['rate'];
		}
		return false;
	}

}

// class emuBasket extends emuShopCommon
// {
// 	public $basket = array();

// 	public $error = false;

// 	public $messages = array();
// 	public $items = array();

// 	public $customer;

// 	public $orderSummary;


// 	public $shippingAddress;
// 	public $billingAddress;
// 	public $paymentMethod;
// 	public $shippingCharge;
// 	public $handlingCharge;
// 	public $couponDiscount;

// 	public $shippingCharges = array();
// 	public $handlingCharges = array();
// 	public $taxes = array();


// 	public $templateTags;

// 	public function __construct()
// 	{
// 		$this->templateTags = self::$staticTemplateTags;

// 		$this->getSessionBasket();
// 		$this->getBasketDetails();
// 		$this->getCustomer();

// 		$this->applyDiscounts();

// 		if( !$this->customer ) return;

// 		do_action( 'emu_shop_basket_preload', $this );

// 		$this->getShippingAddress();
// 		$this->getBillingAddress();
// 		$this->getPaymentMethod();

// 		$this->applyShippingCharge();
// 		$this->applyHandlingCharge();
// 		$this->applyTaxes();

// 		do_action( 'emu_shop_basket_postload', $this );

// 	}

//     public function __get( $member )
// 	{
// 		global $emuShop;

// 		switch( $member )
// 		{
// 			case 'grandTotal':

// 				$grandTotal = $this->basketSubTotal - $this->discountTotal + $this->shippingTotal + $this->handlingTotal + $this->taxTotal;

// 				if( $grandTotal <= 0 ) $grandTotal = 0;

// 				return $grandTotal;

// 			case 'basketSubTotal':

// 				return $this->getBasketSubTotal();

// 			case 'subTotal':

// 				return $this->basketSubTotal - $this->discountTotal;

// 			case 'weight':

// 				return $this->getWeight();

// 			case 'numberItems':

// 				$numberItems = 0;

// 				foreach( $this->basket->items as $basket_item => $qty ) $numberItems += $qty;

// 				return $numberItems;

// 			case 'displayNumberItems':

// 				$numberItems = $this->numberItems;

// 				return "{$numberItems} item".($numberItems > 1 ? 's' : '');

// 			case 'isEmpty':

// 				return $this->numberItems == 0;

// 			case 'displayGrandTotal':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->grandTotal );

// 			case 'displayBasketSubTotal':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->basketSubTotal );

// 			case 'displayShippingTotal':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->shippingTotal );

// 			case 'displayHandlingTotal':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->handlingTotal );

// 			case 'displayTaxTotal':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->taxTotal );

// 			case 'displayDiscountTotal':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->discountTotal );

// 			case 'displaySubTotal':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->subTotal);
// 		}
//     }

// 	public function getBasketSubTotal()
// 	{
// 		$subTotal = 0;

// 		foreach( $this->items as $product ) $subTotal += $product->total;

// 		return $subTotal;
// 	}
// 	public function emptyBasket()
// 	{
// 		global $emuShop;
// 		if( isset( $_SESSION['basket'] ) ) $_SESSION['basket'] = null;
// 		$this->basket = array();
// 		$this->items = array();

// 		$emuShop->getBasket( true );
// 	}

// 	public function addItem( $item, $qty = 1 )
// 	{
// 		global $emuShop;

// 		if( !preg_match( '/(variant|product)\-([0-9]+)/', $item, $matches ) )
// 		{
// 			$this->error = true;
// 			$this->messages[] = "To add an item to the basket it must be in the format [variant|product]-[product id]";
// 			return;
// 		}

// 		$product_id = $matches[2];
// 		$product_type = $matches[1];

// 		if( $product_type == 'variant' )
// 			$product = $emuShop->getInstance( 'emuVariant', array( $product_id ) );
// 		else
// 			$product = $emuShop->getInstance( 'emuProduct', array( $product_id ) );
// 		if( $product->isStockTracked && $product->qtyInStock <= 0 )
// 		{
// 			if( $product->noStockLeftOption == 'deny' )
// 			{
// 				$this->error = true;
// 				$this->messages[] = "This item is currently out of stock and can not be added to your basket.";
// 				return;
// 				unset( $product );
// 			}
// 		}

// 		if( !isset( $this->basket->items[$item] ) ) $this->basket->items[$item] = 0;

// 		$this->basket->items[$item] += $qty;
// 	}

// 	public function updateItemQty( $item, $qty )
// 	{
// 		$this->basket->items[$item] = $qty;
// 		if( floatval( $this->basket->items[$item] ) == 0 ) $this->removeItem( $item );
// 	}

// 	public function removeItem( $item )
// 	{
// 		unset( $this->basket->items[$item] );
// 	}

// 	public function setSessionData( $field, $value = null, $update_basket = true )
// 	{
// 		if( is_array( $field ) ){
// 			$arr_fields = $field;

// 			foreach( $arr_fields as $field => $value ) $this->setSessionData( $field, $value, false );

// 			if( $update_basket ) $this->update();

// 			return;
// 		}

// 		$this->basket->data[$field] = $value;

// 		if( $update_basket ) $this->update();
// 	}

// 	public function getSessionData( $field )
// 	{
// 		if( ! isset( $this->basket->data[ $field ] ) )
// 			return '';

// 		return $this->basket->data[$field];
// 	}

// 	public function update()
// 	{
// 		$_SESSION['basket'] = serialize( $this->basket );
// 	}

// 	public function getSessionBasket()
// 	{
// 		if( isset( $_SESSION['basket'] ) )
// 		{
// 			$session_basket = unserialize( $_SESSION['basket'] );
// 			$this->basket = $session_basket;
// 		}
// 		else
// 		{
// 			// create the shopping basket
// 			$this->basket = (object) array( 'data' => array(), 'items' => array() );
// 			$this->update();
// 		}
// 	}

// 	public function getBasketDetails()
// 	{
// 		global $emuShop;

// 		if( count( $this->basket->items ) == 0 ) return;

// 		$p_ids = array();

// 		// Go through each product in the session basket
// 		foreach( $this->basket->items as $basket_item => $qty )
// 		{
// 			if( preg_match( '/(variant|product)\-([0-9]+)/', $basket_item, $matches ) )
// 			{
// 				switch( $matches[1] )
// 				{
// 					case 'product':
// 						$item = $emuShop->getInstance( 'emuProduct', array( $matches[2] ) ); break;
// 					case 'variant':
// 						$item = $emuShop->getInstance( 'emuVariant', array( $matches[2] ) ); break;
// 				}

// 				// set the item qty
// 				$item->qty = $qty;

// 				// set the price
// 				$item->total = $item->qty * $item->getPrice();

// 				// Put together a total for display
// 				$item->displayTotal = $emuShop->currency.apply_number_format( 'currency', $item->total );

// 				// Calculate and add up the weights
// 				$item->weightTotal = $item->qty * $item->convertedWeight;

// 				// Calculate and add up the shipping cost
// 				switch( $item->shippingOption )
// 				{
// 					case emuM_Shipping::OPTION_CHARGES: // using the global charges

// 						// Global charges based on weight...
// 						$this->shippingWeightTotal += $item->weightTotal;

// 						// ... and price
// 						$this->shippingPriceTotal += $item->total;

// 						/* Note: shippingWeightTotal and shippingPriceTotal are only used to determine which
// 						// charge rules apply and should not be confused with the overall basket weight for example
// 						*/

// 					break;
// 					case emuM_Shipping::OPTION_NO_CHARGE: // not applying a charge to this item

// 						// don't need to do anything

// 					break;
// 					case emuM_Shipping::OPTION_SPECIFIC_CHARGE: // item has a specific charge

// 						// if the item doesn't have a value then use the global shipping charge

// 						$shipping_cost = $item->shippingCost === '' ? (float) get_option('e_default_shipping_charge') : $item->shippingCost;

// 						// all items with an individual price are added up and added to the final shipping total
// 						$this->totalIndividualShippingCost += $item->qty * $shipping_cost;

// 					break;

// 				}

// 				// Calculate and add up the handling cost
// 				switch( $item->handlingOption )
// 				{
// 					case emuM_Handling::OPTION_CHARGES: // using the global charges

// 						// Global charges based on weight...
// 						$this->handlingWeightTotal += $item->weightTotal;

// 						// ... and price
// 						$this->handlingPriceTotal += $item->total;

// 						/* Note: shippingWeightTotal and shippingPriceTotal are only used to determine which
// 						// charge rules apply and should not be confused with the overall basket weight for example
// 						*/

// 					break;
// 					case emuM_Handling::OPTION_NO_CHARGE: // not applying a charge to this item

// 						// don't need to do anything

// 					break;
// 					case emuM_Handling::OPTION_SPECIFIC_CHARGE: // item has a specific charge

// 						// if the item doesn't have a value then use the global shipping charge

// 						$handling_cost = $item->handlingCost === '' ? (float) get_option('e_default_handling_charge') : $item->handlingCost;

// 						// all items with an individual price are added up and added to the final shipping total
// 						$this->totalIndividualHandlingCost += $item->qty * $handling_cost;

// 					break;

// 				}

// 				/*

// 				if( $item->hasShippingCost )
// 				{
// 					// If the item does not have a shipping cost then apply the default shipping charge (as set in shipping options)
// 					$shipping_cost = $item->shippingCost === '' ? (float) get_option('e_default_shipping_charge') : $item->shippingCost;

// 					$item->shippingCostTotal = $item->qty * $shipping_cost;
// 					$this->totalIndividualShippingCost += $item->shippingCostTotal;

// 					$item->shippingWeight = $item->qty * $item->convertedWeight;
// 					$this->shippingWeightTotal += $item->shippingWeight;
// 					$this->shippingPriceTotal += $item->total;
// 				}

// 				// Calculate and add up the handling cost
// 				if( $item->hasHandlingCost )
// 				{
// 					// If the item does not have a handling cost then apply the default handling charge (as set in handling options)
// 					$handling_cost = $item->handlingCost === '' ? (float) get_option('e_default_handling_charge') : $item->handlingCost;

// 					$item->handlingCostTotal = $item->qty * $handling_cost;
// 					$this->totalIndividualHandlingCost += $item->handlingCostTotal;
// 					$item->handlingWeight = $item->qty * $item->convertedWeight;
// 					$this->handlingWeightTotal += $item->handlingWeight;
// 					$this->handlingPriceTotal += $item->total;
// 				}
// 				*/


// 				// Add the product object to the basket
// 				$this->items[] = $item;

// 			}
// 		}
// 	}


// 	public function getWeight()
// 	{
// 		$weight = 0;

// 		foreach( $this->items as $product) $weight += $product->weightTotal;

// 		return $weight;
// 	}

// 	public function getCustomer()
// 	{
// 		global $emuShop;

// 		if( !is_user_logged_in() ) return;

// 		$this->customer = $emuShop->getManager('customer')->getCustomer();
// 	}


// 	function getPaymentMethod()
// 	{
// 		global $emuShop;

// 		$emuShop->loadClass( 'emuPaymentMethod' );

// 		if( $payment_method = $this->getSessionData('payment_method') )
// 			$this->paymentMethod = unserialize( $payment_method );
// 		else
// 		{
// 			if( $this->customer->paymentManager->defaultPaymentMethod )
// 				$this->paymentMethod = $this->customer->paymentManager->defaultPaymentMethod;
// 		}
// 	}

// 	function getShippingAddress()
// 	{
// 		global $emuShop;

// 		$emuShop->loadClass( 'emuAddress' );

// 		if( $shipping_address = $this->getSessionData('shipping_address') )
// 		{
// 			$this->shippingAddress = unserialize( $shipping_address );
// 		}
// 		else
// 		{
// 			if( $this->customer->addressBook->defaultShippingAddress )
// 				$this->shippingAddress = $this->customer->addressBook->defaultShippingAddress;
// 		}
// 	}

// 	function getBillingAddress()
// 	{
// 		global $emuShop;

// 		$emuShop->loadClass( 'emuAddress' );

// 		if( $billing_address = $this->getSessionData('billing_address') )
// 			$this->billingAddress = unserialize( $billing_address );
// 		else
// 		{
// 			if( $this->customer->addressBook->defaultBillingAddress )
// 				$this->billingAddress = $this->customer->addressBook->defaultBillingAddress;
// 		}
// 	}

// 	public function applyShippingCharge()
// 	{
// 		// can only apply a shipping charge if we have a shipping address
// 		$this->shippingTotal = 0; // reset

// 		$this->shippingCharges = array(); // reset

// 		if( !$this->shippingAddress ) return;

// 		global $emuShop;

// 		if( !$emuShop->shippingChargesEnabled ) return;

// 		$this->getApplicableShippingCharges();

// 		foreach( $this->shippingCharges as $charge )
// 		{
// 			if( $charge->description === $this->getSessionData('shipping_charge') || count( $this->shippingCharges ) == 1 )
// 			{
// 				$this->shippingTotal += $charge->charge;
// 				$this->shippingCharge = $charge;
// 			}
// 		}
// 	}


// 	public function applyHandlingCharge()
// 	{
// 		$this->handlingTotal = 0; // reset
// 		$this->handlingCharges = array(); // reset
// 		$this->handlingCharge = false;

// 		global $emuShop;

// 		if( $this->getSessionData('shipping_charge') == $emuShop->pickupText && $emuShop->pickupFreeHandling ) return; // no handling

// 		if( !$emuShop->handlingChargesEnabled ) return;

// 		$this->getApplicableHandlingCharges();

// 		foreach( $this->handlingCharges as $charge )
// 		{
// 			$this->handlingTotal += $charge->charge;
// 			$this->handlingCharge = true;
// 		}

// 		// i.e. items that have a specific handling cost and aren't handled by the
// 		// global handling rules (charges)
// 		if( $this->totalIndividualHandlingCost ) {
// 			$this->handlingTotal += $this->totalIndividualHandlingCost;
// 			$this->handlingCharge = true;
// 		}
// 	}

// 	public function applyDiscounts()
// 	{
// 		global $emuShop;

// 		if( !$coupon_code = $this->getSessionData('coupon_code') ) return;
// 		$coupon = $emuShop->getInstance( 'emuCoupon' );
// 		$coupon_id = $coupon->getCouponID( $coupon_code );
// 		$coupon->dbID = $coupon_id;
// 		$coupon->getCouponData();

// 		if( $coupon->isValid( $this ) )
// 		{
// 			$this->couponDiscount = $coupon;

// 			switch( $coupon->discountType )
// 			{
// 				case 'currency':

// 					$this->discountTotal = $coupon->discount;
// 					break;

// 				case 'percentage':

// 					// want the percentage as a decimal
// 					$percentage = $coupon->discount > 1 ? ( $coupon->discount / 100 ) : $coupon->discount;
// 					$this->discountTotal = $this->basketSubTotal * $percentage;
// 					break;
// 			}
// 		}

// 		// Discounts are taken off the sub total rather than the grand total.
// 		if( $this->basketSubTotal < $this->discountTotal ) $this->discountTotal = $this->basketSubTotal;

// 	}

// 	public function applyTaxes()
// 	{
// 		global $emuShop;

// 		// Can only apply taxes if we have a shipping address
// 		if( !$this->shippingAddress ) return;

// 		// tax is based on the shipping location
// 		if( count( $emuShop->getManager('region')->getStates( array( 'country' => $this->shippingAddress->countryID ) ) ) > 0 )
// 			$state = $this->shippingAddress->stateID;
// 		else
// 			$state = $this->shippingAddress->shippingState;

// 		$tax_rates = $emuShop->getManager('tax')->getTaxRates();

// 		foreach( $tax_rates as $rate )
// 		{
// 			if( $rate->country == $this->shippingAddress->countryID || !$rate->country )
// 			{
// 				if( $rate->state == $state || !$rate->state )
// 				{
// 					if( $rate->rate > 1 ) // then convert to decimal
// 						$rate->rate = $rate->rate / 100;

// 					if ( $rate->country && $rate->state )
// 						$description = "Tax - {$rate->stateName} State ";
// 					else
// 						$description = 'Tax ';

// 					$description .= apply_number_format( 'percentage', ( $rate->rate * 100 ) )."%";

// 					$rate = (array) $rate;
// 					$rate['description'] = $description;

// 					$this->taxes[] = (object) $rate;
// 				}
// 			}
// 		}

// 		if( count( $this->taxes ) == 0 )
// 		{
// 			// Try and apply the default tax rate
// 			$default_tax_rate = get_option( 'e_default_tax_rate' );

// 			if( $default_tax_rate )
// 			{
// 				if( $default_tax_rate > 1 ) $default_tax_rate = $default_tax_rate / 100;

// 				$description = 'Tax '.apply_number_format( 'percentage', ( $default_tax_rate * 100 ) )."%";

// 				$rate = (object) array( 'rate' => $default_tax_rate, 'country' => '', 'countryName' => '', 'state' => '', 'stateName' => '', 'description' => $description	);

// 				$this->taxes[] = $rate;
// 			}
// 		}

// 		// See if any of the taxes are for washington state
// 		if( count( $this->taxes ) > 0 )
// 		{
// 			foreach( $this->taxes as $tax )
// 			{
// 				if( $tax->stateName == 'Washington' )
// 				{
// 					if( $wa_tax = $this->retrieveWATaxRate() )
// 					{
// 						$tax->rate = $wa_tax;
// 						$tax->description = "Tax - Washington State ".apply_number_format( 'percentage', ( $wa_tax * 100 ) )."%";
// 					}
// 				}
// 			}
// 		}

// 		// Now calculate the tax totals
// 		foreach( $this->taxes as $rate )
// 		{
// 			if( $this->subTotal > 0 ) // can't apply tax to a negative sub total
// 			{
// 				if( $rate->stateName == 'Washington' )
// 					$this->taxTotal += ( $rate->rate * ($this->subTotal + $this->shippingTotal) );
// 				else
// 					$this->taxTotal += ( $rate->rate * $this->subTotal );

// 			}
// 		}
// 	}

// 	public function retrieveWATaxRate()
// 	{
// 		$fields = array( 'output' => 'xml', 'zip' => $this->shippingAddress->postalCode, 'addr' => $this->shippingAddress->addressLineOne, 'city' => $this->shippingAddress->city );

// 		$query_string = '';

// 		foreach( $fields as $key => $value )
// 			$query_string .= "$key=" . urlencode( $value ) . "&";

// 		$request = curl_init( 'http://dor.wa.gov/AddressRates.aspx?'.$query_string );

// 		curl_setopt( $request, CURLOPT_HEADER, 0 ); // set to 0 to eliminate header info from response
// 		curl_setopt( $request, CURLOPT_RETURNTRANSFER, 1 ); // Returns response data instead of TRUE(1)

// 		$post_response = curl_exec( $request );
// 		$header  = curl_getinfo( $request );

// 		curl_close( $request ); // close curl object

// 		if( $header['http_code'] == 200 )
// 		{
// 			$xml_response = $post_response;

// 			$root = new \SimpleXMLElement( $xml_response );

// 			$rate_data = $root->attributes();

// 			if( (int) $rate_data['code'] === 0 )
// 			{
// 				return (float) $rate_data['rate'];
// 			}
// 		}

// 		return false;

// 	}

// 	public function getApplicableShippingCharges()
// 	{
// 		global $emuShop;

// 		if( ! $emuShop->shippingChargesEnabled ) return;

// 		// get charges
// 		$applicable_charges = array();

// 		// loop through all the active shipping options and see which ones apply
// 		$charges = $emuShop->getManager('shipping')->getShippingCharges( array( 'isActive' => '1' ) );

// 		$this->shippingCharges = array(); // reset

// 		foreach( $charges as $shippingCharge )
// 		{
// 			if( $shippingCharge->appliesTo( $this->shippingPriceTotal, $this->shippingWeightTotal, $this->shippingAddress->countryID, $this->shippingAddress->stateID ) )
// 				$applicable_charges[] = $shippingCharge;
// 		}

// 		$have_specific_charge = false;

// 		// Loop through all the charges and see if we have a specific charge i.e. a specific country as opposed to 'All Countries'
// 		foreach( $applicable_charges as $charge )
// 		{
// 			if( $charge->countryID ) $have_specific_charge = true;
// 		}

// 		// rebuild the shipping charges
// 		$charges = array();

// 		foreach( $applicable_charges as $shippingCharges )
// 		{
// 			if( !$shippingCharges->countryID && $have_specific_charge ) continue;

// 			if( $shippingCharge->freeShipping )
// 			{
// 				$shippingCharge->description .= ' - Free shipping';
// 				$shippingCharge->charge = 0;
// 			}

// 			$charges[] = $shippingCharges;
// 		}

// 		$free_shipping = false;

// 		// See if the coupon (if one has been applied) has free shipping
// 		if( $this->couponDiscount )
// 		{
// 			if( $this->couponDiscount->freeShipping )
// 			{
// 				$shippingCharge = $emuShop->getInstance( 'emuShippingCharge' );
// 				$shippingCharge->description = 'Free shipping';
// 				$shippingCharge->charge = 0;

// 				// Overwrite all other shipping charges with a single free charge
// 				$charges = array( $shippingCharge );

// 				$free_shipping = true;
// 			}
// 		}

// 		if( !$free_shipping && $this->totalIndividualShippingCost )
// 		{
// 			if( count( $charges ) > 0 )
// 			{
// 				// then add the individual shipping cost to all the options
// 				foreach( $charges as $shippingCharge )
// 				{
// 					if( !$shippingCharge->freeShipping )
// 					{
// 						$shippingCharge->charge += $this->totalIndividualShippingCost;
// 					}
// 				}
// 			}
// 			else
// 			{
// 				// Just add it as a shipping charge
// 				$shippingCharge = $emuShop->getInstance( 'emuShippingCharge' );
// 				$shippingCharge->description = 'Shipping';
// 				$shippingCharge->charge = $this->totalIndividualShippingCost;
// 				$charges[] = $shippingCharge;
// 			}
// 		}

// 		if( $emuShop->pickupEnabled )
// 		{
// 			$shippingCharge = $emuShop->getInstance( 'emuShippingCharge' );
// 			$shippingCharge->description = $emuShop->pickupText;
// 			$shippingCharge->charge = $emuShop->pickupCost;
// 			$charges[] = $shippingCharge;
// 		}

// 		if( has_filter( 'emu_shipping_charges' ) )
// 			$charges = apply_filters( 'emu_shipping_charges', array( $charges, $this ) );

// 		$this->shippingCharges = $charges;
// 	}

// 	public function getApplicableHandlingCharges()
// 	{
// 		global $emuShop;

// 		if( ! $emuShop->handlingChargesEnabled ) return;

// 		// get charges
// 		$applicable_charges = array();

// 		// loop through all the active handling options and see which ones apply
// 		$charges = $emuShop->getManager('handling')->getHandlingCharges( array( 'isActive' => '1' ) );

// 		$this->handlingCharges = array(); // reset

// 		foreach( $charges as $handlingCharge )
// 		{
// 			if( $handlingCharge->appliesTo( $this->handlingPriceTotal, $this->handlingWeightTotal, $this->shippingAddress->countryID, $this->shippingAddress->stateID ) )
// 				$applicable_charges[] = $handlingCharge;
// 		}

// 		$have_specific_charge = false;

// 		// Loop through all the charges and see if we have a specific charge i.e. a specific country as opposed to 'All Countries'
// 		foreach( $applicable_charges as $charge )
// 		{
// 			if( $charge->countryID ) $have_specific_charge = true;
// 		}

// 		// rebuild the handling charges
// 		$charges = array();

// 		foreach( $applicable_charges as $handlingCharge )
// 		{
// 			if( !$handlingCharge->countryID && $have_specific_charge ) continue;

// 			if( $handlingCharge->freeHandling )
// 			{
// 				$handlingCharge->description .= ' - Free handling';
// 				$handlingCharge->charge = 0;
// 			}

// 			$charges[] = $handlingCharge;
// 		}

// 		if( has_filter( 'emu_handling_charges' ) )
// 			$charges = apply_filters( 'emu_handling_charges', array( $charges, $this ) );

// 		$this->handlingCharges = $charges;
// 	}


// }

?>
