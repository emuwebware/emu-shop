<?php


class emuAddressBookProcessor extends emuProcessor
{
	public $requiredFields = array( 'first_name', 'last_name', 'address_line_one', 'city', 'postal_code', 'phone_number', 'country', array( 'entered_state', 'selected_state' ) );

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		extract( $_POST );

		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$customer = $emuShop->getManager('customer')->getCustomer();

		$redirect = false; // only in some sitautions do we redirect

		switch( $this->button )
		{
			case 'Add':
			case 'Update':

				$this->checkRequiredFields();

				if( !$this->hasRequiredFields )
				{
					$this->messages[] = '<strong>Not all required fields were provided</strong> - check those marked with *';

					$this->error = true;
					$emuShop->addMessage( 'address-book', $this->messages, 'error' );
					return;
				}

				if( $dbID = post_val( 'address_id' ) )
					$address = $emuShop->getInstance( 'emuAddress', array( $dbID ) );
				else
				{
					// creating a new address
					$address = $emuShop->getInstance( 'emuAddress' );
					$address->customerID = $customer->dbID;
				}

				$address->firstName = $first_name;
				$address->lastName = $last_name;
				$address->addressLineOne = $address_line_one;
				$address->addressLineTwo = $address_line_two;
				$address->countryID = $country;
				$address->stateID = post_val('selected_state');
				$address->state = post_val('entered_state');
				$address->city = $city;
				$address->postalCode = $postal_code;
				$address->phoneNumber = $phone_number;
				$address->isDefaultBilling = post_val('default_billing') ? 1 : 0;
				$address->isDefaultShipping = post_val('default_shipping') ? 1 : 0;

				$address->update();

				// see if we're editing an address from the order process
				$basket = $emuShop->getBasket();

				if( $emuShop->getSessionData( 'e-action' ) == 'address' )
				{
					$return = $emuShop->getSessionData('return');

					$emuShop->setSessionData( array( 'e-action' => '', 'e-method' => '', 'return' => '', 'address_id' => '' ) );

					$location = $return;
				}
				else
				{

					$this->messages[] = 'Address updated.';
					$this->error = false;

					$emuShop->addMessage( 'address-list', $this->messages, $this->error ? 'error' : 'notice' );

					$location = $_SERVER[ 'HTTP_REFERER' ];
				}

				$redirect = true;

			break;
			case 'Delete':

				$customer->addressBook->getAddresses();
				$customer->addressBook->removeAddress( post_val( 'address_id' ) );
				$customer->addressBook->update();

				$this->messages[] = 'Address removed.';
				$this->error = false;
				$emuShop->addMessage( 'address-list', $this->messages, 'notice' );

				$location = $_SERVER[ 'HTTP_REFERER' ];

				$redirect = true;

			break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		if( $redirect )
		{
			$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );
			header( 'Location: '.$location );
			exit();
		}
	}

}

?>
