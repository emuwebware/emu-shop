<?php

class emuPaymentDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'payment.htm';
        parent::__construct();

	}

	public function build()
	{
		global $emuShop;

		$basket = $emuShop->getBasket();

		$customer = $emuShop->getCustomer();

		$payment_manager = $customer->getPaymentManager();

		$tM = $this->templateManager;

		$template = $this->template;
		$template = $tM->setTemplateConditionals( $template, array( 'has payment methods' => $payment_manager->hasPaymentMethods() ));

		$payment_method = $basket->hasPaymentMethod();

		if( $payment_manager->hasPaymentMethods() )
		{
			$existing_method_template = $tM->getTemplateRepeat( 'existing method', $template );

			$existing_methods = '';

			foreach( $payment_manager->getPaymentMethods() as $method )
			{
				if( $payment_method )
					$method_checked = !$payment_method->isNewRecord() && $payment_method->getID() == $method->getID();
				else
					$method_checked = $method->isDefaultPaymentMethod;

				$tags = array( 'existing method checked' => $method_checked ? 'checked="checked"' : '' );

				$existing_method = $tM->fillTemplate( $existing_method_template, $method->getTemplateTags(), $method );
				$existing_method = $tM->fillTemplate( $existing_method, $tags );

				$existing_methods .= $existing_method;
			}

			$template = $tM->fillTemplateRepeats( $template, array( 'existing method' => $existing_methods ) );
		}

		$orderSummaryDisplay = $emuShop->getInstance( 'emuOrderSummaryDisplay' );
		$orderSummaryDisplay->build();

		// If we haven't got a payment method then create a blank method to fill out the method template tags

		// If we do have a payment method, and it's a new payment method then populate the form with that payment method object
		if( $payment_method && $payment_method->isNewRecord() )
			$form_method = $payment_method;
		else // Populate with empty method
			$form_method = $emuShop->getModel('emuPaymentMethod');

		$template = $tM->fillTemplate( $template, $form_method->getTemplateTags(), $form_method );

		$tags = array(	'new method checked' => $payment_method && $payment_method->isNewRecord() ? ' checked="checked"' : '',
						'exp months' => drop_down( '', 'cc_exp_month', 'emu-select', $form_method->expiryMonth, $emuShop->getMonths(), 'Months' ),
						'exp years' => drop_down( '', 'cc_exp_year', 'emu-select', $form_method->expiryYear, $emuShop->getYears(), 'Years' ),
						'valid from months' => drop_down( '', 'cc_v_from_month', 'emu-select', $form_method->validFromMonth, $emuShop->getMonths(), 'Months' ),
						'valid from years' => drop_down( '', 'cc_v_from_year', 'emu-select', $form_method->validFromYear, $emuShop->getYears((int)date('Y') - 7), 'Years' ),
						'payment types' => drop_down( '', 'payment_type', 'emu-select', $form_method->paymentType, $emuShop->paymentManager->getPaymentTypes(), 'Please select' ),
						'order summary' => $orderSummaryDisplay->content,
						'payment method messages' => $emuShop->getMessages('payment-method')
						);

		$template = $tM->fillTemplate( $template, $tags );
		$template = $tM->fillTemplate( $template, $basket->getTemplateTags(), $basket );

		$this->content = apply_filters( 'emu_shop_payment_content', $template );
	}

}

?>
