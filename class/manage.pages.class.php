<?php

class emuM_Pages extends emuManager
{
	public $pages;

	public function init()
	{
		// Function to append the appropriate e commerce modules into page content
		add_action( 'template_redirect', array( $this, 'autoLoadModule' ), 5 );

		add_action( 'init', array( $this, 'setPageURLs' ), 2 );

		$this->definePages();
	}

	public function registerAdminPages()
	{
		$this->emuApp->registerAdminPage( array( 'name' => 'Pages',
											'filename' => 'pages.php',
											'position' => 1,
											'styles' => array( 'emu-common' ) ) );
	}

	public function getPages()
	{
		return $this->pages;
	}

	public function registerClasses()
	{
		// Processor and Display classes
		$this->emuApp->registerClass( array( 	'emuCouponProcessor'			=> array( 'process.coupons.class.php', 'emuProcessor' ),
											'emuLoginProcessor'				=> array( 'process.login.class.php', 'emuProcessor' ),
											'emuMyAccountProcessor'			=> array( 'process.myaccount.class.php', 'emuProcessor' ),
											'emuAccountProfileProcessor'	=> array( 'process.accountprofile.class.php', 'emuProcessor' ),
											'emuAccountPaymentProcessor'	=> array( 'process.accountpayment.class.php', 'emuProcessor' ),
											'emuPaymentManagerProcessor'	=> array( 'process.paymentmanager.class.php', 'emuProcessor' ),
											'emuAddressBookProcessor'		=> array( 'process.addressbook.class.php', 'emuProcessor' ),
											'emuProductProcessor'			=> array( 'process.product.class.php', 'emuProcessor' ),
											'emuWishlistProcessor'			=> array( 'process.wishlist.class.php', 'emuProcessor' ),
											'emuPageNotFoundProcessor'		=> array( 'process.pagenotfound.class.php', 'emuProcessor' ),
											'emuBasketProcessor'			=> array( 'process.basket.class.php', 'emuProcessor' ),
											'emuPaymentProcessor'			=> array( 'process.payment.class.php', 'emuProcessor' ),
											'emuCustomerCareProcessor'		=> array( 'process.customercare.class.php', 'emuProcessor' ),
											'emuRegistrationProcessor'		=> array( 'process.registration.class.php', 'emuProcessor' ),
											'emuConfirmationProcessor'		=> array( 'process.confirmation.class.php', 'emuProcessor' ),
											'emuReviewProcessor'			=> array( 'process.review.class.php', 'emuProcessor' ),
											'emuCommentProcessor'			=> array( 'process.comment.class.php', 'emuProcessor' ),
											'emuShippingProcessor'			=> array( 'process.shipping.class.php', 'emuProcessor' ),
											'emuDeliveryProcessor'			=> array( 'process.delivery.class.php', 'emuProcessor' ),
											'emuBillingProcessor'			=> array( 'process.billing.class.php', 'emuProcessor' ),
											'emuResetPasswordProcessor'		=> array( 'process.resetpassword.class.php', 'emuProcessor' ),

											'emuBasketDisplay'				=> array( 'display.basket.class.php', 'emuDisplay' ),
											'emuConfirmationDisplay'		=> array( 'display.confirmation.class.php', 'emuDisplay' ),
											'emuLoginDisplay'				=> array( 'display.login.class.php', 'emuDisplay' ),
											'emuMyAccountDisplay'			=> array( 'display.myaccount.class.php', 'emuDisplay' ),
											'emuAccountOrdersDisplay'		=> array( 'display.accountorders.class.php', 'emuDisplay' ),
											'emuOrderSummaryDisplay'		=> array( 'display.ordersummary.class.php', 'emuDisplay' ),
											'emuAccountProfileDisplay'		=> array( 'display.accountprofile.class.php', 'emuDisplay' ),
											'emuAccountActivationDisplay'	=> array( 'display.accountactivation.class.php', 'emuDisplay' ),
											'emuPaymentManagerDisplay'		=> array( 'display.paymentmanager.class.php', 'emuDisplay' ),
											'emuCustomerCareDisplay'		=> array( 'display.customercare.class.php', 'emuDisplay' ),
											'emuAddressBookDisplay'			=> array( 'display.addressbook.class.php', 'emuDisplay' ),
											'emuPageNotFoundDisplay'		=> array( 'display.pagenotfound.class.php', 'emuDisplay' ),
											'emuDeliveryDisplay'			=> array( 'display.delivery.class.php', 'emuDisplay' ),
											'emuOrderDisplay'				=> array( 'display.order.class.php', 'emuDisplay' ),
											'emuBillingDisplay'				=> array( 'display.billing.class.php', 'emuDisplay' ),
											'emuPaymentDisplay'				=> array( 'display.payment.class.php', 'emuDisplay' ),
											'emuProductDisplay'				=> array( 'display.product.class.php', 'emuDisplay' ),
											'emuProductsDisplay'			=> array( 'display.products.class.php', 'emuDisplay' ),
											'emuReviewsDisplay'				=> array( 'display.reviews.class.php', 'emuDisplay' ),
											'emuReviewDisplay'				=> array( 'display.review.class.php', 'emuDisplay' ),
											'emuReviewsSummaryDisplay'		=> array( 'display.reviewssummary.class.php', 'emuDisplay' ),
											'emuShippingDisplay'			=> array( 'display.shipping.class.php', 'emuDisplay' ),
											'emuThankyouDisplay'			=> array( 'display.thankyou.class.php', 'emuDisplay' ),
											'emuWishlistDisplay'			=> array( 'display.wishlist.class.php', 'emuDisplay' ),
											'emuResetPasswordDisplay'		=> array( 'display.resetpassword.class.php', 'emuDisplay' ),
											'emuRegistrationDisplay'		=> array( 'display.registration.class.php', 'emuDisplay' ) ) );

	}

	public function addShortCodes()
	{
		global $emuShop;

		foreach( $this->pages as $page )
		{
			$function = 'global $emuShop; '.
						'$emuShop->loadClass( "'.$page->displayModule.'" ); '.
						'$emuDisplay = new '.$page->displayModule.'; '.
						'$emuDisplay->build(); return $emuDisplay->content; ';

			add_shortcode( $page->shortCode, create_function( '', $function )  );
		}
	}

	function processForm()
	{
		global $emuShop;

		$plugin = request_val( 'e-plugin' );

		if( $plugin !== $emuShop->emuAppID ) return; // wrong emu component

		$processor = false;

		$action = request_val( 'e-action' );

		// Any non-page processing (such as coupon processing which doesn't have
		// an associated page
		//////////////////////////////////////////////////////////////////////////

		switch( $action )
		{
			case 'coupon':

				$processor = 'emuCouponProcessor';
				break;

			case 'comment':

				$processor = 'emuCommentProcessor';
				break;
		}

		// Page associated processing
		//////////////////////////////////////////////////////////////////////////

		if( isset( $this->pages->$action ) )
		{
			if( $this->pages->$action->processorModule )
				$processor = $this->pages->$action->processorModule;
		}

		// Processor
		/////////////////////////////////////////////////////////////////////////
		if( $processor )
		{
			$emuShop->setFormProcessor( $emuShop->getInstance( $processor ) );
			$emuShop->processForm();
		}
	}

	// page manager?
	function setPageURLs()
	{
		foreach( $this->pages as $page )
		{
			$page->url = get_permalink( $page->pageID );
		}
	}

	// page manager?
	function updatePages()
	{
		foreach( $this->pages as $page )
		{
			update_option( $page->optionRef, $page->pageID );
		}
	}

	function generatePages( $pages = null )
	{
		global $current_user; get_currentuserinfo();
		global $emuShop;

		$emu_page = array();

		$emu_page['post_title'] = '';
		$emu_page['post_content'] = '';
		$emu_page['post_status'] = 'publish';
		$emu_page['post_type'] = 'page';
		$emu_page['post_author'] = $current_user->ID;
		$emu_page['comment_status'] = 'closed';

		if( !$pages ) $pages = $this->pages; // generate all pages

		foreach( $pages as $page )
		{
			$emu_page['post_title'] = $page->description;

			if( !$emuShop->autoLoadModules ) $emu_page['post_content'] = "[{$page->shortCode}]";

			$page_id = wp_insert_post( $emu_page );

			$page->pageID = $page_id;
		}

		$this->updatePages();

	}

	function autoLoadModule()
	{
		global $emuShop;

		if( !$emuShop->autoLoadModules ) return;

		global $post;

		$post_id = isset( $post->ID ) ? $post->ID : null;

		switch( @$post->post_type )
        {
            case 'product':

                $productDisplay = $emuShop->getInstance( 'emuProductDisplay' );

                $productDisplay->post = $post;
                $productDisplay->build();

                $post->post_content = $productDisplay->content;

                break;

            case 'review':

                $reviewDisplay = $emuShop->getInstance( 'emuReviewDisplay' );

                $reviewDisplay->post = $post;
                $reviewDisplay->build();

                $post->post_content = $reviewDisplay->content;

                break;
        }

        if( $page = $this->isShopPage( $post_id ) )
        {
			$displayClass = $page->displayModule;
			$emuShop->setUserInterface( $emuShop->getInstance( $displayClass ) );

			$post->post_content = $post->post_content.$emuShop->getUserInterface();

            // To stop wordpress adding <br />'s and <p>'s all over the place
            // Thank you http://www.simonbattersby.com/blog/plugin-to-stop-wordpress-adding-br-tags/
			remove_filter( 'the_content', 'wpautop' );
			remove_filter( 'the_content', 'wptexturize' );
			// add_filter( 'the_content', array( $this, 'wpautop' ) );
        }
	}

	function isShopPage($post_id)
	{
		foreach( $this->pages as $page )
		{
			if( !$page->pageID ) continue;

			if( $post_id == $page->pageID )
			{
				return $page;
			}
		}
		return false;
	}

	function wpautop($pee)
	{
		return wpautop( $pee, $br = 0 );
	}


	function definePages()
	{

		$pages['login'] = (object) array( 				'description' => 'Login',
														'optionRef' => $this->emuApp->emuAppID.'-page_login',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_login'),
														'shortCode' => $this->emuApp->emuAppID.'ShowLogin',
														'displayModule' => 'emuLoginDisplay',
														'processorModule' => 'emuLoginProcessor',
														'secure' => true,
														'url' => '' );

		$pages['registration'] = (object) array( 		'description' => 'Register',
														'optionRef' => $this->emuApp->emuAppID.'-page_register',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_register'),
														'shortCode' => $this->emuApp->emuAppID.'ShowRegistration',
														'displayModule' => 'emuRegistrationDisplay',
														'processorModule' => 'emuRegistrationProcessor',
														'secure' => true,
														'url' => '' );

		$pages['resetPassword'] = (object) array( 		'description' => 'Reset Password',
														'optionRef' => $this->emuApp->emuAppID.'-page_reset_password',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_reset_password'),
														'shortCode' => $this->emuApp->emuAppID.'ShowResetPassword',
														'displayModule' => 'emuResetPasswordDisplay',
														'processorModule' => 'emuResetPasswordProcessor',
														'secure' => true,
														'url' => '' );

		$pages['myAccount'] = (object) array( 			'description' => 'My Account',
														'optionRef' => $this->emuApp->emuAppID.'-page_my_account',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_my_account'),
														'shortCode' => $this->emuApp->emuAppID.'ShowMyAccount',
														'displayModule' => 'emuMyAccountDisplay',
														'processorModule' => false,
														'secure' => true,
														'url' => '' );

		$pages['addressBook'] = (object) array( 		'description' => 'Address Book',
														'optionRef' => $this->emuApp->emuAppID.'-page_address_book',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_address_book'),
														'shortCode' => $this->emuApp->emuAppID.'ShowAddressBook',
														'displayModule' => 'emuAddressBookDisplay',
														'processorModule' => 'emuAddressBookProcessor',
														'secure' => true,
														'url' => '' );

		$pages['paymentManager'] = (object) array( 		'description' => 'Payment Manager',
														'optionRef' => $this->emuApp->emuAppID.'-page_payment_manager',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_payment_manager'),
														'shortCode' => $this->emuApp->emuAppID.'ShowPaymentManager',
														'displayModule' => 'emuPaymentManagerDisplay',
														'processorModule' => 'emuPaymentManagerProcessor',
														'secure' => true,
														'url' => '' );

		$pages['accountProfile'] = (object) array( 		'description' => 'Account Profile',
														'optionRef' => $this->emuApp->emuAppID.'-page_account_profile',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_account_profile'),
														'shortCode' => $this->emuApp->emuAppID.'ShowAccountProfile',
														'displayModule' => 'emuAccountProfileDisplay',
														'processorModule' => 'emuAccountProfileProcessor',
														'secure' => true,
														'url' => '' );

		$pages['accountOrders'] = (object) array( 		'description' => 'Account Orders',
														'optionRef' => $this->emuApp->emuAppID.'-page_account_orders',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_account_orders'),
														'shortCode' => $this->emuApp->emuAppID.'ShowAccountOrders',
														'displayModule' => 'emuAccountOrdersDisplay',
														'processorModule' => false,
														'secure' => true,
														'url' => '' );

		$pages['order'] = (object) array( 				'description' => 'Order',
														'optionRef' => $this->emuApp->emuAppID.'-page_order',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_order'),
														'shortCode' => $this->emuApp->emuAppID.'ShowOrder',
														'displayModule' => 'emuOrderDisplay',
														'processorModule' => false,
														'secure' => true,
														'url' => '' );

		$pages['pageNotFound'] = (object) array( 		'description' => 'Page Not Found',
														'optionRef' => $this->emuApp->emuAppID.'-page_404',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_404'),
														'shortCode' => $this->emuApp->emuAppID.'ShowPageNotFound',
														'displayModule' => 'emuPageNotFoundDisplay',
														'processorModule' => 'emuPageNotFoundProcessor',
														'secure' => false,
														'url' => '' );

		$pages['shipping'] = (object) array( 			'description' => 'Shipping',
														'optionRef' => $this->emuApp->emuAppID.'-page_shipping',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_shipping'),
														'shortCode' => $this->emuApp->emuAppID.'ShowShipping',
														'displayModule' => 'emuShippingDisplay',
														'processorModule' => 'emuShippingProcessor',
														'secure' => true,
														'url' => '' );

		$pages['delivery'] = (object) array( 			'description' => 'Delivery',
														'optionRef' => $this->emuApp->emuAppID.'-page_delivery',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_delivery'),
														'shortCode' => $this->emuApp->emuAppID.'ShowDelivery',
														'displayModule' => 'emuDeliveryDisplay',
														'processorModule' => 'emuDeliveryProcessor',
														'secure' => true,
														'url' => '' );

		$pages['customerCare'] = (object) array( 		'description' => 'Customer Care',
														'optionRef' => $this->emuApp->emuAppID.'-page_customer_care',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_customer_care'),
														'shortCode' => $this->emuApp->emuAppID.'ShowCustomerCare',
														'displayModule' => 'emuCustomerCareDisplay',
														'processorModule' => 'emuCustomerCareProcessor',
														'secure' => false,
														'url' => '' );


		$pages['products'] = (object) array( 			'description' => 'Products',
														'optionRef' => $this->emuApp->emuAppID.'-page_products',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_products'),
														'shortCode' => $this->emuApp->emuAppID.'ShowProducts',
														'displayModule' => 'emuProductsDisplay',
														'processorModule' => 'emuProductProcessor',
														'secure' => false,
														'url' => '' );

		$pages['basket'] = (object) array( 				'description' => 'Basket',
														'optionRef' => $this->emuApp->emuAppID.'-page_basket',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_basket'),
														'shortCode' => $this->emuApp->emuAppID.'ShowBasket',
														'displayModule' => 'emuBasketDisplay',
														'processorModule' => 'emuBasketProcessor',
														'secure' => true,
														'url' => '' );

		$pages['wishlist'] = (object) array( 			'description' => 'Wish List',
														'optionRef' => $this->emuApp->emuAppID.'-page_wishlist',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_wishlist'),
														'shortCode' => $this->emuApp->emuAppID.'ShowWishlist',
														'displayModule' => 'emuWishlistDisplay',
														'processorModule' => 'emuWishlistProcessor',
														'secure' => false,
														'url' => '' );

		$pages['customerReviews'] = (object) array( 	'description' => 'Customer Reviews',
														'optionRef' => $this->emuApp->emuAppID.'-page_customer_reviews',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_customer_reviews'),
														'shortCode' => $this->emuApp->emuAppID.'ShowReviews',
														'displayModule' => 'emuReviewsDisplay',
														'processorModule' => 'emuReviewProcessor',
														'secure' => false,
														'url' => '' );

		$pages['reviewsSummary'] = (object) array( 		'description' => 'Reviews Summary',
														'optionRef' => $this->emuApp->emuAppID.'-page_reviews_summary',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_reviews_summary'),
														'shortCode' => $this->emuApp->emuAppID.'ReviewsSummary',
														'displayModule' => 'emuReviewsSummaryDisplay',
														'processorModule' => false,
														'secure' => false,
														'url' => '' );

		$pages['payment'] = (object) array( 			'description' => 'Payment',
														'optionRef' => $this->emuApp->emuAppID.'-page_payment',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_payment'),
														'shortCode' => $this->emuApp->emuAppID.'ShowPayment',
														'displayModule' => 'emuPaymentDisplay',
														'processorModule' => 'emuPaymentProcessor',
														'secure' => true,
														'url' => '' );

		$pages['billing'] = (object) array( 			'description' => 'Billing',
														'optionRef' => $this->emuApp->emuAppID.'-page_billing',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_billing'),
														'shortCode' => $this->emuApp->emuAppID.'ShowBilling',
														'displayModule' => 'emuBillingDisplay',
														'processorModule' => 'emuBillingProcessor',
														'secure' => true,
														'url' => '' );

		$pages['confirmation'] = (object) array( 		'description' => 'Order Confirmation',
														'optionRef' => $this->emuApp->emuAppID.'-page_confirmation',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_confirmation'),
														'shortCode' => $this->emuApp->emuAppID.'ShowConfirmation',
														'displayModule' => 'emuConfirmationDisplay',
														'processorModule' => 'emuConfirmationProcessor',
														'secure' => true,
														'url' => '' );

		$pages['thankyou'] = (object) array( 			'description' => 'Thank You',
														'optionRef' => $this->emuApp->emuAppID.'-page_thankyou',
														'pageID' => get_option($this->emuApp->emuAppID.'-page_thankyou'),
														'shortCode' => $this->emuApp->emuAppID.'ShowThankyou',
														'displayModule' => 'emuThankyouDisplay',
														'processorModule' => false,
														'secure' => true,
														'url' => '' );
		$this->pages = (object) $pages;
	}

    public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

    }

}


?>
