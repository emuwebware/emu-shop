<?php


class emuAddressBookDisplay extends emuDisplay
{
	public $addressID;

	public function __construct()
	{
        $this->templateFile = 'address-book.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$customer = $emuShop->getManager('customer')->getCustomer();

		$template = $this->template;

		$address_section = $this->templateManager->getTemplateSection( 'addresses', $template );

		if( !$customer->addressBook->hasAddresses() )
			$address_list = 'You do not currently have any addresses. Add a new address using the form below.';
		else
		{
			$address_template = $this->templateManager->getTemplateRepeat( 'address', $address_section );

			$addresses = '';

			foreach( $customer->addressBook->getAddresses() as $address )
			{
				$defaults = array();

				if( $address->isDefaultBilling ) $defaults[] = '<span class="default-billing">Your default billing address</span>';
				if( $address->isDefaultShipping ) $defaults[] = '<span class="default-shipping">Your default shipping address</span>';

				$defaults = count( $defaults ) > 0 ? implode( $defaults, '' ) : '';

				$address_details = $this->templateManager->fillTemplate( $address_template, $address->getTemplateTags(), $address );
				$address_details = $this->templateManager->fillTemplate( $address_details, array( 'defaults' => $defaults ) );

				$addresses .= $address_details;

			}

			$address_list = $this->templateManager->fillTemplateRepeats( $address_section, array( 'address' => $addresses ) );
			$address_list = $this->templateManager->fillTemplate( $address_list, array( 'messages' => $emuShop->getMessages('address-list') ) );

		}

		$template = $this->templateManager->fillTemplateSections( $template, array( 'addresses' => $address_list ) );

		$this->addressID = post_val('address_id');

		// see if we're editing an address from the order process
		$basket = $emuShop->getBasket();

		if( $emuShop->getSessionData( 'e-action' ) == 'address' )
		{
			switch( $emuShop->getSessionData( 'e-method' ) )
			{
				case 'edit':

					$this->addressID = $emuShop->getSessionData( 'address_id' );
					$_POST['e-button'] = 'Edit';

				break;
			}
		}

		$address_form = $this->templateManager->getTemplateSection( 'address form', $template );

		$address = $emuShop->getInstance( 'emuAddress', $this->addressID );

		if( post_val('e-button') !== 'Edit' )
		{
			// Load the address data from post array
			$address->firstName = post_val('first_name');
			$address->lastName = post_val('last_name');
			$address->addressLineOne = post_val('address_line_one');
			$address->addressLineTwo = post_val('address_line_two');
			$address->city = post_val('city');
			$address->postalCode = post_val('postal_code');
			$address->phoneNumber = post_val('phone_number');
			$address->isDefaultBilling = post_val('default_billing') ? 1 : 0;
			$address->isDefaultShipping = post_val('default_shipping') ? 1 : 0;
			$address->countryID = post_val('country');
			$address->stateID = post_val('selected_state');
			$address->state = post_val('entered_state');
		}

		if( $this->addressID )
			$tags = array( 	'edit title' => 'Edit', 'button value' => 'Update' );
		else
			$tags = array( 	'edit title' => 'New', 'button value' => 'Add' );

		$regionOptions = $emuShop->getManager('region')->getRegionOptions( $address->countryID, $address->stateID, $address->state );
		$address_form = $this->templateManager->fillTemplate( $address_form, $address->getTemplateTags(), $address );

		$tags = array_merge( $tags, array(	'messages' => $emuShop->getMessages( 'address-book' ),
											'countries' => $regionOptions->countryDropDown,
											'selected state class' => $regionOptions->stateDropDownClass,
											'states' => $regionOptions->stateDropDown,
											'entered state' => $regionOptions->stateTextInput,
											'entered state class' => $regionOptions->stateTextInputClass,
											'default shipping checkbox' => $address->isDefaultShipping ? ' checked="checked"' : '',
											'default billing checkbox' => $address->isDefaultBilling ? ' checked="checked"' : '' ) );


		$address_form = $this->templateManager->fillTemplate( $address_form, $tags );
		$template = $this->templateManager->fillTemplateSections( $template, array( 'address form' => $address_form ) );
		$template = $this->templateManager->fillTemplateSpecialTags( $template );

		$this->content = apply_filters( 'emu_address_book_content', $template );

	}

}

?>
