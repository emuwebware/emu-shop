<?php

class emuM_Helpers extends emuManager
{
    const SHOP_LTYPE_ADD_TO_BASKET = 'add-to-basket';
    const SHOP_LTYPE_CHECKOUT = 'checkout';

    public function addShortCodes()
    {
        global $emuShop;

        $emuShop->addShortCode( 'emu-shop-link', array( $this, 'getShopLink' ) );
        // to-do $emuShop->addShortCode( 'emu-shop-form', array( $this, 'getShopForm' ) );
    }

    public function getShopLink( $atts )
    {
        $defaults = array(  'type' => '',
                            'pid' => '',
                            'vid' => '' );

        $atts = shortcode_atts( $defaults, $atts );

        $linkHelper = $this->emuApp->linksHelper;

        extract( $atts );

        if( empty( $type ) ) return 'Type cannot be empty';

        switch( $type )
        {

            case self::SHOP_LTYPE_ADD_TO_BASKET:

                return $linkHelper->getAddToBasketLink( $pid, $vid );
                break;

            case self::SHOP_LTYPE_CHECKOUT:

                return $linkHelper->getCheckoutLink();
                break;

        }
    }
}

?>