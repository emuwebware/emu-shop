<?php

class emuM_Payments extends emuManager
{
	public function registerClasses()
	{
        $this->emuApp->registerModel( 'emuPaymentManager', 'model.paymentmanager.class.php', 'emuShopCommon' );
        $this->emuApp->registerModel( 'emuPaymentMethod', 'model.paymentmethod.class.php', 'emuShopCommon' );
        $this->emuApp->registerModel( 'emuPayment', 'model.payment.class.php' );
	}

    function getPaymentTypes()
    {
        $payment_types = array( 'American Express', 'Discover', 'MasterCard', 'Visa' );

        $payment_types = apply_filters( 'emu_shop_get_payment_types', $payment_types );

        return $payment_types;
    }


}


?>
