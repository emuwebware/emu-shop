<?php

class emuCustomerCareDisplay extends emuDisplay
{

	public function __construct()
	{
        $this->templateFile = 'customer-care.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$customer = $emuShop->getManager('customer')->getCustomer();

		$template = $this->template;

		if( isset( $_SESSION['customer-care'] ) )
		{
			$this->content = $this->templateManager->setTemplateConditionals( $template, array( 'customer care form' => false ) );
			$this->content = $this->templateManager->fillTemplate( $this->content, array( 'messages' => 'Your customer care request was successfully submitted. One of our customer care representatives will be in contact shortly.' ) );
			return;
		}
		else
		{
			$template = $this->templateManager->setTemplateConditionals( $template, array( 'customer care form' => true ) );
		}

		if( isset( $customer->addressBook->defaultBillingAddress ) )
			$address = $customer->addressBook->defaultBillingAddress;
		else
		{
			if( isset( $customer->addressBook->defaultShippingAddress ) )
				$address = $customer->addressBook->defaultShippingAddress;
			else
				$address = $emuShop->getInstance( 'emuAddress' ); // blank address
		}

		if( post_val('e-action') == 'customer-care' )
		{
			// then load address values from post array
			extract( $_POST );

			$address->firstName = $first_name;
			$address->lastName = $last_name;
			$address->addressLineOne = $address_line_one;
			$address->addressLineTwo = $address_line_two;
			$address->countryID = $country;
			$address->stateID = post_val('selected_state');
			$address->state = post_val('entered_state');
			$address->city = $city;
			$address->postalCode = $postal_code;
			$address->phoneNumber = $phone_number;
			$customer->email = $email;
		}

		$regionOptions = $emuShop->getManager('region')->getRegionOptions( $address->countryID, $address->stateID, $address->state );

		$template = $this->templateManager->fillTemplate( $template, $address->getTemplateTags(), $address );
		$template = $this->templateManager->fillTemplate( $template, $customer->getTemplateTags(), $customer );

		$tags = array(	'messages' => $emuShop->getMessages( 'customer-care' ),
						'countries' => $regionOptions->countryDropDown,
						'selected state class' => $regionOptions->stateDropDownClass,
						'states' => $regionOptions->stateDropDown,
						'entered state' => $regionOptions->stateTextInput,
						'entered state class' => $regionOptions->stateTextInputClass );

		$template = $this->templateManager->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_customer_care_content', $template );

	}

}

?>
