<?php

class emuVariantGroup extends emuDbEntity
{
    public $variants;

    public function __construct( $dbID = null )
    {
        global $emuShop;

        parent::__construct( $dbID, null, $emuShop->dbPrefix, 'variant_groups' );
    }

    public function getVariants( $include_inactive_variants = false )
    {
        global $emuShop, $wpdb;

        if( !$this->variants)
        {
            $sql =  "select p.dbID from {$emuShop->dbPrefix}products p".
                    " inner join {$emuShop->dbPrefix}variant_groups vg on vg.dbID = p.variantGroupID".
                    " where vg.productID = p.parentID".
                    " and p.isVariant = 1 and p.variantGroupID = {$this->dbID} ".($include_inactive_variants ? '' : ' and isActive = 1').
                    ' order by displayPosition ASC';

            $variant_rs = $wpdb->get_results( $sql, ARRAY_A );

            $variants = array();

            if( count( $variant_rs ) > 0 )
            {
                foreach( $variant_rs as $variant )
                    $variants[] = $emuShop->getInstance( 'emuProduct', $variant['dbID'] );

            }

            $this->variants = $variants;

        }

        return $this->variants;
    }

    // Overrides
    //////////////////////////////////////////////////////////////
    public function delete()
    {
        parent::delete();

        // And remove all variants
        foreach( $this->getVariants() as $variant )
            $variant->delete();
    }

}

?>