<?php



class emuOrderSummaryDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'order-summary.htm';
        parent::__construct();

	}

	public function build()
	{
		global $emuShop;

		$reload_basket = in_array( post_val('e-button'), array( 'Apply Code', 'Remove Code' ) );

		$basket = $emuShop->getBasket( $reload_basket ); // need to refresh if appling or removing the code

		$template = $this->template;

		$summary_item_template = $this->templateManager->getTemplateRepeat( 'order summary item', $template );

		$summary_items = '';

		// Coupon
		if( $coupon = $basket->hasCouponDiscount() )
		{
			$tags = array( 'description' => 'Coupon '.$coupon->code, 'cost' => '-'.$coupon->displayDiscount );
			$summary_items .= $this->templateManager->fillTemplate( $summary_item_template, $tags );
		}

		// Special Discounts
		if( $discounts = $basket->hasSpecialDiscounts() )
		{
			foreach( $discounts as $discount )
			{
				$tags = array( 'description' => $discount->description, 'cost' => '-'.$discount->getDiscount() );
				$summary_items .= $this->templateManager->fillTemplate( $summary_item_template, $tags );
			}
		}

		// Shipping costs
		if( $shipping_charge = $basket->hasShippingCost() )
		{
			$tags = array( 'description' => $shipping_charge->description, 'cost' => $basket->getTotal(BASKET_SHIPPING_TOTAL_FORMATTED) );
			$summary_items .= $this->templateManager->fillTemplate( $summary_item_template, $tags );
		}

		// Handling costs
		if( $handling_charge = $basket->hasHandlingCost() )
		{
			$tags = array( 'description' => 'Handling', 'cost' => $basket->getTotal(BASKET_HANDLING_TOTAL_FORMATTED) );
			$summary_items .= $this->templateManager->fillTemplate( $summary_item_template, $tags );
		}

		// Taxes
		if( $taxes = $basket->hasTaxes() )
		{
			foreach( $taxes as $tax )
			{
				$tags = array( 'description' => $tax->description, 'cost' => $tax->calculatedValueFormatted );
				$summary_items .= $this->templateManager->fillTemplate( $summary_item_template, $tags );
			}

		}

		// Special Charges
		if( $charges = $basket->hasSpecialCharges() )
		{
			foreach( $charges as $charge )
			{
				$tags = array( 'description' => $charge->description, 'cost' => $charge->getCharge() );
				$summary_items .= $this->templateManager->fillTemplate( $summary_item_template, $tags );
			}

		}


		$template = $this->templateManager->fillTemplateRepeats( $template, array( 'order summary item' => $summary_items ) );

		$template = $this->templateManager->fillTemplate( $template, $basket->getTemplateTags(), $basket );

		if ( $basket->getSessionData( 'coupon_code' ) )
		{
			$coupon_button_text = 'Remove Code';
			$coupon_input_visible = false;
		}
		else
		{
			$coupon_button_text = 'Apply Code';
			$coupon_input_visible = true;
		}

		$template = $this->templateManager->setTemplateConditionals( $template, array( 'coupon code' => $coupon_input_visible ) );

		$tags = array( 	'coupon button text' => $coupon_button_text,
						'order summary items' => $summary_items,
						'coupon code' => post_val('coupon_code'),
						'coupon messages' => $emuShop->getMessages( 'coupon' ) );

		$template = $this->templateManager->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_order_summary_content', $template );
	}
}

?>
