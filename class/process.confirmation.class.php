<?php

class emuConfirmationProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();

		$basket->setSessionData( 'order_comments', post_val('order_comments') );

		// Process order now (and bypass payment) if offlinePayments is set or the basket grand total is 0
		if( $emuShop->offlinePayments || $basket->getGrandTotal() == 0 )
		{
			$order = $emuShop->getModel( 'emuOrder' );

			$order->prepareFromBasket( $basket );
			$order->paymentMethod = null; // to override any default payment method
			$order->generate();

			$basket->emptyBasket();

			$location = $emuShop->pageManager->pages->thankyou->url;
		}
		else
		{
			// Move straight on to payment
			$location = $emuShop->pageManager->pages->payment->url;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );

		header( 'Location: '.$location );
		exit();
	}

}

?>
