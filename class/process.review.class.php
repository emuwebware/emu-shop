<?php

class emuReviewProcessor extends emuProcessor
{
	public $requiredFields = array( 'review_title', 'product_rating', 'review_text' );

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$review_id = post_val('review_id');

		$basket = $emuShop->getBasket();

		if( !$review_votes = $basket->getSessionData('review-votes') )
			$review_votes = array();

		switch( $this->button )
		{
			case 'Submit Review':

				$review_id = $this->processReview( $basket );

				$emuShop->addMessage( 'review', $this->messages, $this->error ? 'error' : 'notice' );

				if( !$this->error )
				{
					$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $_SERVER[ 'HTTP_REFERER' ] );

					header( 'Location: '.$location );
					exit();
				}

				return;
				break;

			case 'Inappropriate?': // fall-through

			case 'Report it':
				$emuReview = $emuShop->getInstance( 'emuReview' );
				$emuReview->setReviewInappropriate( $review_id );
				break;

			case 'Yes':
				$emuReview = $emuShop->getInstance( 'emuReview' );
				$emuReview->increaseHelpfulCounter( 'yes', $review_id );
				$review_votes[] = $review_id;
				break;

			case 'No':
				$emuReview = $emuShop->getInstance( 'emuReview' );
				$emuReview->increaseHelpfulCounter( 'no', $review_id );
				$review_votes[] = $review_id;
				break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$basket->setSessionData('review-votes', $review_votes);

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $_SERVER[ 'HTTP_REFERER' ] );

		header( 'Location: '.$location );

		exit();

	}

	public function processReview( $basket )
	{
		global $emuShop;

		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$this->error = true;
			return;
		}

		extract( $_POST );

		global $current_user; get_currentuserinfo();

		$review = $emuShop->getInstance( 'emuReview' );

		$review->title = $review_title;
		$review->text = $review_text;
		$review->postID = $postID;
		$review->productID = $productID;
		$review->rating = $product_rating;
		$review->wpUserID = $current_user->ID;

		$review->update();

		$this->messages[] = 'You\'re review has been successfully submitted and is awaiting moderation.';

		if( !$review_submissions = $basket->getSessionData('review-submissions') )
			$review_submissions = array();

		$review_submissions[] = $productID;

		$basket->setSessionData('review-submissions', $review_submissions);

		$this->error = false;
	}


}

?>
