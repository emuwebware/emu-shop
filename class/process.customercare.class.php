<?php

class emuCustomerCareProcessor extends emuProcessor
{
	public $requiredFields = array( 'first_name', 'last_name', 'email', 'comment' );

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		switch( $this->button )
		{
			case 'Submit':

				$this->processCustomerCare();

				$emuShop->addMessage( 'customer-care', $this->messages, $this->error ? 'error' : 'notice' );

				break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		if( !$this->error )
		{
			$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $_SERVER[ 'HTTP_REFERER' ] );

			header( 'Location: '.$location );
			exit();
		}
	}

	public function processCustomerCare()
	{
		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = '<strong>Not all required fields were provided</strong> - check those marked with *';

			$this->error = true;
			return;
		}

		global $emuShop;

		extract( $_POST );

		$address = $emuShop->getInstance( 'emuAddress' );

		$address->firstName = $first_name;
		$address->lastName = $last_name;
		$address->addressLineOne = $address_line_one;
		$address->addressLineTwo = $address_line_two;
		$address->countryID = $country;
		$address->stateID = post_val('selected_state');
		$address->state = post_val('entered_state');
		$address->city = $city;
		$address->postalCode = $postal_code;
		$address->phoneNumber = $phone_number;

		$templateManager = $emuShop->getManager( 'email-template' );

		$email = $templateManager->getEmailTemplate( 'customer-care.txt' );

		$message = $email->content;

		$message = $templateManager->fillTemplate( $message, $address->getTemplateTags(), $address );

		if( $questions = post_val( 'questions' ) )
		{
			if( is_array( $questions ) )
			{
				$questions = implode( '<br />', $questions );
			}
		}

		$tags = array( 'email' => post_val('email'), 'questions' => $questions, 'comment' => $this->sanitize( post_val( 'comment' ) ), 'date' => apply_date_format( 'standard' ) );

		$message = $templateManager->fillTemplate( $message, $tags );

		$emuShop->mail( $email->to, $email->from, $email->subject, $message, $email->type, $email->cc, $email->bcc );

		$this->error = false;

		$_SESSION['customer-care'] = '1';
	}

}

?>
