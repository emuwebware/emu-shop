<?php


class emuOrderItem extends emuTemplatedDbEntity
{
    public $error = false;
    public $messages = array();

    public function __construct( $dbID = null )
    {
        global $emuShop;

        // defaults
        $this->qty = 1;
        $this->total = 0;

        parent::__construct( $dbID, null, $emuShop->dbPrefix, 'order_items' );
    }

	public function registerTemplateTags()
	{
		$this->templateTags = array( 'order item id' => 'dbID',
									'description' => 'description',
									'quantity' => 'qty',
									'price' => array( 'this', 'getTotal', ORDER_ITEM_PRICE_FORMATTED ),
									'total' => array( 'this', 'getTotal', ORDER_ITEM_TOTAL_FORMATTED ) );
	}

    public function prepareFromProduct( $product, $qty = 1 )
    {
        $this->productID = $product->getID();
        $this->description = $product->name;

        if( $product->isVariant )
            $this->description .= ' ('.$product->variantDescription.')';

        $this->price = $product->getPriceValue();
        $this->qty = $qty; // default

        $this->getItemTotal(); // i.e. populate ->total prop
    }

    public function prepareFromBasketItem( $basket_item )
    {
    	$this->prepareFromProduct( $basket_item, $basket_item->getQty() );
        if($basket_item->hasProductOptions() )
            $this->description .= '<br />'.$basket_item->selectedProductOptions;
    }

	public function formatEmpty($number)
	{
		global $emuShop;

		if( trim( $number ) == $emuShop->currency ) return '-';

		return $number;
	}

	public function getTotalValue()
	{
		return $this->getTotal(ORDER_ITEM_TOTAL);
	}

    public function getTotal($format = null)
    {
		global $emuShop;

		switch( $format )
		{

			case ORDER_ITEM_PRICE:
				return $this->price;

			case ORDER_ITEM_PRICE_FORMATTED:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_ITEM_PRICE) ));

			case ORDER_ITEM_TOTAL:
				return $this->getItemTotal();

			case ORDER_ITEM_TOTAL_FORMATTED:
			default:
				return $this->formatEmpty($emuShop->currency.apply_number_format( 'currency', $this->getTotal(ORDER_ITEM_TOTAL) ));
		}
    }

    public function getItemTotal()
    {
    	$this->total = $this->qty * $this->price;

    	return $this->total;
    }
}

?>
