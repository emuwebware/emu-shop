<?php


class emuBasketProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();

		$item_refs = post_val('item_ref');
		$product_qtys = post_val('item_qty');

		if( $item_refs )
		{
			for($i = 0, $i_count = count( $item_refs ); $i < $i_count; $i++ )
			{
				$item_ref = $item_refs[$i];
				$product_qty = $product_qtys[$i];

				$basket_item = $basket->findItemByRef($item_ref);

				if( post_val( "remove-$item_ref" ) )
				{
					$basket->removeItem( $basket_item );

					$this->messages[] = 'Item removed.';
					$emuShop->addMessage( 'basket', $this->messages, 'error' );
				}
				else
					$basket->updateItemQty( $basket_item, $product_qtys[$i] );
			}

		}

		switch( $this->button )
		{
			case 'Checkout':

				if( $basket->numberItems == 0 )
					$location = $emuShop->pageManager->pages->basket->url;
				else
					$location = $emuShop->pageManager->pages->billing->url;

				break;

			default:

				$location = $_SERVER[ 'HTTP_REFERER' ];
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );

		header( 'Location: '.$location );
		exit();
	}
}

?>
