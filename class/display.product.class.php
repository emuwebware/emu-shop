<?php

class emuProductDisplay extends emuDisplay
{
	public $post;

	public $product;

	public function __construct()
	{
        $this->templateFile = 'product.htm';
        parent::__construct();

	}

	public function getPost()
	{
		if( !$this->post )
		{
			global $post;
			$this->post = $post;
		}
	}

	public function build()
	{
		if( !$this->post )
		{
			$this->content = 'No product!';
			return;
		}

		global $emuShop;

		$product = $emuShop->getInstance( 'emuProduct', array(null, $this->post) );

		$template = $this->template;

		// "Customers who bought this item also bought..."
		$product->getCustomerPurchaseGroup();

		if( count( $product->customerPurchaseGroup ) > 0 )
		{
			$p_group_template = $this->templateManager->getTemplateSection( 'purchase group', $template );

			$p_group_item_template = $this->templateManager->getTemplateRepeat( 'purchase group item', $p_group_template );

			$p_group = '';

			foreach( $product->customerPurchaseGroup as $group_product )
				$p_group .= $this->templateManager->fillTemplate( $p_group_item_template, $group_product->getTemplateTags('p '), $group_product, $group_product->getID() );

			$other_customer_purchases = $this->templateManager->fillTemplateRepeats( $p_group_template, array( 'purchase group item' => $p_group ) );
		}
		else $other_customer_purchases = '';

		$template = $this->templateManager->fillTemplateSections( $template, array( 'purchase group' => $other_customer_purchases ) );
		///////////////////////////////////////////////////////////////////////////

		// Recommended products
		$product->getRecommendedProductGroup();

		if( count( $product->recommendedProductGroup ) > 0 )
		{
			$r_group_template = $this->templateManager->getTemplateSection( 'recommended products', $template );

			$r_group_item_template = $this->templateManager->getTemplateRepeat( 'recommended product', $r_group_template );

			$r_group = '';

			foreach( $product->recommendedProductGroup as $groupProductItem )
			{
				$tmp_template = $this->templateManager->fillTemplate( $r_group_item_template, $groupProductItem->getTemplateTags('r '), $groupProductItem, $groupProductItem->getID() );
				$r_group .= $this->templateManager->setTemplateConditionals( $tmp_template, array(  'quantity dropdown' => $groupProductItem->hasVariants,
															'product options' => count( $groupProductItem->getProductOptions() ) > 0 ) );
			}

			$recommended_products = $this->templateManager->fillTemplateRepeats( $r_group_template, array( 'recommended product' => $r_group ) );
		}
		else $recommended_products = '';

		$template = $this->templateManager->fillTemplateSections( $template, array( 'recommended products' => $recommended_products ) );

		// Reviews
		///////////////////////////////////////////////////////////////////////////
		$product->getReviews();

		// get the reviews template
		$reviews_template = $this->templateManager->getTemplateSection( 'reviews', $template );

		// Reviews item template
		$review_item_template = $this->templateManager->getTemplateRepeat( 'review', $reviews_template );

		$reviews = '';

		$basket = $emuShop->getBasket();

		if( !$review_votes = $basket->getSessionData('review-votes') ) $review_votes = array();


		foreach( $product->reviews as $review )
		{
			if( $review->post->post_status != 'publish' ) continue;

			$review->getComments();

			$comments = '';

			// print_r( $review->comments );

			// build the comments list
			if( count( $review->comments ) > 0 )
			{
				$comments .= '<ul class="review-comments">';
				for( $n = 0; $n < count( $review->comments); $n++ )
				{
					$comment = $review->comments[$n];
					$comments .= '<li><em>&quot;'.$comment->comment_content.'&quot;</em><span class="comment-details"><strong> - '.$comment->comment_author.'</strong>, '.apply_date_format('standard-with-time-alternative', $comment->comment_date).'</span></li>';
				}
				$comments .= '</ul>';
			}

			$show_voting_form = in_array( $review->dbID, $review_votes ) ? false : true;

			$review_item = $this->templateManager->fillTemplate( $review_item_template, $review->getTemplateTags(), $review, $review->ID );
			$review_item = $this->templateManager->fillTemplate( $review_item, array( 'comments' => $comments ) );
			$reviews .= $this->templateManager->setTemplateConditionals( $review_item, array( 'review voting form' => $show_voting_form ) );
		}

		if( !$review_submissions = $basket->getSessionData('review-submissions') ) $review_submissions = array();

		if( is_user_logged_in() )
		{
			if( in_array( $product->dbID, $review_submissions ) )
			{
				$review_form = '<p class="review-message">Your review has been submitted and is awaiting moderation</p>';
			}
			else
			{
				// get the review form
				$review_form = $this->templateManager->getTemplateSection( 'review form', $template );

				$tags = array( 	'product id' => $product->dbID,
								'review form messages' => $emuShop->getMessages( 'review' ),
								'post id' => $product->postID,
								'ratings' => drop_down( '', 'product_rating', '', post_val( 'product_rating' ), $emuShop->getRatingOptions(), 'Please select' ),
								'review title' => post_val( 'review_title' ),
								'review text' => post_val( 'review_text' ) );

				$review_form = $this->templateManager->fillTemplate( $review_form, $tags, null, $product->postID );

				if( !$reviews )
					$reviews = '<p>Be the first to write a review! Submit your own review using the form below...</p>';
			}
		}
		else
		{
			if( !$reviews )
				$review_form = '<p class="review-message"><a href="'.$emuShop->pageManager->pages->login->url.'?return='.$product->permalink.'">Login</a> and be the first to write a review!</p>';
			else
				$review_form = '<p class="review-message"><a href="'.$emuShop->pageManager->pages->login->url.'?return='.$product->permalink.'">Login</a> to leave your own review</a></p>';
		}

		$tags = array( 	'review summary table' => $emuShop->productHelper->buildReviewsSummaryTable( $product ),
						'average rating stars large' => $product->getRatingStars( 'large' ),
						'rating summary' => $product->ratingSummary );

		$reviews_template = $this->templateManager->fillTemplateRepeats( $reviews_template, array( 'review' => $reviews ) );
		$reviews_template = $this->templateManager->fillTemplate( $reviews_template, $tags, null, $this->post );

		$template = $this->templateManager->fillTemplateSections( $template, array( 'reviews' => $reviews_template, 'review form' => $review_form ) );

		$tags = array( 	'product messages' => $emuShop->getMessages('product'),
						'plugin url' => $emuShop->pluginURL );

		// If the product has variants then we don't show the quantity drop down - there will be quantity
		// drop downs in the variant table
		$template = $this->templateManager->setTemplateConditionals( $template, array( 'product purchase fields' => $product->hasVariants == false, 'product options' => $product->hasProductOptions() ) );

		$template = $this->templateManager->fillTemplate( $template, $product->getTemplateTags(), $product, $product->postID );
		$template = $this->templateManager->fillTemplate( $template, $tags );

		$template = $this->templateManager->setTemplateCustomConditionals( $template, $product->postID );

		$this->content = apply_filters( 'emu_product_content', $template );
	}
}

?>
