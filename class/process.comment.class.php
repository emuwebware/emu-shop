<?php

class emuCommentProcessor extends emuProcessor
{
	public $requiredFields = array( 'comment', 'comment_post_ID' );

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();

		switch( $this->button )
		{
			case 'Submit Comment':

				$this->processComment( $basket );

				$emuShop->addMessage( 'comment', $this->messages, $this->error ? 'error' : 'notice' );

				break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );
	}

	public function processComment( $basket )
	{
		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$this->error = true;
			return;
		}

		extract( $_POST );

		$time = current_time('mysql');

		$data = array(
			'comment_post_ID' => $comment_post_ID,
			'comment_author' => $basket->customer->username,
			'comment_author_url' => '',
			'comment_type' => '',
			'comment_author_email' => $basket->customer->email,
			'comment_content' => $comment,
			'comment_parent' => 0,
			'user_id' => $basket->customer->ID,
			'comment_date' => $time,
			'comment_approved' => 0
		);

		$comment_id = wp_new_comment( $data );

		if(  wp_get_comment_status( $comment_id ) !== 'approved' )
		{
			$this->messages[] = 'Your comment has been submitted and is awaiting moderation.';

			if( !$comment_submissions = $basket->getSessionData('comment-submissions') ) $comment_submissions = array();

			$comment_submissions[] = $comment_post_ID;

			$basket->setSessionData('comment-submissions', $comment_submissions);
		}
		else
		{
			$this->messages[] = 'Comment submitted';
		}



	}

}

?>
