<?php


class emuLoginDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'login.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		$login_template = $this->template;

		$regionOptions = $emuShop->regionManager->getRegionOptions( post_val('country'), post_val('selected_state'), post_val('entered_state') );

		$tags = array( 	'login email' => post_val('login_email'),
						'login password' => post_val('login_password'),
						'first name' => post_val('first_name'),
						'last name' => post_val('last_name'),
						'username' => post_val('username'),
						'email' => post_val('email'),
						'email confirm' => post_val('email_confirm'),
						'password' => post_val('password'),
						'reset password url' => $emuShop->pageManager->pages->resetPassword->url,
						'password confirm' => post_val('password_confirm'),
						'address' => post_val('address'),
						'postal code' => post_val('postal_code'),
						'city' => post_val('city'),
						'phone number' => post_val('phone_number'),
						'registration messages' => '',
						'login messages' => $emuShop->getMessages('login'),
						'countries' => $regionOptions->countryDropDown,
						'selected state class' => $regionOptions->stateDropDownClass,
						'states' => $regionOptions->stateDropDown,
						'entered state' => $regionOptions->stateTextInput,
						'entered state class' => $regionOptions->stateTextInputClass,
						'return url' => request_val('return') );

		$login_template = $this->templateManager->fillTemplate( $login_template, $tags );

		$this->content = apply_filters( 'emu_login_content', $login_template );

	}
}

?>
