<?php


class emuRegistrationDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'registration.htm';
        parent::__construct();

	}

	public function build()
	{
		global $emuShop;

		$template = $this->template;

		$tags = array( 	'first name' => post_val('first_name'),
						'last name' => post_val('last_name'),
						'company name' => post_val('company_name'),
						'job title' => post_val('job_title'),
						'department' => post_val('department'),
						'prefix' => post_val('prefix'),
						'suffix' => post_val('suffix'),
						'username' => post_val('username'),
						'email' => request_val('email'),
						'email confirm' => post_val('email_confirm'),
						'password' => post_val('password'),
						'newsletter checkbox' => post_val('newsletter') ? ' checked="checked"' : '',
						'reset password url' => $emuShop->pageManager->pages->resetPassword->url,
						'password confirm' => post_val('password_confirm'),
						'registration messages' => $emuShop->getMessages('registration'),
						'return url' => request_val('return') );

		$template = $this->templateManager->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_registration_content', $template );

	}
}

?>
