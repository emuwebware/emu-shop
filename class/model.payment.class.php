<?php


class emuPayment extends emuShopCommon
{
	public $messages = array();
	public $failed = true;

	public $paymentMethod;
	public $order;
	public $basket;

	public function __construct( $order = null, $paymentMethod = null )
	{
		if( $order ) $this->order = $order;
		if( $paymentMethod ) $this->paymentMethod = $paymentMethod;
	}

	public function makePayment()
	{
		global $emuShop;

		if( !$emuShop->paymentGateway )
		{
			$this->messages[] = "A payment gateway has not been selected";
			return;
		}

		$order = $this->order;
		$paymentMethod = $this->paymentMethod;

		do_action( 'emu_shop_make_payment', $order, $paymentMethod );

		$payment_result = false;

		if( has_filter( 'emu_shop_payment_result' ) )
			$payment_result = apply_filters( 'emu_shop_payment_result', $payment_result );

		$this->failed = $payment_result ? false : true;

		if( has_filter( 'emu_shop_payment_messages' ) )
			$this->messages = apply_filters( 'emu_shop_payment_messages', $this->messages );

	}
}

?>
