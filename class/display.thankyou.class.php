<?php



class emuThankyouDisplay extends emuDisplay
{
    public function __construct()
	{
        $this->templateFile = 'thankyou.htm';
        parent::__construct();
	}

	public function build()
	{
		global $current_user; get_currentuserinfo();
		global $emuShop;

		$template = $this->template;

		if( ! $customer_order_post_id = $emuShop->getSessionData('customer_order') )
		{
			$this->content = 'Order not found!';
			return;
		}

		$post = get_post( $customer_order_post_id );

		setup_postdata( $post );

		// make sure this order comes from this user
		if( get_the_author_meta( 'ID' ) == $current_user->ID )
			$template = $this->templateManager->fillTemplate( $template, array( 'order summary' => get_the_content(), 'email address' => $current_user->user_email ) );
		else
			$template = 'You are not authorized to view this page.';

		$this->content = apply_filters( 'emu_thankyou_content', $template );
	}
}

?>
