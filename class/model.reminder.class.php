<?php

class emuPasswordReminder
{
	public $email;
	public $error;
	public $messages = array();

	public function send()
	{
		global $wpdb, $emuShop;

		if( !$this->email )
		{
			$this->messages[] = 'An email address is required to send a new password';
			$this->error = true;
			return;
		}

		global $wp_version;

		if( $wp_version < '3.1' ) require_once(ABSPATH . WPINC . '/registration.php');

		if( !email_exists( $this->email ) )
		{
			$this->messages[] = 'This email address has not be registered';
			$this->error = true;
			return;
		}

		// get the user id
		$wpUserID = $wpdb->get_var( $wpdb->prepare( "select wpUserID from {$emuShop->dbPrefixShared}customers where email = %s", $this->email ) );

		if( !$wpUserID )
		{
			$this->messages[] = 'Sorry but we can\'t find that user!';
			$this->error = true;
			return;
		}

		// Generate a new password
		$new_password = $emuShop->generateRandomString( $number_chars = 8 );

		$user_data = array( 'ID' => $wpUserID, 'user_pass' => $new_password );

		wp_update_user( $user_data );

		$templateManager = $emuShop->getManager( 'email-template' );

        // Send the new password to the customer
		$email = $templateManager->getEmailTemplate( 'password-reset.txt' );

		$customer = $emuShop->getInstance( 'emuCustomer', array( $wpUserID ) );

		$message = $email->content;

		$message = $templateManager->fillTemplate( $message, $customer->getTemplateTags('customer '), $customer );
		$message = $templateManager->fillTemplate( $message, Array( 'new password' => $new_password ) );

		$email->to = $templateManager->fillTemplate( $email->to, $customer->getTemplateTags('customer '), $customer );
		$email->from = $templateManager->fillTemplate( $email->from, $customer->getTemplateTags('customer '), $customer );
		$email->cc = $templateManager->fillTemplate( $email->cc, $customer->getTemplateTags('customer '), $customer );
		$email->bcc = $templateManager->fillTemplate( $email->bcc, $customer->getTemplateTags('customer '), $customer );

		$emuShop->mail( $email->to, $email->from, $email->subject, $message, $email->type, $email->cc, $email->bcc );

		$this->messages[] = 'Your password has been reset and your new password has been emailed to '.$customer->email;

	}

}

?>
