<?php

class emuSpecialCharge
{
    public $charge;
    public $description;

    public function getCharge($format = null)
    {
        global $emuShop;

        switch( $format )
        {
            case SPECIAL_CHARGE:
                return $this->charge;

            case SPECIAL_CHARGE_FORMATTED:
            default:
                return $emuShop->currency.apply_number_format( 'currency', $this->getCharge(SPECIAL_CHARGE) );
        }
    }

    public function getChargeValue()
    {
        return $this->getCharge(SPECIAL_CHARGE);
    }

}

?>
