<?php

class emuDeliveryProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();

		$basket->setSessionData( array( 'shipping_charge' => post_val('shipping_charge') ) );

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $emuShop->pageManager->pages->confirmation->url );

		header( 'Location: '.$location );
		exit();

	}

}
?>
