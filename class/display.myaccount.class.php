<?php


class emuMyAccountDisplay extends emuDisplay
{
	public $accountActivationTemplate;

	public function __construct()
	{
        $this->templateFile = 'my-account.htm';
        parent::__construct();
	}

	public function build()
	{
		global $emuShop;

		if( ! is_user_logged_in() )
		{
			$accountActivationDisplay = $emuShop->getInstance('emuAccountActivationDisplay');
			$accountActivationDisplay->build();
			$this->content = $accountActivationDisplay->content;
			return;
		}

		$customer = $emuShop->getCustomer();

		$tM = $this->templateManager;

		$template = $this->template;

		// Fill profile tags
		$template = $tM->fillTemplate( $template, $customer->getTemplateTags(), $customer );

		// Fill address tags
		$address_book = $customer->getAddressBook();

		// Customer and address details
		if( $address = $address_book->getDefaultShippingAddress() )
			$default_shipping_address = '<strong>'.$address->firstName.' '.$address->lastName.'</strong><br />'.$address->getAddressAggregate(', ');
		else
			$default_shipping_address = 'You do not currently have a default shipping address';

		if( $address = $address_book->getDefaultBillingAddress() )
			$default_billing_address = '<strong>'.$address->firstName.' '.$address->lastName.'</strong><br />'.$address->getAddressAggregate(', ');
		else
			$default_billing_address = 'You do not currently have a default billing address';

		$template = $tM->fillTemplate( $template, array( 'default billing address' => $default_billing_address, 'default shipping address' => $default_shipping_address ) );

		// Fill orders
		$order_repeat = $tM->getTemplateRepeat( 'order row', $template );

		if( !$customer_orders = $customer->hasOrders() )
		{
			$order_items = '<tr><td colspan="2">You do not currently have any orders.</td></tr>';
			$show_all_orders_link = false;
		}
		else
		{
			$show_all_orders_link = true;
			$order_items = '';
			$order_count = 0;

			for( $i = 0, $i_count = count( $customer_orders ); $i < $i_count && $i < 3; $i++ )
			{
				$order = $customer_orders[$i];
				$order_items .= $tM->fillTemplate( $order_repeat, $order->getTemplateTags(), $order );
			}

		}

		$template = $tM->fillTemplateRepeats( $template, array( 'order row' => $order_items ) );

		$template = $tM->setTemplateConditionals( $template, array( 'view all orders' => $show_all_orders_link ) );

		// Payment method template
		$payment_repeat = $tM->getTemplateRepeat( 'payment method', $template );

		$payment_manager = $customer->getPaymentManager();

		$payment_summary = '';

		if( $payment_methods = $payment_manager->getPaymentMethods() )
		{
			foreach( $payment_methods as $payment_method)
			{
				$payment_summary .= $tM->fillTemplate( $payment_repeat, $payment_method->getTemplateTags(), $payment_method );
			}
		}

		$template = $tM->fillTemplateRepeats( $template, array( 'payment method' => $payment_summary ) );

		// $template = $tM->fillTemplateSections( $template, array( 'orders' => $orders, 'payment summary' => $payment_summary ));


		// $template = $tM->fillTemplate( $template, $customer->getTemplateTags(), $customer );

		$this->content = apply_filters( 'emu_my_account_content', $template );

	}
}

?>
