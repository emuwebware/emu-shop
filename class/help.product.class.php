<?php

class emuH_Product extends emuHelper
{
    public function getSalePriceMarkup($product)
    {
        return '<span class="sale-strike">'.$product->getPrice(PRODUCT_ORIGINAL_PRICE_FORMATTED).'</span> <span class="product-sale-price">'.$product->getPrice(PRODUCT_SALE_PRICE_FORMATTED).' Sale Price</span>';
    }

    public function getRatingSummary($product)
    {
        if( !$product->ratingCount )
        {
            if( is_user_logged_in() )
                $rating_summary = '- be the first to <a href="#reviews">write a review</a>.';
            else
                $rating_summary = '- <a href="'.$this->emuApp->pages->login->url.'?return='.urlencode( $product->permalink.'#reviews' ).'">log in</a> and be the first to write a review.';

        }
        else
         //   $rating_summary = '(<a href="'.$this->emuApp->pages->reviewsSummary->url.'?p='.$product->post->post_name.'">'.$product->ratingCount.' customer review'.( $product->ratingCount > 1 ? 's' : '' ).'</a>)';
            $rating_summary = '(<a href="#reviews">'.$product->ratingCount.' customer review'.( $product->ratingCount > 1 ? 's' : '' ).'</a>)';

        return $rating_summary;
    }

    public function getConciseRatingSummary($product)
    {
        return $product->ratingCount ? $product->ratingCount.' Review'.( $product->ratingCount > 1 ? 's' : '' ) : '0 Reviews';
    }

    public function getStockSummary($product)
    {
         $stock_summary = '';

         if( $product->isStockTracked && count( $product->variants ) == 0 && $product->qtyInStock <= 0 )
             $stock_summary = " - Coming Soon";

         return $stock_summary;
    }

    public function getPriceRange($variant)
    {
        global $emuShop;

        $variant_range = $variant->getVariantPriceRange();

        return 'From '.$emuShop->currency.apply_number_format( 'currency', $variant_range->min );
    }

    public function getFacebookLink($product)
    {
        return '<iframe src="http://www.facebook.com/plugins/like.php?href='.urlencode($product->permalink).'&amp;layout=standard&amp;show_faces=true&amp;width=222&amp;action=like&amp;font=tahoma&amp;colorscheme=light&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:222px; height:80px;" allowTransparency="true"></iframe>';
    }

    public function getProductOptionsMarkup($product)
    {
        return $this->buildProductOptions($product);
    }

    public function listProductOptions($basket_item)
    {
        $list = array();

        foreach( $basket_item->getOptions() as $option_name => $option_selection )
        {
            if( is_array( $option_selection ) )
                $option_selection = implode(', ', $option_selection);

            $list[] = "{$option_name}: {$option_selection}";
        }

        return implode('<br />', $list);
    }

    public function buildProductOptions($product)
    {
        if( !$options = $product->getProductOptions() ) return '';

        $output = '';

        foreach( $options as $option )
        {
            $output .= '<p class="option-header">'.$option->name.'</p>';

            $selected = post_val( $product->postID.'-'.$option->name );

            $output .= '<div class="option-group">';

            switch( strtolower( $option->type ) )
            {
                case 'multiple':

                    if( !is_array( $selected ) ) $selected = array();

                    foreach( $option->options as $opt )
                        $output .= '<p class="option-row"><label><input type="checkbox" name="'.$product->postID.'-'.$option->name.'[]" value="'.$opt.'" '.( in_array( $opt, $selected ) ? 'checked="checked"' : '' ).' /><span>'.$opt.'</span></label></p>';

                    break;

                case 'single':

                    $output .= drop_down( $id = '', $name = $product->postID.'-'.$option->name, $class = '', $selected = post_val( $product->postID.'-'.$option->name ), $options = $option->options, $default_null = 'Select...' );

                    break;
            }

            $output .= '</div>';
        }

        $output = '<div class="product-options">'.$output.'</div>';

        return $output;
    }

    public function getProductImage($size, $product)
    {
        if( $size == 'url' ) return wp_get_attachment_url( get_post_thumbnail_id( $product->postID ) );

        $url = get_the_post_thumbnail( $product->postID, $this->emuApp->emuAppID."-$size", array( 'class' => $this->emuApp->emuAppID."-$size" ) );

        if( is_ssl() ) $url = preg_replace( '/http:/', 'https:', $url );

        return $url;
    }

    public function getStars( $size, $product, $total_stars = 5 )
    {
        $no_stars = $product->ratingAverage;

        $image = array( "large" => (object) array( "height" => "20", "width" => "21" ),
                        "small" => (object) array( "height" => "15", "width" => "16" ) );

        $star = '<img src="'.$this->emuApp->pluginURL.'/image/star_'.$size.'_%s.gif" height="'.$image[$size]->height.'" width="'.$image[$size]->width.'" alt="%1$s star" />';

        // Get the full stars
        $stars = str_repeat( sprintf( $star, 'full' ), floor( $no_stars ) ); // e.g. 4.3 = 4 stars

        if( floor( $no_stars ) == $no_stars )
        {
            $empty_star_count = $total_stars - $no_stars;
        }
        else
        {
            // add the partial star
            $stars .= sprintf( $star, 'half' );

            $empty_star_count = $total_stars - ( floor( $no_stars ) + 1 );
        }

        $stars .= str_repeat( sprintf( $star, 'empty' ), $empty_star_count );

        return $stars;
    }

    function buildVariantQtyDropDown()
    {
        $variant_qty_dd = '';

        for( $n = 0; $n < 11; $n++ )
            $variant_qty_dd .= sprintf( '<option value="%s">%1$s</option>', $n );

        $variant_qty_dd = '<select name="product_qty[]">'.$variant_qty_dd.'</select>';

        return $variant_qty_dd;
    }

    function getVariantsDropDown( $variant_groups )
    {
        $variant_set = '';
        $variant_qty_dd = $this->buildVariantQtyDropDown();

        foreach( $variant_groups as $variant_group )
        {
            $variant_set .= '<table class="product-variants">';
            $variant_set .= '<thead><tr><td>'.$variant_group->groupName.'</td><td>Price</td><td>Qty.</td></tr></thead>';

            foreach( $variant_group->getVariants() as $variant )
            {
                $variant_set .= '<tr>';
                $variant_set .= '<td>';
                $variant_set .= $variant->variantDescription;

                if( $variant->isStockTracked && $variant->qtyInStock <= 0 )
                    $variant_set .= ', Item not in Stock';

                $variant_set .= '</td>';
                $variant_set .= '<td>';
                $variant_set .= $variant->getPrice();
                $variant_set .= '</td>';
                $variant_set .= '<td>';
                $variant_set .= $variant_qty_dd.'<input type="hidden" name="product_id[]" value="'.$variant->getID().'">';
                $variant_set .= '</td>';
                $variant_set .= '</tr>';
            }

            $variant_set .= '</table>';
        }

        return $variant_set;
    }

    public function getEditProductLink($product)
    {
        if( current_user_can( 'manage_options' ) )
            return '<a href="'.get_edit_post_link().'" target="_blank">Edit Product</a>';

        return '';
    }

    public function getEditProductIcon($product)
    {
        if( current_user_can( 'manage_options' ) )
            return '<a href="'.get_edit_post_link().'" target="_blank"><img src="'.plugins_url( "/{$this->emuApp->pluginName}/image/edit.gif" ).'" height="19" width="39" alt="Edit product" /></a>';

        return '';
    }


    function buildReviewsSummaryTable( $product )
    {
        global $wpdb, $emuShop;

        $sql = "select
                    r.rating,
                    count(r.rating) as ratingCount
                from
                    {$emuShop->dbPrefix}reviews r
                inner join {$wpdb->prefix}posts p on p.ID = r.postID
                where
                    r.productID = {$product->dbID}
                and
                    p.post_status = 'publish' group by r.rating";

        $sql = "select
                    r.rating,
                    count(r.rating) as ratingCount,
                    ( select count(r.rating) from {$emuShop->dbPrefix}reviews r inner join {$wpdb->prefix}posts p on p.ID = r.postID where r.productID = {$product->dbID} and p.post_status = 'publish' )
                        as countTotal,
                    (
                        (  count(r.rating) /
                            ( select count(r.rating) from {$emuShop->dbPrefix}reviews r inner join {$wpdb->prefix}posts p on p.ID = r.postID where r.productID = {$product->dbID} and p.post_status = 'publish' )
                        ) * 100
                    ) as percentage
                from
                    {$emuShop->dbPrefix}reviews r
                inner join
                    {$wpdb->prefix}posts p on p.ID = r.postID
                where
                    r.productID = {$product->dbID}
                and
                    p.post_status = 'publish'
                group by
                    r.rating";


        $rating_counts = $wpdb->get_results( $sql );

        $db_ratings = array();

        $count_total = 0;

        // All this faffing to get an array of counts - nice to have a better way to do this.
        foreach( $rating_counts as $count )
        {
            $count_total = $count->countTotal;
            $db_ratings[$count->rating.'-star'] = $count;
        }

        $ratings = array();

        for( $i = 1; $i < 6; $i++ )
        {
            $key = $i.'-star';

            if( isset( $db_ratings[$key] ) )
                $ratings[$key] = $db_ratings[$key];
            else
                $ratings[$key] = (object) array( 'rating' => $i, 'ratingCount' => 0, 'countTotal' => $count_total, 'percentage' => 0 );
        }

        // $template = $this->getInstance( 'emuTemplate' );
        $templateManager = $emuShop->getManager( 'theme' );

        $template = $templateManager->getTemplate( 'review-summary-table.htm' );

        $templateManager->replaceTag( '5 star count', $ratings['5-star']->ratingCount, $template );
        $templateManager->replaceTag( '4 star count', $ratings['4-star']->ratingCount, $template );
        $templateManager->replaceTag( '3 star count', $ratings['3-star']->ratingCount, $template );
        $templateManager->replaceTag( '2 star count', $ratings['2-star']->ratingCount, $template );
        $templateManager->replaceTag( '1 star count', $ratings['1-star']->ratingCount, $template );

        $templateManager->replaceTag( '5 star width', $ratings['5-star']->percentage.'%', $template );
        $templateManager->replaceTag( '4 star width', $ratings['4-star']->percentage.'%', $template );
        $templateManager->replaceTag( '3 star width', $ratings['3-star']->percentage.'%', $template );
        $templateManager->replaceTag( '2 star width', $ratings['2-star']->percentage.'%', $template );
        $templateManager->replaceTag( '1 star width', $ratings['1-star']->percentage.'%', $template );

        return $template;
    }

}

?>