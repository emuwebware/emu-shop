<?php


class emuAddressBook extends emuDB
{
	public $addresses = array();
	public $messages = array();
	public $error = false;
	public $data = array();

	public $addressIDs = array();

	public $sAddress;
	public $bAddress;

	public function __construct( $customer )
	{
		if( is_numeric( $customer ) )
		{
			$this->customerID = $customer;
		}
		else if( is_object( $customer ) )
		{
			$this->customerID = $customer->dbID;
		}
	}

	public function __get( $member )
	{
		global $wpdb, $emuShop;

		switch( $member )
		{
			case 'defaultShippingAddress':

				return $this->getDefaultShippingAddress();

			break;
			case 'defaultBillingAddress':

				return $this->getDefaultBillingAddress();

			break;
			default:

				if( !isset( $this->data[ $member ] ) ) return null;

				return $this->data[ $member ];
		}
    }

    public function getDefaultBillingAddress()
    {
    	foreach( $this->getAddresses() as $address )
    	{
    		if( $address->isDefaultBilling ) return $address;
    	}
    	return false;
    }

    public function getDefaultShippingAddress()
    {
    	foreach( $this->getAddresses() as $address )
    	{
    		if( $address->isDefaultShipping ) return $address;
    	}
    	return false;
    }

    public function __set( $member, $value )
	{
        $this->data[ $member ] = $value;
    }

	public function getAddressListforDropDown()
	{
		$addresses = array();

		foreach( $this->addresses as $address )
		{
			$addresses[$address->dbID] = $address->getAddressAggregate( ', ' );
		}

		return $addresses;
	}

	public function hasAddresses()
	{
		return count( $this->getAddresses() ) > 0;
	}

	public function getAddresses()
	{
		if( $this->customerID && count($this->addresses) == 0 )
		{
			global $wpdb, $emuShop;

			// Reset
			$this->addresses = array();
			$this->addressIDs = array();

			$rs = $wpdb->get_results( "select dbID from {$emuShop->dbPrefixShared}address_book where customerID = {$this->customerID}" );

			if( $rs )
			{
				for( $n = 0; $n < count( $rs ); $n++ )
				{
					$this->addresses[] = $emuShop->getInstance( 'emuAddress', array( $rs[$n]->dbID ) );
					$this->addressIDs[] = $rs[$n]->dbID;
				}
			}

		}

		return $this->addresses;
	}

	public function addAddress( $address )
	{
		if( is_array( $address ) )
		{
			$addresses = $address;
			foreach( $addresses as $address ) $this->addAddress( $address );
			return;
		}

		global $emuShop;

		$new_address = $emuShop->getInstance( 'emuAddress' );
		$new_address->data = (array) $address;

		$this->addresses[] = $new_address;
		$this->messages[] = 'Address added.';

	}

	public function removeAddress( $address )
	{

		if( is_object( $address ) )
			$dbID = $address->dbID;
		else if ( is_numeric( $address ) )
			$dbID = $address;
		else return;

		$addresses = array();

		foreach( $this->addresses as $address )
		{
			if( $address->dbID != $dbID ) $addresses[] = $address;
		}

		$this->addresses = $addresses;

	}

	public function removeAddressFromDB( $address )
	{
		global $emuShop;

		if( is_object( $address ) )
			$dbID = $address->dbID;
		else if ( is_numeric( $address ) )
			$dbID = $address;
		else return;

		global $wpdb;

		$wpdb->query( "delete from {$emuShop->dbPrefixShared}address_book where dbID = {$dbID}" );
	}

	public function update()
	{
		$arr_address_ids = array();

		foreach( $this->addresses as $address ) $arr_address_ids[] = $address->dbID;

		// do we need to remove any addresses?
		foreach( $this->addressIDs as $addressID )
		{
			if( !in_array( $addressID, $arr_address_ids ) ) $this->removeAddressFromDB( $addressID );
		}

		// update remaining addresses
		foreach( $this->addresses as $address )
		{
			$address->customerID = $this->customerID;
			$address->update();
		}

		return true;
	}

}

?>
