<?php


class emuAccountActivator
{
	public $activationCode;
	
	public $error;
	public $messages = array();
	
	public function activate()
	{
		global $wpdb, $emuShop;
		
		if( !$this->activationCode )
		{
			$this->messages[] = 'An activation code is required';
			$this->error = true;
			return;
		}
		
		$affected = $wpdb->query( $wpdb->prepare( "update {$emuShop->dbPrefixShared}customers set isActive = 1 where activationCode = %s", $this->activationCode ) );
		
		if( $affected > 0 )
		{
			$this->messages[] = 'Your account has been activated. Please login below using your email address and password.';
			$this->error = false;
		}
		else
		{
			$this->messages[] = 'Your account has already been activated or the activation code is incorrect.';
			$this->error = true;
		}
		
		
	}

}

?>
