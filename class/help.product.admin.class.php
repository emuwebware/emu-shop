<?php

class emuH_ProductAdmin extends emuHelper
{
    function showStockDetails( $product )
    {
        ?>
        <div class="emu-e">

            <div class="e-stock-options">

                <table>
                <tr>
                    <th>Track stock quantities?</th>
                    <td><?php echo drop_down( '', 'track_stock', '', $product->trackStock, $this->emuApp->productManager->getStockTrackingOptions() )?></td>
                </tr>
                <tr>
                    <th>If the item is no longer in stock</th>
                    <td><?php echo drop_down( '', 'no_stock_left_option', '', $product->noStockLeftOption, $this->emuApp->productManager->getNoStockLeftOptions() )?></td>
                </tr>
                </table>

            </div>

        </div>
        <?php
    }

    public function showProductDetails( $product )
    {
        global $emuShop;

        ?>
        <div class="emuS">

            <table class="product-details">
            <tr>
                <th>ID</th>
                <td class="id-value"><?php echo $product->getID()?></td>
            </tr>
            <tr>
                <th>SKU</th>
                <td class="sku-value"><?php
                if( $product->hasVariants )
                    echo 'See variants';
                else
                    echo $product->SKU ? $product->SKU : '-';
                ?></td>
            </tr>
            <tr>
                <th>Price</th>
                <td class="price-value"><?php echo $product->getPrice()?></td>
            </tr>
            <tr>
                <th>Shipping</th>
                <td class="shipping-value"><?php

                if( $product->hasVariants )
                    echo 'See variants';
                else
                {
                    if( $product->hasShippingCost() )
                        echo $product->getPrice(PRODUCT_SHIPPING_COST_FORMATTED);
                    else
                        echo '-';
                }
                ?></td>
            </tr>
            <tr>
                <th>Handling</th>
                <td class="handling-value"><?php

                if( $product->hasVariants )
                    echo 'See variants';
                else
                {
                    if( $product->hasHandlingCost() )
                        echo $product->getPrice(PRODUCT_HANDLING_COST_FORMATTED);
                    else
                        echo '-';
                }
                ?></td>
            </tr>
            <tr>
                <th>Weight (<?php echo $emuShop->shippingManager->getWeightUnits()?>)</th>
                <td class="weight-value"><?php

                if( $product->hasVariants )
                    echo 'See variants';
                else
                {
                    if( $product->weight )
                        echo $product->weight;
                    else
                        echo '-';
                }
                ?></td>
            </tr>
            <tr>
                <th>Qty. in Stock</th>
                <td class="stock-value"><?php

                if( $product->hasVariants )
                    echo 'See variants';
                else
                {
                    if( $product->qtyInStock )
                        echo $product->qtyInStock;
                    else
                        echo '-';
                }
                ?></td>
            </tr>
            </table>

            <input type="hidden" name="emu-product-form" value="true" />

            <div class="product-action">
                <img src="<?php echo EMU_FRAMEWORK_URL?>/image/activity.gif" height="16" width="16" alt="loading" class="product-loading" /><input type="button" class="button edit-product" value="Edit" />
            </div>

            <div class="hidden">

                <div id="edit-product-form" title="Edit product" class="emu-dialog">

                    <form method="post" action="" class="add-edit-product" id="addEditProductForm" autocomplete="off">

                        <div class="two-column clearfix">

                            <div class="column-one">

                                <table>
                                    <tbody>
                                    <tr class="variant-name">
                                        <th>Variant Name</th>
                                        <td><input type="text" name="name" class="emu-text" value="" tabindex="101" /></td>
                                    </tr>
                                    <tr>
                                        <th>SKU</th>
                                        <td><input type="text" name="sku" class="emu-text" value="" tabindex="102" /><span class="has-variants-message"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Price (<?php echo $emuShop->currency?>)</th>
                                        <td><input type="text" name="price" class="emu-text-short" value="" tabindex="103" /><span class="display-price"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Sale Price (<?php echo $emuShop->currency?>)</th>
                                        <td><input type="text" name="salePrice" class="emu-text-short" value="" tabindex="104" /><span class="has-variants-message"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Shipping Option</th>
                                        <td><?php echo drop_down( '', 'shippingOption', '', 1, $emuShop->getManager('shipping')->getShippingOptions() )?><span class="has-variants-message"></td>
                                    </tr>
                                    <tr class="shipping-charge">
                                        <th>Shipping Charge (<?php echo $emuShop->currency?>)</th>
                                        <td><input type="text" name="shipping" class="emu-text-short" value="" tabindex="105" /><span class="has-variants-message"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Handling Option</th>
                                        <td><?php echo drop_down( '', 'handlingOption', '', 1, $emuShop->getManager('handling')->getHandlingOptions() )?><span class="has-variants-message"></td>
                                    </tr>
                                    <tr class="handling-charge">
                                        <th>Handling Charge (<?php echo $emuShop->currency?>)</th>
                                        <td><input type="text" name="handling" class="emu-text-short" value="" tabindex="106" /><span class="has-variants-message"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Has Tax</th>
                                        <td><label><input type="checkbox" name="hasTax" value="1" checked="checked" tabindex="109" /> Yes</label></td>
                                    </tr>
                                    <tr>
                                        <th>Weight (<?php echo $emuShop->shippingManager->getWeightUnits()?>)</th>
                                        <td><input type="text" name="weight" class="emu-text-short" value="" tabindex="107" /><span class="has-variants-message"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Qty. in Stock</th>
                                        <td><input type="text" name="qtyInStock" class="emu-text-short" value="" tabindex="108" /><span class="has-variants-message"></span></td>
                                    </tr>
                                    <tr>
                                        <th>Is Active</th>
                                        <td><label><input type="checkbox" name="isActive" value="1" checked="checked" tabindex="109" /> Yes</label></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                            <div class="column-two">

                                <p><strong>Custom Variables</strong></p>

                                <table class="custom-vars hidden">
                                <thead>
                                <tr>
                                    <td>Variable Name</td>
                                    <td>Variable Value</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="template hidden">
                                    <td><input type="text" name="varName[]" class="emu-text custom-var-name" value="" tabindex="110" /></td>
                                    <td><input type="text" name="varValue[]" class="emu-text custom-var-value" value="" tabindex="111" /></td>
                                    <td><img src="<?php echo EMU_FRAMEWORK_URL?>/image/del.gif" height="14" width="14" alt="delete" class="delete-custom-var" /></td>
                                </tr>
                                </tbody>
                                </table>

                                <input type="button" name="add-custom-var" value="Add Variable" class="button add-custom-var" />

                            </div>

                        </div>


                        <p class="validateTips"></p>

                        <input type="hidden" name="variantId" value="" />
                        <input type="hidden" name="groupId" value="" />

                    </form>
                    <!-- / form -->

                </div>
                <!-- / edit-product-form -->

                <img src="<?php echo EMU_FRAMEWORK_URL?>/image/activity.gif" height="16" width="16" alt="loading" class="loading-image" />



            </div>
            <!-- / hidden -->

        </div>

        <?php
    }


    public function showProductVariants( $variant_groups )
    {
        ?>

        <div class="emuS">

            <div class="variant-container">

                <div class="variants">

                    <?php

                    foreach( $variant_groups as $variant_group )
                        $this->buildVariantGroupMarkup( false, $variant_group );
                    ?>

                </div>
                <!-- / variants -->

            </div>
            <!-- / variant-container -->

            <table>
            <tr>
                <th>Group name</th>
                <td><input type="text" name="new_group_name" value="" class="new-group-name" /></td>
                <td><input type="button" class="button add-group" value="Add Variant Group" /><img src="<?php echo EMU_FRAMEWORK_URL?>/image/activity.gif" height="16" width="16" alt="loading" class="create-group-loading" /></td>
            </tr>
            </table>

            <?php $this->buildVariantGroupMarkup( $template = true ) ?>
            <!-- / group template -->

        </div>
        <!-- / emuS -->

        <?php

    }

    public function buildVariantGroupMarkup( $template = false, $variant_group = null )
    {
        global $emuShop;

        if( $template )
        {
            $group_name = 'New Variant Group';
            $group_id = '';
        }
        else
        {
            $group_name = $variant_group->groupName;
            $group_id = $variant_group->dbID;
        }

        ?>
        <div class="variant-group<?php echo $template ? ' group-template' : ''?>">

            <div class="header"><a href="#"><?php echo $group_name?></a></div>

            <div class="variant-details">

                <a href="javascript:{}" class="delete-group">Delete Group <img src="<?php echo EMU_FRAMEWORK_URL?>/image/del.gif" height="14" width="14" alt="delete" /></a>

                <table class="group-options">
                <tr>
                    <th>Group name</th>
                    <td><input type="text" name="group_name" value="<?php echo $group_name?>" /></td>
                    <td><input type="button" class="button save-group-name" value="Save" /><img src="<?php echo EMU_FRAMEWORK_URL?>/image/activity.gif" height="16" width="16" alt="loading" class="group-name-loading" /></td>
                </tr>
                </table>

                <table class="variant-list <?php echo $template ? 'hidden' : ''?>">
                    <thead>
                        <tr>
                            <th class="v-id">ID</th>
                            <th class="description">SKU/Name</th>
                            <th class="price">Price</th>
                            <th class="price">Sale Price</th>
                            <th class="shipping">Shipping</th>
                            <th class="handling">Handling</th>
                            <th class="qty">Qty.</th>
                            <th class="weight">Weight</th>
                            <th class="status">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if( $template ) { ?>
                        <tr class="row-template variant-row">
                            <td class="v-id"></td>
                            <td class="description"><span class="sku"></span><span class="name"></span></td>
                            <td class="price"></td>
                            <td class="sale-price"></td>
                            <td class="shipping"></td>
                            <td class="handling"></td>
                            <td class="qty"></td>
                            <td class="weight"></td>
                            <td class="status"></td>
                            <td class="no-border"><img src="<?php echo EMU_FRAMEWORK_URL?>/image/del.gif" height="14" width="14" alt="delete" class="delete-variant" /><input type="hidden" name="variantId" value="" /></td>
                        </tr>
                        <?php } else { ?>

                            <?php
                            foreach( $variant_group->getVariants() as $variant )
                            {
                                $variant = (object) $variant->basicDetails();
                                ?>
                                <tr class="variant-row variant-<?php echo $variant->id?><?php echo $variant->isActive ? '' : ' variant-inactive'?>">
                                    <td class="v-id"><?php echo $variant->id?></td>
                                    <td class="description"><span class="sku"><?php echo $variant->SKU?></span><span class="name"><?php echo $variant->variantDescription?></span></td>
                                    <td class="price"><?php echo $variant->displayOriginalPrice?></td>
                                    <td class="sale-price"><?php echo $variant->displaySalePrice?></td>
                                    <td class="shipping"><?php echo $variant->displayShippingCost?></td>
                                    <td class="handling"><?php echo $variant->displayHandlingCost?></td>
                                    <td class="qty"><?php echo $variant->qtyInStock?></td>
                                    <td class="weight"><?php echo $variant->weight?></td>
                                    <td class="status"><?php echo $variant->displayActive?></td>
                                    <td class="no-border"><img src="<?php echo EMU_FRAMEWORK_URL?>/image/del.gif" height="14" width="14" alt="delete" class="delete-variant" /><input type="hidden" name="variantId" value="<?php echo $variant->dbID?>" /></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>

                <input type="button" class="button add-variant" value="Add Variant" />
                <input type="hidden" name="group_id" class="group-id" value="<?php echo $group_id?>" />

            </div>
            <!-- / variant-details -->

        </div>
        <!-- / variant-group -->

        <?php
    }




}

?>