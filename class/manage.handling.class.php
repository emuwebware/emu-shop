<?php

class emuM_Handling extends emuManager
{
	public $pages;

	// Handling options
	const OPTION_CHARGES = 1; // use the defined shipping charges (rules)
	const OPTION_SPECIFIC_CHARGE = 2; // set an individual shipping charge for this item
	const OPTION_NO_CHARGE = 3; // no charge

	public function registerAdminPages()
	{
		global $emuShop;

		$emuShop->registerAdminPage( array( 'name' => 'Handling',
									'filename' => 'handling.php',
									'position' => 3,
									'styles' => array( 'emu-common' ),
									'scripts' => array() ) );
	}

	public function registerClasses()
	{
		global $emuShop;

		$emuShop->registerClass( 'emuHandlingCharge', 'model.handlingcharge.class.php', 'emuShopCommon' );
	}

	function getHandlingCharges($terms = array())
	{
		global $wpdb, $emuShop;

		$sql_add = array();

		$sql = "select dbID from {$emuShop->dbPrefix}handling_charges %s order by charge desc";

		if(isset($terms['description'])) $sql_add[] = "description = '{$terms['description']}'";
		if(isset($terms['dbID'])) $sql_add[] = "dbID = '{$terms['dbID']}'";
		if(isset($terms['countryID'])) $sql_add[] = "countryID = '{$terms['countryID']}'";
		if(isset($terms['stateID'])) $sql_add[] = "stateID = '{$terms['stateID']}'";
		if(isset($terms['basedOn'])) $sql_add[] = "basedOn = '{$terms['basedOn']}'";
		$sql_add[] = 'isActive = '.( isset( $terms['isActive'] ) ? $terms['isActive'] : '1' );

		$sql_add = count($sql_add) > 0 ? 'where ('.implode(') and (', $sql_add).')' : '';

		$sql = sprintf($sql, $sql_add);

		$charges = array();

		if( $charges_rs = $wpdb->get_results( $sql ) )
		{
			foreach( $charges_rs as $charge )
				$charges[] = $emuShop->getInstance( 'emuHandlingCharge', array( $charge->dbID ), $refresh = true );
		}

		return $charges;

	}

	function clearHandlingCharges()
	{
		global $wpdb, $emuShop;

		return $wpdb->query( "delete from {$emuShop->dbPrefix}handling_charges" );
	}


	function getHandlingOptions()
	{
		return array( self::OPTION_CHARGES => 'Apply Handling Charges', self::OPTION_SPECIFIC_CHARGE => 'Set Individual Charge', self::OPTION_NO_CHARGE => 'No Handling Charge' );
	}

    public function install( $emuShop = null )
    {
		if( !$emuShop ) return;

		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		// handling
		$sql = "CREATE TABLE {$emuShop->dbPrefix}handling_charges (
		dbID int(10) NOT NULL AUTO_INCREMENT,
		countryID int(10) NULL,
		stateID int(10) NULL,
		description varchar(300) default NULL,
		basedOn varchar(10) default NULL,
		rangeFrom varchar(10) default NULL,
		rangeTo varchar(10) default NULL,
		charge float(2) default NULL,
		freeHandling BOOLEAN NULL DEFAULT 0,
		isActive BOOLEAN NOT NULL DEFAULT 1,
		UNIQUE KEY id (dbID)
		);";

		dbDelta($sql);

    }

}


?>
