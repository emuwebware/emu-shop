<?php

class emuShippingCharge extends emuTemplatedDbEntity
{
    public $error = false;
    public $messages = array();

	public $countryName = '';
	public $stateName = '';
	public $conversionsApplied = false;
	public $rangeToConverted;
	public $rangeFromConverted;

    public function __construct( $dbID = null )
    {
        global $emuShop;
        parent::__construct( $dbID, null, $emuShop->dbPrefix, 'shipping_charges' );
    }

    public function registerTemplateTags()
    {
        $this->templateTags = array(   	'country' => 'countryName',
										'state' => 'stateName',
										'range from' => 'rangeFrom',
										'range to' => 'rangeTo',
										'charge' => 'chargeFormatted',
										'description' => 'description',
										'charge id' => 'dbID' );
    }

    public function __get( $member )
    {
        global $emuShop;

        if( $member_value = parent::__get( $member ) ) return $member_value;

        switch( $member )
        {

            case 'displayCharge':
                return $this->chargeFormatted;

            case 'chargeFormatted':
                return $emuShop->currency.apply_number_format( 'currency', $this->charge );

            default:

                if( !isset( $this->data[ $member ] ) ) return null;
                return $this->data[ $member ];

        }
    }

	public function getData()
	{
		parent::getData();

		$this->populateRegionNames();
	}

	public function appliesTo( $price_value, $weight_value, $country_id, $state_id )
	{
		if( !$this->conversionsApplied ) $this->applyUnitConversions();

		// we check wether the charge applies on two counts:

		// 1 - whether the value fits the range and
		// 2 - whether the charge applies in the customer's region

		if( $this->basedOn == 'price' )
			$value_to_check = (float) $price_value;
		else if ( $this->basedOn == 'weight' )
			$value_to_check = (float) $weight_value;

		$range_applies = false;

		if( strlen( $this->rangeFrom ) > 0 && strlen( $this->rangeTo ) == 0 ) // i.e. > i
		{
			if( $value_to_check > $this->rangeFromConverted ) $range_applies = true;
		}
		else if ( strlen( $this->rangeTo ) > 0 && strlen( $this->rangeFrom ) == 0 ) // i.e. < i
		{
			if( $value_to_check <= $this->rangeToConverted ) $range_applies = true;
		}
		else if( strlen( $this->rangeFrom ) > 0 && strlen( $this->rangeTo ) > 0 )
		{
			if( $value_to_check > $this->rangeFromConverted && $value_to_check <= $this->rangeToConverted ) $range_applies = true;
		}


		if( !$range_applies ) return false; // no point in checking region if the range doesn't apply

		if( $this->countryID )
		{
			if( $this->countryID == $country_id )
			{
				$region_applies = true;

				// but does it match the state (if one is defined)?
				if( $this->stateID )
					$region_applies = $this->stateID == $state_id ? true : false;
			}
			else
				$region_applies = false;
		}
		else // a country isn't defined so the region is worldwide
			$region_applies = true;

		return $region_applies;

	}

	public function applyUnitConversions()
	{
		global $emuShop;

		$this->rangeToConverted = $emuShop->applyUnitConversion( $this->rangeTo );
		$this->rangeFromConverted = $emuShop->applyUnitConversion( $this->rangeFrom );
	}

	public function populateRegionNames()
	{
		global $emuShop;

		if( $country = $emuShop->regionManager->getCountries( array( 'ID' => $this->countryID ) ) )
			$this->countryName = $country[0]->name;

		if( $this->stateID )
		{
			if( $state = $emuShop->regionManager->getStates( array( 'ID' => $this->stateID ) ) )
				$this->stateName = $state[0]->name;
		}
		else
			$this->stateName = $this->state;
	}

}

// class emuShippingCharge extends emuShopCommon
// {
// 	public $data = array();

// 	public $countryName = '';
// 	public $stateName = '';
// 	public $conversionsApplied = false;
// 	public $rangeToConverted;
// 	public $rangeFromConverted;

// 	public static $staticTemplateTags = array( 	'country' => 'countryName',
// 												'state' => 'stateName',
// 												'range from' => 'rangeFrom',
// 												'range to' => 'rangeTo',
// 												'charge' => 'displayCharge',
// 												'description' => 'description',
// 												'charge id' => 'dbID' );

// 	public $templateTags;

// 	public function __construct( $chargedbID = null )
// 	{
// 		$this->templateTags = self::$staticTemplateTags;

// 		if( $chargedbID )
// 		{
// 			$this->dbID = $chargedbID;
// 			$this->getShippingCharge();
// 		}
// 	}

// 	public function getShippingCharge()
// 	{
// 		global $wpdb, $emuShop;

// 		if( !$this->dbID ) return;

// 		$this->data = $wpdb->get_row( "select * from {$emuShop->dbPrefix}shipping_charges where dbID = {$this->dbID}", ARRAY_A );

// 		if( $this->countryID )
// 		{
// 			if( $country = $emuShop->getManager('region')->getCountries( array( 'ID' => $this->countryID ) ) )
// 				$this->countryName = $country[0]->name;
// 		}

// 		if( $this->stateID )
// 		{
// 			if( $state = $emuShop->getManager('region')->getStates( array( 'ID' => $this->stateID ) ) )
// 				$this->stateName = $state[0]->name;
// 		}
// 		else
// 			$this->stateName = $this->state;
// 	}

// 	public function __get( $member )
// 	{
// 		global $wpdb, $emuShop;

// 		switch( $member )
// 		{
// 			case 'displayCharge':

// 				return $emuShop->currency.apply_number_format( 'currency', $this->charge );

// 			default:

// 				if( !isset( $this->data[ $member ] ) ) return null;

// 				return $this->data[ $member ];
// 		}
//     }

//     public function __set( $member, $value )
// 	{
//         $this->data[ $member ] = $value;
//     }

// 	public function appliesTo( $price_value, $weight_value, $country_id, $state_id )
// 	{
// 		if( !$this->conversionsApplied ) $this->applyUnitConversions();

// 		// we check wether the charge applies on two counts:

// 		// 1 - whether the value fits the range and
// 		// 2 - whether the charge applies in the customer's region

// 		if( $this->basedOn == 'price' )
// 			$value_to_check = (float) $price_value;
// 		else if ( $this->basedOn == 'weight' )
// 			$value_to_check = (float) $weight_value;

// 		$range_applies = false;

// 		if( strlen( $this->rangeFrom ) > 0 && strlen( $this->rangeTo ) == 0 ) // i.e. > i
// 		{
// 			if( $value_to_check > $this->rangeFromConverted ) $range_applies = true;
// 		}
// 		else if ( strlen( $this->rangeTo ) > 0 && strlen( $this->rangeFrom ) == 0 ) // i.e. < i
// 		{
// 			if( $value_to_check <= $this->rangeToConverted ) $range_applies = true;
// 		}
// 		else if( strlen( $this->rangeFrom ) > 0 && strlen( $this->rangeTo ) > 0 )
// 		{
// 			if( $value_to_check > $this->rangeFromConverted && $value_to_check <= $this->rangeToConverted ) $range_applies = true;
// 		}


// 		if( !$range_applies ) return false; // no point in checking region if the range doesn't apply

// 		if( $this->countryID )
// 		{
// 			if( $this->countryID == $country_id )
// 			{
// 				$region_applies = true;

// 				// but does it match the state (if one is defined)?
// 				if( $this->stateID )
// 					$region_applies = $this->stateID == $state_id ? true : false;
// 			}
// 			else
// 				$region_applies = false;
// 		}
// 		else // a country isn't defined so the region is worldwide
// 			$region_applies = true;

// 		return $region_applies;

// 	}

// 	public function applyUnitConversions()
// 	{
// 		global $emuShop;

// 		$this->rangeToConverted = $emuShop->applyUnitConversion( $this->rangeTo );
// 		$this->rangeFromConverted = $emuShop->applyUnitConversion( $this->rangeFrom );
// 	}

// 	public function update()
// 	{
// 		global $wpdb, $emuShop;

// 		if( $this->dbID )
// 			$this->updateRecord( "{$emuShop->dbPrefix}shipping_charges", $this->data, array( 'isActive' => '%b' ), array( 'dbID' => $this->dbID ) );
// 		else
// 			$this->dbID = $this->insertRecord( "{$emuShop->dbPrefix}shipping_charges", $this->data );

// 		return true;
// 	}
// }


?>
