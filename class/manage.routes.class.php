<?php

class emuM_Routes extends emuManager
{
    public function init()
    {
        // Check authentication for some of the pages e.g. they need to be logged in
        // and have basket items to be on the order confirmation page...
        add_action( 'template_redirect', array( $this, 'checkAuthentication' ), 2 );
        add_action( 'template_redirect', array( $this, 'applySecureRedirects' ), 1 );
    }

    public function applySecureRedirects()
    {
        global $emuShop;

        if( !$emuShop->SSLEnabled ) return;

        global $post;

        $shop_page = false;

        // Loop through all the shop-related pages
        foreach( $emuShop->pageManager->getPages() as $page )
        {
            // If the current page is a shop-related page...
            if( $page->pageID == @$post->ID )
            {
                $shop_page = true;

                // ... then if that page should be secure and we're NOT currently under a secure connection...
                if( $page->secure &! $emuShop->isSecure() )
                {
                    // ... then redirect to the secure protocol
                    header( 'Location: '. $emuShop->getSecureURL( $page ) );
                    exit();
                }
                // ... otherwise if we are on a secure connection but the current page isn't a secure page...
                else if( $emuShop->isSecure() &! $page->secure )
                {
                    // ... then redirect to the non-secure protocol
                    header( 'Location: '. $page->url );
                    exit();
                }
            }
        }

        // Lastly, if we are on a secure connection but not on a shop page then
        // swap over to the non-secure connection unless the page is in the non shop secure pages whitelist
        if( $emuShop->isSecure() &! $shop_page )
        {
            if( !in_array( rtrim($_SERVER['REQUEST_URI'], '/'), apply_filters('emu_shop_non_shop_secure_pages', array()) ) )
            {
                $home_url = get_home_url(null, null, 'http');
                $home_url = str_replace( 'https', 'http', $home_url );

                header( 'Location: '.$home_url.$_SERVER['REQUEST_URI'] );
                exit();
            }
        }
    }

    // core / page manager ?
    function checkAuthentication()
    {
        global $post, $id, $emuShop;

        $basket = $emuShop->getBasket();

        $pages = $emuShop->pageManager->getPages(); // save a bit of typing

        $location = false;
        $return = false;

        switch( @$post->ID )
        {
            case $pages->login->pageID:

                if ( is_user_logged_in() ) // then they don't need to go the login page
                    $location = $pages->shipping->url;

                break;

            case $pages->wishlist->pageID:

                if( !is_user_logged_in() )
                {
                    $location = $pages->login->url;
                    $return = true;
                }

                break;

            case $pages->shipping->pageID:

                if( $basket->isEmpty )
                    $location = $pages->basket->url;
                else
                {
                    if( !is_user_logged_in() && $emuShop->allowGuestCheckout == false )
                    {
                        $location = $pages->login->url;
                        $return = true;
                    }
                }
                break;

            case $pages->delivery->pageID:

                // If there is only 1 or no shipping charges we can send
                // them straight on to the confirmation page.

                if( !is_user_logged_in() && $emuShop->allowGuestCheckout == false )
                {
                    $location = $pages->login->url;
                    $return = true;
                }
                else
                {
                    if( count( $basket->getApplicableShippingCharges() ) <= 1 )
                        $location = $pages->confirmation->url;
                }

                break;

            case $pages->billing->pageID:

                if( $basket->isEmpty )
                    $pages->basket->url;
                else
                {
                    if( !is_user_logged_in() && $emuShop->allowGuestCheckout == false )
                    {
                        $location = $pages->login->url;
                        $return = true;
                    }
                }
                break;

            case $pages->payment->pageID:

                if ( !is_user_logged_in() && $emuShop->allowGuestCheckout == false )
                {
                    if( !$basket->isEmpty )
                    {
                        $location = $pages->login->url;
                        $return = true;
                    }
                    else
                        $location = $pages->basket->url;
                }
                else
                {
                    if( $basket->isEmpty )
                        $location = $pages->basket->url;
                }
                break;

            case $pages->confirmation->pageID:

                if( $basket->isEmpty )
                {
                    $location = $pages->basket->url;
                    header('Location: '.$pages->basket->url ); exit();
                }
                else
                {
                    if ( ! is_user_logged_in() && $emuShop->allowGuestCheckout == false )
                    {
                        $location = $pages->login->url;
                        $return = true;
                    }
                }

                break;
        }

        if( $location )
        {

            if( $return )
                $emuShop->addSessionData('login_return_url', $_SERVER['REQUEST_URI']);

            header( "Location: $location" );
            exit();
        }
    }

}


?>
