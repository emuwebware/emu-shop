<?php


class emuTaxRate extends emuDbEntity
{
    public $error = false;
    public $messages = array();

	public $countryName = '';
	public $stateName = '';

    public $description;

    public $calculatedValue;

    public function __construct( $dbID = null )
    {
        global $emuShop;
        parent::__construct( $dbID, null, $emuShop->dbPrefix, 'tax_rates' );
    }

    public function __get( $member )
    {
        global $emuShop;

        if( $member_value = parent::__get( $member ) ) return $member_value;

        switch( $member )
        {
            case 'calculatedValueFormatted':

                return $emuShop->currency.apply_number_format( 'currency', $this->getCalculatedValue() );

            default:

                if( !isset( $this->data[ $member ] ) ) return null;
                return $this->data[ $member ];

        }
    }

    public function calculateUsingValue( $value )
    {
        $this->calculatedValue = 0;

        if( is_numeric( $value ) )
            $this->calculatedValue = $this->getRateAsDecimal() * $value;

        return $this->calculatedValue;
    }

    public function getCalculatedValue()
    {
        return $this->calculatedValue;
    }

    public function hasState()
    {
        return $this->stateID ? true : false;
    }

    public function appliesTo( $country_name, $state_name )
    {
        if( $this->countryName == $country_name )
        {
            if( $this->hasState() )
            {
                return $this->stateName == $state_name;
            }
            else
                return true;
        }
        return false;
    }

	public function getData()
	{
		parent::getData();

		$this->populateRegionNames();
        $this->generateDescription();
	}

    public function getRateAsDecimal()
    {
        $decimal_rate = $this->rate;

        if( $decimal_rate > 1 )
            $decimal_rate = $decimal_rate / 100;

        return $decimal_rate;
    }

    public function updateRate($new_rate)
    {
        $this->rate = $new_rate;
        $this->generateDescription();
    }

    public function generateDescription()
    {
        $decimal_rate = $this->getRateAsDecimal();

        if( $this->hasState() )
            $this->description = "Tax - {$this->stateName} State ";
        else
            $this->description = 'Tax ';

        $this->description .= apply_number_format( 'percentage', ( $decimal_rate * 100 ) )."%";

        return $this->description;
    }

	public function populateRegionNames()
	{
		global $emuShop;

		if( $country = $emuShop->regionManager->getCountries( array( 'ID' => $this->countryID ) ) )
			$this->countryName = $country[0]->name;

		if( $this->stateID )
		{
			if( $state = $emuShop->regionManager->getStates( array( 'ID' => $this->stateID ) ) )
				$this->stateName = $state[0]->name;
		}
		else
			$this->stateName = $this->state;
	}



}



// class emuTaxRate extends emuShopCommon
// {
// 	public $data = array();

// 	public $templateTags;

// 	public function __construct( $tax_rate_id = null )
// 	{
// 		// $this->templateTags = self::$staticTemplateTags;

// 		if( $tax_rate_id )
// 		{
// 			$this->dbID = $tax_rate_id;
// 			$this->getTaxRate();
// 		}
// 	}

// 	public function populateRegionNames()
// 	{
// 		global $emuShop;

// 		if( $country = $emuShop->getManager('region')->getCountries( array( 'ID' => $this->countryID ) ) )
// 			$this->countryName = $country[0]->name;

// 		if( $this->stateID )
// 		{
// 			if( $state = $emuShop->getManager('region')->getStates( array( 'ID' => $this->stateID ) ) )
// 				$this->stateName = $state[0]->name;
// 		}
// 		else
// 			$this->stateName = $this->state;
// 	}

// 	public function getTaxRate()
// 	{
// 		global $wpdb, $emuShop;

// 		if( !$this->dbID ) return;

// 		$this->data = $wpdb->get_row( "select * from {$emuShop->dbPrefixShared}address_book where dbID = {$this->dbID}", ARRAY_A );

// 		$this->populateRegionNames();
// 	}

// 	public function getAddressAggregate( $separater = '<br />' )
// 	{
// 		$address = array();

// 		$address[] = $this->addressLineOne;

// 		if( $this->addressLineTwo ) $address[] = $this->addressLineTwo;

// 		$address[] = $this->city;
// 		$address[] = $this->stateName;
// 		$address[] = $this->postalCode;
// 		$address[] = $this->countryName;

// 		return implode( $separater, $address );
// 	}

// 	public function __get( $member )
// 	{
// 		global $wpdb, $emuShop;

// 		switch( $member )
// 		{
// 			case 'addressAggregateComma':

// 				return $this->getAddressAggregate(', ');

// 			case 'addressAggregate':

// 				return $this->getAddressAggregate();

// 			default:

// 				if( !isset( $this->data[ $member ] ) ) return null;

// 				return $this->data[ $member ];
// 		}
//     }

//     public function __set( $member, $value )
// 	{
//         $this->data[ $member ] = $value;
//     }

// 	public function clearDefaults( $type )
// 	{
// 		global $wpdb, $emuShop;

// 		switch( $type )
// 		{
// 			case 'shipping': $field = 'isDefaultShipping'; break;
// 			case 'billing': $field = 'isDefaultBilling'; break;
// 		}

// 		$wpdb->query( "update {$emuShop->dbPrefixShared}address_book set $field = false where customerID = {$this->customerID}" );
// 	}

// 	public function update()
// 	{
// 		global $wpdb, $emuShop;

// 		if( $this->isDefaultBilling )
// 			$this->clearDefaults( 'billing' );

// 		if( $this->isDefaultShipping )
// 			$this->clearDefaults( 'shipping' );

// 		if( $this->dbID )
// 			$this->updateRecord( "{$emuShop->dbPrefixShared}address_book", $this->data, array( 'isDefaultBilling' => '%b', 'isDefaultShipping' => '%b' ), array( 'dbID' => $this->dbID ) );
// 		else
// 			$this->dbID = $this->insertRecord( "{$emuShop->dbPrefixShared}address_book", $this->data );

// 		return true;
// 	}
// }

?>
