<?php

class emuH_Region extends emuHelper
{

    function getRegionOptions( $country_id = null, $state_id = null, $state = null )
    {
        if( $country_id )
        {
            // see if that country has any states
            $states = $this->emuApp->regionManager->getStates( array( 'country' => $country_id ) );

            if( count( $states ) > 0 )
            {
                // we have states
                $selected_state_class = '';
                $selected_state = $this->buildStateOptions( $states, $state_id );

                $entered_state_class = 'emu-hidden';
                $entered_state = '';
            }
            else
            {
                // country has no states
                $selected_state_class = 'emu-hidden';
                $selected_state = '';

                $entered_state_class = '';
                $entered_state = $state;
            }
        }
        else
        {
            // we don't have a country selection yet so assume we don't have any states (true for most cases)

            $selected_state_class = 'emu-hidden';
            $selected_state = '';

            $entered_state_class = '';
            $entered_state = $state;

        }

        return (object) array( 'countryDropDown' => $this->emuApp->regionManager->getCountryOptions( $country_id ), 'stateDropDownClass' => $selected_state_class, 'stateDropDown' => $selected_state, 'stateTextInputClass' => $entered_state_class, 'stateTextInput' => $entered_state );
    }

    function buildStateOptions( $states, $state_id )
    {
        $state_dd = '';

        foreach( $states as $state )
        {
            $selected = $state->ID == $state_id ? ' selected="selected"' : '';

            $state_dd .= '<option value="'.$state->ID.'"'.$selected.'>'.$state->name.'</option>';
        }

        return $state_dd;
    }

}