<?php

class emuOrderConstants extends emuConstants
{
    public function addConstants()
    {
        $totals = $this->createConstantGroup( 'ORDER_', 'Order Constants' );

        $totals->addConstant( 'SUB_TOTAL', 'Sub total' );
        $totals->addConstant( 'SUB_TOTAL_FORMATTED', 'Sub total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'ITEMS_TOTAL', 'Item(s) total' );
        $totals->addConstant( 'ITEMS_TOTAL_FORMATTED', 'Item(s) total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'DISCOUNT_TOTAL', 'Discount total' );
        $totals->addConstant( 'DISCOUNT_TOTAL_FORMATTED', 'Discount total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'CHARGES_TOTAL', 'Charges total' );
        $totals->addConstant( 'CHARGES_TOTAL_FORMATTED', 'Charges total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'PAYMENTS_TOTAL', 'Payments total' );
        $totals->addConstant( 'PAYMENTS_TOTAL_FORMATTED', 'Payments total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'AMOUNT_DUE', 'Amount due (grand total - payments)' );
        $totals->addConstant( 'AMOUNT_DUE_FORMATTED', 'Amount due formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'GRAND_TOTAL', 'Grand total' );
        $totals->addConstant( 'GRAND_TOTAL_FORMATTED', 'Grand total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $status = $this->createConstantGroup( 'ORDER_STATUS_', 'Order Status Constants' );

        $status->addConstant( 'RECEIVED', 'Order received', 1 );
        $status->addConstant( 'PROCESSING', 'Order being processed', 2 );
        $status->addConstant( 'SHIPPED', 'Order has shipped', 3 );

        $status = $this->createConstantGroup( 'ORDER_DATE_', 'Order Date Constants' );

        $status->addConstant( 'RECEIVED', 'Date received' );
        $status->addConstant( 'RECEIVED_FORMATTED', 'Order received' );

        $totals = $this->createConstantGroup( 'ORDER_ITEM_', 'Order Item Constants' );

        $totals->addConstant( 'TOTAL', 'Total' );
        $totals->addConstant( 'TOTAL_FORMATTED', 'Total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'PRICE', 'Price' );
        $totals->addConstant( 'PRICE_FORMATTED', 'Price formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');


    }

}


