<?php

class emuM_Products extends emuManager
{
    public function init()
    {
        // add the various meta boxes (products page mainly)
        add_action( 'admin_menu', array( $this, 'registerMetaBoxes' ) );

        // add_action( 'save_post', array( $this, 'wpSaveProductDetails' ) );
        add_action( 'delete_post', array( $this, 'wpRemoveProduct' ) , 10, 2);
        add_action( 'save_post', array( $this, 'wpSaveProduct' ) );
        // Add open graph product data to each product page
        add_action( 'wp_head', array( $this, 'addOGMetaData' ) );

        // Add custom columns
        add_filter( 'manage_edit-product_columns', array( $this, 'addCustomColumns' ) );
        add_action( 'manage_pages_custom_column', array( $this, 'manageCustomColumns' ), 10, 2 );

        // Filter for view products page
        add_action( 'restrict_manage_posts', array( $this, 'addPCatRestriction' ) );
        add_filter( 'parse_query', array( $this, 'prepareCustomFilters' ) );

        $this->registerAJAXMethod( 'get-product-details', array( $this, 'AJAX_getProductDetails') );
        $this->registerAJAXMethod( 'save-product-details', array( $this, 'AJAX_saveProductDetails') );
        $this->registerAJAXMethod( 'delete-variant', array( $this, 'AJAX_deleteVariant') );
        $this->registerAJAXMethod( 'delete-variant-group', array( $this, 'AJAX_deleteVariantGroup') );
        $this->registerAJAXMethod( 'create-variant-group', array( $this, 'AJAX_createVariantGroup') );
        $this->registerAJAXMethod( 'update-group-name', array( $this, 'AJAX_updateGroupName') );

    }

    public function AJAX_updateGroupName()
    {
        extract( $_POST );

        $variant_group = $this->emuApp->getInstance( 'emuVariantGroup', $group_id );
        $variant_group->groupName = $group_name;
        $variant_group->update();
    }

    public function AJAX_createVariantGroup()
    {
        extract( $_POST );

        $product = $this->emuApp->getInstance( 'emuProduct', array(null, $post_id) );

        if( $product->isNewRecord() )
            $product->update();

        $variant_group = $this->emuApp->getInstance( 'emuVariantGroup' );
        $variant_group->groupName = $group_name;
        $variant_group->productID = $product->getID();
        $variant_group->update();

        echo $variant_group->getID();
    }

    public function AJAX_deleteVariantGroup()
    {
        extract( $_POST );

        $variant_group = $this->emuApp->getInstance( 'emuVariantGroup', $group_id );
        $variant_group->delete();

        echo '1';
    }

    public function AJAX_deleteVariant()
    {
        extract( $_POST );

        $product = $this->emuApp->getInstance( 'emuProduct', $variant_id );
        $product->delete();

        echo '1';
    }

    public function AJAX_getProductDetails()
    {
        extract( $_POST );

        $product = $this->emuApp->getInstance( 'emuProduct', array( $variant_id, $post_id ) );

        header('Content-type: application/json');

        echo json_encode( $product->basicDetails() );
    }

    public function AJAX_saveProductDetails()
    {
        extract( $_POST );

        // save-product-details action used for saving both
        // products and product variants.

        // in either case get the parent product (via the post_id)
        $product = $this->emuApp->getInstance( 'emuProduct', array(null, $post_id) );

        if( $product->isNewRecord() )
            $product->save(); // so that we can get the product id

        if( $groupId ) // then we're saving a variant
        {
            $variant_parent_id = $product->getID();

            if( $variantId )
                $product = $this->emuApp->getInstance( 'emuProduct', array( $variantId, $post_id ) );
            else
            {
                $product = $this->emuApp->getInstance( 'emuProduct' );
                $product->isVariant = true;
                $product->setPost($post_id);
                $product->setParent( $variant_parent_id );
                $product->setVariantGroup( $groupId );
            }
            $product->variantDescription = $name;
        }

        // Now save the rest of the product(or variant) details
        // clear them first to save faffing...
        $product->clearCustomVars();

        // Are there any custom vars?
        if( count( $varName ) > 1 )
        {
            // add the custom vars (ignore first set which is the dom template)
            for( $i = 1; $i < count( $varName ); $i++ )
                $product->addCustomVar( $varName[$i], $varValue[$i] );
        }

        $product->SKU = $sku;
        $product->price = $price;
        $product->salePrice = $salePrice;

        $product->shippingOption = $shippingOption;
        $product->handlingOption = $handlingOption;

        if( $product->shippingOption == emuM_Shipping::OPTION_SPECIFIC_CHARGE )
            $product->shippingCost = $shipping;
        else
            $product->shippingCost = null;

        if( $product->handlingOption == emuM_Handling::OPTION_SPECIFIC_CHARGE )
            $product->handlingCost = $handling;
        else
            $product->handlingCost = null;

        $product->weight = $weight;
        $product->qtyInStock = $qtyInStock;
        $product->isActive = post_val('isActive') ? 1 : 0;
        $product->hasTax = post_val('hasTax') ? 1 : 0;

        $product->save();

        header('Content-type: application/json');

        echo json_encode( $product->basicDetails() );
    }

    public function registerAdminPages()
    {

        // $this->emuApp->registerAdminPage( array( 'name' => 'Bulk Update',
        //                          'filename' => 'bulk-update.php',
        //                          'position' => 9,
        //                          'styles' => array( 'emu-common' ),
        //                          'scripts' => array() ) );
    }

    public function loadScripts()
    {
        global $emuShop;
        global $wp_version;

        if( version_compare( $wp_version, '3.3', '<' ) )
        {
            $emuShop->registerScript( 'emu-product-admin',
                                    plugins_url( "/{$emuShop->pluginName}/js/product-admin.js" ),
                                    array( 'jquery-ui-dialog', 'emu-jquery-accordion', 'jquery-ui-sortable' ),
                                    'is_admin()',
                                    'product' );
        }
        else
        {
            $emuShop->registerScript( 'emu-product-admin',
                                    plugins_url( "/{$emuShop->pluginName}/js/product-admin.js" ),
                                    array( 'jquery-ui-dialog', 'jquery-ui-accordion', 'jquery-ui-sortable' ),
                                    'is_admin()',
                                    'product' );

        }
    }

    public function loadStyles()
    {
        global $emuShop;

        $emuShop->loadStyle( 'emu-product-admin',
                                plugins_url( "/{$emuShop->pluginName}/style/product-admin.css" ),
                                array( 'emu-jquery-ui' ),
                                'is_admin()',
                                'product' );
    }

    public function addShortCodes()
    {
        global $emuShop;

        $emuShop->addShortCode( 'emu-product', array( $this, 'getProductDetail' ) );

        //// Depracated - variants can now be accessed via emu-product shortcode
        $emuShop->addShortCode( 'emu-variant', array( $this, 'getProductDetail' ) );
    }

    function getProductDetail( $options )
    {
        extract( shortcode_atts( array(
            'id' => '',
            'tag' => ''
        ), $options ) );

        if( !( $id && $tag ) ) return " 'id' and 'tag' required for emu-product shortcode ";

        global $emuShop;

        $templateManager = $emuShop->getManager('theme');

        $product = $emuShop->getInstance( 'emuProduct', $id );

        $tags = $product->getTemplateTags();

        if( isset( $tags[ $tag ] ) )
            return $templateManager->getTagValue( $product, $tags[ $tag ] );

        return " emu-product: tag '$tag' not found ";
    }

    function addPCatRestriction()
    {
        global $typenow;
        global $wp_query;
        global $wp_version;

        if ( $typenow == 'product' )
        {
            $taxonomy = 'pcat';

            $product_taxonomy = get_taxonomy( $taxonomy );

            $selected = $wp_version >= '3.0' ? @$wp_query->query['pcat'] : @$wp_query->query['term'];

            wp_dropdown_categories(array(
                'show_option_all' =>  __("Show All {$product_taxonomy->label}"),
                'taxonomy'        =>  $taxonomy,
                'name'            =>  'pcat',
                'orderby'         =>  'name',
                'selected'        =>  $selected,
                'hierarchical'    =>  true,
                'depth'           =>  3,
                'show_count'      =>  true,
                'hide_empty'      =>  true,
            ));
        }

    }

    function prepareCustomFilters( $query )
    {
        global $pagenow, $wp_version;

        $qv = &$query->query_vars;

        if( $wp_version >= '3.0' )
        {
            if ( $pagenow == 'edit.php' && isset( $qv['pcat'] ) && is_numeric( $qv['pcat'] ) )
            {
                if( $qv['pcat'] > 0 )
                {
                    $term = get_term_by( 'id', $qv['pcat'], 'pcat' );

                    $qv['pcat'] = $term->slug;
                }
            }
        }
        else
        {
            if ( $pagenow == 'edit.php' && isset( $qv['taxonomy'] ) && $qv['taxonomy'] == 'pcat' && isset( $qv['term'] ) && is_numeric( $qv['term'] ) )
            {
                $term = get_term_by( 'id', $qv['term'], 'pcat' );

                $qv['term'] = $term->slug;
            }
        }
    }



    function manageCustomColumns( $column_name, $id )
    {
        global $wpdb, $emuShop;

        if( in_array( $column_name, array( 'e-p-image','e-p-menu-order', 'e-p-price', 'e-p-category', 'e-p-rating', 'e-p-stock', 'e-p-id' ) ) )
        {
            if( !isset( $GLOBALS["product-$id"] ) )
            {
                $product = $emuShop->getInstance( 'emuProduct', array( null, $id ) );
                $GLOBALS["product-$id"] = $product;
            }
            else
            {
                $product = $GLOBALS["product-$id"];
            }
        }

        switch ( $column_name )
        {
            case 'e-p-image':

                echo get_the_post_thumbnail( $id, array( 60, 80 ), array( 'class' => '' ) );
                break;

            case 'e-p-menu-order':
//d($product->getPostData($id));
                echo $product->post->menu_order;
                break;

            case 'e-p-id':

                echo $product->getID();
                break;

            case 'e-p-price':

                echo $product->getPrice();
                break;

            case 'e-p-category':

                echo $product->categories;
                break;

            case 'e-p-rating':

                if( !$rating = $product->getRatingStars( 'large' ) )
                    $rating = 'No customer ratings';
                // else
                //  $rating .= '<br />'.preg_replace( '/[\(\)]/', '', $product->ratingSummary );

                echo $rating;
                break;

            case 'e-p-stock':

                if( $product->isStockTracked )
                {

                    if( is_null( $product->qtyInStock ) )
                    {
                        $stock = 'Tracking but no stock qty set.';
                    }
                    else
                    {
                        if( (int) $product->qtyInStock <= 0 )
                            $stock = '<span style="color: red;">'.$product->qtyInStock.'</span>';
                        else
                            $stock = $product->qtyInStock;
                    }
                }
                else $stock = 'Not tracking';

                echo $stock;

                break;

        }

        if( isset( $product ) ) unset( $product );

    }

    function addCustomColumns( $columns )
    {
        $columns = array();

        $columns['cb'] = '<input type="checkbox" />';
        $columns['e-p-image'] = 'Image';
        $columns['e-p-menu-order'] = 'Order';

        $columns['e-p-id'] = 'ID';
        $columns['title'] = _x('Product Name', 'column name');
        $columns['e-p-category'] = __('Categories');
        $columns['e-p-price'] = 'Price';
        $columns['e-p-rating'] = 'Customer Rating';
        $columns['e-p-stock'] = 'In Stock';

        return $columns;
    }


    public function getStars( $size, $no_stars, $total_stars = 5 )
    {
        global $emuShop;

        $image = array( "large" => (object) array( "height" => "20", "width" => "21" ),
                        "small" => (object) array( "height" => "15", "width" => "16" ) );

        $star = '<img src="'.$emuShop->pluginURL.'/image/star_'.$size.'_%s.gif" height="'.$image[$size]->height.'" width="'.$image[$size]->width.'" alt="%1$s star" />';

        // Get the full stars
        $stars = str_repeat( sprintf( $star, 'full' ), floor( $no_stars ) ); // e.g. 4.3 = 4 stars

        if( floor( $no_stars ) == $no_stars )
        {
            $empty_star_count = $total_stars - $no_stars;
        }
        else
        {
            // add the partial star
            $stars .= sprintf( $star, 'half' );

            $empty_star_count = $total_stars - ( floor( $no_stars ) + 1 );

        }

        $stars .= str_repeat( sprintf( $star, 'empty' ), $empty_star_count );

        return $stars;
    }




    public function registerMetaBoxes()
    {
        add_meta_box( 'emu-product-details', 'Product Details', array( $this, 'showProductDetails' ), 'product', 'normal', 'high' );
        add_meta_box( 'emu-product-variants', 'Product Variants', array( $this, 'showProductVariants' ), 'product', 'normal', 'high' );
        add_meta_box( 'emu-product-stockcontrol', 'Stock Control', array( $this, 'wpGetStockDetails' ), 'product', 'normal', 'high' );
    }


    function wpRemoveProduct( $post_id )
    {
        global $emuShop;

        $post = get_post( $post_id );

        if( $post->post_type !== 'product' ) return $post_id;

        $product = $emuShop->getInstance( 'emuProduct', array(null, $post_id) );
        $product->deleteRecord();
    }

    function wpSaveProduct( $post_id )
    {
        global $emuShop;

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;

        if( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) return $post_id;

        if( !post_val('emu-product-form') ) return $post_id;

        $post = get_post( $post_id );

        if( $post->post_type !== 'product' ) return $post_id;

        $product = $emuShop->getInstance( 'emuProduct', array(null, $post) );

        if( $product->isNewRecord() ) $product->saveRecord();
    }


    function wpGetStockDetails( $post )
    {
        $product = $this->emuApp->getInstance( 'emuProduct', array(null, $post) );
        $this->emuApp->productAdminHelper->showStockDetails($product);
    }

    public function showProductDetails( $post )
    {
        $product = $this->emuApp->getInstance( 'emuProduct', array(null, $post) );
        $this->emuApp->productAdminHelper->showProductDetails($product);
    }

    public function showProductVariants( $post )
    {
        $product = $this->emuApp->getInstance( 'emuProduct', array(null, $post) );
        $variant_groups = $product->getVariantGroups( $include_inactive_variants = true );
        $this->emuApp->productAdminHelper->showProductVariants($variant_groups);
    }

    function getProducts( $terms = array() )
    {
        global $emuShop;

        $terms = array_merge( array( 'post_type' => 'product' ), $terms );

        $product_posts = get_posts( $terms );

        $products = array();

        if( count( $product_posts ) == 0 ) return $products;

        foreach( $product_posts as $post )
        {
            setup_postdata( $post );

            $products[] = $emuShop->getInstance('emuProduct', array(null, $post) );
        }

        return $products;

    }

    function getStockTrackingOptions()
    {
        return array( '1' => 'Yes', '0' => 'No', '' => 'Inherit from global options' );
    }

    function getNoStockLeftOptions()
    {
        return array( 'deny' => 'Deny purchases', 'allow' => 'Allow purchases' );
    }

    public function addOGMetaData()
    {
        if( ! ( is_single() && get_post_type() == 'product' ) ) return;

        global $post;

        $url = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );

        $og_meta = '<meta xmlns:og="http://opengraphprotocol.org/schema/" property="og:title" content="'.get_the_title( $post->ID ).'" />';
        $og_meta = '<meta xmlns:og="http://opengraphprotocol.org/schema/" property="og:type" content="product" />';
        $og_meta = '<meta xmlns:og="http://opengraphprotocol.org/schema/" property="og:image" content="'.$url.'" />'."\n";
        $og_meta = '<meta xmlns:og="http://opengraphprotocol.org/schema/" property="og:url" content="'.get_permalink( $post->ID ).'" />';
        $og_meta = '<meta xmlns:og="http://opengraphprotocol.org/schema/" property="fb:admins" content="seattleadmin" />';

        echo apply_filters( 'emu_shop_og_meta', $og_meta );
    }


    public function registerClasses()
    {
        $this->emuApp->registerClass( 'emuProduct', 'model.product.class.php' );
        $this->emuApp->registerClass( 'emuBasketItem', 'model.basketitem.class.php', array('emuProduct') );
        $this->emuApp->registerClass( 'emuVariantGroup', 'model.variantgroup.class.php' );
    }

    public function registerCustomPostTypes()
    {
        global $emuShop;

        $labels = array(
            'name' => 'Products',
            'singular_name' => 'Product',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Product',
            'edit_item' => 'Edit Product',
            'new_item' => 'New Product',
            'view_item' => 'View Product',
            'search_items' => 'Search Products',
            'not_found' => 'No products found',
            'not_found_in_trash' => 'No products found in Trash',
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'product'),
            'capability_type' => 'post',
            'hierarchical' => true,
            'menu_position' => 4,
            'supports' => array( 'title', 'excerpt', 'editor', 'author', 'custom-fields', 'revisions', 'thumbnail', 'page-attributes' ),
        );

        register_post_type( 'product', $args );

        register_taxonomy( 'pcat', 'product' , array( 'label' => 'Product Categories', 'show_ui' => true, 'hierarchical' => true, 'rewrite' => array( 'slug' => 'product-category' ), 'query_var' => true ) );
        register_taxonomy( 'ptag', 'product' , array( 'label' => 'Product Tags', 'show_ui' => true, 'hierarchical' => false, 'rewrite' => array( 'slug' => 'product-tag' ), 'query_var' => true ) );
    }

    public function install( $emuShop = null )
    {
        if( !$emuShop ) return;

        global $wpdb;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        // products and variants
        $sql = "CREATE TABLE {$emuShop->dbPrefix}products (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        postID int(10) NULL,
        userID int(10) NULL,
        shippingOption int(10) NULL,
        handlingOption int(10) NULL,
        shippingCost float(2) default NULL,
        handlingCost float(2) default NULL,
        price float(2) default NULL,
        salePrice float(2) default NULL,
        weight varchar(10) default NULL,
        isActive BOOLEAN NOT NULL DEFAULT 1,
        hasTax BOOLEAN NOT NULL DEFAULT 1,
        parentID int(10) NULL,
        variantGroupID varchar(300) default NULL,
        SKU varchar(300) default NULL,
        variantDescription varchar(300) default NULL,
        customVars text default NULL,
        qtyInStock int(10) NULL,
        trackStock BOOLEAN NULL,
        isVariant BOOLEAN DEFAULT 0,
        displayPosition int(10) NULL,
        noStockLeftOption varchar(20) NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

        // reviews
        $sql = "CREATE TABLE {$emuShop->dbPrefix}reviews (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        postID int(10) NULL,
        productID int(10) NULL,
        wpUserID int(10) NULL,
        rating float(1) default NULL,
        helpfulNoCount int(10) NOT NULL DEFAULT 0,
        helpfulYesCount int(10) NOT NULL DEFAULT 0,
        reviewDate DATETIME default NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

        // variant groups
        $sql = "CREATE TABLE {$emuShop->dbPrefix}variant_groups (
        dbID int(10) NOT NULL AUTO_INCREMENT,
        productID int(10) NULL,
        groupName varchar(300) default NULL,
        UNIQUE KEY id (dbID)
        );";

        dbDelta($sql);

    }

}

?>
