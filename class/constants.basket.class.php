<?php

class emuBasketConstants extends emuConstants
{
    public function addConstants()
    {
        $totals = $this->createConstantGroup( 'BASKET_', 'Constants to retrieve basket details' );

        $totals->addConstant( 'ITEM_SUB_TOTAL', 'Items sub total' );
        $totals->addConstant( 'ITEM_SUB_TOTAL_FORMATTED', 'Items sub total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'ITEM_TAX_TOTAL', 'Items sub total (for taxes)' );
        $totals->addConstant( 'ITEM_TAX_TOTAL_FORMATTED', 'Items sub total (for taxes) formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'SUB_TOTAL', 'Basket sub total (items sub total - discounts)' );
        $totals->addConstant( 'SUB_TOTAL_FORMATTED', 'Basket sub total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'SHIPPING_TOTAL', 'Shipping total' );
        $totals->addConstant( 'SHIPPING_TOTAL_FORMATTED', 'Shipping total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'HANDLING_TOTAL', 'Handling total' );
        $totals->addConstant( 'HANDLING_TOTAL_FORMATTED', 'Handling total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'DISCOUNT_TOTAL', 'Discount total' );
        $totals->addConstant( 'DISCOUNT_TOTAL_FORMATTED', 'Discount total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'SPECIAL_CHARGES_TOTAL', 'Special charges total' );
        $totals->addConstant( 'SPECIAL_CHARGES_TOTAL_FORMATTED', 'Special charges total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'SPECIAL_DISCOUNTS_TOTAL', 'Special discounts total' );
        $totals->addConstant( 'SPECIAL_DISCOUNTS_TOTAL_FORMATTED', 'Special discounts total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'TAX_TOTAL', 'Tax total' );
        $totals->addConstant( 'TAX_TOTAL_FORMATTED', 'Tax total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $totals->addConstant( 'GRAND_TOTAL', 'Grand total' );
        $totals->addConstant( 'GRAND_TOTAL_FORMATTED', 'Grand total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $basket_item = $this->createConstantGroup( 'BASKET_ITEM_', 'Basket Item Constants' );

        $basket_item->addConstant( 'TOTAL', 'Total' );
        $basket_item->addConstant( 'TOTAL_FORMATTED', 'Total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $basket_item->addConstant( 'SHIPPING_COST_TOTAL', 'Shipping Total' );
        $basket_item->addConstant( 'SHIPPING_COST_TOTAL_FORMATTED', 'Shipping Total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $basket_item->addConstant( 'HANDLING_COST_TOTAL', 'Handling Total' );
        $basket_item->addConstant( 'HANDLING_COST_TOTAL_FORMATTED', 'Handling Total formatted with currency symbol and rounded to 2 decimal places e.g. $23.00');

        $basket_item->addConstant( 'WEIGHT_TOTAL', 'Weight Total' );
        $basket_item->addConstant( 'WEIGHT_TOTAL_FORMATTED', 'Weight Total formatted' );

        $basket_item = $this->createConstantGroup( 'SPECIAL_', 'Special Basket Item Constants' );

        $basket_item->addConstant( 'DISCOUNT', 'Special Discount cost' );
        $basket_item->addConstant( 'DISCOUNT_FORMATTED', 'Special Discount cost formatted with currency symbol and rounded to 2 decimal places e.g. $23.00' );
        $basket_item->addConstant( 'CHARGE', 'Special Charge cost' );
        $basket_item->addConstant( 'CHARGE_FORMATTED', 'Special Charge cost formatted with currency symbol and rounded to 2 decimal places e.g. $23.00' );

    }

}


