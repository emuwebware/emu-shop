<?php


class emuPageNotFoundDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = '404.htm';
        parent::__construct();

	}

	public function build()
	{
		global $emuShop;

		$customer = $emuShop->getManager('customer')->getCustomer();

		$template = $this->template;

		if( isset( $_SESSION['404'] ) )
		{
			$this->content = $this->templateManager->setTemplateConditionals( $template, array( 'find form' => false ) );
			$this->content = $this->templateManager->fillTemplate( $this->content, array( 'messages' => '<p>Your request for help has been submitted. One of our customer care representatives will be in contact shortly.</p>' ) );
			return;
		}
		else
		{
			$template = $this->templateManager->setTemplateConditionals( $template, array( 'find form' => true ) );
		}

		if( post_val('e-action') == '404' )
		{
			// then load address values from post array
			extract( $_POST );

			$customer->firstName = $first_name;
			$customer->lastName = $last_name;
			$customer->email = $email;
		}
		else
		{
			$comment = 'Couldn\'t find page... http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		}

		$template = $this->templateManager->fillTemplate( $template, $customer->getTemplateTags(), $customer );

		$tags = array(	'messages' => $emuShop->getMessages( '404' ), 'comment' => $comment );

		$template = $this->templateManager->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_page_not_found_content', $template );

	}

}

?>
