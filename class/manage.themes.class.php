<?php

class emuM_ShopThemes extends emuM_Templates
{
    public $themes = array();
	public $themeDir;

    public $currentTheme;

    public function init()
    {
		$this->themeDir = $this->emuApp->pluginPath.'/theme';

        $this->emuApp->registerClass( 'emuShopTheme', 'model.shoptheme.class.php' );

        add_action( 'shop_settings_loaded', array( $this, 'initThemes' ) );
        add_action( 'init', array( $this, 'loadThemeStyles' ) );
        add_action( 'init', array( $this, 'loadThemeScripts' ) );

        $this->registerProcessorFunction('activatetheme', array( $this, 'activateTheme') );
    }

    public function registerClasses()
    {
    }

    public function activatetheme()
    {
        if( !$theme = request_val('theme') ) return;

        $coreSettings = $this->emuApp->settings->getSettingsGroup( $this->emuApp->emuAppID.'_core' );

        $setting = $coreSettings->getSetting( 'shopTheme' );
        $setting->value = $theme;

        $this->emuApp->settings->saveSettings();
        $this->emuApp->shopTheme = $theme;

        $this->currentTheme = $this->themes[$theme];
    }

	function initThemes( $emuShop )
	{
		$this->themes = $this->getThemes( $emuShop );

		$coreSettings = $emuShop->settings->getSettingsGroup( $emuShop->emuAppID.'_core' );

		$arr_themes = array( '' => 'Select a theme' );

		foreach( $this->themes as $theme_name => $theme )
			$arr_themes[ $theme_name ] = $theme_name;

		$setting = $coreSettings->getSetting( 'shopTheme' );

		$setting->type = 'option';
		$setting->options = $arr_themes;

		$emuShop->settings->saveSettings();
	}

    function loadThemeStyles()
    {
        if( !$theme = $this->getCurrentTheme() ) return;

        wp_enqueue_style( 'emu-shop-theme', $theme->getStyleSheetURL(), false );
    }

    function loadThemeScripts()
    {
        if( !$theme = $this->getCurrentTheme() ) return;

        foreach( $theme->getScriptURLs() as $filename => $script_url )
            wp_enqueue_script( $filename, $script_url, array('jquery') );
    }

	function getThemes( $emuShop )
	{
		$theme_files = $emuShop->getFiles( $this->themeDir, 'info' );

		$themes = array();

		if ( is_array( $theme_files ) ) {

			foreach ( $theme_files as $theme_file ) {

				$basename = str_replace( $this->themeDir.'/', '', $theme_file );

				if( file_exists( $theme_file ) )
				{
					$file_data = implode( '', file( $theme_file ));

					$name = '';
					$description = '';
                    $parent_theme = '';

					if ( preg_match( '|Emu Shop Theme:(.*)$|mi', $file_data, $name ) )
						$name = _cleanup_header_comment($name[1]);

					if ( preg_match( '|Emu Shop Theme Description:(.*)$|mi', $file_data, $description ) )
						$description = _cleanup_header_comment($description[1]);

					if ( preg_match( '|Emu Shop Theme Author:(.*)$|mi', $file_data, $author ) )
						$author = _cleanup_header_comment($author[1]);

                    if ( preg_match( '|Emu Shop Theme Version:(.*)$|mi', $file_data, $version ) )
                        $version = _cleanup_header_comment($version[1]);

					if ( preg_match( '|Emu Shop Theme Parent:(.*)$|mi', $file_data, $parent_theme ) )
						$parent_theme = _cleanup_header_comment($parent_theme[1]);

					if ( !empty( $name ) )
					{
                        $theme_dir = str_replace( $emuShop->pluginPath.'/theme/', '', dirname( $theme_file ) ) ;
                        $theme_url = $emuShop->pluginURL.'/theme/'.$theme_dir;

                        $theme = $emuShop->getInstance('emuShopTheme');

                        $theme->name = $name;
                        $theme->description = $description;
                        $theme->author = $author;
                        $theme->version = $version;
                        $theme->url = $theme_url;
                        $theme->path = dirname( $theme_file );
                        $theme->scriptPath = dirname( $theme_file ).'/theme.js';
                        $theme->libDir = dirname( $theme_file ).'/lib';
                        $theme->parent = $parent_theme;
                        $theme->stylesheetPath = dirname( $theme_file ).'/theme.css';
                        $theme->stylesheetURL = $theme_url.'/theme.css';
                        $theme->scriptURL = $theme_url.'/theme.js';
                        $theme->libURL = $theme_url.'/lib';
                        $theme->imageURL = $theme_url.'/images';
                        $theme->basename = $basename;

                        $themes[$name] = $theme;
					}
				}
			}
		}

		return $themes;

	}

	public function registerAdminPages()
	{
		global $emuShop;

        $emuShop->registerAdminPage( array( 'name' => 'Themes', 'filename' => 'themes.php', 'position' => 8 ) );
    }

    public function getTheme($name)
    {
        if( !isset( $this->themes[ $name ] ) )
        {
            return false;
        }
        return $this->themes[ $name ];
    }

    public function getCurrentTheme()
    {
        global $emuShop;

        if( !$this->currentTheme )
        {
            if( !( $emuShop->shopTheme ) ) return false;

            if( !$theme = $this->getTheme( $emuShop->shopTheme ) )
            {
                trigger_error( "Couldn't find theme '{$emuShop->shopTheme}' - check Emu Shop settings", E_USER_WARNING );
                return false;
            }

            $this->currentTheme = $theme;
        }

        return $this->currentTheme;
    }

	public function getTemplate( $filename, $theme = null )
	{
        if( !$theme )
            $theme = $this->getCurrentTheme();

        if( !$theme ) return '';

        return $theme->getTemplate( $filename );
    }


	// from emuM_Templates
    /////////////////////////////////////////////////
    public function processAjax() {}

    public function getTemplateFiles(){}

    public function updateTemplate( $template, $terms = array() ){}

    public function refreshTemplates(){}

	public function populateTemplates(){}

    public function clearTemplates(){}

	public function getTemplateRecord( $filename ){}

    public function getTemplates( $terms = array() ){}

	public function loadTemplateData(){}

    public function install( $emuApp = null ){}

}

?>
