<?php


class emuOrderDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'order.htm';
        parent::__construct();

	}

	public function build()
	{
		global $wpdb;
		global $emuShop;

		if( !is_user_logged_in() )
		{
			$this->content = 'You must be logged in to view this page';
			return;
		}

		$order_ref = request_val('reference');

		$customer = $emuShop->getCustomer();

		$sql = "select orderStatus, orderDetails, orderDate, orderRefNumber from {$emuShop->dbPrefix}orders where ";

		if( !current_user_can( 'manage_options' ) )
			$sql .= "customerID = {$customer->dbID} and ";

		$sql .= "orderRefNumber = %s order by orderDate desc";

		$order_row = $wpdb->get_row( $wpdb->prepare( $sql, $order_ref ) );

		if( !$order_row )
		{
			$this->content = 'Order not found!';
			return;
		}

		$order_template = $this->template;

		$order = unserialize( base64_decode( $order_row->orderDetails ) );

		$order_template = $this->templateManager->fillTemplate( $order_template, array( 'order reference number' => $order_ref, 'order summary' => $order->orderSummary ) );

		$this->content = apply_filters( 'emu_order_content', $order_template );
	}
}

?>
