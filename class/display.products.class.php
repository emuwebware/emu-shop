<?php


class emuProductsDisplay extends emuDisplay
{
	public $terms = array();

	public $products;

    public function __construct()
	{
        $this->templateFile = 'products.htm';
        parent::__construct();


		if( $category = get_query_var('pcat') )
			$this->setCategory( 'pcat', $category );

		if( $category = get_query_var('ptag') )
			$this->setCategory( 'ptag', $category );

		if( $search = get_query_var('s') )
			$this->setSearchTerm( $search );

		if( $page = get_query_var('paged') )
			$this->setPagingTerm( $page );
	}

	public function setCategory( $taxonomy, $category )
	{
		$this->terms[$taxonomy] = $category;
	}

	public function setPagingTerm( $page )
	{
		$this->terms['paged'] = $page;
	}

	public function setNumberProducts( $number )
	{
		$this->terms['showposts'] = $number;
	}

	public function setSearchTerm( $search_term )
	{
		$this->terms['s'] = $search_term;
	}

	public function build()
	{
		global $emuShop;

		$this->terms['orderby'] = 'menu_order';
		$this->terms['order'] = 'ASC';

		$products = $emuShop->getManager('product')->getProducts( $this->terms );

		$this->products = $products;

		if( count( $products ) == 0 )
		{
			$this->content = 'No products found!';
			return;
		}

		$product_list = '';

		$template = $this->template;

		// Load the product item template
		$product_template = $this->templateManager->getTemplateRepeat( 'product', $template );

		foreach( $products as $product )
			$product_list .= $this->templateManager->fillTemplate( $product_template, $product->getTemplateTags(), $product, $product->postID );

		$template = $this->templateManager->fillTemplateRepeats( $template, array( 'product' => $product_list ) );

		$template = $this->templateManager->fillTemplate( $template, array( 'product messages' => $emuShop->getMessages( 'product' ) ) );

		$this->content = apply_filters( 'emu_products_content', $template );

	}
}

?>
