<?php

class emuPageNotFoundProcessor extends emuProcessor
{
	public $requiredFields = array( 'first_name', 'email', 'comment' );

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		switch( $this->button )
		{
			case 'Submit':

				$this->processComment();

				$emuShop->addMessage( '404', $this->messages, $this->error ? 'error' : 'notice' );

				break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );
	}

	public function processComment()
	{
		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = '<strong>Not all required fields were provided</strong> - check those marked with *';

			$this->error = true;
			return;
		}

		global $emuShop;

		extract( $_POST );

		$templateManager = $emuShop->getManager( 'theme' );

		$email = $emuShop->getManager('email-template')->getEmailTemplate( 'page-not-found.txt' );

		$message = $email->content;

		$tags = array( 	'email' => $this->sanitize( post_val('email') ),
						'first name' => $this->sanitize( post_val('first_name') ),
						'last name' => $this->sanitize( post_val('last_name') ),
						'comment' => stripslashes( $this->sanitize( post_val( 'comment' ) ) ),
						'date' => apply_date_format( 'standard' ) );

		$message = $templateManager->fillTemplate( $message, $tags );

		$emuShop->mail( $email->to, $email->from, $email->subject, $message, $email->type, $email->cc, $email->bcc );

		$this->error = false;

		$_SESSION['404'] = '1';
	}

}

?>
