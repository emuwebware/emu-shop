<?php


class emuCoupon extends emuDbEntity
{
    public $error = false;
    public $messages = array();

    public function __construct( $dbID = null, $coupon_code = null )
    {
        global $emuShop;

        if( $coupon_code )
        	$this->findCouponID( $coupon_code );

        parent::__construct( $dbID, null, $emuShop->dbPrefix, 'coupons', array( 'isActive' => '%b' ) );
    }

    public function __get( $member )
    {
        global $emuShop;

        if( $member_value = parent::__get( $member ) ) return $member_value;

        switch( $member )
        {
			case 'displayDiscount':

				switch( $this->discountType )
				{
					case 'currency':

						return $emuShop->currency.apply_number_format( 'currency', $this->discount );

					case 'percentage':

						$perc = $this->discount < 1 ? ( $this->discount * 100 ) : $this->discount;
						return apply_number_format( 'percentage', $perc ).'%';
				}
				break;

            default:

                if( !isset( $this->data[ $member ] ) ) return null;
                return $this->data[ $member ];

        }
    }


	public function removeCoupon( $coupon_id )
	{
		global $wpdb, $emuShop;

		$sql = "delete from {$emuShop->dbPrefix}coupons where dbID = $coupon_id";

		$wpdb->query( $sql );

	}


	public function isValid( $basket = null )
	{
		global $emuShop;

		$valid = true;

		// Check if we even have a coupon to check!
		if( !$this->dbID )
		{
			$this->messages[] = "No coupon found";
			return false;
		}

		if( count( $this->data ) == 0 )
		{
			// Then we have a coupon id but no associated record
			$this->messages[] = "Coupon does not exist";
			return false;
		}

		if( !$basket ) $basket = $emuShop->getBasket();

		if( $basket->isEmpty )
		{
			$this->messages[] = "Coupon exists but can not be applied to an empty basket.";
			return false;
		}

		if( !$this->isActive )
		{
			$this->messages[] = "This coupon is unavailable";
			return false;
		}

		// See if it fits within the correct date range
		$now = time();

		if( $this->startDate )
		{
			$the_time = strtotime( $this->startDate );

			if( $now < $the_time )
			{
				$this->messages[] = "This coupon is only valid from ".apply_date_format( 'standard', $this->startDate );
				return false;
			}
		}

		if( $this->endDate )
		{
			$the_time = strtotime( $this->endDate );

			if( $now > $the_time )
			{
				$this->messages[] = "This coupon expired on ".apply_date_format( 'standard', $this->endDate );
				return false;
			}
		}

		// Does it fit within the cost range

		if( $this->applyFrom && ( $basket->subTotal < $this->applyFrom ) )
		{
			$this->messages[] = "Coupon only applies to orders that are more than {$emuShop->currency}".apply_number_format( 'currency', $this->applyFrom );
			return false;
		}

		if( $this->applyTo && ( $basket->subTotal > $this->applyTo ) )
		{
			$this->messages[] = "Coupon only applies to orders that are less than {$emuShop->currency}".apply_number_format( 'currency', $this->applyTo );
			return false;
		}

		// If we got here then the coupon must be valid
		return true;
	}

	public function getUseCount()
	{
		global $wpdb, $emuShop;

		return $wpdb->get_var( "select count(*) from {$emuShop->dbPrefix}orders where couponID = {$this->dbID}" );
	}

	public function getCouponID( $coupon_code )
	{
		global $wpdb, $emuShop;

		$sql = $wpdb->prepare ( "select dbID from {$emuShop->dbPrefix}coupons where code = '%s'", $coupon_code );
		return $wpdb->get_var( $sql );
	}

	public function findCouponID( $coupon_code )
	{
		global $wpdb, $emuShop;

		$sql = $wpdb->prepare ( "select dbID from {$emuShop->dbPrefix}coupons where code = '%s'", $coupon_code );

		if( $coupon_id = $wpdb->get_var( $sql ) );
			$this->dbID = $coupon_id;
	}

}


// class emuCoupon extends emuShopCommon
// {
// 	public $data = array();
// 	public $messages = array();

// 	public function __construct( $coupon_id = null )
// 	{
// 		if( $coupon_id )
// 		{
// 			$this->dbID = $coupon_id;
// 			$this->getCouponData();
// 		}

// 	}

//     public function __get( $member )
// 	{
// 		global $emuShop;

// 		switch( $member )
// 		{
// 			case 'displayDiscount':

// 				switch( $this->discountType )
// 				{
// 					case 'currency':

// 						return $emuShop->currency.apply_number_format( 'currency', $this->discount );

// 					case 'percentage':

// 						$perc = $this->discount < 1 ? ( $this->discount * 100 ) : $this->discount;
// 						return apply_number_format( 'percentage', $perc ).'%';
// 				}
// 				break;

// 			default:

// 				if( !isset( $this->data[ $member ] ) ) return null;

// 				return $this->data[ $member ];
// 		}
//     }

// 	public function __set( $member, $value )
// 	{
//         $this->data[ $member ] = $value;
//     }

// 	public function removeCoupon( $coupon_id )
// 	{
// 		global $wpdb, $emuShop;

// 		$sql = "delete from {$emuShop->dbPrefix}coupons where dbID = $coupon_id";

// 		$wpdb->query( $sql );

// 	}

// 	public function refresh()
// 	{
// 		$this->getCouponData();
// 	}

// 	public function getCouponData()
// 	{
// 		global $wpdb, $emuShop;

// 		$sql = "select * from {$emuShop->dbPrefix}coupons where dbID = {$this->dbID}";

// 		if( $row = $wpdb->get_row( $sql, ARRAY_A ) )
// 		{
// 			$this->data = array_merge( $this->data, $row );
// 		}
// 		else return;

// 	}

// 	public function isValid( $basket = null )
// 	{
// 		global $emuShop;

// 		$valid = true;

// 		// Check if we even have a coupon to check!
// 		if( !$this->dbID )
// 		{
// 			$this->messages[] = "No coupon found";
// 			return false;
// 		}

// 		if( count( $this->data ) == 0 )
// 		{
// 			// Then we have a coupon id but no associated record
// 			$this->messages[] = "Coupon does not exist";
// 			return false;
// 		}

// 		if( !$basket ) $basket = $emuShop->getBasket();

// 		if( $basket->isEmpty )
// 		{
// 			$this->messages[] = "Coupon exists but can not be applied to an empty basket.";
// 			return false;
// 		}

// 		if( !$this->isActive )
// 		{
// 			$this->messages[] = "This coupon is unavailable";
// 			return false;
// 		}

// 		// See if it fits within the correct date range
// 		$now = time();

// 		if( $this->startDate )
// 		{
// 			$the_time = strtotime( $this->startDate );

// 			if( $now < $the_time )
// 			{
// 				$this->messages[] = "This coupon is only valid from ".apply_date_format( 'standard', $this->startDate );
// 				return false;
// 			}
// 		}

// 		if( $this->endDate )
// 		{
// 			$the_time = strtotime( $this->endDate );

// 			if( $now > $the_time )
// 			{
// 				$this->messages[] = "This coupon expired on ".apply_date_format( 'standard', $this->endDate );
// 				return false;
// 			}
// 		}

// 		// Does it fit within the cost range

// 		if( $this->applyFrom && ( $basket->subTotal < $this->applyFrom ) )
// 		{
// 			$this->messages[] = "Coupon only applies to orders that are more than {$emuShop->currency}".apply_number_format( 'currency', $this->applyFrom );
// 			return false;
// 		}

// 		if( $this->applyTo && ( $basket->subTotal > $this->applyTo ) )
// 		{
// 			$this->messages[] = "Coupon only applies to orders that are less than {$emuShop->currency}".apply_number_format( 'currency', $this->applyTo );
// 			return false;
// 		}

// 		// If we got here then the coupon must be valid
// 		return true;
// 	}

// 	public function getUseCount()
// 	{
// 		global $wpdb, $emuShop;

// 		return $wpdb->get_var( "select count(*) from {$emuShop->dbPrefix}orders where couponID = {$this->dbID}" );
// 	}

// 	public function update()
// 	{
// 		global $wpdb, $emuShop;

// 		// Update / create the database record
// 		if( $this->dbID )
// 			$this->updateRecord( "{$emuShop->dbPrefix}coupons", $this->data, array( 'isActive' => '%b' ), array( 'dbID' => $this->dbID ) );
// 		else
// 			$this->dbID = $this->insertRecord( "{$emuShop->dbPrefix}coupons", $this->data, array( 'isActive' => '%b' ) );
// 	}

// 	public function getCouponID( $coupon_code )
// 	{
// 		global $wpdb, $emuShop;

// 		$sql = $wpdb->prepare ( "select dbID from {$emuShop->dbPrefix}coupons where code = '%s'", $coupon_code );
// 		return $wpdb->get_var( $sql );
// 	}

// }


?>
