<?php

class emuProductProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();

		$product_qtys = request_val('product_qty');
		$product_ids = request_val('product_id');

		$this->error = false;

		switch( $this->button )
		{
			case 'Add to Wishlist':

				$customer = $basket->getCustomer();

				$customer->addToWishlist( $product_ids );

				$this->messages[] = 'Item(s) added to <a href="'.$emuShop->pageManager->pages->wishlist->url.'">your wishlist</a>.';

				$emuShop->addMessage( 'product', $this->messages, 'notice' );

				$location = $_SERVER[ 'HTTP_REFERER' ];

				break;

			case 'Buy Now':
			case 'Add to Basket':
			case 'AddtoBasket': // problem with header redirect with a space(+)

				$item_added = false;

				// Check the product options using one of the products (they will all have the same product options because they all use the same post)

				// Get one of the products
				$product = $emuShop->getModel('emuProduct', $product_ids[0] );

				$all_options_selected_error = false;

				$product_options = array();

				// Prepare the product options array (if the product has any options)
				if( $options = $product->getProductOptions() )
				{
					foreach( $options as $option )
				 	{
				 		if( $option_selection = post_val( $product->postID.'-'.$option->name ) )
				 		{
				 			$product_options[$option->name] = $option_selection;
				 		}
				 		else
				 			$all_options_selected_error = true;
				 	}
				}

				if( $all_options_selected_error )
				{
					$this->error = true;
					$this->messages[] = 'Please select your order options before placing your order';
				}
				else
				{
					for( $i = 0, $i_count = count( $product_ids ); $i < $i_count; $i++ )
					{
						$product_qty = $product_qtys[$i];
						$product_id = $product_ids[$i];

						if( (int) $product_qty > 0 )
						{
							$basket->addItem( $product_id, $product_qty, $product_options );
							$item_added = true;
						}
					}

					if( !$item_added )
					{
						$this->error = true;
						$this->messages[] = 'No items added to your basket - did you select a quantity?';
					}

					if( $basket->error )
					{
						$this->error = true;
						$this->messages = array_merge( $this->messages, $basket->messages );
					}
				}

				if( !$this->error )
				{
                    if( $emuShop->stayOnProductPage )
                    {
                        $this->messages[] = apply_filters( 'emu_shop_added_to_basket_message', 'Item(s) added to <a href="'.$emuShop->pageManager->pages->basket->url.'">your basket</a>. If you have finished shopping then go straight to <a href="'.$emuShop->pageManager->pages->login->url.'?return='.$emuShop->pageManager->pages->shipping->url.'"> shipping and payment</a>.' );
                        $emuShop->addMessage( 'product', $this->messages, 'notice' );
                        $location = $_SERVER[ 'HTTP_REFERER' ];
                    }
                    else
                        $location = $emuShop->pageManager->pages->basket->url;
                }
				else
				{
					$emuShop->addMessage( 'product', $this->messages, 'error' );
                    $location = $_SERVER[ 'HTTP_REFERER' ];
				}

				header( 'Location: '.$location );
				exit();

			case 'Checkout':

				// redirect to the login page
				$location = $emuShop->pageManager->pages->login->url;
				break;

            default:

                return;

		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $location );

		header( 'Location: '.$location );
		exit();

	}

}

?>
