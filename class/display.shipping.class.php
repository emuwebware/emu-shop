<?php

class emuShippingDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'shipping.htm';
        parent::__construct();

	}

	public function build()
	{
		global $emuShop;

		$basket = $emuShop->getBasket();

		$customer = $emuShop->getCustomer();

		$address_book = $customer->getAddressBook();

		$tM = $this->templateManager;

		$template = $this->template;
		$template = $tM->setTemplateConditionals( $template, array( 'has address book entries' => $address_book->hasAddresses() ));

		$shipping_address = $basket->hasShippingAddress();

		if( $address_book->hasAddresses() )
		{
			$existing_address_template = $tM->getTemplateRepeat( 'existing address', $template );

			$existing_addresses = '';

			foreach( $address_book->getAddresses() as $address )
			{
				if( $shipping_address )
					$address_checked = !$shipping_address->isNewRecord() && $shipping_address->getID() == $address->getID();
				else
					$address_checked = $address->isDefaultShipping;

				$tags = array( 'existing address checked' => $address_checked ? 'checked="checked"' : '' );

				$existing_address = $tM->fillTemplate( $existing_address_template, $address->getTemplateTags(), $address );
				$existing_address = $tM->fillTemplate( $existing_address, $tags );

				$existing_addresses .= $existing_address;
			}

			$template = $tM->fillTemplateRepeats( $template, array( 'existing address' => $existing_addresses ) );
		}

		$orderSummaryDisplay = $emuShop->getInstance( 'emuOrderSummaryDisplay' );
		$orderSummaryDisplay->build();

		// If we haven't got a shipping address then create a blank address to fill out the address template tags

		// If we do have a shipping address, and it's a new shipping address then populate the form with that shipping address object
		if( $shipping_address && $shipping_address->isNewRecord() )
			$form_address = $shipping_address;
		else // Populate with empty address
			$form_address = $emuShop->getModel('emuAddress');

		$template = $tM->fillTemplate( $template, $form_address->getTemplateTags(), $form_address );

		$region_options = $emuShop->regionHelper->getRegionOptions( $form_address->countryID, $form_address->stateID, $form_address->state );

		$tags = array(	'new address checked' => $shipping_address && $shipping_address->isNewRecord() ? ' checked="checked"' : '',
						'countries' => $region_options->countryDropDown,
						'selected state class' => $region_options->stateDropDownClass,
						'states' => $region_options->stateDropDown,
						'entered state' => $region_options->stateTextInput,
						'entered state class' => $region_options->stateTextInputClass,
						'order summary' => $orderSummaryDisplay->content,
						'shipping address messages' => $emuShop->getMessages('shipping-address')
						);

		$template = $tM->fillTemplate( $template, $tags );

		$this->content = apply_filters( 'emu_shipping_content', $template );
	}
}

?>
