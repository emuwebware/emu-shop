<?php


class emuPaymentManagerDisplay extends emuDisplay
{
	public $paymentMethodID;

	public function __construct()
	{
        $this->templateFile = 'payment-manager.htm';
        parent::__construct();

	}

	public function build()
	{
		global $emuShop;

		$customer = $emuShop->getManager('customer')->getCustomer();

		// Refresh the payment manager to include any changes from form processing
		$customer->paymentManager->getPaymentMethods();

		$template = $this->template;

		$method_section = $this->templateManager->getTemplateSection( 'payment methods', $template );

		if( count( $customer->paymentManager->paymentMethods ) == 0 )
			$payment_methods = 'You do not currently have any payment methods. Use the form below to add a new payment method';
		else
		{
			$payment_method = $this->templateManager->getTemplateRepeat( 'payment method', $method_section );

			$payment_methods = '';

			foreach( $customer->paymentManager->paymentMethods as $paymentMethod )
			{
				$default = $paymentMethod->isDefaultPaymentMethod ? '<span class="default-method">Your default payment method</span>' : '';

				$method = $this->templateManager->fillTemplate( $payment_method, $paymentMethod->getTemplateTags(), $paymentMethod );
				$payment_methods .= $this->templateManager->fillTemplate( $method, array( 'default' => $default ) );
			}

			$payment_methods = $this->templateManager->fillTemplateRepeats( $method_section, array( 'payment method' => $payment_methods ) );
			$payment_methods = $this->templateManager->fillTemplate( $payment_methods, array( 'messages' => $emuShop->getMessages('method-list') ) );
		}

		$template = $this->templateManager->fillTemplateSections( $template, array( 'payment methods' => $payment_methods ) );

		$template = $this->templateManager->fillTemplate( $template, $customer->getTemplateTags(), $customer );

		// Payment method form
		$this->paymentMethodID = post_val('payment_method_id');

		// see if we're editing an address from the order process
		$basket = $emuShop->getBasket();

		if( $emuShop->getSessionData( 'e-action' ) == 'payment-method' )
		{
			switch( $emuShop->getSessionData( 'e-method' ) )
			{
				case 'edit':

					$this->paymentMethodID = $emuShop->getSessionData( 'payment_method_id' );
					$_POST['e-button'] = 'Edit';

				break;
			}
		}

		// Load the payment method
		$paymentMethod = $emuShop->getInstance( 'emuPaymentMethod', array( $this->paymentMethodID ) );

		if( post_val('e-button') !== 'Edit' )
		{
			$paymentMethod->paymentMethod = post_val('payment_method_id');
			$paymentMethod->expiryMonth = post_val('cc_exp_month');
			$paymentMethod->expiryYear = post_val('cc_exp_year');
			$paymentMethod->validFromMonth = post_val('cc_v_from_month');
			$paymentMethod->validFromYear = post_val('cc_v_from_year');
			$paymentMethod->cardNumber = post_val('card_number');
			$paymentMethod->isDefaultPaymentMethod = post_val('default_payment_method') ? 1 : 0;
		}

		if( $this->paymentMethodID )
			$tags = array(	'edit title' => 'Edit', 'button value' => 'Update' );
		else
			$tags = array( 	'edit title' => 'New', 'button value' => 'Add' );

		$tags = array_merge( $tags, array(
                            'exp months' => drop_down( '', 'cc_exp_month', 'emu-select', $paymentMethod->expiryMonth, $emuShop->getMonths(), 'Months' ),
							'exp years' => drop_down( '', 'cc_exp_year', 'emu-select', $paymentMethod->expiryYear, $emuShop->getYears(), 'Years' ),
                            'valid from months' => drop_down( '', 'cc_v_from_month', 'emu-select', $paymentMethod->validFromMonth, $emuShop->getMonths(), 'Months' ),
							'valid from years' => drop_down( '', 'cc_v_from_year', 'emu-select', $paymentMethod->validFromYear, $emuShop->getYears((int)date('Y') - 7), 'Years' ),
							'default payment method check' => $paymentMethod->isDefaultPaymentMethod ? ' checked="checked"' : '',
							'payment types' => drop_down( '', 'payment_type', 'emu-select', $paymentMethod->paymentType, $emuShop->paymentManager->getPaymentTypes(), 'Please select' ),
							'messages' => $emuShop->getMessages( 'payment-manager' ) ) );

		$payment_form = $this->templateManager->getTemplateSection( 'payment method form', $template );

		$payment_form = $this->templateManager->fillTemplate( $payment_form, $tags );
		$payment_form = $this->templateManager->fillTemplate( $payment_form, $paymentMethod->getTemplateTags(), $paymentMethod );

		$template = $this->templateManager->fillTemplateSections( $template, array( 'payment method form' => $payment_form ) );

		$this->content = apply_filters( 'emu_payment_manager_content', $template );
	}

}

?>
