<?php


class emuAccountActivationDisplay extends emuDisplay
{
	public function __construct()
	{
        $this->templateFile = 'account-activation.htm';
        parent::__construct();
	}

	public function build()
	{
		$this->content = $this->templateManager->fillTemplateSpecialTags( $this->template );
		$this->content = apply_filters( 'emu_account_activation_content', $this->content );
	}
}

?>
