<?php


class emuAccountProfileProcessor extends emuProcessor
{
	public $requiredFields = array( 'first_name', 'last_name' );

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		// check that one or other of states has been entered
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$customer = $emuShop->getManager('customer')->getCustomer();

		$change_email = false; $change_password = false;

		if( post_val( 'new_email' ) )
		{
			// They're trying to change the email
			$this->requiredFields = array_merge( $this->requiredFields, array( 'new_email', 'new_email_confirm' ) );
			$change_email = true;
		}

		if( post_val( 'new_password' ) )
		{
			// They're (also) trying to change the password
			$this->requiredFields = array_merge( $this->requiredFields, array( 'new_password', 'new_password_confirm' ) );
			$change_password = true;
		}

		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = '<strong>Not all required fields were provided</strong> - check those marked with *';
			$this->error = true;
		}

		// if we're changing the password check the fields match
		if( $change_password && ( strcmp( post_val( 'new_password' ), post_val( 'new_password_confirm' ) ) <> 0 ) )
		{
			$this->messages[] = 'Passwords do not match';
			$this->error = true;
		}

		if( $change_email )
		{
			if( strcmp( post_val( 'new_email' ), $customer->email ) == 0 )
			{
				// No need to change the email - they are the same!
				$this->error = true;
				$this->messages[] = 'Email has not been changed - your new email is the same as your old email!';
			}

			// Make sure that the confirm fields match
			if( strcmp( post_val( 'new_email' ), post_val( 'new_email_confirm' ) ) <> 0 )
			{
				$this->messages[] = 'Emails do not match';
				$this->error = true;
			}

			if( !$emuShop->isValidEmail( post_val( 'new_email' ) ) )
			{
				$this->messages[] = 'Email address is not valid';
				$this->error = true;
			}

			global $wp_version;

			if( $wp_version < '3.1' ) require_once(ABSPATH . WPINC . '/registration.php');

			if( email_exists( post_val( 'new_email' ) ) )
			{
				$this->messages[] = 'An account with this email address has already been registered.';
				$this->error = true;
			}
		}

		if( $this->error )
		{
			$emuShop->addMessage( 'account-profile', $this->messages, 'error' );
			return;
		}

		extract( $_POST );

		$customer->firstName = $first_name;
		$customer->lastName = $last_name;
		$customer->companyName = post_val('company_name');
		$customer->jobTitle = post_val('job_title');
		$customer->department = post_val('department');
		$customer->prefix = post_val('prefix');
		$customer->suffix = post_val('suffix');
		$customer->allowEmailContact = post_val('newsletter') ? 1 : 0;

		if( $change_password ) $customer->password = $new_password;
		if( $change_email ) $customer->email = $new_email;

		if( !$customer->update() )
		{
			$this->error = true;
			$this->messages = $customer->messages;
		}
		else
		{
			$_POST['new_email'] = '';
			$_POST['new_email_confirm'] = '';
			$_POST['new_password'] = '';
			$_POST['new_password_confirm'] = '';

			$this->messages[] = 'Account Profile updated.';
			$this->error = false;
		}

		$emuShop->addMessage( 'account-profile', $this->messages, $this->error ? 'error' : 'notice' );

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

 		if(!$this->error)
 		{
			$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $_SERVER['REQUEST_URI'] );

			header( 'Location: '.$location );
			exit();
		}


	}

}

?>
