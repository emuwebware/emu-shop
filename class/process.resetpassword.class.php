<?php

class emuResetPasswordProcessor extends emuProcessor
{
	public $requiredFields = array( 'login_email');

	public function __construct()
	{
		parent::__construct();
		$this->checkRequiredFields();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		switch( $this->button )
		{
			case 'Reset Password':

				if( !$this->hasRequiredFields )
				{
					$this->messages[] = 'Not all required fields were provided - check those marked with *';

					$this->error = true;
					$emuShop->addMessage( 'reset', $this->messages, 'error' );
					return;
				}

				extract( $_POST );

				$reminder = $emuShop->getInstance( 'emuPasswordReminder' );

				$reminder->email = $login_email;

				$reminder->send();

				$this->error = $reminder->error;
				$this->messages = $reminder->messages;

				wp_clear_auth_cookie();

				$emuShop->addMessage( 'reset', $this->messages, $this->error ? 'error' : 'notice' );

			break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );

		$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $_SERVER[ 'HTTP_REFERER' ] );

		header( 'Location: '.$location );

		exit();

	}
}

?>
