<?php

class emuRegistrationProcessor extends emuProcessor
{
	public $requiredFields = array( 'first_name', 'last_name', 'email', 'username', 'email_confirm', 'password', 'password_confirm' );

	public $reqsFailed = false;

	public function __construct()
	{
		parent::__construct();

	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		switch( $this->button )
		{
			case 'Submit':

				$this->processRegistration();

				if( $this->error )
				{
					$emuShop->addMessage( 'registration', $this->messages, 'error' );
				}
				else
				{
					do_action( 'emu_shop_'.__CLASS__.'_post_process' );

					$location = apply_filters( 'emu_shop_'.__CLASS__.'_redirect_location', $emuShop->pageManager->pages->myAccount->url );

					header( 'Location: '.$location );
					exit();
				}

				break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_post_process' );


	}

	public function processRegistration()
	{
		global $emuShop;

		$this->checkRequiredFields();

		// check that one or other of states has been entered
		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$this->error = true;
			return;
		}

		// Make sure that the confirm fields match
		if( strcmp( post_val( 'email' ), post_val( 'email_confirm' ) ) <> 0 )
		{
			$this->messages[] = '<strong>Emails do not match</strong>';
			$this->reqsFailed = true;
		}

		if( strcmp( post_val( 'password' ), post_val( 'password_confirm' ) ) <> 0 )
		{
			$this->messages[] = '<strong>Passwords do not match</strong>';
			$this->reqsFailed = true;
		}

		if( !$emuShop->isValidPassword( post_val( 'password' ) ) )
		{
			$this->messages[] = '<strong>Password must be between 6 - 20 characters long and contain only numbers and letters.</strong>';
			$this->reqsFailed = true;
		}

		if( !$emuShop->isValidEmail( post_val( 'email' ) ) )
		{
			$this->messages[] = '<strong>Email address is not valid</strong>';
			$this->reqsFailed = true;
		}

		global $wp_version;

		if( $wp_version < '3.1' ) require_once(ABSPATH . WPINC . '/registration.php');

		if( username_exists( post_val( 'username' ) ) )
		{
			$this->messages[] = 'The username \'<strong>'.post_val('username').'</strong>\' has already been taken';
			$this->reqsFailed = true;
		}

		if( email_exists( post_val( 'email' ) ) )
		{
			$this->messages[] = '<strong>An account with this email address has already been registered.</strong> If you have forgotten your password we can <a href="'.$emuShop->pageManager->pages->resetPassword->url.'">send you a new password</a>.';
			$this->reqsFailed = true;
		}

		if( $this->reqsFailed )
		{
			$this->error = true;
			return;
		}

		extract( $_POST );

		// Create an activation code
		$activation_code = $emuShop->generateRandomString( $number_chars = 10 );

		$customer = $emuShop->getInstance( 'emuCustomer' );

		$customer->email = $email;
		$customer->username = $username;
		$customer->password = $password;
		$customer->firstName = $first_name;
		$customer->lastName = $last_name;
		$customer->companyName = post_val( 'company_name' );
		$customer->jobTitle = post_val( 'job_title' );
		$customer->department = post_val( 'department' );
		$customer->prefix = post_val( 'prefix' );
		$customer->suffix = post_val( 'suffix' );
		$customer->allowEmailContact = post_val( 'newsletter' ) ? 1 : 0;

		if( $emuShop->bypassAccountActivation )
			$customer->isActive = true;
		else
			$customer->activationCode = $activation_code;

		if( !$customer->update() )
		{
			$this->error = true;
			$this->messages = $customer->messages;

			$emuShop->addMessage( 'registration', $this->messages, 'error' );
			return;
		}

		if( $emuShop->bypassAccountActivation )
		{
			// log them in

			$creds = array( 'user_login' => $username, 'user_password' => $password, 'remember' => false );

			$user = wp_signon( $creds, false );

		}
		else
		{
			$emuShop->getManager('customer')->sendActivationCode( $activation_code, $customer );
			$this->messages[] = 'Your account has been created but <strong>must be activated</strong> before you can use it. A confirmation email with details of how to activate your account have been sent to '.$customer->email.'.';
		}

		$this->error = false;

	}


}

?>
