<?php

class emuM_Baskets extends emuManager
{
    public $basket;

	public function registerClasses()
	{
        $this->emuApp->registerModel( 'emuBasket', 'model.basket.class.php' );
        $this->emuApp->registerModel( 'emuBasketItem', 'model.basketitem.class.php', array('emuProduct') );
        $this->emuApp->registerModel( 'emuSpecialCharge', 'model.specialcharge.class.php' );
		$this->emuApp->registerModel( 'emuSpecialDiscount', 'model.specialdiscount.class.php' );
	}

    function getBasket( $refresh = false )
    {
        if( !$this->basket || $refresh )
            $this->basket = $this->emuApp->getInstance( 'emuBasket', $refresh );

        do_action( 'emu_shop_get_basket', $this->basket );

        return $this->basket;
    }


}


?>
