<?php

class emuPaymentProcessor extends emuProcessor
{
	public $requiredFields = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function process()
	{
		global $emuShop;

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

		$basket = $emuShop->getBasket();
		$customer = $basket->customer;

		switch( $this->button )
		{
			case 'Edit Payment Method':

				$emuShop->setSessionData( array( 	'e-action' => 'payment-method',
													'e-method' => 'edit',
													'payment_method_id' => post_val('existing_method'),
													'return' => $emuShop->pageManager->pages->payment->url ) );

				header( 'Location: '.$emuShop->pageManager->pages->paymentManager->url );

				exit();

			case 'Make Payment':

				$payment_manager = $customer->getPaymentManager();

				if( !post_val('existing_method') && $payment_manager->hasPaymentMethods() )
				{
					$this->messages[] = 'Please enter a new method or choose an existing payment method';
					$emuShop->addMessage( 'payment-method', $this->messages, 'error' );
					$this->error = true;
					return;
				}

				if( post_val('existing_method') == 'new' || !$payment_manager->hasPaymentMethods() )
					$payment_method = $this->processNewMethod();
				else
					$payment_method = $this->processExistingMethod();

				// regardless of whether all required fields have been filled...
				$basket->setPaymentMethod($payment_method);

				if( !$this->error )
				{
					$this->makePayment( $payment_method, $basket );

					if( $this->error )
					{
						$emuShop->addMessage( 'payment-method', $this->messages, 'error' );
						return;
					}
					else
					{
						// Payment was successful!
						header( 'Location: '.$emuShop->pageManager->pages->thankyou->url );
						exit();
					}

				}

			break;
		}

		do_action( 'emu_shop_'.__CLASS__.'_pre_process' );

	}

	public function processExistingMethod()
	{
		global $emuShop;

		$method_id = post_val('existing_method');
		$payment_method = $emuShop->getModel('emuPaymentMethod', $method_id);
		$payment_method->cv2Number = post_val('cv2_number_existing');

		if( post_val('method_default') )
		{
			$payment_method->isDefaultPaymentMethod = 1;
			$payment_method->update();
		}

		return $payment_method;
	}

	public function processNewMethod()
	{
		global $emuShop;

		extract( $_POST );

		$basket = $emuShop->getBasket();

		$payment_method = $emuShop->getModel( 'emuPaymentMethod' );

		$payment_method->customerID = $basket->customer->dbID;
		$payment_method->cardNumber = $card_number;
		$payment_method->paymentType = $payment_type;
		$payment_method->expiryMonth = $cc_exp_month;
		$payment_method->expiryYear = $cc_exp_year;
        $payment_method->validFromMonth = post_val('cc_v_from_month');
		$payment_method->validFromYear = post_val('cc_v_from_year');
        $payment_method->cv2Number = post_val('cv2_number');
        $payment_method->isDefaultPaymentMethod = post_val('payment_default') ? 1 : 0;

		$this->requiredFields = array( 'payment_type', 'card_number', 'cc_exp_month', 'cc_exp_year' );

		if( $emuShop->cv2Required )
            $this->requiredFields[] = 'cv2_number';

		$this->checkRequiredFields();

		if( !$this->hasRequiredFields )
		{
			$this->messages[] = 'Not all required fields were provided - check those marked with *';
			$this->error = true;
		}

		// strip non-numbers from the card number
		$_POST['card_number'] = preg_replace( '/[^0-9]/', '', post_val('card_number') );

		// And check the card number is still valid (after characters have been removed)
		if( strlen( trim ( post_val('card_number') ) == 0 ) ) {
			$this->messages[] = '<strong>Card number is invalid</strong>';
			$this->error = true;
		}

		if( $this->error )
			$emuShop->addMessage( 'payment-method', $this->messages, 'error' );

		if( post_val( 'save_method' ) && !$this->error )
			$payment_method->update();

		return $payment_method;
	}


	public function makePayment( $paymentMethod, $basket )
	{
		global $emuShop;

		$payment = $emuShop->getInstance( 'emuPayment' );

		$order = $emuShop->getModel('emuOrder');
		$order->prepareFromBasket( $basket );

		$payment->paymentMethod = $paymentMethod;
		$payment->order = $order;
		$payment->basket = $basket;

		$payment->makePayment();

		if( $payment->failed &! $emuShop->paymentsDisabled )
		{
			$this->error = true;
			$this->messages = $payment->messages;
		}
		else
		{
			// All is fine, generate the order
			$order->addPayment( $paymentMethod->getDisplayPaymentMethod(), $order->getGrandTotalValue() );
			$order->generate();

			$basket->emptyBasket();
		}
		return true;
	}

}

?>
