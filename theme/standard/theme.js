
jQuery(document).ready(function() {

    addRegionActions( jQuery('.emu-shop select[name="country"]'), jQuery('.emu-shop select[name="selected_state"]'), jQuery('.emu-shop input[name="entered_state"]') );
    addRegionActions( jQuery('.emu-shop select[name="shipping_country"]'), jQuery('.emu-shop select[name="shipping_selected_state"]'), jQuery('.emu-shop input[name="shipping_entered_state"]') );

    jQuery('input[value="Pay"]').click(function() {
        jQuery(this).hide();
    });

    var newAddressForm = jQuery('#newAddressForm');
    var newAddressRadio = jQuery('#newAddressRadio');
    var chooseAddressForm = jQuery('#chooseAddressForm');
    var existingAddressDetails = jQuery('#existingAddressDetails');

    if( chooseAddressForm.length ) {

        // Monitor check boxes
        jQuery('.address-radio', chooseAddressForm).change(function(){

            var isChecked = jQuery(this).is(':checked');

            if( jQuery(this).attr('id') == 'newAddressRadio' ) {

                if( isChecked && newAddressForm.is(':hidden'))
                    newAddressForm.slideDown();

                if( isChecked && existingAddressDetails.is(':visible') )
                    existingAddressDetails.slideUp();
            }
            else {

                // We've picked one of the existing addresses
                if ( existingAddressDetails.is(':hidden') )
                    existingAddressDetails.slideDown();

                if( newAddressForm.is(':visible') )
                    newAddressForm.slideUp('fast');
            }
        })

        // Show/Hide the newAddressForm on load
        if( newAddressRadio.is(":checked") ) {
            existingAddressDetails.hide();
            newAddressForm.show();
        }
        else
            newAddressForm.hide();
    }

    var newMethodForm = jQuery('#newMethodForm');
    var newMethodRadio = jQuery('#newMethodRadio');
    var chooseMethodForm = jQuery('#chooseMethodForm');
    var storedMethodDetails = jQuery('#storedMethodDetails');

    if( chooseMethodForm.length ) {

        // Monitor check boxes
        jQuery('.method-radio', chooseMethodForm).change(function(){

            var isChecked = jQuery(this).is(':checked');

            if( jQuery(this).attr('id') == 'newMethodRadio' ) {

                if( isChecked && newMethodForm.is(':hidden'))
                    newMethodForm.slideDown();

                if( isChecked && storedMethodDetails.is(':visible') )
                    storedMethodDetails.slideUp();
            }
            else {

                // We've picked one of the existing methods
                if ( storedMethodDetails.is(':hidden') )
                    storedMethodDetails.slideDown();

                if( newMethodForm.is(':visible') )
                    newMethodForm.slideUp('fast');
            }
        })

        // Show/Hide the newMethodForm on load
        if( newMethodRadio.is(":checked") ) {
            storedMethodDetails.hide();
            newMethodForm.show();
        }
        else
            newMethodForm.hide();
    }


});

function addRegionActions( countryDropDown, stateDropDown, stateInput )
{
    countryDropDown.change(function() {

        var countryID = jQuery(this).val();

        if(countryID)
        {
            // send the remove request
            jQuery.post( document.URL,
                {
                    'e-action':'get_states',
                    'e-plugin':'emuS',
                    'country_id': countryID
                },
                function(result){

                    if(result.length == 0)
                    {
                        // there are no states for this country so show the state input
                        stateDropDown.fadeOut("fast", function() { stateInput.fadeIn("fast"); jQuery('option', stateDropDown).remove(); });
                    }
                    else
                    {
                        // clear existing options
                        jQuery('option', stateDropDown).remove();

                        var newOption = jQuery('<option></option>').val('').html('Select a state').appendTo(stateDropDown);

                        for( var i = 0; result[i]; i++ )
                        {
                            var option = result[i];
                            var newOption = jQuery('<option></option>');
                            newOption.val(option.ID).html(option.name).appendTo(stateDropDown);
                        }
                        stateInput.val('').fadeOut("fast", function() { stateDropDown.fadeIn("fast") });
                    }
                }
            );
        }
    });
}
