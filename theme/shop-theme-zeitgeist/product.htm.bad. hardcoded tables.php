<form method="post" action="">

<div class="highlight">

  <div class="highlight-content clearfix">

    <div class="padding">

      <div class="content">

        <h1 class="no-top-padding">[product name]</h1>

       [product excerpt]

        [custom conditional="Purchasing Disabled" condition=""]
        <div class="white-background conditional_Purchasing_Disabled">
    
            <table class="full-width figures">
                <thead>
                    <tr>
                        <th>High Incidence*</th> <th>1 Group</th>
                    </tr>
                </thead>
                <tr>
                    <td class="one" >Metro New York or Los Angeles 	</td>  <td >$9,000</td>
                </tr>
                <tr class="even">
                    <td class="one" >All other markets 	</td>  <td >$8,000</td>
                </tr>
            </table>
    		<em class="pricing">* Additional recruiting costs for hard to find target markets might apply and will require custom pricing (i.e. CEOs, surgeons, etc.)</em> 
            
        </div>
        <p class="align-right action"><strong>Interested in purchasing this service?</strong> <a href="/service-request-form/?service=[product slug]" class="button">Contact Us</a></p>

        [/custom]

        [custom conditional="Show Price" condition="true"]
        <div class="white-background cond_Show_Price_true">
        
        <table class="full-width figures">
            <thead>
                <tr>
                    <th ></th>
                    <th>Short Survey</th>
                    <th>Medium Survey</th>
                    <th>Long Survey</td>
                </tr>
			</thead>
            <tr>
                <td class="one" >Number of questions </td>
                <td >1-15</td>
                <td>16-30</td>
                <td >31-50</td>
            </tr>
            <tr class="even">
                <td class="one" >Price</td>
                <td >$9,000</td>
                <td >$14,000</td>
                <td >$18,000</td>
            </tr>
        </table>
        
        </div>
        [/custom]

        [custom conditional="Service" condition="Research Autobahn"]
        <div class="white-background cond_Service_condition_Research_Autobahn ">
        <table class="full-width">
        <tr>
          <th>Yearly Subscription</th>
          <th>Each Additional Seat</th>
        </tr>
        <tr>
          <td>[custom field="Display Price"]</td>
          <td>[custom field="Additional Seat Price"]</td>
        </tr>
        </table>
        </div>
        [/custom]

        [custom conditional="Service" condition="Forschung"]
        <div class="white-background cond_Service_Forschung">
        <table class="full-width">
        <tr>
          <th>Preis: [custom field="Display Price"]</th>
        </tr>
        </table>
        </div>
        [/custom]



        [custom conditional="Service" condition="Omnibus"]
        <div class="white-background cond_Service_Omnibus">
        <table class="full-width">
        <tr>
          <th>Price</th>
        </tr>
        <tr>
          <td>[custom field="Display Price"]</td>
        </tr>
        </table>
        </div>
        [/custom]

        [custom conditional="Purchasing Disabled" condition="true"]
        <p class="align-right action"><strong>Interested in purchasing this service?</strong> <a href="/service-request-form/?service=[product slug]" class="button">Contact Us</a>
        [/custom]

        [custom conditional="Purchasing Disabled" condition="translated"]
        <p class="align-right action"><strong>Falls Sie an diesem Service interessiert sind, </strong> <a href="/service-request-form/?service=[product slug]" class="button">kontaktieren Sie uns</a>
        [/custom]

      </div>

      [product image size seven]

    </div>
    <!-- / padding -->

  </div>
  <!-- / highlight-content -->

  <div class="bg-top"></div><div class="bg-bottom"></div>

</div>
<!-- / highlight -->

<input type="hidden" name="product_id[]" value="[product id]" />
[conditional tag="product purchase fields"]<input type="hidden" name="product_qty[]" value="1" />[/conditional]
<input type="hidden" name="e-action" value="products" />
<input type="hidden" name="e-plugin" value="[emu component]" />

</form>

[product description]

