
var templateContent, template;
var imageSaving;
var editor;

jQuery(document).ready(function() {

	templateContent = jQuery('#templateContent');
	imageSaving = jQuery('#template-saving');
	template = jQuery('#template');
	
	jQuery('input[name="save-changes"]').click(function() {
		saveTemplate();
	});
	
	function saveTemplate() {
	
		imageSaving.fadeIn("fast");
		
		jQuery.post( document.URL, 
				{ 
					'e-action':'save_template', 
					'template_content': editor.getCode(),
					'e-plugin': 'emuS',
					'template_id': template.val()
				}, 
				function(result){ 
					
					if (result === null || result === undefined) {
						// something went wrong...
					} 
					else {
						if(result.ok) {

						}	
						imageSaving.fadeOut("fast");
					}
				}	
			);
	}
	
	template.change(function() {
		
		imageSaving.fadeIn("fast");

		jQuery.post( document.URL, 
			{ 
				'e-action':'get_template', 
				'e-plugin': 'emuS',
				'template_id': jQuery(this).val()
			}, 
			function(result){ 
				
				if (result === null || result === undefined) {
					// something went wrong...
				} 
				else {
					if(result.ok) {
						editor.setCode(result.templateContent);
						// templateContent.val(result.templateContent);
					}	
					imageSaving.fadeOut("fast");
				}
			}	
			);
		});
	
	
	// alert(emu.url);
	
	/*
	var editor = CodeMirror.fromTextArea('templateContent', {
		height: "550px",
		parserfile: ["parsexml.js", "parsecss.js", "tokenizejavascript.js", "parsejavascript.js", "parsehtmlmixed.js"],
		stylesheet: [emu.url + "/style/codemirror/xmlcolors.css", emu.url + "/style/codemirror/jscolors.css", emu.url + "/style/codemirror/csscolors.css"],
		path: emu.url + "/js/codemirror/",
		lineNumbers: true,
		textWrapping: false
		});
	*/
	
	editor = new CodeMirror( CodeMirror.replace("templateEditor"), {
		height: "550px",
		parserfile: ["parsexml.js", "parsecss.js", "tokenizejavascript.js", "parsejavascript.js", "parsehtmlmixed.js"],
		stylesheet: [emu.url + "/style/codemirror/xmlcolors.css", emu.url + "/style/codemirror/jscolors.css", emu.url + "/style/codemirror/csscolors.css"],
		path: emu.url + "/js/codemirror/",
		lineNumbers: true,
		textWrapping: false,
		content: document.getElementById("templateContent").value
		});
	
	
	// jQuery("textarea").tabby();

});
