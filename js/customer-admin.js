var customVarTemplate;
var customVarTable;

jQuery(document).ready(function(){

	customVarTable = jQuery('table.custom-var');
	customVarTemplate = jQuery('tr.custom-var-template', customVarTable);

	jQuery('input.add-custom-var').click(function() {
		
		var customVarRow = customVarTemplate.clone().appendTo(jQuery('tbody', customVarTable));
		
		attachVarRowEvents( customVarRow );
		
		customVarRow.removeClass();
	})

	jQuery( 'tr:not(.template)', customVarTable ).each( function() { attachVarRowEvents( jQuery(this) ) } );
	
})

function attachVarRowEvents( varRow ) {
	jQuery( 'a.delete-custom-var', varRow ).click(function () {
		varRow.fadeOut('fast', function() { varRow.remove() });
		return false;
	})
}

