
var productDialog;
var tips;
var postId;
var productForm;
var displayTable;
var variantSpecific;
var variantSpecificMsg;

var variantContainer;
var curActiveGroupIndex;

var customVarTable, customVarTemplate;
var customVarTabIndex = 1000;

var groupTemplate;

var btnEditProduct, btnNewGroup;

var imgLoadingProduct, imgLoadingGroup;
var imgLoading;

jQuery(document).ready(function(){

	tips = jQuery( ".validateTips", productDialog );

	postId = jQuery("#post_ID").val();

	productForm = { sku: jQuery('input[name="sku"]', productDialog ),
					name: jQuery('input[name="name"]', productDialog ),
					price: jQuery('input[name="price"]', productDialog ),
					displayPrice: jQuery('span.display-price', productDialog ),
					salePrice: jQuery('input[name="salePrice"]', productDialog ),
					shipping: jQuery('input[name="shipping"]', productDialog ),
					handling: jQuery('input[name="handling"]', productDialog ),
					weight: jQuery('input[name="weight"]', productDialog ),
					qtyInStock: jQuery('input[name="qtyInStock"]', productDialog ),
					isActive: jQuery('input[name="isActive"]', productDialog ),
					hasTax: jQuery('input[name="hasTax"]', productDialog ),
					shippingOption: jQuery('select[name="shippingOption"]', productDialog),
					handlingOption: jQuery('select[name="handlingOption"]', productDialog),
					variantId: jQuery('input[name="variantId"]', productDialog ),
					variantGroupId: jQuery('input[name="groupId"]', productDialog ),
					}

	var pdTable = jQuery( 'table.product-details' );

	displayTable = {	id: jQuery( 'td.id-value', pdTable ),
						sku: jQuery( 'td.sku-value', pdTable ),
						price: jQuery( 'td.price-value', pdTable ),
						shipping: jQuery( 'td.shipping-value', pdTable ),
						handling: jQuery( 'td.handling-value', pdTable ),
						weight: jQuery( 'td.weight-value', pdTable ),
						qtyInStock: jQuery( 'td.stock-value', pdTable ) };

	variantSpecific = jQuery([]).add( productForm.sku )
								.add( productForm.price )
								.add( productForm.salePrice )
								.add( productForm.shipping )
								.add( productForm.handling )
								.add( productForm.weight )
								.add( productForm.shippingOption )
								.add( productForm.handlingOption )
								.add( productForm.qtyInStock );

	variantSpecificMsg = jQuery( 'span.has-variants-message' );

	groupTemplate = jQuery( '.group-template' );
	variants = jQuery( '.variants' );
	variantContainer = jQuery('.variant-container');

	productDialog = jQuery( "#edit-product-form" );

	// Buttons
	btnEditProduct = jQuery('input.edit-product');
	btnNewGroup = jQuery('input.add-group');

	// Ajax loading images
	imgLoadingProduct = jQuery( 'img.product-loading' );
	imgLoadingGroup = jQuery( 'img.create-group-loading' );
	imgLoading = jQuery('img.loading-image');

	productDialog.dialog({
		width: 800,
		resizable: false,
		draggable: true,
		autoOpen: false,
		dialogClass: 'emu-framework emuS',
		close: function() {
			tips.removeClass().text('');
		}
	});

	customVarTable = jQuery( 'table.custom-vars', productDialog );
	customVarTemplate = jQuery( 'tr.template', productDialog );

	attachDialogEvents();

	btnEditProduct.click(function() {

		startLoader( 'product' );

		// get the product details
		getProductDetails( postId, null, function( product ) {

			resetProductForm();
			updateProductForm( product );
			openProductDialog();

			stopLoader( 'product' );

		});

	})

	btnNewGroup.click(function() {

		var newGroup = groupTemplate.clone().removeClass('group-template');
		var groupName = jQuery('input.new-group-name').val();

		startLoader( 'group' );

		createVariantGroup( groupName, function( groupId ) {

			clearAccordion();

			variants.append( newGroup );

			jQuery( 'input.group-id', newGroup ).val( groupId );
			jQuery( 'input[name="group_name"]', newGroup ).val( groupName );
			jQuery( '.header a', newGroup ).text( groupName );

			attachGroupEvents( newGroup );

			rebuildAccordion();

			jQuery('input.new-group-name').val("");

			stopLoader( 'group' );

		});
	})

	jQuery( '.variant-group', variants ).each(function() {
		attachGroupEvents( jQuery(this) );
	})

	rebuildAccordion();

})

function attachGroupEvents( group ){

	// Add a new variant row
	jQuery( 'input.add-variant', group ).click(function() {
		resetProductForm();
		openVariantDialog( group, null );
	})

	// Delete the variant group
	jQuery( '.delete-group', group ).click(function() {

		deleteGroup( group, function() {
			clearAccordion(); group.fadeOut('fast', function() { group.remove(); rebuildAccordion(); refreshProductDetails(); });
		})

	});

	// Save the group name
	jQuery( 'input.save-group-name', group ).click(function() {

		var btnSave = jQuery(this).attr("disabled", true);
		var imgLoadingGroupName = jQuery('img.group-name-loading', group).show();

		var groupName = jQuery( 'input[name="group_name"]', group ).val();

		updateGroupName( groupName, group, function() {
			jQuery( '.header a', group).text( groupName );
			btnSave.attr("disabled", false);
			imgLoadingGroupName.fadeOut('fast');
		})
	})

	// attach variant row events
	jQuery( 'tr.variant-row', group ).each(function() {
		attachRowEvents( jQuery(this), group );
	})

	// make variant rows sortable
	jQuery( 'table.variant-list tbody', group ).sortable({
		helper: fixHelper
	}).disableSelection();

}

function attachDialogEvents() {

	jQuery( 'input.add-custom-var', productDialog ).click(function() {
		addCustomVar( null, null );
	})

	productForm.shippingOption.change(function() {

		var chargeRow = productDialog.find( 'tr.shipping-charge' );

		if( jQuery(this).val() == '2' ) { // set specific shipping charge
			if( !chargeRow.is(":visible") )
				chargeRow.fadeIn('fast');
		}
		else {
			if( chargeRow.is(":visible") )
				chargeRow.fadeOut('fast');
		}

	})

	productForm.handlingOption.change(function() {

		var chargeRow = productDialog.find( 'tr.handling-charge' );

		if( jQuery(this).val() == '2' ) { // set specific handling charge
			if( !chargeRow.is(":visible") )
				chargeRow.fadeIn('fast');
		}
		else {
			if( chargeRow.is(":visible") )
				chargeRow.fadeOut('fast');
		}

	})

}

function addCustomVar( varName, varValue ) {

	// add blank var
	var newCustomVar = customVarTemplate.clone();
	newCustomVar.appendTo( jQuery( 'tbody', customVarTable ) ).removeClass();

	var customVarName = jQuery( 'input.custom-var-name', newCustomVar );
	var customVarValue = jQuery( 'input.custom-var-value', newCustomVar );

	if( varName || varValue ) {
		// set the var values
		customVarName.val( varName );
		customVarValue.val( varValue );
	}

	customVarName.attr("tabindex", customVarTabIndex );
	customVarTabIndex++;

	customVarValue.attr("tabindex", customVarTabIndex );
	customVarTabIndex++;

	attachCustomVarEvents( newCustomVar );

	if( customVarTable.hasClass("hidden") )
		customVarTable.removeClass("hidden");

}

function attachCustomVarEvents( customVar ) {
	jQuery( 'img.delete-custom-var', customVar ).click(function() {
		customVar.fadeOut('fast', function() {

			customVar.remove();

			if( jQuery( 'tbody tr', customVarTable ).length < 2 )
				customVarTable.addClass("hidden");

		} );
	})
}


function attachRowEvents( row, group ) {

	var variantId = jQuery( 'input[name="variantId"]', row ).val();

	// delete variant
	jQuery( '.delete-variant', row ).click(function() {

		deleteVariant( variantId, function() {
			row.fadeOut('fast', function() { row.remove(); variants.accordion( "resize" ); refreshProductDetails(); } );
		})

	})

	row.hover(function() { jQuery(this).addClass('row-hover') }, function() { jQuery(this).removeClass('row-hover') })

	// edit variant
	jQuery('td:not(.no-border)', row).click(function() {

		// get the product details
		getProductDetails( postId, variantId, function( variant ) {

			resetProductForm();
			updateProductForm( variant );
			openVariantDialog( group, variantId );

		});

	})

}


// Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
    ui.children().each(function() {
        jQuery(this).width(jQuery(this).width());
    });
    return ui;
};

function resetProductForm() {

	jQuery.each( productForm, function(key, elementObj) {
		if( key != 'isActive' && key != 'hasTax' ) elementObj.val('');
	});

	productForm.isActive.attr("checked", true);
	productForm.hasTax.attr("checked", true);

	// hide the shipping and handling individual charge inputs
	productDialog.find( 'tr.shipping-charge, tr.handling-charge').hide();

	variantSpecificMsg.text("").hide();
	jQuery('span.display-price', productDialog).text('');
	variantSpecific.show();

	// remove custom vars
	jQuery('tbody tr:not(.template)', customVarTable).remove();

	if( !customVarTable.hasClass("hidden") ) customVarTable.addClass("hidden");

	customVarTabIndex = 1000;
}

function deleteGroup( group, onComplete ) {

	if( !confirm( 'Are you sure you sure you want to delete this group and all group variants?' ) ) return;

	jQuery.post( document.URL,
		{
			'e-plugin': 'emuS',
			'e-action':'delete-variant-group',
			'group_id': jQuery( 'input.group-id', group ).val(),
			'post_id': postId
		},
		function(result){

			if (result === null || result === undefined) {
				// something went wrong...
				// loadingFinished();
			}
			else {

				onComplete( result );

				// alert( result );
				//loadSearchResults( result );
			}
		}
	);

}


function updateProductDisplayDetails( product ) {

	displayTable.price.html( product.displayPrice );

	if( product.hasVariants ) {

		// refresh the product values table
		var vSpecificItems = [ displayTable.handling, displayTable.qtyInStock, displayTable.shipping, displayTable.sku, displayTable.weight ];

		jQuery.each( vSpecificItems, function(){
			jQuery(this).text( 'See variants' );
		});

		return;
	}
	displayTable.id.text( product.postID );

	displayTable.handling.text( product.displayHandlingCost );
	displayTable.shipping.text( product.displayShippingCost );

	displayTable.sku.text( product.SKU );
	displayTable.weight.text( product.weight );
	displayTable.qtyInStock.text( product.qtyInStock );
}

function refreshProductDetails() {

	// get the product details
	getProductDetails( postId, null, function( product ) {
		updateProductDisplayDetails( product );
	});
}

function updateProductForm( product ) {

	// load any custom vars
	if( product.customVars )
		jQuery.each( product.customVars, addCustomVar );

	if( product.isActive == '1' ) {
		productForm.isActive.attr( "checked", true );
	} else {
		productForm.isActive.attr( "checked", false );
	}

	if( product.hasTax == '1' ) {
		productForm.hasTax.attr( "checked", true );
	} else {
		productForm.hasTax.attr( "checked", false );
	}

	if( product.hasVariants ) {

		variantSpecific.hide();
		variantSpecificMsg.text("See variants").show();
		jQuery('span.display-price', productDialog).text( product.displayPrice );
		return;
	}

	if( product.parentID ) { // then it's a variant
		productForm.name.val( product.variantDescription );
	}

	productForm.price.val( product.price );
	productForm.salePrice.val( product.salePrice );

	var chargeRow = productDialog.find( 'tr.shipping-charge' );

	if( product.shippingOption == '2' )  {// specific charge
		chargeRow.show();
		productForm.shipping.val( product.shippingCost );
	}
	else
		chargeRow.hide();

	productForm.shippingOption.val( product.shippingOption );

	var chargeRow = productDialog.find( 'tr.handling-charge' );

	if( product.handlingOption == '2' )  {// specific charge
		chargeRow.show();
		productForm.handling.val( product.handlingCost );
	}
	else
		chargeRow.hide();

	productForm.handlingOption.val( product.handlingOption );

	productForm.weight.val( product.weight );
	productForm.sku.val( product.SKU );
	productForm.qtyInStock.val( product.qtyInStock );
}

function saveFormValues( onComplete ) {

	jQuery('.ui-dialog-buttonset').append( imgLoading.clone() );
	jQuery( '.ui-button-text').css("color", "#dddddd");

	var formFields = jQuery(":input", productDialog ).serializeArray();

	var params = {};

	jQuery.each(formFields, function(index,value) {

		if( params[value.name] === undefined )
			params[value.name] = value.value;
		else
		{
			if( typeof params[value.name] == 'object' )
				params[value.name].push( value.value );
			else
				params[value.name] = [params[value.name], value.value];
		}
	});

	params['e-plugin'] = 'emuS';
	params['e-action'] = 'save-product-details';
	params['post_id'] = postId;

	jQuery.post( document.URL,
			params,
			function(result){

				if (result === null || result === undefined) {
					alert( 'An error has occured. Product details have not been saved.');
				}
				else {
					onComplete( result );
				}
			}
		);
}

function deleteVariant( variantId, onComplete ) {

	if( !confirm( 'Are you sure you sure you want to delete this variant?' ) ) return;

	jQuery.post( document.URL,
		{
			'e-plugin': 'emuS',
			'e-action':'delete-variant',
			'variant_id': variantId,
			'post_id': postId
		},
		function(result){

			if (result === null || result === undefined) {
				// something went wrong...
				// loadingFinished();
			}
			else {

				onComplete( result );

				// alert( result );
				//loadSearchResults( result );
			}
		}
	);
}

function createVariantGroup( groupName, onComplete ) {

	jQuery.post( document.URL,
		{
			'e-plugin': 'emuS',
			'e-action':'create-variant-group',
			'group_name': groupName,
			'post_id': postId
		},
		function(result){

			if (result === null || result === undefined) {
				// something went wrong...
				// loadingFinished();
			}
			else {

				onComplete( result );

				// alert( result );
				//loadSearchResults( result );
			}
		}
	);


}

function openVariantDialog( group, variantId ) {

	jQuery( 'tr.variant-name', productDialog ).show();

	// set the variant group
	jQuery( 'input[name="groupId"]' ).val( jQuery( 'input.group-id', group ).val() );

	if( variantId )
		jQuery( 'input[name="variantId"]' ).val( variantId );

	productDialog.dialog( "option", "buttons", {
			"Save": function() {

				saveFormValues(function( variant ) {

					refreshProductDetails();

					updateVariantRow( variant, group );

					if( jQuery( '.variant-list', group ).hasClass("hidden") )
						jQuery( '.variant-list', group ).removeClass("hidden");

					variants.accordion( "resize" );
					productDialog.dialog( "close" );
				});
			},
			Cancel: function() {
				/*
				tips.removeClass().text('');
				*/
				jQuery( this ).dialog( "close" );

			}
		});

	productDialog.dialog( "open" );

}

function openProductDialog() {

	jQuery( 'tr.variant-name', productDialog ).hide();

	jQuery( 'input[name="groupId"]' ).val();

	productDialog.dialog( "option", "buttons", {
		"Save": function() {

			saveFormValues(function( product ) {
				updateProductDisplayDetails( product );
				productDialog.dialog( "close" );
			});

		},
		Cancel: function() {
			/*
			tips.removeClass().text('');
			*/
			jQuery( this ).dialog( "close" );

		}
	});

	productDialog.dialog( "open" );

}


function updateGroupName( groupName, group, onComplete ) {

	jQuery.post( document.URL,
		{
			'e-plugin': 'emuS',
			'e-action':'update-group-name',
			'group_name': groupName,
			'group_id': jQuery('input.group-id', group).val(),
			'post_id': postId
		},
		function(result){

			if (result === null || result === undefined) {
				// something went wrong...
				// loadingFinished();
			}
			else {

				onComplete( result );
			}
		}
	);

}


function updateVariantRow( variant, group ) {

	var newRow = false;

	// get the variant row
	var row = jQuery( 'tr.variant-' + variant.dbID, group );

	if( !row.length ) {
		// row doesn't exist - create a new row
		var rowTemplate = jQuery( '.row-template', groupTemplate );
		row = rowTemplate.clone().removeClass('row-template').addClass( 'variant-' + variant.dbID );
		newRow = true;
	}

	if( variant.isActive )
		row.removeClass('variant-inactive');
	else
		row.addClass('variant-inactive');

	jQuery( '.v-id', row ).text( variant.dbID  );
	jQuery( '.name', row ).text( variant.variantDescription  );
	jQuery( '.sku', row ).text( variant.SKU );
	jQuery( '.price', row ).html( variant.displayOriginalPrice );
	jQuery( '.sale-price', row ).html( variant.displaySalePrice );
	jQuery( '.shipping', row ).html( variant.displayShippingCost );
	jQuery( '.handling', row ).html( variant.displayHandlingCost );
	jQuery( '.qty', row ).text( variant.qtyInStock );
	jQuery( '.weight', row ).text( variant.weight );
	jQuery( '.status', row ).text( variant.displayActive );
	jQuery( 'input[name="variantId"]', row ).val( variant.dbID );

	if( newRow ) {
		var variantTable = jQuery( '.variant-list', group );
		jQuery( 'tbody', variantTable ).append( row );
		attachRowEvents( row, group );
	}
}

function clearAccordion() {

	curActiveGroupIndex = variants.accordion("option", "active");

	if( typeof curActiveGroupIndex !== 'number' ) curActiveGroupIndex = 0;

	variantContainer.css("visibility", "hidden");

	if( variants.hasClass('ui-accordion') )
		variants.accordion( "destroy" );
}

function rebuildAccordion() {

	if( variants.hasClass('ui-accordion') )
		clearAccordion();

	variants.accordion( { header: 'div.header', collapsible: true, active: curActiveGroupIndex, create: function() { variantContainer.css('visibility', 'visible'); } } );
}

function getProductDetails( postId, variantId, onLoad ) {

	jQuery.post( document.URL,
		{
			'e-plugin': 'emuS',
			'e-action':'get-product-details',
			'variant_id': variantId == null ? '' : variantId,
			'post_id': postId
		},
		function(result){

			if (result === null || result === undefined) {
			}
			else {
				onLoad( result );
			}
		}
	);
}

function startLoader( section ) {

	switch(section){
		case 'product':
			btnEditProduct.attr("disabled", true);
			imgLoadingProduct.show();
		break;
		case 'group':
			btnNewGroup.attr("disabled", true);
			imgLoadingGroup.show();
		break;
	}
}

function stopLoader( section ) {

	switch(section) {
		case 'product':
			btnEditProduct.attr("disabled", false);
			imgLoadingProduct.fadeOut('fast');
		break;
		case 'group':
			btnNewGroup.attr("disabled", false);
			imgLoadingGroup.fadeOut('fast');
		break;
	}


}


