
jQuery(document).ready(function() {

	// Set up the chkboxes
	
	jQuery('#chk-all-templates').click(function() {
		jQuery('input[name="templates[]"]').attr('checked', jQuery(this).is(':checked'));
	});
	
	jQuery('#chk-all-lists-tables').click(function() {
		jQuery('input[name="lists[]"]').attr('checked', jQuery(this).is(':checked'));
		jQuery('input[name="tables[]"]').attr('checked', jQuery(this).is(':checked'));
	});

	jQuery('#chk-all-styles').click(function() {

		jQuery('input[name="standard-styles[]"]').attr('checked', jQuery(this).is(':checked'));
		jQuery('input[name="custom-styles[]"]').attr('checked', jQuery(this).is(':checked'));
	
	});
	

});

