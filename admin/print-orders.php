<?php

require('../../../../wp-blog-header.php');

global $emuShop;

if( !is_user_logged_in() ) exit();
if( !current_user_can('manage_options') ) exit();


global $wpdb;

$order_post_id = get_val( 'oid' );

$order_id = $wpdb->get_var( $wpdb->prepare( "select dbID from {$emuShop->dbPrefix}orders where postID = %d", $order_post_id ) );

if( ! $order_id ) exit();

$order = $emuShop->getInstance( 'emuOrder', array( $order_id ) );

$post = get_post( $order_post_id );


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"> 
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<title>Order</title> 
	<script type="text/javascript">
	
	
	</script>
	<style type="text/css">
	
	body { font-family: calibri, arial; }
	table { width: 500px; border-collapse: collapse; }
	table th, table td { text-align: left; vertical-align: top; padding: 5px 0px; border-bottom: 2px solid #ccc}
	
	</style>
</head>
<body onload="javascript: window.print();">
<?php echo $post->post_content ?>

</body>
</html>

