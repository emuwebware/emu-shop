<?php

global $emuShop;

if( post_val('submit') )
{
	$emuShop->updateMeta( 'small_weight_unit', post_val('small_weight_unit') );
	$emuShop->updateMeta( 'large_weight_unit', post_val('large_weight_unit') );
	$emuShop->updateMeta( 'unit_conversion', post_val('unit_conversion') );

	$emuShop->updateMeta( 'default_shipping_charge', post_val('default_charge') );

	// update_option('e_small_weight_unit', post_val('small_weight_unit'));
	// update_option('e_large_weight_unit', post_val('large_weight_unit'));
	// update_option('e_unit_conversion', post_val('unit_conversion'));

	// update_option('e_default_shipping_charge', post_val('default_charge'));

	$emuShop->getManager('shipping')->clearShippingCharges();

	// process the country data
	if( $description = post_val( 'description' ) )
	{
		extract($_POST);

		for( $n = 1; $n < count( $description ); $n++ )
		{
			$shippingCharge = $emuShop->getInstance( 'emuShippingCharge' );
			$shippingCharge->countryID = $country[$n];
			$shippingCharge->stateID = $state[$n];
			$shippingCharge->description = $description[$n];
			$shippingCharge->basedOn = $shipping_type[$n];
			$shippingCharge->rangeFrom = $range_from[$n];
			$shippingCharge->rangeTo = $range_to[$n];
			$shippingCharge->charge = $charge[$n];
			$shippingCharge->isActive = $charge_active[$n];
			$shippingCharge->freeShipping = $free_shipping[$n];
			$shippingCharge->update();
		}
	}
}

function shipping_charges()
{
	global $emuShop;

	$charges = $emuShop->getManager('shipping')->getShippingCharges();

	foreach( $charges as $charge )
	{
		?>

		<table class="existing-charge">
		<tr>
			<th>Description</th><td><input type="text" class="" value="<?php echo $charge->description?>" name="description[]" /> e.g. <em>UPS Special Delivery</em></td>
		</tr>
		<tr>
			<th>Region</th>
			<td>
			<select name="country[]" class="country"><option value="">All countries</option>
			<?php

			$countries = $emuShop->getManager('region')->getCountries();

			foreach( $countries as $country )
			{
				$selected = $country->ID == $charge->countryID ? ' selected="selected"' : '';

				echo '<option value="'.$country->ID.'"'.$selected.'>'.$country->name.'</option>';
			}

			?>
			</select><?php

			if( $charge->countryID )
				$states = $emuShop->getManager('region')->getStates( array( 'country' => $charge->countryID ) );
			else
				$states = array();

			?><select name="states" class="state <?php echo count($states) == 0 ? 'hidden' : ''?>"><option value="">All States</option>
			<?php

			foreach( $states as $state )
			{
				$selected = $state->ID == $charge->stateID ? ' selected="selected"' : '';
				echo '<option value="'.$state->ID.'"'.$selected.'>'.$state->name.'</option>';
			}

			?></select><input type="hidden" class="state-value" name="state[]" value="<?php echo $charge->stateID?>" /></td>
		</tr>
		<tr>
			<th>Based on</th><td><select name="shipping_type[]" class="based-on"><option value="price" <?php echo $charge->basedOn == 'price' ? ' selected="selected"' : ''?>>Price</option><option value="weight" <?php echo $charge->basedOn == 'weight' ? ' selected="selected"' : ''?>>Weight</option></select></td>
		</tr>
		<tr>
			<th>Range from</th>
			<td><span class="price <?php echo $charge->basedOn == 'weight' ? 'hidden' : ''?>"><?php echo $emuShop->currency;?></span><input type="text" class="small-text" value="<?php echo $charge->rangeFrom?>" name="range_from[]" /> <span class="weight <?php echo $charge->basedOn == 'price' ? 'hidden' : ''?>"><?php echo $emuShop->shippingManager->getWeightUnits()?></span> <em>for an open-ended range e.g. > 50, only enter a from value</em></td>
		</tr>
		<tr>
			<th>Range to</th>
			<td><span class="price <?php echo $charge->basedOn == 'weight' ? 'hidden' : ''?>"><?php echo $emuShop->currency;?></span><input type="text" class="small-text" value="<?php echo $charge->rangeTo?>" name="range_to[]" /> <span class="weight <?php echo $charge->basedOn == 'price' ? 'hidden' : ''?>"><?php echo $emuShop->shippingManager->getWeightUnits()?></span></td>
		</tr>
		<tr>
			<th>Shipping Charge</th>
			<td><input type="text" class="small-text" value="<?php echo $charge->charge?>" name="charge[]" /></td>
		</tr>
		<tr>
			<th>Charge Active</th>
			<td><select name="charge_active[]"><option value="1" <?php echo $charge->isActive ? ' selected="selected"' : ''?>>Yes</option><option value="0" <?php echo $charge->isActive ? '' : ' selected="selected"'?>>No</option></select></td>
		</tr>
		<tr>
			<th>Free Shipping</th>
			<td><select name="free_shipping[]"><option value="1" <?php echo $charge->freeShipping ? ' selected="selected"' : ''?>>Yes</option><option value="0" <?php echo $charge->freeShipping ? '' : ' selected="selected"'?>>No</option></select></td>
		</tr>
		<tr>
			<th>Remove</th>
			<td><a href="" class="remove">Remove</a></td>
		</tr>

		</table>

		<?php

	}


}


?>
<script type="text/javascript">


jQuery(document).ready(function() {

	var chargeContainer = jQuery('div.shipping-set');

	jQuery('table.existing-charge', chargeContainer).each(function() { attachEvents(jQuery(this)); });

	jQuery('#weight_unit').change(function() {

		jQuery('span.weight').html(jQuery(this).val());

	});

	jQuery('#addCharge').click(function() {

		var newCharge = jQuery('table.template').clone().removeClass('template');

		newCharge.appendTo(chargeContainer);

		attachEvents(newCharge);

		return false;
	});

});



function attachEvents(charge)
{
	// remove charge
	jQuery('a.remove', charge).click(function() {

		jQuery(this).parents('table').fadeOut("slow", function() { jQuery(this).remove() });
		return false;

	});

	jQuery('select.based-on', charge).change(function() {

		if(jQuery(this).val() == 'price') {
			jQuery('span.weight', charge).hide();
			jQuery('span.price', charge).show();
		}
		else {
			jQuery('span.weight', charge).show();
			jQuery('span.price', charge).hide();
		}
	});


	var stateDropDown = jQuery('select.state', charge);
	var stateValue = jQuery('input.state-value', charge);

	stateDropDown.change(function() {

		if(jQuery(this).val().length == 0)
			stateValue.val('');
		else
			stateValue.val(jQuery(this).val());
	});

	jQuery('select.country', charge).change(function() {

		var countryID = jQuery(this).val();

		if(countryID)
		{
			// send the remove request
			jQuery.post( document.URL,
				{
					'e-action':'get_states',
					'e-plugin':'emuS',
					'country_id': countryID
				},
				function(result){

					if(result.length == 0)
					{
						// this country has no states so remove them
						if(!stateDropDown.is(":hidden")) stateDropDown.fadeOut("slow", function() { jQuery('option', stateDropDown).remove(); stateValue.val('') } );
					}
					else
					{
						jQuery('option', stateDropDown).remove();
						var newOption = jQuery('<option></option>').val('').html('All States').appendTo(stateDropDown);

						for( var i = 0; result[i]; i++ )
						{
							var option = result[i];
							var newOption = jQuery('<option></option>');
							newOption.val(option.ID).html(option.name).appendTo(stateDropDown);
						}
						stateDropDown.fadeIn("fast");
					}
				}
			);
		}
		else
		{
			 stateDropDown.fadeOut("slow", function() { jQuery('option', stateDropDown).remove(); });
			 stateValue.val('');
		}
	});


}



</script>
<style type="text/css">

div#shipping-options ul { display: block; height: 27px; clear: left; }
div#shipping-options ul li { float: left; display: inline; line-height: 25px; margin-right: 6px; }
div.shipping-set input.small-text { width: 60px; }
div.shipping-set table { border-bottom: 2px solid #ccc; }

div.shipping-set table.template { display: none; }

div.shipping-set table th { text-align: left; font-weight: normal; }
div.shipping-set table td,
div.shipping-set table th { padding: 5px 10px 5px 0 }
div.shipping-set table td label { padding-right: 10px; }
div.shipping-set select.hidden { display: none; }
span.hidden { display: none; }

table.form-table td label { padding-right: 10px; }

</style>

<form method="post" action="" />

	<div class="wrap">

		<h2>Shipping</h2>

		<h3>Options</h3>
		<table class="form-table">
			<tr valign="top">
				<th scope="row">Large weight unit</th>
				<td><input type="text" class="small-text" value="<?php echo $emuShop->getMeta('large_weight_unit')?>" id="large_weight_unit" name="large_weight_unit"> e.g. kg, lb</td>
			</tr>
			<tr valign="top">
				<th scope="row">Small weight unit</th>
				<td><input type="text" class="small-text" value="<?php echo $emuShop->getMeta('small_weight_unit')?>" id="small_weight_unit" name="small_weight_unit"> e.g. g, oz</td>
			</tr>
			<tr valign="top">
				<th scope="row">Unit conversion (no. of Smalls per Large)</th>
				<td><input type="text" class="small-text" value="<?php echo $emuShop->getMeta('unit_conversion')?>" id="unit_conversion" name="unit_conversion"> e.g. there are 1000g per kg so enter: 1000</td>
			</tr>
			<tr valign="top">
				<th scope="row">Default charge per item</th>
				<td><input type="text" class="small-text" value="<?php echo $emuShop->getMeta('default_shipping_charge')?>" id="default_charge" name="default_charge"> will be applied where a shipping option can not be applied</td>
			</tr>

		</table>

		<h3>Shipping Charges <a class="button add-new-h3" href="post-new.php" id="addCharge">Add</a></h3>

		<div class="shipping-set">

			<table class="template">
			<tr>
				<th>Description</th><td><input type="text" class="" value="" name="description[]" /> e.g. <em>UPS Special Delivery</em></td>
			</tr>
			<tr>
				<th>Regions</th>
				<td>
				<select name="country[]" class="country"><option value="">All countries</option>
				<?php

				$countries = $emuShop->getManager('region')->getCountries();

				foreach($countries as $country)
				{
					echo '<option value="'.$country->ID.'">'.$country->name.'</option>';
				}

				?>
				</select><select name="states" class="state hidden"><option value="">All States</option></select><input type="hidden" class="state-value" name="state[]" value="" /></td>
			</tr>
			<tr>
				<th>Based on</th><td><select name="shipping_type[]" class="based-on"><option value="weight">Weight</option><option value="price">Price</option></select></td>
			</tr>
			<tr>
				<th>Range from</th>
				<td><span class="price hidden"><?php echo $emuShop->currency;?></span><input type="text" class="small-text" value="" name="range_from[]" /> <span class="weight"><?php echo $emuShop->shippingManager->getWeightUnits()?></span>  for an open-ended range e.g. > 50, only enter a <em>from</em> value</td>
			</tr>
			<tr>
				<th>Range to</th>
				<td><span class="price hidden"><?php echo $emuShop->currency;?></span><input type="text" class="small-text" value="" name="range_to[]" /> <span class="weight"><?php echo $emuShop->shippingManager->getWeightUnits()?></span></td>
			</tr>
			<tr>
				<th>Shipping Charge</th>
				<td><input type="text" class="small-text" value="" name="charge[]" /></td>
			</tr>
			<tr>
				<th>Charge Active</th>
				<td><select name="charge_active[]"><option value="1">Yes</option><option value="0">No</option></select></td>
			</tr>
			<tr>
				<th>Free Shipping</th>
				<td><select name="free_shipping[]"><option value="1">Yes</option><option value="0" selected="selected">No</option></select></td>
			</tr>
			<tr>
				<th>Remove</th>
				<td><a href="" class="remove">Remove</a></td>
			</tr>
			</table>

			<?php shipping_charges(); ?>

		</div>

		<p>
		<input type="submit" value="Save Changes" class="button-primary" name="submit" />
		</p>

	</div>

</form>
