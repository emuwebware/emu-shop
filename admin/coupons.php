<?php

global $emuShop;

switch( post_val( 'e-button' ) )
{
	case 'Save Changes':

		extract( $_POST );

		$coupon = $emuShop->getInstance( 'emuCoupon' );

		$coupon->dbID = $coupon_id;
		$coupon->code = $code;
		$coupon->discount = $discount_value;
		$coupon->discountType = $discount_type;
		$coupon->startDate = apply_date_format( 'db', "$start_month/$start_day/$start_year 00:00:00" );
		$coupon->maxUses = $max_uses;
		$coupon->freeShipping = post_val('free_shipping') ? 1 : 0;

		if( post_val( 'unlimited' ) )
			$coupon->endDate = '';
		else
			$coupon->endDate = apply_date_format( 'db', "$end_month/$end_day/$end_year 23:59:59" );

		switch( $applies_to )
		{
			case 'all':
				$coupon->applyTo = '';
				$coupon->applyFrom = '';
				break;
			case 'over':
				$coupon->applyTo = '';
				$coupon->applyFrom = $applies_value;
				break;
			case 'under':
				$coupon->applyTo = $applies_value;
				$coupon->applyFrom = '';
				break;
		}
		$coupon->isActive = $is_active;

		$coupon->update();

	break;
	case 'Remove':


		$emuShop->loadClass( 'emuCoupon' );

		extract( $_POST );

		emuCoupon::removeCoupon( $coupon_id );

	break;
}

function get_coupons()
{
	global $emuShop;

	$coupons = $emuShop->promotionManager->getCoupons();

	if( count( $coupons ) == 0 )
	{
		?>
		<div class="properties"> You do not currently have any coupons. Click <strong>Add</strong> to create a coupon. </div>
		<?php
		return;
	}

	foreach( $coupons as $coupon )
	{
		?>

		<div class="properties existing-coupons">

			<form method="post" action="" />

				<?php build_coupon( $coupon ) ?>

			</form>

		</div>

		<?php
	}

}

function build_coupon( $coupon = null )
{
	global $emuShop;

	$apply_type = 'all';
	$apply_value = '';

	$date = getdate();

	$end_day = $date['mday'];
	$end_month = $date['mon'];
	$end_year = $date['year'];

	$start_day = $date['mday'];
	$start_month = $date['mon'];
	$start_year = $date['year'];

	if( $coupon )
	{
		if( $coupon->applyFrom )
		{
			$apply_type = 'over';
			$apply_value = $coupon->applyFrom;
		}
		else if( $coupon->applyTo )
		{
			$apply_type = 'under';
			$apply_value = $coupon->applyTo;
		}

		if( $coupon->endDate )
		{
			$endDate = getdate( strtotime( $coupon->endDate ) );
			$end_day = $endDate['mday'];
			$end_month = $endDate['mon'];
			$end_year = $endDate['year'];
		}

		if( $coupon->startDate )
		{
			$startDate = getdate( strtotime( $coupon->startDate ) );
			$start_day = $startDate['mday'];
			$start_month = $startDate['mon'];
			$start_year = $startDate['year'];
		}
	}
?>

	<table class="properties">
	<tr>
		<th>Code</th>
		<td><input type="text" name="code" value="<?php echo $coupon ? $coupon->code : ''?>" /><input type="button" name="generate_code" class="button" value="Generate" /></td>
	</tr>
	<tr>
		<th>Discount</th>
		<td><input type="text" name="discount_value" class="currency" value="<?php echo $coupon ? $coupon->discount : ''?>" /><?php echo drop_down( '', 'discount_type', '', $coupon ? $coupon->discountType : '', $emuShop->promotionManager->getDiscountTypes() )?></td>
	</tr>
	<tr>
		<th>Start Date</th>
		<td><input type="text" name="start_day" class="small-text" value="<?php echo $start_day?>" /><?php echo drop_down( '', 'start_month', '', $start_month, $emuShop->getMonths() )?> <?php echo drop_down( '', 'start_year', '', $start_year, $emuShop->getYears() )?></td>
	</tr>
	<tr>
		<th>End Date</th>
		<td><input type="text" name="end_day" class="small-text" value="<?php echo $end_day?>" /><?php echo drop_down( '', 'end_month', '', $end_month, $emuShop->getMonths() )?> <?php echo drop_down( '', 'end_year', '', $end_year, $emuShop->getYears() )?> <label>Unlimited <input type="checkbox" name="unlimited" value="unlimited" <?php echo $coupon ? ( $coupon->endDate ? '' : 'checked="checked"' ) : 'checked="checked"' ?> /></label></td>
	</tr>
	<tr>
		<th>Applies To</th>
		<td><?php echo drop_down( '', 'applies_to', '', $apply_type, $emuShop->getCouponAppliesTo() )?><?php echo $emuShop->currency?><input type="text" name="applies_value" class="currency" value="<?php echo $apply_value?>" /></td>
	</tr>
	<tr>
		<th>Max No. Uses</th>
		<td><input type="text" name="max_uses" value="<?php echo $coupon ? $coupon->maxUses : ''?>" class="small-text" /></td>
	</tr>
	<tr>
		<th>Current No. Uses</th>
		<td><?php echo $coupon ? $coupon->getUseCount() : '-'?></td>
	</tr>
	<tr>
		<th>Status</th>
		<td><?php echo drop_down( '', 'is_active', '', $coupon ? $coupon->isActive : 1, $emuShop->getStatusTypes() )?></td>
	</tr>
	<tr>
		<th>Free Shipping</th>
		<td><label><input type="checkbox" name="free_shipping" value="1" <?php echo $coupon ? ($coupon->freeShipping ? ' checked="checked"' : '' ) : ''?> /> Free shipping</label></td>
	</tr>
	<tr>
		<th>Actions</th>
		<td><input type="submit" name="e-button" class="button-primary" value="Save Changes" /><input type="submit" name="e-button" class="button" value="Remove" /></td>
	</tr>
	</table>

	<input type="hidden" name="coupon_id" value="<?php echo $coupon ? $coupon->dbID : ''?>" />

<?php

}


?>
<script type="text/javascript">

	jQuery(document).ready( function() {

		var couponContainer = jQuery('#coupons');

		// attach events to existing coupons
		jQuery('div.existing-coupons', couponContainer).each(function() { attachEvents( jQuery(this) ); });

		jQuery('#addCoupon').click(function() {

			var newCoupon = jQuery('div.template').clone().removeClass('template');

			newCoupon.appendTo(couponContainer);
			attachEvents(newCoupon);
			return false;
		});

	});

	function attachEvents(coupon)
	{
		var unlimited = jQuery('input[name="unlimited"]', coupon);

		setEndDateDisabled( unlimited.is(":checked"), coupon );

		unlimited.change(function() {
			setEndDateDisabled( jQuery(this).is(":checked"), coupon );
		});

		var code = jQuery('input[name="code"]', coupon);

		jQuery('input[name="generate_code"]').click(function() {
			code.val( getRandomString() );
		});

		var appliesValue = jQuery('input[name="applies_value"]', coupon);
		var appliesTo = jQuery('select[name="applies_to"]', coupon);

		setAppliesValueVisible( appliesTo.val(), appliesValue );

		appliesTo.change(function() {
			setAppliesValueVisible( jQuery(this).val(), appliesValue );
		});

	}

	function setAppliesValueVisible( appliesTo, appliesValue ) {

		if( appliesTo == 'all' )
			appliesValue.hide();
		else {
			if( appliesValue.is(":hidden") ) appliesValue.fadeIn('fast');
		}

	}


	function setEndDateDisabled( disabled, coupon )
	{
		var endDateFields = [
							jQuery('input[name="end_day"]', coupon),
							jQuery('select[name="end_month"]', coupon),
							jQuery('select[name="end_year"]', coupon)
							]

		for( var i = 0; endDateFields[i]; i++ )
			endDateFields[i].attr('disabled', disabled);
	}

	function getRandomString() {
		var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
		var string_length = 8;
		var randomstring = '';
		for ( var i=0; i<string_length; i++ ) {
			var rnum = Math.floor(Math.random() * chars.length);
			randomstring += chars.substring(rnum,rnum+1);
		}
		return randomstring;
	}



</script>

<style type="text/css">

#coupons { float: left; }
#coupons table th { font-weight: normal; }
#coupons table td { width: 265px; }
input.small-text { width: 40px; }
input.currency { width: 60px; }

div.template { display: none; }

#coupons div.properties { margin-bottom: 10px; }

</style>



<div class="wrap" id="coupons">

	<h2>Coupons <a class="button add-new-h2" href="javascript:{}" id="addCoupon">Add</a></h2>

	<?php get_coupons() ?>

	<div class="properties template">

		<form method="post" action="" />

			<?php build_coupon() ?>

		</form>

	</div>

</div>

