<?php

global $emuShop;


if( post_val( 'save-changes' ) )
{
	extract( $_POST );

	// update the products
	for( $n = 0; $n < count( $product_post_ID ); $n++ )
	{
		$product = $emuShop->getInstance( 'emuProduct', array( $product_post_ID[$n] ) );

		$product->price = back_to_number( $product_price[$n] );
		$product->salePrice = back_to_number( $product_sale_price[$n] );
		$product->hasShippingCost = $product_shipping_value[$n] == 'true' ? 1 : 0;
		$product->shippingCost = $product_shipping_cost[$n];
		$product->hasHandlingCost = $product_handling_value[$n] == 'true' ? 1 : 0;
		$product->handlingCost = $product_handling_cost[$n];
		$product->weight = $product_weight[$n];
		$product->qtyInStock = $product_stock_qty[$n];
		$product->SKU = $product_SKU[$n];

		$product->update();

		$product = null;
	}

	// Update the variants
	for( $n = 0; $n < count( $variant_id ); $n++ )
	{
		$variant = $emuShop->getInstance( 'emuVariant', array( $variant_id[$n] ) );

		$variant->price = back_to_number( $variant_price[$n] );
		$variant->salePrice = back_to_number( $variant_sale_price[$n] );
		$variant->hasShippingCost = $variant_shipping_value[$n] == 'true';
		$variant->shippingCost = $variant_shipping_cost[$n];
		$variant->hasHandlingCost = $variant_handling_value[$n] == 'true';
		$variant->handlingCost = $variant_handling_cost[$n];
		$variant->weight = $variant_weight[$n];
		$variant->isActive = $variant_is_active_value[$n] == 'true';
		$variant->qtyInStock = $variant_stock_qty[$n];
		$variant->displayPosition = $variant_display_position[$n];
		$variant->SKU = $variant_SKU[$n];
		$variant->variantDescription = $variant_description[$n];

		$variant->update();

		$variant = null;
	}

}

$categories = get_terms( 'pcat' );

$cat_dd = array(); $first_cat = null;

foreach( $categories as $cat )
{
	if( !$first_cat ) $first_cat = $cat->slug;
	$cat_dd[$cat->slug] = $cat->name;
}

if( !$current_cat = post_val( 'view_pcat' ) )
	$current_cat = $first_cat;

function get_products( $current_cat )
{
	global $emuShop;

	$posts = get_posts( array( 'post_type' => 'product', 'numberposts' => 99999, 'pcat' => $current_cat ) );

	foreach( $posts as $post )
	{
		setup_postdata( $post );

		$product = $emuShop->getInstance( 'emuProduct', array( $post, $show_all = true ) );
		?>
			<tr>
				<td><a href="/wp-admin/post.php?post=<?php echo $post->ID?>&action=edit"><?php echo $product->name ?></a><input type="hidden" name="product_post_ID[]" value="<?php echo $post->ID?>" /><input type="hidden" name="product_dbID[]" value="<?php echo $product->dbID?>" /></td>
				<td><input type="text" id="product_SKU" name="product_SKU[]" value="<?php echo $product->SKU ?>" class="sku" autocomplete="off" /></td>
				<td><input type="text" id="default_price" name="product_price[]" value="<?php echo apply_number_format( 'currency', $product->price )?>" class="price" autocomplete="off" /></td>
				<td><input type="text" id="default_sale_price" name="product_sale_price[]" value="<?php echo apply_number_format( 'currency', $product->salePrice )?>" class="price" autocomplete="off" /></td>
				<td class="center"><input type="hidden" name="product_shipping_value[]" value="<?php echo $product->hasShippingCost ? 'true' : 'false' ?>" /><label><input type="checkbox" name="product_shipping[]" value="true" <?php echo $product->hasShippingCost ? ' checked="checked"' : ''?> />Yes</label></td>
				<td><input type="text" id="default_shipping_cost" name="product_shipping_cost[]" value="<?php echo apply_number_format( 'currency', $product->shippingCost )?>" class="price" autocomplete="off" /></td>
				<td class="center"><input type="hidden" name="product_handling_value[]" value="<?php echo $product->hasHandlingCost ? 'true' : 'false' ?>" /><label><input type="checkbox" name="product_handling[]" value="true" <?php echo $product->hasHandlingCost ? ' checked="checked"' : ''?> />Yes</label></td>
				<td><input type="text" id="default_handling_cost" name="product_handling_cost[]" value="<?php echo apply_number_format( 'currency', $product->handlingCost )?>" class="price" autocomplete="off" /></td>
				<td><input type="text" id="default_weight" name="product_weight[]" value="<?php echo $product->weight?>" class="weight" autocomplete="off" /></td>
				<td><input type="text" id="default_qty_stock" name="product_stock_qty[]" value="<?php echo $product->qtyInStock ?>" class="weight" autocomplete="off" /></td>
				<td class="center"></td>
				<td></td>
			</tr>
		<?php

		// product variants
		$variant_set = '';

		foreach( $product->variants as $variant_group )
		{
			foreach( $variant_group->variants as $variant )
			{
				$variant_set .= '<tr class="variant-row">
									<td><input type="text" name="variant_description[]" value="'.$variant->variantDescription.'" class="desc" autocomplete="off" /> <em>'.$variant_group->group->groupName.'</em><input type="hidden" name="variant_group[]" value="'.$variant_group->group->groupName.'" /><input type="hidden" name="variant_id[]" value="'.$variant->dbID.'" /></td>
									<td><input type="text" name="variant_SKU[]" value="'.$variant->SKU.'" class="sku" autocomplete="off" /></td>
									<td><input type="text" name="variant_price[]" value="'.apply_number_format( 'currency', $variant->price ).'" class="price" autocomplete="off" /></td>
									<td><input type="text" name="variant_sale_price[]" value="'.apply_number_format( 'currency', $variant->salePrice ).'" class="price" autocomplete="off" /></td>
									<td class="center"><input type="hidden" name="variant_shipping_value[]" value="'.( $variant->hasShippingCost ? 'true' : 'false' ).'" /><label><input type="checkbox" name="variant_shipping[]" value="true" '.( $variant->hasShippingCost ? ' checked="checked"' : '').'/>Yes</label></td>
									<td><input type="text" name="variant_shipping_cost[]" value="'.apply_number_format( 'currency', $variant->shippingCost ).'" class="price" autocomplete="off" /></td>
									<td class="center"><input type="hidden" name="variant_handling_value[]" value="'.( $variant->hasHandlingCost ? 'true' : 'false' ).'" /><label><input type="checkbox" name="variant_handling[]" value="true" '.( $variant->hasHandlingCost ? ' checked="checked"' : '').'/>Yes</label></td>
									<td><input type="text" name="variant_handling_cost[]" value="'.apply_number_format( 'currency', $variant->handlingCost ).'" class="price" autocomplete="off" /></td>
									<td><input type="text" name="variant_weight[]" value="'.$variant->weight.'" class="weight" autocomplete="off" /></td>
									<td><input type="text" name="variant_stock_qty[]" value="'.$variant->qtyInStock.'" class="weight" autocomplete="off" /></td>
									<td><input type="text" name="variant_display_position[]" value="'.$variant->displayPosition.'" class="price" autocomplete="off" /></td>
									<td class="center"><label><input type="checkbox" name="variant_is_active[]" value="true" '.( $variant->isActive ? ' checked="checked"' : '').'/>Yes</label><input type="hidden" name="variant_is_active_value[]" value="'.( $variant->isActive ? 'true' : 'false' ).'" /></td>
								</tr>';
			}
		}

		echo $variant_set;
	}
}

?>
<script type="text/javascript">

</script>

<style type="text/css">

div.emu-e { float: left; }

td a { text-decoration: none; font-weight: bold }
td a:hover {text-decoration: underline; }

td .save-changes { float: right; }

</style>

<form method="post" action="" />

	<div class="wrap emu-e">

		<h2>Bulk Update</h2>

		<div class="e-product-options properties" id="bulk-product-options">
			<table>
				<tr>
					<td colspan="12"><input type="submit" value="Save Changes" name="save-changes" class="button-primary save-changes" /><strong>Category</strong> <?php echo drop_down( '', 'view_pcat', '', post_val('view_pcat'), $cat_dd )?><input type="submit" value="View" class="button" name="view-category" /></td>
				</tr>
				<tr>
					<th>Product</th>
					<th>SKU</th>
					<th>Price (<?php echo $emuShop->currency;?>)</th>
					<th>Sale Price (<?php echo $emuShop->currency;?>)</th>
					<th>Add Shipping</th>
					<th>Shipping Cost</th>
					<th>Add Handling</th>
					<th>Handling Cost</th>
					<th>Weight (<?php echo $emuShop->shippingManager->getWeightUnits()?>)</th>
					<th>Stock Qty.</th>
					<th>Position</th>
					<th>Is Active</th>
				</tr>
				<?php get_products( $current_cat ) ?>
			</table>

			<p>
				<input type="submit" value="Save Changes" class="button-primary save-changes" name="save-changes" />
			</p>

		</div>

	</div>

</form>

