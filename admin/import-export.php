<?php

global $emuShop;

$nID = 0;

$error = false;
$message = false;

if ( post_val('emu-shop-import') && isset( $_FILES['importFile'] ) )
{
	if( $_FILES['importFile']['size'] == 0 )
	{
		$error = "No file was provided";
	}
	else
	{
		$uploads 		= wp_upload_dir();
		$upload_dir 	= $uploads['path'];

		$filename 		= $_FILES['importFile']["name"];
		$tmp_path 		= $_FILES['importFile']["tmp_name"];
		$target_path	= "$upload_dir/$filename";

		if( move_uploaded_file ( $tmp_path, $target_path ) )
		{
			$import_data = unserialize( base64_decode( file_get_contents( $target_path ) ) );
			
			$result = $emuShop->import( $import_data );
			
			if( $result->ok )
				$message = "File has been successfully imported";
			else
				$error = $result->message;
		}
		else
		{
			$error = "A problem has occured, the file has not been imported.";
		}
	}
}


?>

<div class="wrap" id="emu-module">

	<h2>Import</h2>

	<form method="post" enctype="multipart/form-data" accept-charset="utf-8" action="">
	
		<?php
		if( $error )
			echo '<div class="info emu-error">'.$error.'</div>';
		else if ( $message )
			echo '<div class="info">'.$message.'</div>';
		?>
			
		<div class="emu-import">
		
			<div class="properties">
				
				<table class="properties">
				<tr>
					<td>Import file</td>
					<td><input type="file" name="importFile" id="import-file" /></td>
					<td><input type="submit" name="emu-shop-import" class="button-primary" value="Import" /></td>
				</tr>
				</table>
			
				
			</div>
			
		</div>
	
	</form>

	<div class="clear"></div>
	
		
	<h2>Export</h2>

	<form method="post" action="" />
		
		<div class="emu-export">

			<div class="column-one">
				
				<div class="properties emu-templates">
				
					<h3>Content Templates</h3>
				
					<table class="properties">
					<?php
					
						$templates = $emuShop->getManager( 'template' )->getTemplates( array( 'templateType' => 'content' ) );

						if( !$templates )
						{
							?>
							<tr>
								<td></td><th>No templates found</th>
							</tr>
							<?php
						}
						else
						{
								
							foreach( $templates as $template )
							{
								?>
								<tr>
									<td><input type="checkbox" name="templates[]" class="chkbox" id="chk<?php echo $nID?>" value="<?php echo $template->filename?>" /></td><th><label for="chk<?php echo $nID?>"><?php echo $template->filename?></label></th>
								</tr>
								<?php
								$nID++;
							}
							?>
							<tr>
								<td><input type="checkbox" name="chk-all-templates" class="chkbox" id="chk-all-templates" /></td><th><label for="chk-all-templates"><strong>All</strong></label></th>
							</tr>
						<?php
						}
						?>
					</table>

				</div>
				<!-- / templates -->

				
			</div>
			<!-- / column-one -->
			
			<div class="column-two">

				<div class="properties emu-templates">
				
					<h3>Email Templates</h3>
				
					<table class="properties">
					<?php
					
						$templates = $emuShop->getManager( 'template' )->getTemplates( array( 'templateType' => 'email' ) );

						if( !$templates )
						{
							?>
							<tr>
								<td></td><th>No templates found</th>
							</tr>
							<?php
						}
						else
						{
								
							foreach( $templates as $template )
							{
								?>
								<tr>
									<td><input type="checkbox" name="etemplates[]" class="chkbox" id="chk<?php echo $nID?>" value="<?php echo $template->filename?>" /></td><th><label for="chk<?php echo $nID?>"><?php echo $template->filename?></label></th>
								</tr>
								<?php
								$nID++;
							}
							?>
							<tr>
								<td><input type="checkbox" name="chk-all-templates" class="chkbox" id="chk-all-etemplates" /></td><th><label for="chk-all-etemplates"><strong>All</strong></label></th>
							</tr>
						<?php
						}
						?>
					</table>

				</div>
				<!-- / templates -->
			</div>
			<!-- / column-two -->
			
		</div>
		<!-- / emu-export -->

		<div class="clear"></div>
		
		<input type="submit" name="emu-shop-export" class="button" value="Export All" />
		<input type="submit" name="emu-shop-export" class="button-primary" value="Export" />

		
	</form>
	
	
</div>
<!-- / wrap -->

