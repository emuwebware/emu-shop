<?php

global $emuShop;

$emuShop->loadClass( array( 'emuProduct', 'emuBasket', 'emuCustomer', 'emuOrder', 'emuReview', 'emuPaymentMethod', 'emuAddress' ) );

function get_tag_reference( $object )
{
	switch( $object )
	{
		case 'emuProduct':
			$tags = emuProduct::$staticTemplateTags;
			break;
		case 'emuBasket':
			$tags = emuBasket::$staticTemplateTags;
			break;
		case 'emuCustomer':
			$tags = emuCustomer::$staticTemplateTags;
			break;
		case 'emuOrder':
			$tags = emuOrder::$staticTemplateTags;
			break;		
		case 'emuReview':
			$tags = emuReview::$staticTemplateTags;
			break;		
		case 'emuPaymentMethod':
			$tags = emuPaymentMethod::$staticTemplateTags;
			break;
		case 'emuAddress':
			$tags = emuAddress::$staticTemplateTags;
			break;				
	}
	
	$tag_rows = '';
	
	foreach( $tags as $tag_name => $field ) $tag_rows .= '<tr><td>['.$tag_name.']</td></tr>';
	
	return '<table class="properties">'.$tag_rows.'</table>';
	
}
					
?>
<script type="text/javascript">
	
	
</script>

<style type="text/css">
	
	.tag-set { float: left; }


</style>

<div class="wrap" id="coupons">

	<h2>Tag Reference</h2>
	
	<div class="properties template">
	
		<div class="tag-set">
			<h3>emuProduct</h3>
			<?php echo get_tag_reference( 'emuProduct' );?>
		</div>

		<div class="tag-set">
			<h3>emuBasket</h3>
			<?php echo get_tag_reference( 'emuBasket' );?>
		</div>

		<div class="tag-set">
			<h3>emuCustomer</h3>
			<?php echo get_tag_reference( 'emuCustomer' );?>
		</div>

		<div class="tag-set">
			<h3>emuOrder</h3>
			<?php echo get_tag_reference( 'emuOrder' );?>
		</div>

		<div class="tag-set">
			<h3>emuReview</h3>
			<?php echo get_tag_reference( 'emuReview' );?>
		</div>

		<div class="tag-set">
			<h3>emuAddress</h3>
			<?php echo get_tag_reference( 'emuAddress' );?>
		</div>

		<div class="tag-set">
			<h3>emuPaymentMethod</h3>
			<?php echo get_tag_reference( 'emuPaymentMethod' );?>
		</div>

		
		<div class="clear"></div>
		
	</div>

</div>

