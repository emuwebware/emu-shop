<?php

global $emuShop;

switch( post_val( 'submit' ) )
{
	case 'Generate Pages':
		
		// only generate pages that are undefined
		
		$pages_to_generate = array();
		
		foreach( $emuShop->getManager('page')->pages as $page )
		{
			if( !post_val( $page->optionRef ) ) $pages_to_generate[] = $page;
		}
		
		$emuShop->getManager('page')->generatePages( $pages_to_generate );
		
	break;
	case 'Save Changes':
		
		foreach( $emuShop->getManager('page')->pages as $page )
			$page->pageID = @post_val( $page->optionRef );
		
		$emuShop->getManager('page')->updatePages();
		
	break;
}
	
	
$all_pages = get_pages();

$pages_array = array();

$pages_array[''] = 'Please select...';

foreach( $all_pages as $page )
	$pages_array[$page->ID] = preg_replace( '/^(.{25})(.{1,})/', '$1...', $page->post_title );

?>
<script type="text/javascript">

</script>

<style type="text/css">

div#page-details { float: left; }

</style>

<form method="post" action="" />

	<div class="wrap">
	
		<h2>Pages</h2>

		<div id="page-details" class="properties">
		
			<table class="properties">
				<?php
				
				$arr_pages = array();
				
				foreach( $emuShop->getManager('page')->pages as $page )
				{
					$arr_pages[] = $page->pageID;
				?>
				<tr>
					<th><?php echo $page->description?> Page</th>
					<td><?php echo drop_down( '', $page->optionRef, '', $page->pageID, $pages_array )?></td>
				</tr> 
				<?php
				}
				?>
			</table>
			
			<p>
				<input type="submit" name="submit" class="button" value="Generate Pages" />
				<input type="submit" name="submit" class="button-primary" value="Save Changes" />
			</p>
			
			<p>Shopping Page IDs<br /><textarea rows="2" cols="48"><?php echo implode( ',', $arr_pages ) ;?></textarea></p>

		</div>


	</div>

</form>
