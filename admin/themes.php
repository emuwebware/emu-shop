<?php

global $emuShop;

$current_theme = $emuShop->themeManager->getCurrentTheme();

?>


<form method="post" action="" />

	<div class="wrap">

		<h2>Shop Themes</h2>

        <table cellspacing="0" class="wp-list-table widefat plugins">
        <thead>
            <tr>
                <th style="" class="manage-column column-name" id="name" scope="col">Theme</th>
                <th style="" class="manage-column column-description" id="description" scope="col">Description</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th style="" class="manage-column column-name" scope="col">Theme</th>
                <th style="" class="manage-column column-description" scope="col">Description</th>
            </tr>
        </tfoot>

        <tbody id="the-list">

            <?php

            foreach( $emuShop->themeManager->themes as $theme )
            {
                $theme_active = $theme == $current_theme;
                ?>
                <tr class="<?php echo $theme_active ? 'active' : 'inactive'?>">
                    <td class="plugin-title">
                        <strong><?php echo $theme->name?></strong>
                        <div class="row-actions-visible">
                            <?php if( $theme_active ) { ?>
                            Currently Active Theme
                            <?php } else { ?>
                            <span class="activate"><a class="edit" title="Activate this plugin" href="/wp-admin/admin.php?page=emuS-themes&e-action=activatetheme&e-plugin=emuS&theme=<?php echo urlencode($theme->name)?>">Activate</a></span>
                            <?php } ?>
                        </div>
                    </td>
                    <td class="column-description desc">
                        <div class="plugin-description">
                            <p><?php echo $theme->description?></p>
                        </div>
                        <div class="<?php echo $theme_active ? 'active' : 'inactive'?> second plugin-version-author-uri">
                            Version <?php echo $theme->version?> | By <?php echo $theme->author?>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
            </table>

        </div>

</form>
