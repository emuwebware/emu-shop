<?php

add_meta_box( 'emu-order-details', 'Order Status', 'get_order_details', 'customer-order', 'normal', 'high' );

add_action( 'save_post', 'save_order_details' );

add_action( 'delete_post', 'remove_order' , 10, 2);

function remove_order( $post_id ) 
{
	global $emuShop;
	
	// $emuShop->removeProduct( $post_id );
}

function save_order_details( $post_id )
{
	if( ! post_val( 'e-action' ) == 'order-admin' ) return $post_id;
	
	global $emuShop;
	global $wpdb;
	
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
	
	if( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) return $post_id;
	
	// Make sure we're doing a proper save (this function seems to be called when the page loads as well)
	if(! post_val( 'save-form' ) ) return $post_id;
	
	if( $order_status = post_val( 'order_status' ) )
	{
		$order_status = $order_status == 'null' ? 'null' : "'$order_status'";
		
		$sql = "update {$wpdb->prefix}emu_orders set orderStatus = $order_status where postID = $post_id";
		$wpdb->query( $sql );
	}
}

function get_order_details ( $post )
{
	
	global $emuShop, $wpdb;
	
	$order = $wpdb->get_row( "select orderDetails, orderStatus from {$wpdb->prefix}emu_orders where postID = {$post->ID}" );
	
	/*
	$order =  unserialize( base64_decode( $order ) );
	
	print_r($order);
	*/
	
	?>
	<div class="emu-e">
		
		<div class="e-status-options">
			
			<table>
			<tr>
				<th>Order Status</th>
				<td><?php echo drop_down( '', 'order_status', '', $order->orderStatus, $emuShop->getManager('order')->getOrderStatusOptions() )?></td>
			</tr>
			</table>
			<input type="hidden" name="save-form" value="true" />	
			<input type="hidden" name="e-action" value="order-admin" />
		</div>
		
	</div>
	<!-- / emu-e -->
	<?php
}

?>
