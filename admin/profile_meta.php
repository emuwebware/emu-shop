<?php



function my_show_extra_profile_fields( $user ) 
{ 
	global $emuShop;
?>

	<h3>eCommerce: Account information</h3>

	<table class="form-table">

		<tr>
			<th><label for="address">Address</label></th>
			<td><input type="text" name="address" id="address" value="<?php echo esc_attr( get_the_author_meta( 'address', $user->ID ) ); ?>" class="regular-text" /><br /><span class="description"></span></td>
		</tr>
		<tr>
			<th><label for="address">Postal Code</label></th>
			<td><input type="text" name="postal_code" id="postal_code" value="<?php echo esc_attr( get_the_author_meta( 'postal_code', $user->ID ) ); ?>" class="regular-text" /><br /><span class="description"></span></td>
		</tr>
		<tr>
			<th><label for="address">Phone Number</label></th>
			<td><input type="text" name="phone_number" id="phone_number" value="<?php echo esc_attr( get_the_author_meta( 'phone_number', $user->ID ) ); ?>" class="regular-text" /><br /><span class="description"></span></td>
		</tr>
		<tr>
			<th><label for="address">Country</label></th>
			<td><select name="country"><?php
				$countries = $emuShop->getManager('region')->getCountries();

				$country_id = esc_attr( get_the_author_meta( 'country', $user->ID ) );
				
				foreach( $countries as $country )
				{
					$selected = $country->ID == $country_id ? ' selected="selected"' : '';
					
					echo '<option value="'.$country->ID.'"'.$selected.'>'.$country->name.'</option>';
				}?></select></td>
		</tr>
		<tr>
			<th><label for="address">Selected state</label></th>
			<td><select name="selected_state"><?php
			
			$states = $emuShop->getManager('region')->getStates( array('country' => $country_id) );

			$selected_state = esc_attr( get_the_author_meta( 'selected_state', $user->ID ) );

			foreach( $states as $state )
			{
				$selected = $state->ID == $selected_state ? ' selected="selected"' : '';
				
				echo '<option value="'.$state->ID.'"'.$selected.'>'.$state->name.'</option>';
			}
			
			?></select></td>
		</tr>		
		<tr>
			<th><label for="address">Entered State</label></th>
			<td><input type="text" name="entered_state" id="entered_state" value="<?php echo esc_attr( get_the_author_meta( 'entered_state', $user->ID ) ); ?>" class="regular-text" /><br /><span class="description"></span></td>
		</tr>
		<tr>
			<th><label for="address">Phone Number</label></th>
			<td><input type="text" name="phone_number" id="phone_number" value="<?php echo esc_attr( get_the_author_meta( 'phone_number', $user->ID ) ); ?>" class="regular-text" /><br /><span class="description"></span></td>
		</tr>

	</table>
<?php 

}

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_user_meta( $user_id, 'address', post_val('address') );
	update_user_meta( $user_id, 'postal_code', post_val('postal_code') );
	update_user_meta( $user_id, 'phone_number', post_val('phone_number') );
	update_user_meta( $user_id, 'country', post_val('country') );
	update_user_meta( $user_id, 'selected_state', post_val('selected_state') );
	update_user_meta( $user_id, 'entered_state', post_val('entered_state') );

}


?>
