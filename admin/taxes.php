<?php
global $emuShop;

if( post_val('submit') )
{
	// set the default tax rate;
	$default_tax = post_val('default_tax');

	$default_tax = preg_replace('/[^0-9\.]/', '', $default_tax);

	$emuShop->updateMeta('default_tax_rate', (float) $default_tax);

	$states = post_val('state');
	$countries = post_val('country');
	$tax = post_val('tax');

	$emuShop->getManager('tax')->clearTaxRates();

	for( $i = 1, $i_count = count( $countries ); $i < $i_count; $i++ )
	{
		$tax_rate = $emuShop->getModel('emuTaxRate');

		$tax_rate->countryID = $countries[$i];
		$tax_rate->stateID = $states[$i];
		$tax_rate->rate = $tax[$i];
		$tax_rate->update();
	}
}

function tax_exceptions()
{
	global $emuShop;

	$tax_rates = $emuShop->taxManager->getTaxRates();

	foreach( $tax_rates as $tax_rate )
	{
		?>
		<ul class="existing-rate">
			<li class="region">Country <select name="country[]" class="country"><option value="">Select a country</option>
			<?php

			$countries = $emuShop->regionManager->getCountries();

			foreach( $countries as $country )
			{
				$selected = $country->ID == $tax_rate->countryID ? ' selected="selected"' : '';

				echo '<option value="'.$country->ID.'"'.$selected.'>'.$country->name.'</option>';
			}

			?>
			</select></li>
			<?php

			$states = $emuShop->regionManager->getStates( array( 'country' => $tax_rate->countryID ) );

			?>
			<li class="state <?php echo count($states) == 0 ? 'hidden' : ''?>">State <select name="states" class="state">
			<option value="all">All States</option>
			<?php

			foreach( $states as $state )
			{
				$selected = $state->ID == $tax_rate->stateID ? ' selected="selected"' : '';
				echo '<option value="'.$state->ID.'"'.$selected.'>'.$state->name.'</option>';
			}

			?>
			</select><input type="hidden" class="state-value" name="state[]" value="<?php echo $tax_rate->stateID?>" /></li>
			<li>Tax rate <input type="text" class="small-text" value="<?php echo $tax_rate->rate?>" name="tax[]">%</li>
			<li><a href="" class="remove">Remove</a>
		</ul>
		<?php

	}
}


?>
<script type="text/javascript">


jQuery(document).ready(function() {

	var exContainer = jQuery('#exceptions');

	// attach events to existing tax exceptions
	jQuery('ul.existing-rate', exContainer).each(function() { attachEvents(jQuery(this)); });

	function attachEvents(region) {

		// remove event
		jQuery('a.remove', region).click(function() {

			jQuery(this).parents('ul').fadeOut("slow", function() { jQuery(this).remove() });
			return false;
		});

		var stateDropDown = jQuery('select.state', region);
		var stateArea = jQuery('li.state', region);
		var stateValue = jQuery('input.state-value', region);

		stateDropDown.change(function() {

			if(jQuery(this).val().length == 0)
				stateValue.val('');
			else
				stateValue.val(jQuery(this).val());
		});


		jQuery('select.country', region).change(function() {

			var countryID = jQuery(this).val();

			if(countryID)
			{
				// send the remove request
				jQuery.post( document.URL,
					{
						'e-action':'get_states',
						'e-plugin':'emuS',
						'country_id': countryID
					},
					function(result){

						if(result.length == 0)
						{
							if(!stateArea.is(":hidden")) stateArea.fadeOut("slow");
							// clear the state value as well
							stateValue.val('');
						}
						else
						{
							// clear existing options
							jQuery('option', stateDropDown).remove();

							var newOption = jQuery('<option></option>').val('all').html('All States').appendTo(stateDropDown);
							stateValue.val('all');

							for( var i = 0; result[i]; i++ )
							{
								var option = result[i];
								var newOption = jQuery('<option></option>');
								newOption.val(option.ID).html(option.name).appendTo(stateDropDown);
							}
							stateArea.fadeIn("slow");
						}
					}
				);
			}
		});
	}

	jQuery('#addRegion').click(function() {

		var newRegion = jQuery('ul.template').clone().removeClass('template');

		newRegion.appendTo(exContainer);

		attachEvents(newRegion);

		return false;
	});

});


</script>
<style type="text/css">

div#exceptions { padding-bottom: 20px; }
div#exceptions ul { display: block; height: 27px; clear: left; }
div#exceptions ul li { float: left; display: inline; line-height: 25px; margin-right: 6px; }
div#exceptions ul li input.small-text { width: 60px; }
div#exceptions ul.template { display: none; }
div#exceptions ul li.hidden { display: none; }

</style>

<form method="post" action="" />

	<div class="wrap">

		<h2>Taxes</h2>

		<table class="form-table">
			<tr valign="top">
				<th scope="row">Default tax rate, e.g. 15%</th>
				<td><input type="text" class="small-text" value="<?php echo (float) $emuShop->getMeta('default_tax_rate');?>" id="default_tax" name="default_tax">%</td>
			</tr>
		</table>

		<h3>Exceptions <a class="button add-new-h3" href="post-new.php" id="addRegion">Add</a></h3>

		<p>Here you can set special tax rates for specific regions.</p>

		<div id="exceptions">

			<ul class="template">
				<li class="region">Country <select name="country[]" class="country"><option value="">Select a country</option>
				<?php

				$countries = $emuShop->regionManager->getCountries();

				foreach($countries as $country)
				{
					echo '<option value="'.$country->ID.'">'.$country->name.'</option>';
				}

				?>
				</select></li>
				<li class="state hidden">State <select name="states" class="state"><option value="">-</option></select><input type="hidden" class="state-value" name="state[]" value="" /></li>
				<li>Tax rate <input type="text" class="small-text" value="" name="tax[]">%</li>
				<li><a href="" class="remove">Remove</a>
			</ul>

			<?php tax_exceptions(); ?>

		</div>


		<input type="submit" value="Save Changes" class="button-primary" name="submit" />


	</div>

</form>
