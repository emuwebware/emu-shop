<?php

/*
Emu Payment Gateway: SagePay
Emu Payment Gateway Description: SagePay payment gateway
*/
global $emuShop, $emuSagePay;

if( !class_exists( 'SagePay' ) )
	include_once( $emuShop->getManager('gateway')->paymentGatewayDir.'/SagePay/sagepay.class.php' );

$emuSagePay = new SagePay();

$SPSettings = $emuShop->settings->createSettingsGroup( 'SagePay', 'SagePay Settings' );

$SPSettings->addSetting( 'vendorName', '', 'Vendor Name', 1 );
// $SPSettings->addSetting( 'transactionKey', '', 'Transaction Key', 2 );
$SPSettings->addSetting( 'cur', 'gbp', 'Currency', 3 );
$SPSettings->addSetting( 'env', 'development', 'Environment', 4, array( 'DEVELOPMENT' => 'Development', 'LIVE' => 'Live' ) );

$emuShop->settings->addSettingsGroup( $SPSettings );
$emuShop->settings->saveSettings();

$emuSagePay->vendorName = $SPSettings->getSettingValue( 'vendorName' );
$emuSagePay->transactionKey = $SPSettings->getSettingValue( 'transactionKey' );
$emuSagePay->env = $SPSettings->getSettingValue( 'env' );
$emuSagePay->currency = $SPSettings->getSettingValue( 'cur' );

add_action( 'emu_shop_make_payment', 'do_sage_pay_payment', 1, 2 );
add_filter( 'emu_shop_payment_result', 'update_shop_payment_result' );
add_filter( 'emu_shop_payment_messages', 'update_shop_payment_messages' );
add_filter( 'emu_shop_get_payment_types', 'set_sage_pay_payment_types', 1 );

function set_sage_pay_payment_types( $payment_types )
{
    return array(   'VISA' => 'Visa Credit',
                    'DELTA' => 'Visa Debit',
                    'UKE' => 'Visa Electron',
                    'MC' => 'MasterCard',
                    'MAESTRO' => 'Maestro',
                    'AMEX' => 'American Express',
                    'DC' => 'Diner\'s Club',
                    'JCB' => 'JCB Card'
                    );
}

function do_sage_pay_payment( $order, $paymentMethod )
{
	global $emuSagePay;

    $data = array();

    $billing_address = $order->getBillingAddress();

    $data['CardHolder'] = $billing_address->firstName.' '.$billing_address->lastName;
    $data['CardNumber'] = $paymentMethod->cardNumber;
    $data['StartDateMonth'] = $paymentMethod->validFromMonth ? str_pad($paymentMethod->validFromMonth, 2, "0", STR_PAD_LEFT) : '';
    $data['StartDateYear'] = $paymentMethod->validFromYear ? substr( $paymentMethod->validFromYear, -2 ) : '';
    $data['ExpiryDateMonth'] = $paymentMethod->expiryMonth ? str_pad($paymentMethod->expiryMonth, 2, "0", STR_PAD_LEFT) : '';
    $data['ExpiryDateYear'] = $paymentMethod->expiryYear ? substr( $paymentMethod->expiryYear, -2 ) : '';
    $data['CardType'] = $paymentMethod->paymentType;
    $data['IssueNumber'] = '';
    $data['CV2'] = $paymentMethod->cv2Number;
    $data['BillingFirstnames'] = $billing_address->firstName;
    $data['BillingSurname'] = $billing_address->lastName;
    $data['BillingAddress1'] = $billing_address->addressLineOne;
    $data['BillingAddress2'] = $billing_address->addressLineTwo;
    $data['BillingCity'] = $billing_address->city;
    $data['BillingCountry'] = $emuSagePay->getCountryCode( $billing_address->countryName );
    $data['BillingPostCode'] = $billing_address->postalCode;
    $data['Amount'] = $order->getGrandTotalValue();

    $emuSagePay->setData( $data );
    $emuSagePay->execute();
}

function update_shop_payment_result( $result )
{
    global $emuSagePay;

    return $emuSagePay->status == 'success';
}

function update_shop_payment_messages( $messages )
{
    global $emuSagePay;

    if($emuSagePay->status !== 'success')
        $messages[] = $emuSagePay->error.'<!-- '.$emuSagePay->response['StatusDetail'].'-->';

    return $messages;
}


?>
