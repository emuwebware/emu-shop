<?php
/**
 * SagePay class
 * Handles the formatting of requests to SagePay,
 * the actual request and response of the request
 *
 * @package Payment
 * @author Pete Robinson
 * @version 1.0
 **/

class SagePay
{
	
	public
		$status = '',                   // status returned from the cURL request
		$error = '',                    // stores any errors
		$vendorTxCode = '',             // vendor transaction code. must be unqiue
		$acsurl = '',                   // used to store data for 3D Secure
		$pareq = '',                    // used to store data for 3D Secure
		$response = array(),            // response from SagePay cURL request
		$env = '',                      // environment, set according to 'ENV' site constant
		$md = '';                       // used to store data for 3D Secure
	
    private
		$url = '',                      // the URL to post the cURL request to (set further down)
		$data = array(),                // the data to post
		$price = 0,                     // transaction amount
		$standardFields = array(),      // holds standard SagePay info (currency etc)
		$description = 'New order from your online store',	// Description of the order sent to SagePay
		$curl_str = '';                 // the url encoded string derrived from the $this->data array
	
    const
		TYPE = 'PAYMENT',               // Transaction type
		PROTOCOL_VERSION = '2.22';              // Currency transaction is to be in. For multi-currency sites, you need to change this from a constant to a property
	
    public $vendorName = '';    
    public $transactionKey = '';
	public $currency = '';
    
	/**
	 * Constructor method
	 * Sets the $this->env property, assigns the necessary urls,
	 * sets the price, sets and formats the data to pass to SagePay
	 * @return void
	 * @param arr $data - the data provided by the user (billing, price and card)
	 * @author Pete Robinson
	 **/
	public function __construct()
	{
		
	}   
	
	
	/**
	 * setData method
	 * Assigns the user data and SagePay config data to $this->data
	 * @return void
	 * @param arr $data - billing and card info from user
	 * @author Pete Robinson
	 **/
	public function setData($data)
	{
        $data = $this->formatRawData($data);
        
        $this->setPrice($data['Amount']);
                
		// Set the billing, card and purchase details as provided by the user
		$this->data = $data;
		
		// Format the StartDate field
		if($data['StartDateMonth']){
			// If so, add start date to data array to be appended to POST
			$this->data['StartDate'] = $data['StartDateMonth'] . $data['StartDateYear'];
		}

		// Format the ExpiryDate field
		$this->data['ExpiryDate'] = $data['ExpiryDateMonth'] . $data['ExpiryDateYear'];
		
		// set the vendorTxCode
		$this->vendorTxCode = $data['VendorTxCode'];
		
		// set the required fields to pass to SagePay
		$this->standardFields = array(
			'VPSProtocol' => self::PROTOCOL_VERSION,
			'TxType' => self::TYPE,
			'Vendor' => $this->vendorName,
			'VendorTxCode' => $this->vendorTxCode,
			'Amount' => $this->price,
			'Currency' => $this->currency,
			'Description' => $this->description
		);
		
		// Add Payment Type
		$this->data['PaymentType'] = self::TYPE;
		
		// Add currency details
		$this->data['Currency'] = $this->currency;
		
		// Add vendor and transaction details
		$this->data['VendorTxCode'] = $this->vendorTxCode;
		$this->data['Description'] = $this->description;
		$this->data['Vendor'] = $this->vendorName;
        
        $this->formatData();
	}
	
	
	/**
	 * setUrls method
	 * Selects which SagePay url to use (live or test)
	 * based on the $this->env property
	 * @return void
	 * @author Pete Robinson
	 **/
	private function setUrls()
	{
		$this->url = ($this->env == 'DEVELOPMENT') ? 'https://test.sagepay.com/gateway/service/vspdirect-register.vsp' : 'https://live.sagepay.com/gateway/service/vspdirect-register.vsp';
	}
	
	
	/**
	 * setPrice method
	 *
	 * @return void
	 * @author Pete Robinson
	 **/
	private function setPrice($price)
	{
		$this->price = $price;
	}
	
	
	/**
	 * setVendorTxCode method
	 *
	 * @return void
	 * @author Pete Robinson
	 **/
	private function setVendorTxCode($code)
	{
		$this->vendorTxCode = $code;
	}
	
	
	/**
	 * formatData method
	 * Takes $this->data and converts it to
	 * a url encoded query string
	 * @return void
	 * @author Pete Robinson
	 **/
	private function formatData()
	{
		$arr = array();

		// loop through $this->data
		foreach($this->data as $key => $value){
			// assign as an item of $arr (field=value)
			$arr[] = $key . '='. urlencode($value);
		}

		// Implode the array using & as the glue and store the data
		$this->curl_str = implode('&', $arr);
	}



	/**
	 * execute method
	 * Executes the cURL request to SagePay and formats the result
	 *
	 * @return void
	 * @author Pete Robinson
	 **/
	public function execute()
	{
        // sets the url to post to based on ENV
		$this->setUrls();
        
		// Max exec time of 1 minute.
		set_time_limit(60);
        
		// Open cURL request
		$curl_session = curl_init();

		// Set the url to post request to
		curl_setopt ($curl_session, CURLOPT_URL, $this->url);
		// cURL params
		curl_setopt ($curl_session, CURLOPT_HEADER, 0);
		curl_setopt ($curl_session, CURLOPT_POST, 1);
		// Pass it the query string we created from $this->data earlier
		curl_setopt ($curl_session, CURLOPT_POSTFIELDS, $this->curl_str);
		// Return the result instead of print
		curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, 1);
		// Set a cURL timeout of 30 seconds
		curl_setopt($curl_session, CURLOPT_TIMEOUT,30);
		curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, FALSE);


		// Send the request and convert the return value to an array
		$response = preg_split('/$\R?^/m',curl_exec($curl_session));
		
		// Check that it actually reached the SagePay server
		// If it didn't, set the status as FAIL and the error as the cURL error
		if (curl_error($curl_session)){
			$this->status = 'FAIL';
			$this->error = curl_error($curl_session);
		}

		// Close the cURL session
		curl_close ($curl_session);
		
		// Turn the reponse into an associative array
		for ($i=0; $i < count($response); $i++){
			// Find position of first "=" character
			$splitAt = strpos($response[$i], "=");
			// Create an associative array
			$this->response[trim(substr($response[$i], 0, $splitAt))] = trim(substr($response[$i], ($splitAt+1)));
		}
		
		// Return values. Assign stuff based on the return 'Status' value from SagePay
		switch($this->response['Status']) {
			case 'OK':
				// Transactino made succssfully
				$this->status = 'success';
				$_SESSION['transaction']['VPSTxId'] = $this->response['VPSTxId']; // assign the VPSTxId to a session variable for storing if need be
				$_SESSION['transaction']['TxAuthNo'] = $this->response['TxAuthNo']; // assign the TxAuthNo to a session variable for storing if need be
				break;
			case '3DAUTH':
				// Transaction required 3D Secure authentication
				// The request will return two parameters that need to be passed with the 3D Secure
				$this->acsurl = $this->response['ACSURL']; // the url to request for 3D Secure
				$this->pareq = $this->response['PAReq']; // param to pass to 3D Secure
				$this->md = $this->response['MD']; // param to pass to 3D Secure
				$this->status = '3dAuth'; // set $this->status to '3dAuth' so your controller knows how to handle it
				break;
			case 'REJECTED':
				// errors for if the card is declined
				$this->status = 'declined';
				$this->error = 'Your payment was not authorised by your bank or your card details where incorrect.';
				break;
			case 'NOTAUTHED':
				// errors for if their card doesn't authenticate
				$this->status = 'notauthed';
				$this->error = 'Your payment was not authorised by your bank or your card details where incorrect.';
				break;
			case 'INVALID':
				// errors for if the user provides incorrect card data
				$this->status = 'invalid';
				$this->error = 'One or more of your card details where invalid. Please try again.';
				break;
			case 'FAIL':
				// errors for if the transaction fails for any reason
				$this->status = 'fail';
				$this->error = 'An unexpected error has occurred. Please try again.';
				break;
			default:
				// default error if none of the above conditions are met
				$this->status = 'error';
				$this->error = 'An error has occurred. Please try again.';
				break;
		}
	}
	
	
	/**
	 * formatRawData static method
	 * Takes the array from the form the user fills out
	 * and returns an array with the correct array keys assigned to each item
	 * 
	 * @param array $arr - the array of user data to process
	 * @return array
	 * @author Pete Robinson
	 **/
	public function formatRawData($arr)
	{
     	// this is test card details for SagePay, if we are testing, we don't want to use live card details
		if($this->env == 'DEVELOPMENT' && false) {
			// this is where the VendorTxCode is set. Once it's set here, don't set it anywhere else, use this one
			$data['VendorTxCode'] = 'prefix_' . time() . rand(0, 9999);
			$data['CardHolder'] = 'DELTA';
            $data['CardNumber'] = '4462000000000003';
			$data['StartDateMonth'] = '01';
			$data['StartDateYear'] = '06';
			$data['ExpiryDateMonth'] = '01';
			$data['ExpiryDateYear'] = '13';
			$data['CardType'] = 'VISA';
			$data['IssueNumber'] = '';
			$data['CV2'] = '123';
			$data['BillingFirstnames'] = 'Tester';
			$data['BillingSurname'] = 'Testing';
			$data['BillingAddress1'] = '88';
			$data['BillingAddress2'] = '432 Testing Road';
			$data['BillingCity'] = 'Test Town';
			$data['BillingCountry'] = 'GB';
			$data['BillingPostCode'] = '412';
			$data['Amount'] = $arr['Amount'];
		} else {
			// this is where the VendorTxCode is set. Once it's set here, don't set it anywhere else, use this one
			$data['VendorTxCode'] = 'prefix_' . time() . rand(0, 9999);
			
			// If you're using different names for your input fields for this data (card and billing address), use this section
			// to map the data to the array keys that SagePay expects. I've used the same keys so this piece of code is pretty much redundant
			$data['CardHolder'] = $arr['CardHolder'];
			$data['CardNumber'] = $arr['CardNumber'];
			$data['StartDateMonth'] = $arr['StartDateMonth'];
			$data['StartDateYear'] = $arr['StartDateYear'];
			$data['ExpiryDateMonth'] = $arr['ExpiryDateMonth'];
			$data['ExpiryDateYear'] = $arr['ExpiryDateYear'];
			$data['CardType'] = $arr['CardType'];
			$data['IssueNumber'] = $arr['IssueNumber'];
			$data['CV2'] = $arr['CV2'];
			$data['BillingFirstnames'] = $arr['BillingFirstnames'];
			$data['BillingSurname'] = $arr['BillingSurname'];
			$data['BillingAddress1'] = $arr['BillingAddress1'];
			$data['BillingAddress2'] = $arr['BillingAddress2'];
			$data['BillingCity'] = $arr['BillingCity'];
			$data['BillingCountry'] = $arr['BillingCountry'];
			$data['BillingPostCode'] = $arr['BillingPostCode'];
			$data['Amount'] = $arr['Amount'];
		}
		
		return $data;
	}
    

	public function getCountryCode( $country_name )
	{
		if( isset( $this->countryCodes[ $country_name ] ) ) return $this->countryCodes[ $country_name ];
	}
		
	public $countryCodes = array( 	'Afghanistan' => 'AF',
							'Aland Islands' => 'AX',
							'Albania' => 'AL',
							'Algeria' => 'DZ',
							'American Samoa' => 'AS',
							'Andorra' => 'AD',
							'Angola' => 'AO',
							'Anguilla' => 'AI',
							'Antarctica' => 'AQ',
							'Antigua and Barbuda' => 'AG',
							'Argentina' => 'AR',
							'Armenia' => 'AM',
							'Aruba' => 'AW',
							'Australia' => 'AU',
							'Austria' => 'AT',
							'Azerbaijan' => 'AZ',
							'Bahamas' => 'BS',
							'Bahrain' => 'BH',
							'Bangladesh' => 'BD',
							'Barbados' => 'BB',
							'Belarus' => 'BY',
							'Belgium' => 'BE',
							'Belize' => 'BZ',
							'Benin' => 'BJ',
							'Bermuda' => 'BM',
							'Bhutan' => 'BT',
							'Bolivia' => 'BO',
							'Bosnia and Herzegovina' => 'BA',
							'Botswana' => 'BW',
							'Bouvet Island' => 'BV',
							'Brazil' => 'BR',
							'British Indian Ocean Territory' => 'IO',
							'Brunei Darussalam' => 'BN',
							'Bulgaria' => 'BG',
							'Burkina Faso' => 'BF',
							'Burundi' => 'BI',
							'Cambodia' => 'KH',
							'Cameroon' => 'CM',
							'Canada' => 'CA',
							'Cape Verde' => 'CV',
							'Cayman Islands' => 'KY',
							'Central African Rep' => 'CF',
							'Chad' => 'TD',
							'Chile' => 'CL',
							'China' => 'CN',
							'Christmas Island' => 'CX',
							'Cocos (Keeling) Islands' => 'CC',
							'Colombia' => 'CO',
							'Comoros' => 'KM',
							'Congo' => 'CG',
							'Congo {Democratic Rep}' => 'CD',
							'Cook Islands' => 'CK',
							'Costa Rica' => 'CR',
							'Ivory Coast' => 'CI',
							'Croatia' => 'HR',
							'Cuba' => 'CU',
							'Cyprus' => 'CY',
							'Czech Republic' => 'CZ',
							'Denmark' => 'DK',
							'Djibouti' => 'DJ',
							'Dominica' => 'DM',
							'Dominican Republic' => 'DO',
							'Ecuador' => 'EC',
							'Egypt' => 'EG',
							'El Salvador' => 'SV',
							'Equatorial Guinea' => 'GQ',
							'Eritrea' => 'ER',
							'Estonia' => 'EE',
							'Ethiopia' => 'ET',
							'Falkland Islands (Malvinas)' => 'FK',
							'Faroe Islands' => 'FO',
							'Fiji' => 'FJ',
							'Finland' => 'FI',
							'France' => 'FR',
							'French Guiana' => 'GF',
							'French Polynesia' => 'PF',
							'French Southern Territories' => 'TF',
							'Gabon' => 'GA',
							'Gambia' => 'GM',
							'Georgia' => 'GE',
							'Germany' => 'DE',
							'Ghana' => 'GH',
							'Gibraltar' => 'GI',
							'Greece' => 'GR',
							'Greenland' => 'GL',
							'Grenada' => 'GD',
							'Guadeloupe' => 'GP',
							'Guam' => 'GU',
							'Guatemala' => 'GT',
							'Guernsey' => 'GG',
							'Guinea' => 'GN',
							'Guinea-Bissau' => 'GW',
							'Guyana' => 'GY',
							'Haiti' => 'HT',
							'Heard Island and Mcdonald Islands' => 'HM',
							'Holy See (Vatican City State)' => 'VA',
							'Honduras' => 'HN',
							'Hong Kong' => 'HK',
							'Hungary' => 'HU',
							'Iceland' => 'IS',
							'India' => 'IN',
							'Indonesia' => 'ID',
							'Iran' => 'IR',
							'Iraq' => 'IQ',
							'Ireland' => 'IE',
							'Isle of Man' => 'IM',
							'Israel' => 'IL',
							'Italy' => 'IT',
							'Jamaica' => 'JM',
							'Japan' => 'JP',
							'Jersey' => 'JE',
							'Jordan' => 'JO',
							'Kazakhstan' => 'KZ',
							'Kenya' => 'KE',
							'Kiribati' => 'KI',
							'Korea North' => 'KP',
							'Korea South' => 'KR',
							'Kuwait' => 'KW',
							'Kyrgyzstan' => 'KG',
							'Laos' => 'LA',
							'Latvia' => 'LV',
							'Lebanon' => 'LB',
							'Lesotho' => 'LS',
							'Liberia' => 'LR',
							'Libya' => 'LY',
							'Liechtenstein' => 'LI',
							'Lithuania' => 'LT',
							'Luxembourg' => 'LU',
							'Macao' => 'MO',
							'Macedonia' => 'MK',
							'Madagascar' => 'MG',
							'Malawi' => 'MW',
							'Malaysia' => 'MY',
							'Maldives' => 'MV',
							'Mali' => 'ML',
							'Malta' => 'MT',
							'Marshall Islands' => 'MH',
							'Martinique' => 'MQ',
							'Mauritania' => 'MR',
							'Mauritius' => 'MU',
							'Mayotte' => 'YT',
							'Mexico' => 'MX',
							'Micronesia' => 'FM',
							'Moldova' => 'MD',
							'Monaco' => 'MC',
							'Mongolia' => 'MN',
							'Montenegro' => 'ME', 
							'Montserrat' => 'MS',
							'Morocco' => 'MA',
							'Mozambique' => 'MZ',
							'Myanmar' => 'MM',
							'Namibia' => 'NA',
							'Nauru' => 'NR',
							'Nepal' => 'NP',
							'Netherlands' => 'NL',
							'Netherlands Antilles' => 'AN',
							'New Caledonia' => 'NC',
							'New Zealand' => 'NZ',
							'Nicaragua' => 'NI',
							'Niger' => 'NE',
							'Nigeria' => 'NG',
							'Niue' => 'NU',
							'Norfolk Island' => 'NF',
							'Northern Mariana Islands' => 'MP',
							'Norway' => 'NO',
							'Oman' => 'OM',
							'Pakistan' => 'PK',
							'Palau' => 'PW',
							'Palestinian Territory\, Occupied' => 'PS',
							'Panama' => 'PA',
							'Papua New Guinea' => 'PG',
							'Paraguay' => 'PY',
							'Peru' => 'PE',
							'Philippines' => 'PH',
							'Pitcairn' => 'PN',
							'Poland' => 'PL',
							'Portugal' => 'PT',
							'Puerto Rico' => 'PR',
							'Qatar' => 'QA',
							'Reunion' => 'RE',
							'Romania' => 'RO',
							'Russian Federation' => 'RU',
							'Rwanda' => 'RW',
							'Saint Helena' => 'SH',
							'Saint Kitts and Nevis' => 'KN',
							'Saint Lucia' => 'LC',
							'Saint Pierre and Miquelon' => 'PM',
							'Saint Vincent and The Grenadines' => 'VC',
							'Samoa' => 'WS',
							'San Marino' => 'SM',
							'Sao Tome and Principe' => 'ST',
							'Saudi Arabia' => 'SA',
							'Senegal' => 'SN',
							'Serbia' => 'RS',
							'Seychelles' => 'SC',
							'Sierra Leone' => 'SL',
							'Singapore' => 'SG',
							'Slovakia' => 'SK',
							'Slovenia' => 'SI',
							'Solomon Islands' => 'SB',
							'Somalia' => 'SO',
							'South Africa' => 'ZA',
							'South Georgia and The South Sandwich Islands' => 'GS',
							'Spain' => 'ES',
							'Sri Lanka' => 'LK',
							'Sudan' => 'SD',
							'Suriname' => 'SR',
							'Svalbard and Jan Mayen' => 'SJ',
							'Swaziland' => 'SZ',
							'Sweden' => 'SE',
							'Switzerland' => 'CH',
							'Syria' => 'SY',
							'Taiwan' => 'TW',
							'Tajikistan' => 'TJ',
							'Tanzania' => 'TZ',
							'Thailand' => 'TH',
							'Timor-Leste' => 'TL',
							'Togo' => 'TG',
							'Tokelau' => 'TK',
							'Tonga' => 'TO',
							'Trinidad and Tobago' => 'TT',
							'Tunisia' => 'TN',
							'Turkey' => 'TR',
							'Turkmenistan' => 'TM',
							'Turks and Caicos Islands' => 'TC',
							'Tuvalu' => 'TV',
							'Uganda' => 'UG',
							'Ukraine' => 'UA',
							'United Arab Emirates' => 'AE',
							'United Kingdom' => 'GB',
							'United States' => 'US',
							'United States Minor Outlying Islands' => 'UM',
							'Uruguay' => 'UY',
							'Uzbekistan' => 'UZ',
							'Vanuatu' => 'VU',
							'Venezuela' => 'VE',
							'Viet Nam' => 'VN',
							'Virgin Islands\, British' => 'VG',
							'Virgin Islands\, U.S.' => 'VI',
							'Wallis and Futuna' => 'WF',
							'Western Sahara' => 'EH',
							'Yemen' => 'YE',
							'Zambia' => 'ZM',
							'Zimbabwe' => 'ZW' );	    
	
}

/**
 * SecureAuth class
 * Handles the integration with 3dSecure
 *
 * @package Payment
 * @author Pete Robinson
 * @version 1.0
 **/
class SecureAuth
{
	
	public
		$vendorTxCode = '',	// vendor transaction code. must be unqiue
		$status = '',		// status returned from the cURL request
		$error = '';		// stores any errors
	private
		$md = '',			// param received from SagePay to pass with the 3D Secure request
		$pareq = '',		// param received from SagePay to pass with the 3D Secure request
		$data = array(),	// the data to post to the 3D Secure server
		$response = '',		// the response from the server
		$url = '',			// the url to pos the cURL request to
		$env = '',			// the environment, set according to 'ENV' site constant
		$curl_str = '';		// the url encoded string derrived from the $this->data array
	
	
	/**
	 * Constructor method
	 * Sets the $this->env property, assigns the necessary urls,
	 * sets and formats the data to pass to 3D Secure
	 * @return void
	 * @param arr $data - the data provided by the user (billing, price and card)
	 * @author Pete Robinson
	 **/
	public function __construct($data)
	{
		$this->data = $data;
		
		$this->env = ENV;
		$this->setUrls();
		$this->formatData();
		$this->execute();
	}
	
	/**
	 * setUrls method
	 * Selects which SagePay url to use (live or test)
	 * based on the $this->env property
	 * @return void
	 * @author Pete Robinson
	 **/
	private function setUrls()
	{
		$this->url = ($this->env == 'DEVELOPMENT') ? 'https://test.sagepay.com/gateway/service/direct3dcallback.vsp' : 'https://live.sagepay.com/gateway/service/direct3dcallback.vsp';
	}
	
	
	/**
	 * formatData method
	 * Takes $this->data and converts it to
	 * a url encoded query string
	 * @return void
	 * @author Pete Robinson
	 **/
	private function formatData()
	{
		// Initialise arr variable
		$str = array();

		// Step through the fields
		foreach($this->data as $key => $value){
			// Stick them together as key=value pairs (url encoded)
			$str[] = $key . '=' . urlencode($value);
		}

		// Implode the arry using & as the glue and store the data
		$this->curl_str = implode('&', $str);
	}
	
	
	/**
	 * execute method
	 * Executes the cURL request to SagePay and formats the result
	 *
	 * @return void
	 * @author Pete Robinson
	 **/
	private function execute()
	{
		// Max exec time of 1 minute.
		set_time_limit(60);
		// Open cURL request
		$curl_session = curl_init();

		// Set the url to post request to
		curl_setopt ($curl_session, CURLOPT_URL, $this->url);
		// cURL params
		curl_setopt ($curl_session, CURLOPT_HEADER, 0);
		curl_setopt ($curl_session, CURLOPT_POST, 1);
		// Pass it the query string we created from $this->data earlier
		curl_setopt ($curl_session, CURLOPT_POSTFIELDS, $this->curl_str);
		// Return the result instead of print
		curl_setopt($curl_session, CURLOPT_RETURNTRANSFER,1); 
		// Set a cURL timeout of 30 seconds
		curl_setopt($curl_session, CURLOPT_TIMEOUT,30); 
		curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		// Send the request and convert the return value to an array
		$response = preg_split('/$\R?^/m',curl_exec($curl_session));
		
		// Check that it actually reached the SagePay server
		// If it didn't, set the status as FAIL and the error as the cURL error
		if (curl_error($curl_session)){
			$this->status = 'FAIL';
			$this->error = curl_error($curl_session);
		}

		// Close the cURL session
		curl_close ($curl_session);
		
		// Turn the response into an associative array
		for ($i=0; $i < count($response); $i++) {
			// Find position of first "=" character
			$splitAt = strpos($response[$i], '=');
			// Create an associative array
			$this->response[trim(substr($response[$i], 0, $splitAt))] = trim(substr($response[$i], ($splitAt+1)));
		}
		
		// Return values. Assign stuff based on the return 'Status' value from SagePay
		switch($this->response['Status']) {
			case 'OK':
				// Transactino made succssfully
				$this->status = 'success';
				$_SESSION['transaction']['VPSTxId'] = $this->response['VPSTxId']; // assign the VPSTxId to a session variable for storing if need be
				$_SESSION['transaction']['TxAuthNo'] = $this->response['TxAuthNo']; // assign the TxAuthNo to a session variable for storing if need be
				break;
			case '3DAUTH':
				// Transaction required 3D Secure authentication
				// The request will return two parameters that need to be passed with the 3D Secure
				$this->acsurl = $this->response['ACSURL']; // the url to request for 3D Secure
				$this->pareq = $this->response['PAReq']; // param to pass to 3D Secure
				$this->md = $this->response['MD']; // param to pass to 3D Secure
				$this->status = '3dAuth'; // set $this->status to '3dAuth' so your controller knows how to handle it
				break;
			case 'REJECTED':
				// errors for if the card is declined
				$this->status = 'declined';
				$this->error = 'Your payment was not authorised by your bank or your card details where incorrect.';
				break;
			case 'NOTAUTHED':
				// errors for if their card doesn't authenticate
				$this->status = 'notauthed';
				$this->error = 'Your payment was not authorised by your bank or your card details where incorrect.';
				break;
			case 'INVALID':
				// errors for if the user provides incorrect card data
				$this->status = 'invalid';
				$this->error = 'One or more of your card details where invalid. Please try again.';
				break;
			case 'FAIL':
				// errors for if the transaction fails for any reason
				$this->status = 'fail';
				$this->error = 'An unexpected error has occurred. Please try again.';
				break;
			default:
				// default error if none of the above conditions are met
				$this->status = 'error';
				$this->error = 'An error has occurred. Please try again.';
				break;
		}
		
		// set error sessions if the request failed or was declined to be handled by controller
		if($this->status != 'success') {
			$_SESSION['error']['status'] = $this->status;
			$_SESSION['error']['description'] = $this->error;
		}
	}
}
?>
