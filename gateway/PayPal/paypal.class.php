<?php

class payPalGateway
{
	public $environment 	= 'sandbox';	// or 'beta-sandbox' or 'live'
	public $currencyCode 	= 'USD';
	public $paymentTypes 	= array( 'Authorization', 'Sale', 'Order' );

	public $API_username;
	public $API_password;
	public $API_signature;

	public $error;
	public $messages = array();

	public $paymentSuccessful = false;

	public function __construct( $API_username = null, $API_password = null, $API_signature = null )
	{
		if( $API_username ) $this->API_username = $API_username;
		if( $API_password ) $this->API_password = $API_password;
		if( $API_signature ) $this->API_signature = $API_signature;
	}

	public function getDirectPaymentRequestTemplate()
	{
		return (object) array( 	'paymentType' => 'Sale',
								'userIPAddress' => $_SERVER['REMOTE_ADDR'],
								'cardType' => '',
								'cardNumber' => '',
								'cvv2Number' => '',
								'expiryMonth' => '',
								'expiryYear' => '',
								'amount' => '',
								'currencyID' => $this->currencyCode,
								'description' => 'Website Payment',
								'firstName' => '',
								'lastName' => '',
								'addressLineOne' => '',
								'addressLineTwo' => '',
								'state' => '',
								'country' => '',
								'city' => '',
								'phoneNumber' => '',
								'postalCode' => '' );
	}

	public function urlEncodePaymentDetails( $paymentRequest )
	{
		if( is_object( $paymentRequest ) )
			$paymentRequest = (array) $paymentRequest;

		foreach( $paymentRequest as $property => $value )
			$paymentRequest[ $property ] = urlencode( $value );


		return (object) $paymentRequest;
	}

	public function doDirectPayment( $paymentRequest = null )
	{
		if( !$paymentRequest )
		{
			$this->messages[] = 'No payment request details given';
			return;
		}

		$paymentRequest = $this->urlEncodePaymentDetails( $paymentRequest );

		// Set request-specific fields.
		$paymentType 		= $paymentRequest->paymentType;
		$firstName 			= $paymentRequest->firstName;
		$lastName 			= $paymentRequest->lastName;
		$creditCardType 	= $paymentRequest->cardType;
		$creditCardNumber 	= $paymentRequest->cardNumber;
		$expDateMonth 		= $paymentRequest->expiryMonth;
		$ipAddress			= $paymentRequest->userIPAddress;

		// Month must be padded with leading zero
		$padDateMonth 		= urlencode( str_pad( $expDateMonth, 2, '0', STR_PAD_LEFT ) );

		$expDateYear 		= $paymentRequest->expiryYear;
		$cvv2Number 		= $paymentRequest->cvv2Number;
		$address1 			= $paymentRequest->addressLineOne;
		$address2 			= $paymentRequest->addressLineTwo;
		$city 				= $paymentRequest->city;
		$state 				= $paymentRequest->state;
		$zip 				= $paymentRequest->postalCode;
		$country 			= $this->getCountryCode( urldecode( $paymentRequest->country ) );
		$amount 			= $paymentRequest->amount;
		$currencyID 		= $paymentRequest->currencyID;

		// Add request-specific fields to the request string.
		$nvpStr =	"&PAYMENTACTION=$paymentType&IPADDRESS=$ipAddress&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
					"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
					"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";

		// echo $nvpStr;

		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = $this->PPHttpPost( 'DoDirectPayment', $nvpStr );

		if( $this->error ) return;

		switch( strtoupper( $httpParsedResponseAr["ACK"] ) )
		{
			case 'SUCCESS':
			case 'SUCCESSWITHWARNING':

				$this->paymentSuccessful = true;

			default:

				$this->error = true;
				$this->messages[] = urldecode( @$httpParsedResponseAr['L_LONGMESSAGE0'] );
		}


	}

	public function getExpressCheckout( $token )
	{
		$token = urlencode(htmlspecialchars($_REQUEST['token']));

		// Add request-specific fields to the request string.
		$nvpStr = "&TOKEN=$token";

		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = $this->PPHttpPost( 'GetExpressCheckoutDetails', $nvpStr );

		if( $this->error ) return;

		switch( strtoupper( $httpParsedResponseAr["ACK"] ) )
		{
			case 'SUCCESS':
			case 'SUCCESSWITHWARNING':

				// Extract the response details.
				$payerID = urldecode( $httpParsedResponseAr['PAYERID'] );
				$first_name = urldecode( $httpParsedResponseAr["FIRSTNAME"] );
				$last_name = urldecode( $httpParsedResponseAr["LASTNAME"] );
				$ship_to_name = urldecode( $httpParsedResponseAr["SHIPTONAME"] );
				$street1 = urldecode( $httpParsedResponseAr["SHIPTOSTREET"] );
				$street1 = urldecode( $httpParsedResponseAr["SHIPTOSTREET"] );

				if( array_key_exists("SHIPTOSTREET2", $httpParsedResponseAr) )
					$street2 = urldecode( $httpParsedResponseAr["SHIPTOSTREET2"] );
				else
					$street2 = '';

				$city_name = urldecode( $httpParsedResponseAr["SHIPTOCITY"] );
				$state_province = urldecode( $httpParsedResponseAr["SHIPTOSTATE"] );
				$postal_code = urldecode( $httpParsedResponseAr["SHIPTOZIP"] );
				$country_code = urldecode( $httpParsedResponseAr["SHIPTOCOUNTRYCODE"] );
				$country_name = urldecode( $httpParsedResponseAr["SHIPTOCOUNTRYNAME"] );

				return (object) array( 	'payerID' => $payerID,
										'firstName' => $first_name,
										'lastName' => $last_name,
										'shippingName' => $ship_to_name,
										'addressLineOne' => $street1,
										'addressLineTwo' => $street2,
										'city' => $city_name,
										'state' => $state_province,
										'postalCode' => $postal_code,
										'countryCode' => $country_code,
										'countryName' => $country_name );

			default:

				$this->error = true;
				$this->messages[] = urldecode( @$httpParsedResponseAr['L_LONGMESSAGE0'] );
		}

		return false;
	}

	public function setExpressCheckout( $amount, $return_url, $cancel_url, $payment_type = 'Authorization', $currency_code = null )
	{
		if( !$currency_code ) $currency_code = urlencode( $this->currencyCode );

		// Set request-specific fields.
		$paymentAmount 	= urlencode( $amount );
		$currencyID 	= urlencode( $currency_code );
		$paymentType 	= urlencode( $payment_type );
		$returnURL 		= urlencode( $return_url );
		$cancelURL 		= urlencode( $cancel_url );

		// Add request-specific fields to the request string.
		$nvpStr = "&Amt=$paymentAmount&ReturnUrl=$returnURL&CANCELURL=$cancelURL&PAYMENTACTION=$paymentType&CURRENCYCODE=$currencyID";

		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = $this->PPHttpPost( 'SetExpressCheckout', $nvpStr );

		if( $this->error ) return;

		switch( strtoupper( $httpParsedResponseAr["ACK"] ) )
		{
			case 'SUCCESS':
			case 'SUCCESSWITHWARNING':

				// Redirect to paypal.com.
				$token = urldecode( $httpParsedResponseAr["TOKEN"] );

				$payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token&useraction=commit";

				if("sandbox" === $this->environment || "beta-sandbox" === $this->environment) {
					$payPalURL = "https://www.{$this->environment}.paypal.com/webscr&cmd=_express-checkout&token=$token&useraction=commit";
				}

				header("Location: $payPalURL");

				exit;

			default:

				$this->error = true;
				$this->messages[] = urldecode( @$httpParsedResponseAr['L_LONGMESSAGE0'] );
		}
	}


	function doExpressCheckout( $amount, $token, $payer_id, $payment_type = 'Sale', $currency_code = null  )
	{
		if( !$currency_code ) $currency_code = urlencode( $this->currencyCode );

		$payerID 		= urlencode( $payer_id );
		$token 			= urlencode( $token );
		$paymentType 	= urlencode( $payment_type );
		$paymentAmount 	= urlencode( $amount );
		$currencyID 	= urlencode( $currency_code );

		// Add request-specific fields to the request string.
		$nvpStr = "&TOKEN=$token&PAYERID=$payerID&PAYMENTACTION=$paymentType&AMT=$paymentAmount&CURRENCYCODE=$currencyID";

		// Execute the API operation; see the PPHttpPost function above.
		$httpParsedResponseAr = $this->PPHttpPost( 'DoExpressCheckoutPayment', $nvpStr );

		if( $this->error ) return;

		switch( strtoupper( $httpParsedResponseAr["ACK"] ) )
		{
			case 'SUCCESS':
			case 'SUCCESSWITHWARNING':

				$this->paymentSuccessful = true;

				return;

			default:

				$this->error = true;
				$this->messages[] = urldecode( @$httpParsedResponseAr['L_LONGMESSAGE0'] );
		}
	}

	function PPHttpPost( $methodName_, $nvpStr_ )
	{
		// Set up your API credentials, PayPal end point, and API version.
		$this->API_username = urlencode( $this->API_username );
		$this->API_password = urlencode( $this->API_password );
		$this->API_signature = urlencode( $this->API_signature );

		switch( $this->environment )
		{
			case 'sandbox':
			case 'beta-sandbox':

				$API_Endpoint = "https://api-3t.{$this->environment}.paypal.com/nvp";
				break;

			default:

				$API_Endpoint = "https://api-3t.paypal.com/nvp";
		}

		$version = urlencode('51.0');

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD={$this->API_password}&USER={$this->API_username}&SIGNATURE={$this->API_signature}$nvpStr_";

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// Get response from the server.
		$httpResponse = curl_exec($ch);

		if( !$httpResponse )
		{
			$this->error = true;
			$this->messages[] = curl_error($ch).'('.curl_errno($ch).')';
			return;
		}

		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}

		if( ( 0 == sizeof( $httpParsedResponseAr ) ) || !array_key_exists( 'ACK', $httpParsedResponseAr ) )
		{
			$this->error = true;
			$this->messages[] = "Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.";
			return;
		}

		return $httpParsedResponseAr;
	}

	function getCountryCode( $country_name )
	{
		if( isset( $this->countryCodes[ $country_name ] ) ) return $this->countryCodes[ $country_name ];
	}

	public $countryCodes = array( 	'Afghanistan' => 'AF',
							'Aland Islands' => 'AX',
							'Albania' => 'AL',
							'Algeria' => 'DZ',
							'American Samoa' => 'AS',
							'Andorra' => 'AD',
							'Angola' => 'AO',
							'Anguilla' => 'AI',
							'Antarctica' => 'AQ',
							'Antigua and Barbuda' => 'AG',
							'Argentina' => 'AR',
							'Armenia' => 'AM',
							'Aruba' => 'AW',
							'Australia' => 'AU',
							'Austria' => 'AT',
							'Azerbaijan' => 'AZ',
							'Bahamas' => 'BS',
							'Bahrain' => 'BH',
							'Bangladesh' => 'BD',
							'Barbados' => 'BB',
							'Belarus' => 'BY',
							'Belgium' => 'BE',
							'Belize' => 'BZ',
							'Benin' => 'BJ',
							'Bermuda' => 'BM',
							'Bhutan' => 'BT',
							'Bolivia' => 'BO',
							'Bosnia and Herzegovina' => 'BA',
							'Botswana' => 'BW',
							'Bouvet Island' => 'BV',
							'Brazil' => 'BR',
							'British Indian Ocean Territory' => 'IO',
							'Brunei Darussalam' => 'BN',
							'Bulgaria' => 'BG',
							'Burkina Faso' => 'BF',
							'Burundi' => 'BI',
							'Cambodia' => 'KH',
							'Cameroon' => 'CM',
							'Canada' => 'CA',
							'Cape Verde' => 'CV',
							'Cayman Islands' => 'KY',
							'Central African Rep' => 'CF',
							'Chad' => 'TD',
							'Chile' => 'CL',
							'China' => 'CN',
							'Christmas Island' => 'CX',
							'Cocos (Keeling) Islands' => 'CC',
							'Colombia' => 'CO',
							'Comoros' => 'KM',
							'Congo' => 'CG',
							'Congo {Democratic Rep}' => 'CD',
							'Cook Islands' => 'CK',
							'Costa Rica' => 'CR',
							'Ivory Coast' => 'CI',
							'Croatia' => 'HR',
							'Cuba' => 'CU',
							'Cyprus' => 'CY',
							'Czech Republic' => 'CZ',
							'Denmark' => 'DK',
							'Djibouti' => 'DJ',
							'Dominica' => 'DM',
							'Dominican Republic' => 'DO',
							'Ecuador' => 'EC',
							'Egypt' => 'EG',
							'El Salvador' => 'SV',
							'Equatorial Guinea' => 'GQ',
							'Eritrea' => 'ER',
							'Estonia' => 'EE',
							'Ethiopia' => 'ET',
							'Falkland Islands (Malvinas)' => 'FK',
							'Faroe Islands' => 'FO',
							'Fiji' => 'FJ',
							'Finland' => 'FI',
							'France' => 'FR',
							'French Guiana' => 'GF',
							'French Polynesia' => 'PF',
							'French Southern Territories' => 'TF',
							'Gabon' => 'GA',
							'Gambia' => 'GM',
							'Georgia' => 'GE',
							'Germany' => 'DE',
							'Ghana' => 'GH',
							'Gibraltar' => 'GI',
							'Greece' => 'GR',
							'Greenland' => 'GL',
							'Grenada' => 'GD',
							'Guadeloupe' => 'GP',
							'Guam' => 'GU',
							'Guatemala' => 'GT',
							'Guernsey' => 'GG',
							'Guinea' => 'GN',
							'Guinea-Bissau' => 'GW',
							'Guyana' => 'GY',
							'Haiti' => 'HT',
							'Heard Island and Mcdonald Islands' => 'HM',
							'Holy See (Vatican City State)' => 'VA',
							'Honduras' => 'HN',
							'Hong Kong' => 'HK',
							'Hungary' => 'HU',
							'Iceland' => 'IS',
							'India' => 'IN',
							'Indonesia' => 'ID',
							'Iran' => 'IR',
							'Iraq' => 'IQ',
							'Ireland' => 'IE',
							'Isle of Man' => 'IM',
							'Israel' => 'IL',
							'Italy' => 'IT',
							'Jamaica' => 'JM',
							'Japan' => 'JP',
							'Jersey' => 'JE',
							'Jordan' => 'JO',
							'Kazakhstan' => 'KZ',
							'Kenya' => 'KE',
							'Kiribati' => 'KI',
							'Korea North' => 'KP',
							'Korea South' => 'KR',
							'Kuwait' => 'KW',
							'Kyrgyzstan' => 'KG',
							'Laos' => 'LA',
							'Latvia' => 'LV',
							'Lebanon' => 'LB',
							'Lesotho' => 'LS',
							'Liberia' => 'LR',
							'Libya' => 'LY',
							'Liechtenstein' => 'LI',
							'Lithuania' => 'LT',
							'Luxembourg' => 'LU',
							'Macao' => 'MO',
							'Macedonia' => 'MK',
							'Madagascar' => 'MG',
							'Malawi' => 'MW',
							'Malaysia' => 'MY',
							'Maldives' => 'MV',
							'Mali' => 'ML',
							'Malta' => 'MT',
							'Marshall Islands' => 'MH',
							'Martinique' => 'MQ',
							'Mauritania' => 'MR',
							'Mauritius' => 'MU',
							'Mayotte' => 'YT',
							'Mexico' => 'MX',
							'Micronesia' => 'FM',
							'Moldova' => 'MD',
							'Monaco' => 'MC',
							'Mongolia' => 'MN',
							'Montenegro' => 'ME',
							'Montserrat' => 'MS',
							'Morocco' => 'MA',
							'Mozambique' => 'MZ',
							'Myanmar' => 'MM',
							'Namibia' => 'NA',
							'Nauru' => 'NR',
							'Nepal' => 'NP',
							'Netherlands' => 'NL',
							'Netherlands Antilles' => 'AN',
							'New Caledonia' => 'NC',
							'New Zealand' => 'NZ',
							'Nicaragua' => 'NI',
							'Niger' => 'NE',
							'Nigeria' => 'NG',
							'Niue' => 'NU',
							'Norfolk Island' => 'NF',
							'Northern Mariana Islands' => 'MP',
							'Norway' => 'NO',
							'Oman' => 'OM',
							'Pakistan' => 'PK',
							'Palau' => 'PW',
							'Palestinian Territory\, Occupied' => 'PS',
							'Panama' => 'PA',
							'Papua New Guinea' => 'PG',
							'Paraguay' => 'PY',
							'Peru' => 'PE',
							'Philippines' => 'PH',
							'Pitcairn' => 'PN',
							'Poland' => 'PL',
							'Portugal' => 'PT',
							'Puerto Rico' => 'PR',
							'Qatar' => 'QA',
							'Reunion' => 'RE',
							'Romania' => 'RO',
							'Russian Federation' => 'RU',
							'Rwanda' => 'RW',
							'Saint Helena' => 'SH',
							'Saint Kitts and Nevis' => 'KN',
							'Saint Lucia' => 'LC',
							'Saint Pierre and Miquelon' => 'PM',
							'Saint Vincent and The Grenadines' => 'VC',
							'Samoa' => 'WS',
							'San Marino' => 'SM',
							'Sao Tome and Principe' => 'ST',
							'Saudi Arabia' => 'SA',
							'Senegal' => 'SN',
							'Serbia' => 'RS',
							'Seychelles' => 'SC',
							'Sierra Leone' => 'SL',
							'Singapore' => 'SG',
							'Slovakia' => 'SK',
							'Slovenia' => 'SI',
							'Solomon Islands' => 'SB',
							'Somalia' => 'SO',
							'South Africa' => 'ZA',
							'South Georgia and The South Sandwich Islands' => 'GS',
							'Spain' => 'ES',
							'Sri Lanka' => 'LK',
							'Sudan' => 'SD',
							'Suriname' => 'SR',
							'Svalbard and Jan Mayen' => 'SJ',
							'Swaziland' => 'SZ',
							'Sweden' => 'SE',
							'Switzerland' => 'CH',
							'Syria' => 'SY',
							'Taiwan' => 'TW',
							'Tajikistan' => 'TJ',
							'Tanzania' => 'TZ',
							'Thailand' => 'TH',
							'Timor-Leste' => 'TL',
							'Togo' => 'TG',
							'Tokelau' => 'TK',
							'Tonga' => 'TO',
							'Trinidad and Tobago' => 'TT',
							'Tunisia' => 'TN',
							'Turkey' => 'TR',
							'Turkmenistan' => 'TM',
							'Turks and Caicos Islands' => 'TC',
							'Tuvalu' => 'TV',
							'Uganda' => 'UG',
							'Ukraine' => 'UA',
							'United Arab Emirates' => 'AE',
							'United Kingdom' => 'GB',
							'United States' => 'US',
							'United States Minor Outlying Islands' => 'UM',
							'Uruguay' => 'UY',
							'Uzbekistan' => 'UZ',
							'Vanuatu' => 'VU',
							'Venezuela' => 'VE',
							'Viet Nam' => 'VN',
							'Virgin Islands\, British' => 'VG',
							'Virgin Islands\, U.S.' => 'VI',
							'Wallis and Futuna' => 'WF',
							'Western Sahara' => 'EH',
							'Yemen' => 'YE',
							'Zambia' => 'ZM',
							'Zimbabwe' => 'ZW' );

}
?>
