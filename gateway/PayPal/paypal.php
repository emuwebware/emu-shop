<?php

/*
Emu Payment Gateway: PayPal
Emu Payment Gateway Description: PayPal Payments Pro
*/

global $emuShop;
global $api_username, $api_password, $api_signature;

$PayPalSettings = $emuShop->settings->createSettingsGroup( 'PayPalPro', 'PayPal Payments Pro Settings' );

$PayPalSettings->addSetting( 'apiUsername', '', 'API Username', 1 );
$PayPalSettings->addSetting( 'apiPassword', '', 'API Password', 2, 'password' );
$PayPalSettings->addSetting( 'apiSignature', '', 'API Signature', 3 );
$PayPalSettings->addSetting( 'apiMode', 'sandbox', 'API Mode', 4, array( 'sandbox' => 'Sandbox', 'live' => 'Live' ) );

$emuShop->settings->addSettingsGroup( $PayPalSettings );
$emuShop->settings->saveSettings();

$api_username 	= $PayPalSettings->getSettingValue( 'apiUsername' );
$api_password 	= $PayPalSettings->getSettingValue( 'apiPassword' );
$api_signature 	= $PayPalSettings->getSettingValue( 'apiSignature' );

if( !class_exists( 'payPalGateway' ) )
	include_once( $emuShop->getManager('gateway')->paymentGatewayDir.'/PayPal/paypal.class.php' );

global $emuShopPayPalGateway;

$emuShopPayPalGateway = new payPalGateway( $api_username, $api_password, $api_signature );
$emuShopPayPalGateway->environment = $PayPalSettings->getSetting( 'apiMode' );


// Add the express checkout button to the basket and payment pages
add_filter( 'emu_basket_content', 'add_shop_express_checkout_button' );
add_filter( 'emu_shop_payment_content', 'add_shop_express_checkout_button' );

// Add the code to run the express checkout API
add_action( 'emu_shop_emuBasketProcessor_post_process', 'do_shop_express_checkout', 1, 0 );
add_action( 'emu_shop_emuConfirmationProcessor_post_process', 'do_shop_express_checkout', 1, 0 );

// Add the code to incorporate details from express checkout process into confirmation (and basket)
add_action( 'emu_shop_basket_preload', 'add_shop_express_details', 1, 1 );

// Add doExpressPayment functionality to confirmation processing
add_action( 'emu_shop_emuPaymentProcessor_pre_process', 'do_shop_express_payment', 1, 0 );

add_action( 'emu_shop_make_payment', 'do_shop_direct_payment', 1, 2 );

add_filter( 'emu_shop_payment_result', 'update_shop_payment_result' );
add_filter( 'emu_shop_payment_messages', 'update_shop_payment_messages' );
add_filter( 'emuS_template_manager_get_template', 'show_shop_paypal_confirmation', 1, 3 );
add_filter( 'emu_shop_get_payment_types', 'set_shop_paypal_payment_types', 1, 1 );

function set_shop_paypal_payment_types( $payment_types )
{
	return array( 'Amex', 'Visa', 'Mastercard' );
}

function show_shop_paypal_confirmation( $template_content, $filename, $type )
{
	if( $filename != 'payment.htm' ) return $template_content;

	global $emuShop;

	$basket = $emuShop->getBasket();

	if( !$token = $basket->getSessionData( 'token' ) ) return $template_content;

	$template = $emuShop->getManager( 'template' );

	$template_content = $template->fillTemplateSections( $template_content, array( 'existing payment method' => '<p>Click <strong><em>Make Payment</em></strong> to place your order.</p>' ) );
	$template_content = $template->fillTemplate( $template_content, array( 'payment form class' => 'emu-hidden' ) );

	return $template_content;
}

function do_shop_direct_payment( $order, $paymentMethod )
{
	global $emuShop;
	global $emuShopPayPalGateway;

	$request = $emuShopPayPalGateway->getDirectPaymentRequestTemplate();

	$request->cardNumber = $paymentMethod->cardNumber;
	$request->cardType = $paymentMethod->paymentType;
	$request->cvv2Number = $paymentMethod->cv2Number;
	$request->expiryMonth = $paymentMethod->expiryMonth;
	$request->expiryYear = $paymentMethod->expiryYear;
	$request->amount = $order->getGrandTotalValue();

	$billing_address = $order->getBillingAddress();

	$request->firstName = $billing_address->firstName;
	$request->lastName = $billing_address->lastName;
	$request->addressLineOne = $billing_address->addressLineOne;
	$request->addressLineTwo = $billing_address->addressLineTwo;
	$request->state = $billing_address->stateName;
	$request->country = $billing_address->countryName;
	$request->city = $billing_address->city;
	$request->phoneNumber = $billing_address->phoneNumber;
	$request->postalCode = $billing_address->postalCode;

	$emuShopPayPalGateway->doDirectPayment( $request );

}

function update_shop_payment_result( $result )
{
	global $emuShopPayPalGateway;
	return $emuShopPayPalGateway->paymentSuccessful;
}

function update_shop_payment_messages( $messages )
{
	global $emuShopPayPalGateway;
	return array_merge( $messages, $emuShopPayPalGateway->messages );
}

function do_shop_express_payment()
{
	global $emuShop;
	global $emuShopPayPalGateway;

	$basket = $emuShop->getBasket();

	$order = $emuShop->getInstance( 'emuOrder' );
	$order->prepareFromBasket( $basket );

	if( !$token = $basket->getSessionData( 'token' ) ) return;

	$payer_id = $basket->getSessionData( 'payerID' );

	$emuShopPayPalGateway->doExpressCheckout( $order->getGrandTotalValue(), $token, $payer_id );

	if( $emuShopPayPalGateway->error )
	{
		print_r( $emuShopPayPalGateway->messages );
		exit();
	}

	// otherwise payment went fine so generate the order

	if( $emuShopPayPalGateway->paymentSuccessful )
	{
		$order->addPayment( 'PayPal', $order->getGrandTotalValue() );
		$order->generate();

		$basket->emptyBasket();

		$location = $emuShop->pageManager->pages->thankyou->url;

		header( 'Location: '.$location );
		exit();
	}
}

function add_shop_express_details( $basket )
{
	global $emuShopPayPalGateway;
	global $emuShop;
	global $post;

	if( $token = get_val( 'token' ) )
	{
		$payer_id = get_val( 'PayerID' );
		$basket->setSessionData( array( 'token' => $token, 'payerID' => $payer_id ) );
	}

	if(	!( @$post->ID == $emuShop->pageManager->pages->delivery->pageID && is_user_logged_in() ) ) return;

	if( !$token = $basket->getSessionData( 'token' ) ) return;

	if( $details = $emuShopPayPalGateway->getExpressCheckout( $token ) )
	{
		// Create the billing address
		$customer = $basket->customer;
		$address = $emuShop->getInstance( 'emuAddress' );
		$address->customerID = $customer->dbID;

		$address->firstName = $details->firstName;
		$address->lastName = $details->lastName;
		$address->addressLineOne = 'via PayPal';

		$basket->setBillingAddress( $address );

		// Create the shipping address
		$address = $emuShop->getInstance( 'emuAddress' );
		$address->customerID = $customer->dbID;

		$address->firstName = $details->shippingName;
		$address->addressLineOne = $details->addressLineOne;
		$address->addressLineTwo = $details->addressLineTwo;

		// see if we can find the country
		$countries = $emuShop->getManager('region')->getCountries( array( 'name' => $details->countryName ) );

		if( count( $countries ) == 0 )
			$country_id = 1; // default US
		else
			$country_id = $countries[0]->ID;

		$address->state = $details->state;
		$address->countryName = $details->countryName;
		$address->stateName = $details->state;
		$address->countryID = $country_id;
		$address->city = $details->city;
		$address->postalCode = $details->postalCode;

		$basket->setShippingAddress( $address );
	}
	else
	{
		print_r( $emuShopPayPalGateway->messages );
		exit();
	}
}

function do_shop_express_checkout()
{
	global $emuShopPayPalGateway;
	global $emuShop;

	$basket = $emuShop->getBasket();

	if( !post_val( 'ExpressCheckout' ) )
	{
		if( $basket->getSessionData( 'token' ) )
		{
			// They longer want to use paypal so clear the paypal data
			$basket->setSessionData( array( 'token' => null, 'payerID' => null, 'shipping_address' => null, 'billing_address' => null ) );
		}
		return;
	}

	$emuShopPayPalGateway->setExpressCheckout( $basket->getGrandTotal(), $emuShop->pageManager->pages->delivery->url, $emuShop->pageManager->pages->basket->url );

	if( $emuShopPayPalGateway->error )
	{
		print_r( $emuShopPayPalGateway->messages );
		exit();
	}

}


function add_shop_express_checkout_button( $content )
{
	global $emuShop;
	global $post;

	if( ! in_array( @$post->ID, array( $emuShop->pageManager->pages->basket->pageID, $emuShop->pageManager->pages->payment->pageID ) ) ) return $content;

	$content .= '<form method="post" action="" target="_blank"/>'.
				'<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" name="Checkout with PayPal" />'.
				'<input type="hidden" name="ExpressCheckout" value="ExpressCheckout" />'.
				'<input type="hidden" value="basket" name="e-action">'.
				'<input type="hidden" value="'.$emuShop->emuAppID.'" name="e-plugin">'.
				'</form>';

	return $content;
}


?>
