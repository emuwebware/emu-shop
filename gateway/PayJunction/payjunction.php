<?php

/*
Emu Payment Gateway: PayJunction
Emu Payment Gateway Description: PayJunction payment gateway
*/

global $emuShop;
global $QuickLink;

if( !class_exists( 'QuickLink' ) )
	include_once( $emuShop->getManager('gateway')->paymentGatewayDir.'/PayJunction/payjunction.class.php' );

$PJSettings = $emuShop->settings->createSettingsGroup( 'PayJunction', 'PayJunction Gateway Settings' );

$PJSettings->addSetting( 'login', '', 'Login', 1 );
$PJSettings->addSetting( 'password', '', 'Password', 2, 'password' );
$PJSettings->addSetting( 'apiMode', 'test', 'API Mode', 4, array( 'test' => 'Test', 'live' => 'Live' ) );

$emuShop->settings->addSettingsGroup( $PJSettings );
$emuShop->settings->saveSettings();

//Instantiate the QuickLink Class
$QuickLink = new QuickLink( $PJSettings->getSettingValue( 'login' ), $PJSettings->getSettingValue( 'password' ) );
$QuickLink->setSecurity( 'AWZ', 'I', 'false', 'false', 'false' );

if( $PJSettings->getSettingValue( 'apiMode' ) == 'test' )
{
	$QuickLink->setServerTest();
	$QuickLink->quicklink( 'pj-ql-01', 'pj-ql-01p' ); // change to test login cred.
	$QuickLink->echo = false;
}
else
{
	$QuickLink->setServerLive();
}

add_action( 'emu_shop_make_payment', 'setup_shop_payment_messages', 1, 2 );
add_filter( 'emu_shop_payment_result', 'update_shop_payment_result' );
add_filter( 'emu_shop_payment_messages', 'update_shop_payment_messages' );

function setup_shop_payment_messages( $order, $paymentMethod )
{
	global $QuickLink;

	$billing_address = $order->getBillingAddress();

	$response = $QuickLink->Charge(	$billing_address->firstName.' '.$billing_address->lastName,
									$paymentMethod->cardNumber,
									$paymentMethod->expiryMonth,
									$paymentMethod->expiryYear,
									$cvs = $paymentMethod->cvv2Number,
									$billing_address->addressLineOne,
									$billing_address->city,
									$billing_address->stateName,
									$billing_address->postalCode,
									$order->getGrandTotalValue(),
									$transaction_id = "");

}

function update_shop_payment_result( $result )
{
	global $QuickLink;

	if( !isset( $QuickLink->lastResponse['dc_response_code'] ) ) return false;

	switch( $QuickLink->lastResponse['dc_response_code'] )
	{
		case '00':
		case '85':
			$result = true;
			break;
		default:
			$result = false;
	}

	return $result;
}

function update_shop_payment_messages( $messages )
{
	global $QuickLink;

	if( !isset( $QuickLink->lastResponse['dc_response_message'] ) )
	{
		$messages[] = 'Sorry, there is currently a problem with the payment processing system - please try again later.';
		return $messages;
	}

	$message = '';

	switch( $QuickLink->lastResponse['dc_response_code'] )
	{
		case 'NL':
		case 'AB':
			$message = 'Sorry, there is currently a problem with the payment processing system - please try again later.';

			break;
		case 'FE':
			$message = 'Some of the details provided are incorrect - please enter the card number, expiry and type as they appear on your card.';
			break;
		case '00':
		case '85':
			$message = 'Approved.';
			break;
		default:
			$message = 'Your card has been declined. Please make sure the details entered are correct.';
	}

	$messages[] = $message.'<!-- Code '.$QuickLink->lastResponse['dc_response_code'].'-->';

	return $messages;
}


?>
