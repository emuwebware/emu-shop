<?php

class authorizeDotNet
{
	const RESPONSE_CODE			= 0;
	const RESPONSE_APPROVED 	= 1;
	const RESPONSE_DECLINED 	= 2;
	const RESPONSE_ERROR 		= 3;
	const RESPONSE_HELD 		= 4;

	const RESPONSE_TEXT 		= 3;

	public $postTestURL 			= 'https://test.authorize.net/gateway/transact.dll';
	public $postURL				= 'https://secure.authorize.net/gateway/transact.dll';
	public $loginID;
	public $transactionKey;

	public $env;

	public $lineItems = array();

	public $error;
	public $messages = array();

	public $paymentDetails;
	public $paymentSuccessful = false;

	public function __construct( $login_id = null, $transaction_key = null )
	{
		if( $login_id ) $this->loginID = $login_id;
		if( $transaction_key ) $this->transactionKey = $transaction_key;
	}

	public function addOrderItem( $product_name, $qty, $price )
	{
		$this->lineItems[] = 'x_line_item=Item<|>Product<|>'.preg_replace('/[^a-zA-Z0-9\s]/', '', $product_name).'<|>'.$qty.'<|>'.$price.'<|>Y';
	}

	public function getPaymentDetails()
	{
		if( $this->paymentDetails ) return $paymentDetails;

		return (object) array( 	'cardNumber' => '',
								'expiryMonth' => '',
								'expiryYear' => '',
								'amount' => '',
								'description' => 'Website Payment',
								'billing' => (object) array( 	'firstName' => '',
																'lastName' => '',
																'company' => '',
																'address' => '',
																'state' => '',
																'country' => '',
																'city' => '',
																'phoneNumber' => '',
																'email' => '',
																'postalCode' => '' ),
								'shipping' => (object) array( 	'firstName' => '',
																'lastName' => '',
																'company' => '',
																'address' => '',
																'state' => '',
																'country' => '',
																'city' => '',
																'postalCode' => '' ) );



	}

	public function makePayment( $paymentDetails = null )
	{
		if( !$paymentDetails )
		{
			if( !$this->paymentDetails )
			{
				$this->error = true;
				$this->messages[] = 'No payment details provided - use setPaymentDetails() or pass paymentDetails object as makePayment() parameter';
			}

			$paymentDetails = $this->paymentDetails;
		}


		$post_values = array(

			// the API Login ID and Transaction Key must be replaced with valid values
			"x_login"			=> $this->loginID,
			"x_tran_key"		=> $this->transactionKey,

			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "FALSE",

			"x_type"			=> "AUTH_CAPTURE",
			"x_method"			=> "CC",
			"x_card_num"		=> $paymentDetails->cardNumber,
			"x_exp_date"		=> $paymentDetails->expiryMonth.substr( $paymentDetails->expiryYear, 2, 2 ),

			"x_amount"			=> $paymentDetails->amount,
			"x_description"		=> $paymentDetails->description,

			"x_first_name"		=> $paymentDetails->billing->firstName,
			"x_last_name"		=> $paymentDetails->billing->lastName,
			"x_company"			=> $paymentDetails->billing->company,
			"x_address"			=> $paymentDetails->billing->address,
			"x_state"			=> $paymentDetails->billing->state,
			"x_city"			=> $paymentDetails->billing->city,
			"x_country"			=> $paymentDetails->billing->country,
			"x_phone"			=> $paymentDetails->billing->phoneNumber,
			"x_email"			=> $paymentDetails->billing->email,
			"x_zip"				=> $paymentDetails->billing->postalCode,

			// now the shipping fields
			"x_ship_to_first_name"		=> $paymentDetails->shipping->firstName,
			"x_ship_to_last_name"		=> $paymentDetails->shipping->lastName,
			"x_ship_to_company"			=> $paymentDetails->shipping->company,
			"x_ship_to_address"			=> $paymentDetails->shipping->address,
			"x_ship_to_state"			=> $paymentDetails->shipping->state,
			"x_ship_to_country"			=> $paymentDetails->shipping->country,
			"x_ship_to_city"			=> $paymentDetails->shipping->city,
			"x_ship_to_zip"				=> $paymentDetails->shipping->postalCode

			// Additional fields can be added here as outlined in the AIM integration
			// guide at: http://developer.authorize.net
		);

		// This section takes the input fields and converts them to the proper format
		// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		//$post_string = rtrim( $post_string, "& " );

		// Add the individual products

		if( $this->lineItems )
			$post_string .= implode('&', $this->lineItems);
		else
			$post_string = rtrim( $post_string, "& " );


		// This sample code uses the CURL library for php to establish a connection,
		// submit the post, and record the response.
		// If you receive an error, you may want to ensure that you have the curl
		// library enabled in your php configuration
		$request = curl_init($this->getPostURL()); // initiate curl object
			curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
			$post_response = curl_exec($request); // execute curl post and store results in $post_response
			// additional options may be required depending upon your server configuration
			// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close ($request); // close curl object

		// This line takes the response and breaks it into an array using the specified delimiting character
		$response_array = explode( $post_values["x_delim_char"], $post_response );

		$transaction_result = $response_array[self::RESPONSE_CODE];

		switch( $response_array[self::RESPONSE_CODE] )
		{
			case self::RESPONSE_APPROVED:

				$this->error = false;
				$this->paymentSuccessful = true;

				break;
			/*
			case RESPONSE_DECLINED;
			case RESPONSE_ERROR;
			case RESPONSE_HELD;
			default:
			*/
		}

		$this->messages[] = $response_array[self::RESPONSE_TEXT];

	}

	private function getPostURL()
	{
		return $this->env == 'DEVELOPMENT' ? $this->postTestURL : $this->postURL;
	}

	public function setPaymentDetails( $paymentDetails )
	{
		if( !$paymentDetails ) return false;
		$this->paymentDetails = $paymentDetails;
	}

}
?>
