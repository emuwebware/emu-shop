<?php

/*
Emu Payment Gateway: Authorize.net
Emu Payment Gateway Description: Authorize.net payment gateway
*/

global $emuShop;

if( !class_exists( 'authorizeDotNet' ) )
	include_once( $emuShop->getManager('gateway')->paymentGatewayDir.'/Authorize.net/authorizenet.class.php' );

global $emuShopAuthorizeDotNet;

$emuShopAuthorizeDotNet = new authorizeDotNet();

$ADNSettings = $emuShop->settings->createSettingsGroup( 'Authorize.net', 'Authorize.net Settings' );

$ADNSettings->addSetting( 'loginID', '', 'Login ID', 1 );
$ADNSettings->addSetting( 'transactionKey', '', 'Transaction Key', 2 );
$ADNSettings->addSetting( 'env', 'development', 'Environment', 4, array( 'DEVELOPMENT' => 'Development', 'LIVE' => 'Live' ) );

$emuShop->settings->addSettingsGroup( $ADNSettings );
$emuShop->settings->saveSettings();

$emuShopAuthorizeDotNet->loginID = $ADNSettings->getSettingValue( 'loginID' );
$emuShopAuthorizeDotNet->transactionKey = $ADNSettings->getSettingValue( 'transactionKey' );
$emuShopAuthorizeDotNet->env = $ADNSettings->getSettingValue( 'env' );

add_action( 'emu_shop_make_payment', 'make_authorizenet_payment', 1, 2 );
add_filter( 'emu_shop_payment_result', 'update_shop_payment_result' );
add_filter( 'emu_shop_payment_messages', 'update_shop_payment_messages' );

function make_authorizenet_payment( $order, $paymentMethod )
{
	global $emuShopAuthorizeDotNet;

	$paymentDetails = $emuShopAuthorizeDotNet->getPaymentDetails();

	$paymentDetails->cardNumber = $paymentMethod->cardNumber;
	$paymentDetails->expiryMonth = $paymentMethod->expiryMonth;
	$paymentDetails->expiryYear = $paymentMethod->expiryYear;
	$paymentDetails->amount = $order->getGrandTotalValue();

	if( $billing_address = $order->getBillingAddress() )
	{
		$paymentDetails->billing->firstName = $billing_address->firstName;
		$paymentDetails->billing->lastName = $billing_address->lastName;
		$paymentDetails->billing->company = $billing_address->company;
		$paymentDetails->billing->address = $billing_address->addressLineOne;
		$paymentDetails->billing->state = $billing_address->stateName;
		$paymentDetails->billing->country = $billing_address->countryName;
		$paymentDetails->billing->city = $billing_address->city;
		$paymentDetails->billing->phoneNumber = $billing_address->phoneNumber;
		$paymentDetails->billing->email = $order->customerEmail;
		$paymentDetails->billing->postalCode = $billing_address->postalCode;
	}

	if( $shipping_address = $order->getShippingAddress() )
	{
		$paymentDetails->shipping->firstName = $shipping_address->firstName;
		$paymentDetails->shipping->lastName = $shipping_address->lastName;
		$paymentDetails->shipping->company = $shipping_address->company;
		$paymentDetails->shipping->address = $shipping_address->addressLineOne;
		$paymentDetails->shipping->state = $shipping_address->stateName;
		$paymentDetails->shipping->country = $shipping_address->countryName;
		$paymentDetails->shipping->city = $shipping_address->city;
		$paymentDetails->shipping->postalCode = $shipping_address->postalCode;
	}

	foreach( $order->getItems() as $order_item )
	{
		$emuShopAuthorizeDotNet->addOrderItem( $order_item->description, $order_item->qty, $order_item->getTotal(ORDER_ITEM_PRICE) );
	}

	$emuShopAuthorizeDotNet->makePayment( $paymentDetails );
}

function update_shop_payment_result( $result )
{
	global $emuShopAuthorizeDotNet;
	return $emuShopAuthorizeDotNet->paymentSuccessful;
}

function update_shop_payment_messages( $messages )
{
	global $emuShopAuthorizeDotNet;
	return array_merge( $messages, $emuShopAuthorizeDotNet->messages );
}


?>
