<?php

/*
Emu Payment Gateway: U.S. Bank
Emu Payment Gateway Description: U.S. Bank payment gateway
*/
global $emuShop;

if( !class_exists( 'usBank' ) )
    include_once( $emuShop->getManager('gateway')->paymentGatewayDir.'/USBank/us-bank.class.php' );

global $usBank;

$usBank = new usBank();

$usBankSettings = $emuShop->settings->createSettingsGroup( 'usBank', 'US Bank Settings' );

$usBankSettings->addSetting( 'merchantID', '', 'Merchant ID', 1 );
$usBankSettings->addSetting( 'userID', '', 'User ID', 2 );
$usBankSettings->addSetting( 'pin', '', 'PIN', 2, 'password' );
$usBankSettings->addSetting( 'mode', 'test', 'Mode', 4, array( 'test' => 'Test', 'live' => 'Live' ) );
$usBankSettings->addSetting( 'testMerchantID', '', 'Test Merchant ID', 1 );
$usBankSettings->addSetting( 'testUserID', '', 'Test User ID', 2 );
$usBankSettings->addSetting( 'testPin', '', 'Test PIN', 2, 'password' );

$emuShop->settings->addSettingsGroup( $usBankSettings );
$emuShop->settings->saveSettings();

$usBank->merchantID = $usBankSettings->getSettingValue( 'merchantID' );
$usBank->userID = $usBankSettings->getSettingValue( 'userID' );
$usBank->pin = $usBankSettings->getSettingValue( 'pin' );
$usBank->testMerchantID = $usBankSettings->getSettingValue( 'testMerchantID' );
$usBank->testUserID = $usBankSettings->getSettingValue( 'testUserID' );
$usBank->testPin = $usBankSettings->getSettingValue( 'testPin' );
$usBank->mode = $usBankSettings->getSettingValue( 'mode' );

add_action( 'emu_shop_make_payment', 'setup_shop_payment_messages', 1, 2 );
add_filter( 'emu_shop_payment_result', 'update_shop_payment_result' );
add_filter( 'emu_shop_payment_messages', 'update_shop_payment_messages' );

function setup_shop_payment_messages( $order, $paymentMethod )
{
	global $usBank;

	$paymentDetails = $usBank->getPaymentDetails();

	$paymentDetails->cardNumber = $paymentMethod->cardNumber;
	$paymentDetails->expiryMonth = $paymentMethod->expiryMonth;
	$paymentDetails->expiryYear = $paymentMethod->expiryYear;
	$paymentDetails->amount = $order->getGrandTotalValue();
    $paymentDetails->cvv2Number = $paymentMethod->cv2Number;


	if( $billing_address = $order->getBillingAddress() )
	{
		$paymentDetails->billing->firstName = $billing_address->firstName;
		$paymentDetails->billing->lastName = $billing_address->lastName;
		$paymentDetails->billing->company = $billing_address->company;
		$paymentDetails->billing->address = $billing_address->addressLineOne;
		$paymentDetails->billing->state = $billing_address->stateName;
		$paymentDetails->billing->country = $billing_address->countryName;
		$paymentDetails->billing->city = $billing_address->city;
		$paymentDetails->billing->phoneNumber = $billing_address->phoneNumber;
		$paymentDetails->billing->email = $order->customerEmail;
		$paymentDetails->billing->postalCode = $billing_address->postalCode;
	}

	if( $shipping_address = $order->getShippingAddress() )
	{
		$paymentDetails->shipping->firstName = $shipping_address->firstName;
		$paymentDetails->shipping->lastName = $shipping_address->lastName;
		$paymentDetails->shipping->company = $shipping_address->company;
		$paymentDetails->shipping->address = $shipping_address->addressLineOne;
		$paymentDetails->shipping->state = $shipping_address->stateName;
		$paymentDetails->shipping->country = $shipping_address->countryName;
		$paymentDetails->shipping->city = $shipping_address->city;
		$paymentDetails->shipping->postalCode = $shipping_address->postalCode;
	}

	$usBank->makePayment( $paymentDetails );

}

function update_shop_payment_result( $result )
{
	global $usBank;
	return $usBank->paymentSuccessful;
}

function update_shop_payment_messages( $messages )
{
	global $usBank;
	return array_merge( $messages, $usBank->messages );
}




?>