<?php

class usBank
{
    const RESPONSE_CODE         = "ssl_approval_code";
    const RESPONSE_APPROVED     = "AA";

    public $postURL = 'https://www.myvirtualmerchant.com/VirtualMerchant/process.do';
    public $testPostURL = 'https://demo.myvirtualmerchant.com/VirtualMerchantDemo/process.do';

    public $merchantID;
    public $userID;
    public $pin;
    public $testMerchantID;
    public $testUserID;
    public $testPin;
    public $mode;

    public $error;
    public $messages = array();

    public $paymentDetails;
    public $paymentSuccessful = false;

    public function __construct( $merchant_id = null, $user_id = null, $pin = null )
    {
        if( $merchant_id ) $this->merchantID = $merchant_id;
        if( $user_id ) $this->userID = $user_id;
        if( $pin ) $this->pin = $pin;
    }

    public function getPaymentDetails()
    {
        if( $this->paymentDetails ) return $paymentDetails;

        return (object) array(  'cardNumber' => '',
                                'expiryMonth' => '',
                                'expiryYear' => '',
                                'amount' => '',
                                'description' => 'Website Payment',
                                'billing' => (object) array(    'firstName' => '',
                                                                'lastName' => '',
                                                                'company' => '',
                                                                'address' => '',
                                                                'state' => '',
                                                                'country' => '',
                                                                'city' => '',
                                                                'phoneNumber' => '',
                                                                'email' => '',
                                                                'postalCode' => '' ),
                                'shipping' => (object) array(   'firstName' => '',
                                                                'lastName' => '',
                                                                'company' => '',
                                                                'address' => '',
                                                                'state' => '',
                                                                'country' => '',
                                                                'city' => '',
                                                                'postalCode' => '' ) );



    }

    public function makePayment( $paymentDetails = null )
    {
        if( !$paymentDetails )
        {
            if( !$this->paymentDetails )
            {
                $this->error = true;
                $this->messages[] = 'No payment details provided - use setPaymentDetails() or pass paymentDetails object as makePayment() parameter';
            }

            $paymentDetails = $this->paymentDetails;
        }

        if( $this->mode = 'test' )
        {
            $post_url = $this->testPostURL;
            $ssl_merchant_id = $this->testMerchantID;
            $ssl_user_id = $this->testUserID;
            $ssl_pin = $this->testPin;
        }
        else // live
        {
            $post_url = $this->postURL;
            $ssl_merchant_id = $this->merchantID;
            $ssl_user_id = $this->userID;
            $ssl_pin = $this->pin;
        }

        $post_values = array(

            # Credentials and Configuration
            "ssl_merchant_id"               => $ssl_merchant_id,
            "ssl_user_id"                   => $ssl_user_id,
            "ssl_pin"                       => $ssl_pin,
            "ssl_show_form"                 => "FALSE",
            "ssl_cvv2cvc2_indicator"        => "1",
            "ssl_result_format"             => "ASCII",

            # Order details
            "ssl_transaction_type"          => "ccsale",
            "ssl_amount"                    => $paymentDetails->amount,
            "ssl_card_number"               => $paymentDetails->cardNumber,
            "ssl_exp_date"                  => str_pad($paymentDetails->expiryMonth, 2, "0", STR_PAD_LEFT).substr( $paymentDetails->expiryYear, 2, 2 ),
            "ssl_cvv2cvc2"                  => $paymentDetails->cvv2Number,
            "ssl_avs_address"               => $paymentDetails->billing->address,
            "ssl_avs_zip"                   => $paymentDetails->billing->postalCode,
            "ssl_description"               => $paymentDetails->description,

            "ssl_first_name"                => $paymentDetails->billing->firstName,
            "ssl_last_name"                 => $paymentDetails->billing->lastName,
            "ssl_company"                   => $paymentDetails->billing->company,
            "ssl_state"                     => $paymentDetails->billing->state,
            "ssl_city"                      => $paymentDetails->billing->city,
            "ssl_country"                   => $paymentDetails->billing->country,
            "ssl_phone"                     => $paymentDetails->billing->phoneNumber,
            "ssl_email"                     => $paymentDetails->billing->email,


            // Shipping
            "ssl_ship_to_first_name"        => $paymentDetails->shipping->firstName,
            "ssl_ship_to_last_name"         => $paymentDetails->shipping->lastName,
            "ssl_ship_to_company"           => $paymentDetails->shipping->company,
            "ssl_ship_to_address1"          => $paymentDetails->shipping->address,
            "ssl_ship_to_state"             => $paymentDetails->shipping->state,
            "ssl_ship_to_country"           => $paymentDetails->shipping->country,
            "ssl_ship_to_city"              => $paymentDetails->shipping->city,
            "ssl_ship_to_zip"               => $paymentDetails->shipping->postalCode

            // Additional fields can be added here as outlined in the AIM integration
            // guide at: http://developer.authorize.net
        );

        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
        $post_string = "";

        foreach( $post_values as $key => $value ) { $post_string .= "$key=" . urlencode( $value ) . "&"; }

        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
            curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
            $post_response = curl_exec($request); // execute curl post and store results in $post_response
            // additional options may be required depending upon your server configuration
            // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
        foreach( explode( "\n", $post_response ) as $key_value_pair )
        {
            $kvpair = explode( "=", $key_value_pair );
            $response_array[$kvpair[0]] = $kvpair[1];
        }

        if( isset( $response_array["errorCode"] ) )
        {
            // transaction not successful
            $this->error = true;
            $this->paymentSuccessful = false;
            $this->messages[] = $response_array["errorName"].": ".$response_array["errorMessage"]."<!-- Error Message: ".$response_array["errorName"]." -->";
        }
        else
        {
            // Transaction Approved
            $this->error = false;
            $this->paymentSuccessful = true;
        }
    }

    public function setPaymentDetails( $paymentDetails )
    {
        if( !$paymentDetails ) return false;
        $this->paymentDetails = $paymentDetails;
    }

}
?>
