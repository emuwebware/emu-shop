<?php

/*
Emu Payment Gateway: Test
Emu Payment Gateway Description: Test Payment Gateway
*/

global $emuShop, $TestSettings;

$TestSettings = $emuShop->settings->createSettingsGroup( 'TestGateway', 'Test Gateway Settings' );

$TestSettings->addSetting( 'validCreditCardNo', '1111222233334444', 'Test Credit Card Number', 1 );

$emuShop->settings->addSettingsGroup( $TestSettings );
$emuShop->settings->saveSettings();

add_action( 'emu_shop_make_payment', 'gateway_make_payment', 1, 2 );
add_filter( 'emu_shop_payment_result', 'update_shop_payment_result' );
add_filter( 'emu_shop_payment_messages', 'update_shop_payment_messages' );

function gateway_make_payment( $order, $paymentMethod )
{
    global $TestSettings, $paymentSuccess;
    $paymentSuccess = $TestSettings->getSettingValue( 'validCreditCardNo' ) == $paymentMethod->cardNumber;
}

function update_shop_payment_result( $result )
{
	global $paymentSuccess;
    return $paymentSuccess;
}

function update_shop_payment_messages( $messages )
{
	global $paymentSuccess;

    $messages[] = $paymentSuccess ? 'Approved' : 'Declined';

	return $messages;
}


?>