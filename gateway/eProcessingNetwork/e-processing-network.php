<?php

/*
Emu Payment Gateway: eProcessing Network
Emu Payment Gateway Description: eProcessing Network payment gateway
*/
global $emuShop;

if( !class_exists( 'eProcessingNetwork' ) )
	include_once( $emuShop->getManager('gateway')->paymentGatewayDir.'/eProcessingNetwork/e-processing-network.class.php' );

global $emuShopeProcessingNetwork;

$emuShopeProcessingNetwork = new eProcessingNetwork();

$EPSettings = $emuShop->settings->createSettingsGroup( 'eProcessing', 'eProcessing Network Settings' );

$EPSettings->addSetting( 'loginID', '', 'Login ID', 1 );
$EPSettings->addSetting( 'transactionKey', '', 'Transaction Key', 2 );

$emuShop->settings->addSettingsGroup( $EPSettings );
$emuShop->settings->saveSettings();

$emuShopeProcessingNetwork->loginID = $EPSettings->getSettingValue( 'loginID' );
$emuShopeProcessingNetwork->transactionKey = $EPSettings->getSettingValue( 'transactionKey' );

add_action( 'emu_shop_make_payment', 'setup_shop_payment_messages', 1, 2 );
add_filter( 'emu_shop_payment_result', 'update_shop_payment_result' );
add_filter( 'emu_shop_payment_messages', 'update_shop_payment_messages' );

function setup_shop_payment_messages( $order, $paymentMethod )
{
	global $emuShopeProcessingNetwork;

	$paymentDetails = $emuShopeProcessingNetwork->getPaymentDetails();

	$paymentDetails->cardNumber = $paymentMethod->cardNumber;
	$paymentDetails->expiryMonth = $paymentMethod->expiryMonth;
	$paymentDetails->expiryYear = $paymentMethod->expiryYear;
	$paymentDetails->amount = $order->getGrandTotalValue();


	if( $billing_address = $order->getBillingAddress() )
	{
		$paymentDetails->billing->firstName = $billing_address->firstName;
		$paymentDetails->billing->lastName = $billing_address->lastName;
		$paymentDetails->billing->company = $billing_address->company;
		$paymentDetails->billing->address = $billing_address->addressLineOne;
		$paymentDetails->billing->state = $billing_address->stateName;
		$paymentDetails->billing->country = $billing_address->countryName;
		$paymentDetails->billing->city = $billing_address->city;
		$paymentDetails->billing->phoneNumber = $billing_address->phoneNumber;
		$paymentDetails->billing->email = $order->customerEmail;
		$paymentDetails->billing->postalCode = $billing_address->postalCode;
	}

	if( $shipping_address = $order->getShippingAddress() )
	{
		$paymentDetails->shipping->firstName = $shipping_address->firstName;
		$paymentDetails->shipping->lastName = $shipping_address->lastName;
		$paymentDetails->shipping->company = $shipping_address->company;
		$paymentDetails->shipping->address = $shipping_address->addressLineOne;
		$paymentDetails->shipping->state = $shipping_address->stateName;
		$paymentDetails->shipping->country = $shipping_address->countryName;
		$paymentDetails->shipping->city = $shipping_address->city;
		$paymentDetails->shipping->postalCode = $shipping_address->postalCode;
	}

	foreach( $order->getItems() as $order_item )
	{
		$emuShopeProcessingNetwork->addOrderItem( $order_item->description, $order_item->qty, $order_item->getTotal(ORDER_ITEM_PRICE) );
	}

	$emuShopeProcessingNetwork->makePayment( $paymentDetails );

}

function update_shop_payment_result( $result )
{
	global $emuShopeProcessingNetwork;
	return $emuShopeProcessingNetwork->paymentSuccessful;
}

function update_shop_payment_messages( $messages )
{
	global $emuShopeProcessingNetwork;
	return array_merge( $messages, $emuShopeProcessingNetwork->messages );
}


?>
