<?php

/*
Emu Widget: Emu Shop Links
Emu Widget Class: emueShopLinks
Emu Widget Description: Add shop links
*/

function check_empty_strings( &$args, $terms )
{
	foreach( $terms as $term_key => $term_value )
	{
		if( $term_value === '' ) $args[$term_value] = '';
	}
}

function show_shop_links( $terms = array() )
{
	$defaults = array( 	'before' => '<ul class="emu-shop-links">',
						'after' => '</ul>',
						'before_item' => '<li class="emu-shop-link">',
						'after_item' => '</li>',
						'echo' => 1,
						'wishlist_text' => 'Your Wishlist',
						'cart_text' => 'Shopping Cart',
						'my_account_text' => 'My Account',
						'checkout_text' => 'Checkout');

	$args = wp_parse_args( $terms, $defaults );

	check_empty_strings( $args, $terms );

	extract( $args );

	global $emuShop;

	$basket = $emuShop->getBasket();

	if( is_user_logged_in() )
	{
	  $customer = $emuShop->getManager('customer')->getCustomer();

	  $user_links[] = '<a href="/?emuLogout">Hello '.$customer->firstName.' - Logout</a>';
	  if( !empty($my_account_text) ) $user_links[] = '<a href="'.$emuShop->pageManager->pages->myAccount->url.'">'.$my_account_text.'</a>';
	  if( !empty($wishlist_text) ) $user_links[] = '<a href="'.$emuShop->pageManager->pages->wishlist->url.'">'.$wishlist_text.' ('.$customer->wishListNoItems.')</a>';
	}
	else
	{
	  $return_url = $_SERVER['REQUEST_URI'];

	  global $post;

	  if( !empty($wishlist_text) ) $user_links[] = '<a href="'.$emuShop->pageManager->pages->wishlist->url.'">'.$wishlist_text.'</a>';
	}

	if( !empty($cart_text) ) $user_links[] = '<a href="'.$emuShop->pageManager->pages->basket->url.'" />'.$cart_text.' ('.$basket->numberItems.')</a>';
	if( !empty($checkout_text) ) $user_links[] = '<a href="'.$emuShop->pageManager->pages->billing->url.'" />'.$checkout_text.'</a>';

	$std_links = $before.$before_item.implode( $after_item.$before_item, $user_links ).$after_item.$after;

	if( $echo ) echo $std_links;
	return $std_links;
}

/**
 * emueShopLinks Class
 */
class emueShopLinks extends WP_Widget
{
	/** constructor */
	function emueShopLinks()
	{
		// global $emuBuild;

		parent::WP_Widget( false, $name = 'Emu Shop Links' );

		if ( is_admin() ) {
			// wp_register_style( 'emu-widget-admin', plugins_url( "/{$emuBuild->pluginName}/style/emu-widget-control.css" ), false);
			// wp_enqueue_style( 'emu-widget-admin' );
		}
	}

    function widget($args, $instance) {

		global $emuShop;

		extract( $args );

		$insert_before_item		= @$instance['insert_before_item'];
		$insert_after_item		= @$instance['insert_after_item'];
		$insert_before_group	= @$instance['insert_before_group'];
		$insert_after_group		= @$instance['insert_after_group'];
		$wishlist_text			= @$instance['wishlist_text'];
		$cart_text				= @$instance['cart_text'];
		$my_account_text		= @$instance['my_account_text'];
		$checkout_text			= @$instance['checkout_text'];

		echo $before_widget;

		$terms = array( 'before' => $insert_before_group,
						'after' => $insert_after_group,
						'before_item' => $insert_before_item,
						'after_item' => $insert_after_item,
						'wishlist_text' => $wishlist_text,
						'cart_text' => $cart_text,
						'my_account_text' => $my_account_text,
						'checkout_text' => $checkout_text );

		show_shop_links( $terms );

		echo $after_widget;

	}

    function update($new_instance, $old_instance) { return $new_instance; }


	function form($instance) {

		$insert_before_item		= esc_attr( @$instance['insert_before_item'] );
		$insert_after_item		= esc_attr( @$instance['insert_after_item'] );
		$insert_before_group	= esc_attr( @$instance['insert_before_group'] );
		$insert_after_group		= esc_attr( @$instance['insert_after_group'] );
		$wishlist_text			= esc_attr( @$instance['wishlist_text'] );
		$cart_text				= esc_attr( @$instance['cart_text'] );
		$my_account_text		= esc_attr( @$instance['my_account_text'] );
		$checkout_text			= esc_attr( @$instance['checkout_text'] );

		if( is_null( $wishlist_text ) ) $wishlist_text = 'Your Wishlist';
		if( is_null( $cart_text ) ) $cart_text = 'Shopping Cart';
		if( is_null( $my_account_text ) ) $my_account_text = 'My Account';
		if( is_null( $checkout_text ) ) $checkout_text = 'Checkout';

		?>
		<div class="emu-widget-control">

			<div>
				<label for="<?php echo $this->get_field_id('insert_before_group'); ?>">Insert <em>before</em> group:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_before_group'); ?>" name="<?php echo $this->get_field_name('insert_before_group'); ?>" type="text" value="<?php echo $insert_before_group; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_before_item'); ?>">Insert <em>before</em> each item:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_before_item'); ?>" name="<?php echo $this->get_field_name('insert_before_item'); ?>" type="text" value="<?php echo $insert_before_item; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_after_item'); ?>">Insert <em>after</em> each item:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_after_item'); ?>" name="<?php echo $this->get_field_name('insert_after_item'); ?>" type="text" value="<?php echo $insert_after_item; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_after_group'); ?>">Insert <em>after</em> group:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_after_group'); ?>" name="<?php echo $this->get_field_name('insert_after_group'); ?>" type="text" value="<?php echo $insert_after_group; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('wishlist_text'); ?>"><em>Wishlist</em> text:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('wishlist_text'); ?>" name="<?php echo $this->get_field_name('wishlist_text'); ?>" type="text" value="<?php echo $wishlist_text; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('cart_text'); ?>"><em>Shopping Cart</em> text:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('cart_text'); ?>" name="<?php echo $this->get_field_name('cart_text'); ?>" type="text" value="<?php echo $cart_text; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('my_account_text'); ?>"><em>My Account</em> text:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('my_account_text'); ?>" name="<?php echo $this->get_field_name('my_account_text'); ?>" type="text" value="<?php echo $my_account_text; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('checkout_text'); ?>"><em>Checkout</em> text:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('checkout_text'); ?>" name="<?php echo $this->get_field_name('checkout_text'); ?>" type="text" value="<?php echo $checkout_text; ?>" />
			</div>
			<div class="info"><strong>Tip:</strong> Set link text to nothing to remove link</div>
		</div>

		<?php
    }


} // class emueShopLinks



?>
